// modules =================================================
var express        = require('express');
var app            = express();
var mongoose       = require('mongoose');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var passport = require('passport');
var config = require('./app/settings/config');
var formidable = require('formidable');

// configuration ===========================================
	
// config files
var db = require('./config/db').configure(mongoose);

require('./app/settings/passport')(passport);
require('./app/settings/express').configure(app, passport);

require('./app/controllers/transcode')

var port = process.env.PORT || config.server.port; // set our port
// mongoose.connect(db.url); // connect to our mongoDB database (commented out after you enter in your own credentials)

// get all data/stuff of the body (POST) parameters
// app.use(bodyParser.json()); // parse application/json 
// app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
// app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded

app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users

//Watson Call API
app.get('/callwatson', function (req, res) {
	
	'use strict';
	var reqparam = req.param('data');
	reqparam = reqparam.replace("/", "");
	
	const SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
	const fs = require('fs');
	var request = require('request');

	const speech_to_text = new SpeechToTextV1({
	  username: '32d30f2c-8b67-439b-90d5-a28dba999520',
	  password: 'OYK2HIiJymiG'
	});

	const params = {
	  model: 'en-US_BroadbandModel',
	  content_type: 'audio/wav',
	  'interim_results': false,
	  'word_confidence': true,
	  'word_alternatives_threshold': 0.9,
	  timestamps: true
	};

	// create the stream
	//const recognizeStream = speech_to_text.createRecognizeStream(params);

	// pipe in some audio
	//fs.createReadStream('http://d3l6fnyzzmn75t.cloudfront.net/shortclip/wav-shortclip.wav').pipe(recognizeStream);
	
	var audioFile = 'http://d3l6fnyzzmn75t.cloudfront.net/' + reqparam + '/wav-' + reqparam + '.wav';
	//console.log(audioFile);
	// create the stream
	var recognizeStream = speech_to_text.createRecognizeStream(params);
	// pipe in some audio
	request(audioFile).pipe(recognizeStream);	
	
	// listen for 'data' events for just the final text
	// listen for 'results' events to get the raw JSON with interim results, timings, etc.

	recognizeStream.setEncoding('utf8'); // to get strings instead of Buffers from `data` events

	//['data', 'results', 'error', 'connection-close'].forEach(function(eventName) {
	//  recognizeStream.on(eventName, console.log.bind(console, eventName + ' event: '));
	//});
	
	// ['data', 'results', 'error', 'connection-close'].forEach(function(eventName) {
	  // recognizeStream.on(eventName, console.log.bind(console, eventName + ' event: '));		  
	// });
	
	// and pipe out the transcription
	//recognizeStream.pipe(fs.createWriteStream('uploads/watson-' + reqparam + '.txt'));
	
	if(fileExists('uploads/watson-' + reqparam + '.txt'))
	{
		res.send("file exists");
		res.end();
	}
	else{
		// Listen for events.
		//recognizeStream.on('data', function(event) { onEvent('Data:', event); });
		recognizeStream.on('results', function(event) { onEvent('Results:', event); });
		recognizeStream.on('error', function(event) { onEvent('Error:', event); });
		recognizeStream.on('close-connection', function(event) { onEvent('Close:', event); });
		
		// Displays events on the console.
		function onEvent(name, event) {
			var stream = fs.createWriteStream('uploads/watson-' + reqparam + '.txt', {'flags': 'a'});
			stream.once('open', function(fd) {
				stream.write(JSON.stringify(event, null, 2));
				stream.end();
			});
			
			//Delete in-progress file.
			if(fileExists('uploads/watson-' + reqparam + '-inprogress.txt'))
			{
				fs.unlink('uploads/watson-' + reqparam + '-inprogress.txt', (err) => {
				  if (err) throw err;
				  console.log('successfully deleted ' + reqparam + ' in-progress file');
				});
			};
		};
	};
});

function fileExists(path) {
  var fs = require('fs');
  try  {
    return fs.statSync(path).isFile();
  }
  catch (e) {

    if (e.code == 'ENOENT') { // no such file or directory. File really does not exist
      //console.log("File does not exist.");
      return false;
    }

    //console.log("Exception fs.statSync (" + path + "): " + e);
    throw e; // something else went wrong, we don't have rights, ...
  }
}

//Get the watson json data from the file.
app.get('/getwatson', function (req, res) {
	
	'use strict';
	var fs = require('fs');
	var reqparam = req.param('data');
	reqparam = reqparam.replace("/", "");
	
	fs.readFile('uploads/watson-' + reqparam + '.txt', 'utf8', function(err, data) {  
		if (err) res.send("no data");
		res.send(data);
	});
});

//get descriptive indicators from file
app.get('/getCaptionIndicators', function (req, res) {
	
	'use strict';
	var fs = require('fs');
	
	fs.readFile('uploads/indicatorlist.txt', 'utf8', function(err, data) {  
		if (err) res.send("no data");
		res.send(data);
	});
});

//List all the media files from the S3 bucket
app.get('/listbucketmedia_old', function (req, res) {

	'use strict';
	var AWS = require('aws-sdk');
	AWS.config.update({accessKeyId: 'AKIAJ4BOFTQF3HC4XHHQ', secretAccessKey: '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', region: 'us-west-2'});
	var s3 = new AWS.S3();

	var params = { 
		Bucket: 'transcoding-audio-video-output',
		Delimiter: '/'
	}
	var responseKeys = '';
	s3.listObjects(params, function (err, data) {
		//console.log(data);
		if(err)throw err;		
		res.send(data["CommonPrefixes"]);
	});

});

//List all the media files from the S3 bucket
app.get('/listbucketmedia', function (req, res) {

	'use strict';
	var AWS = require('aws-sdk');
	AWS.config.update({accessKeyId: 'AKIAJ4BOFTQF3HC4XHHQ', secretAccessKey: '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', region: 'us-west-2'});
	var s3 = new AWS.S3();

	var params = { 
		Bucket: 'transcoding-audio-video-output',
		Delimiter: '/'
	}
	
	var responseKeys = '';
	s3.listObjects(params, function (err, data) {
		var prefixCollection = data["CommonPrefixes"];
		var projectArray = {};
		for (var i = 0, len = prefixCollection.length; i < len; i++) {
			var projectname = prefixCollection[i].Prefix.replace("/", "");
			projectArray[projectname] = false;
			
			//If in progress file exists, it should notify the user about the progress.
 			if(fileExists('uploads/watson-' + projectname + '-inprogress.txt'))
			{
				projectArray[projectname] = true;
			}
		}
		
		if(err)throw err;		
		res.json(projectArray);
	});
});

var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });


// app.post('/s3upload', upload.single('fileUpload'), function (req, res) {
// 	var form = new formidable.IncomingForm();
//     form.parse(req, function(err, fields, files) {
//         var params = {Bucket: 'transcoding-video-caption', Key: 'test90', Body: req.files.file};
//         s3.upload(params, function(err, data) {
//             console.log(err, data);
//         });
//         // cloudinary.uploader.upload(files.upload.path, function(result){
//         //     if(result.error) return cb(result.error);
//         //     cb(null, result);
//         // })
//     });
	
// });



app.post('/uploadtobucket', upload.single('fileUpload'), function (req, res) {
	
	const path = require('path');
	var fs = require('fs'),
	S3FS = require('s3fs'),
	s3fsImpl = new S3FS('transcoding-audio-video', {
		accessKeyId: 'AKIAJ4BOFTQF3HC4XHHQ',
		secretAccessKey: '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'
	});
	
	var file = req.file;
	
	//create inprogress file to let user know that processing is in progress.
	var extension = path.extname(file.originalname);
	var fileWoExt = path.basename(file.originalname,extension);
	var inpstream = fs.createWriteStream('uploads/watson-' + fileWoExt + '-inprogress.txt');
	inpstream.once('open', function(fd) {
		inpstream.write("");
		inpstream.end();
	});
	
	var stream = fs.createReadStream(file.path);
	return s3fsImpl.writeFile(file.originalname, stream).then(function () {
		fs.unlink(file.path, function (err) {
			if (err) {
				console.error(err);
			}
		});
		res.redirect('/?status=done');
	});
});




// routes ==================================================
require('./app/routes')(app); // pass our application into our routes
// start app ===============================================
// app.get('/', function(req, res) {
// 	res.sendfile('');
// });
app.listen(port);	
console.log('Magic happens on port ' + port); 			// shoutout to the user
exports = module.exports = app; 						// expose app
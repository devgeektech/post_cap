var app = angular.module('app', ['ui.router', 'angular-jwt', 'toaster', 'ngAnimate', 'rt.asyncseries', 'xeditable', 'ui.bootstrap', 'dropbox-picker', 'ngFileUpload', 'btford.socket-io', 'lk-google-picker'])
    .constant('_', window._)
    .config(['$urlRouterProvider', '$stateProvider', '$httpProvider', 'DropBoxSettingsProvider', '$locationProvider', 'lkGoogleSettingsProvider',
        function ($urlRouterProvider, $stateProvider, $httpProvider, DropBoxSettingsProvider, $locationProvider, lkGoogleSettingsProvider) {
            DropBoxSettingsProvider.configure({
                linkType: 'direct', //dropbox link type
                multiselect: true, //dropbox multiselect
                extensions: ['.mp4', '.mkv', '.avi', '.flv'], //dropbox file extensions
                box_clientId: 'ivx89985o0nmzqy', // box CLient Id
                box_linkType: 'shared', //box link type
                box_multiselect: 'true' //box multiselect
            });

            lkGoogleSettingsProvider.configure({
                apiKey: '60peIIyaMnwNBr0MGQZrCpg5',
                clientId: '393459784136-fvolgghakkhsqu5j578pss2cv2365t8u.apps.googleusercontent.com',
                scopes: ['https://www.googleapis.com/auth/drive'],
                locale: 'en',
                features: ['MULTISELECT_ENABLED', 'NAV_HIDDEN'],
                views: ['DocsView()']
            });

            $urlRouterProvider.otherwise('/dashboard/overview'); /* default state */
            $stateProvider

                /* ===================================$state definition start=======================================*/


                /*=========================Base Info==========================*/

                .state('base', {
                    abstract: true,
                    url: '',
                    templateUrl: 'views/base.html'
                })

                /*=========================End base Info==========================*/






                /*=========================Dashboard theme Info==========================*/

                .state('dashboard', {
                    url: '/dashboard',
                    parent: 'base',
                    abstract: true,
                    views: {
                        '@': {
                            templateUrl: 'views/dashboard.html',
                        },
                        'sidebar@dashboard': {
                            templateUrl: 'views/theme/sidebar.html',
                            controller: 'sidebarController'
                        },
                        'top@dashboard': {
                            templateUrl: 'views/theme/top.html',
                            conrtoller: 'navbarController'
                        },
                        'footer@dashboard': {
                            templateUrl: 'views/theme/footer.html'
                        }
                    }
                })
                .state('dashboard.overview', {
                    url: '/overview',
                    templateUrl: 'views/overview.html',
                    controller: 'topBarController'
                })
                // .state('dashboard.overview', {
                //     url: '/overview',
                //     templateUrl: 'views/overview.html',
                //     controller: 'newProjectController',
                //     controllerAs: 'uc'
                // })


                /*=========================End Dashboard theme Info==========================*/





                /*=========================User Panel Info==========================*/

                .state('dashboard.clientUserpanel', {
                    url: '/clientUserpanel',
                    templateUrl: 'views/users/clientUserpanel.html'
                })

                .state('dashboard.clientUser', {
                    url: '/clientUser',
                    templateUrl: 'views/writer/writer.html',
                    controller: 'userController',
                    controllerAs: 'uc'
                })
                .state('dashboard.projectUser', {
                    url: '/projectUser',
                    templateUrl: 'views/writer/writer.html',
                    controller: 'userController',
                    controllerAs: 'uc'
                })
                .state('dashboard.project-profile-user', {
                    url: '/project-profile-user',
                    templateUrl: 'views/writer/project-profile-writer.html',
                    controller: 'userController',
                    controllerAs: 'uc'
                })
                .state('dashboard.listUser', {
                    url: '/listusers',
                    templateUrl: 'views/users/listUsers.html',
                    controller: 'listUserController'
                })
                .state('dashboard.setPrice', {
                    url: '/userPrice/:id',
                    templateUrl: 'views/users/addUserPrice.html',
                    params: {
                        userDetail: null
                    }
                })
                .state('dashboard.updatePrice', {
                    url: '/updatePrice/:id',
                    templateUrl: 'views/users/updatePrice.html',
                    controller: 'listUserController'
                })

                .state('dashboard.Card', {
                    url: '/card',
                    templateUrl: 'views/card/cardDetail.htm',
                    controller: 'cardController'
                })

                /*=========================End User Panel Info==========================*/



                /*=========================label Panel Info==========================*/

                .state('dashboard.lable', {
                    url: '/lable/:labelId',
                    templateUrl: '/views/label/label.client.html',
                    controller: 'labelController'

                })
                .state('dashboard.lable2', {
                    url: '/lable2',
                    templateUrl: '/views/label/label.client.html',
                    controller: 'labelController',
                    controllerAs: 'lab',
                    params: {
                        type: 'Medium'
                    }
                })
                .state('dashboard.lable3', {
                    url: '/lable3',
                    templateUrl: '/views/label/label.client.html',
                    controller: 'labelController',
                    controllerAs: 'lab',
                    params: {
                        type: 'Major'
                    }
                })
                .state('dashboard.lable4', {
                    url: '/lable4',
                    templateUrl: '/views/label/label.client.html',
                    controller: 'labelController',
                    controllerAs: 'lab',
                    params: {
                        type: 'Critical'
                    }
                })

                /*=========================End label Panel Info==========================*/




                /*=========================Writer Panel Info==========================*/

                .state('dashboard.writer', {
                    url: '/writer',
                    templateUrl: 'views/writer/writer.html',
                    controller: 'newProjectController',
                    controllerAs: 'uc'

                })

                .state('dashboard.project-profile-writer', {
                    url: '/project-profile-writer',
                    templateUrl: 'views/writer/project-profile-writer.html',
                    controller: 'newProjectController',
                    controllerAs: 'uc'
                })
                /*=========================End Writer Panel Info==========================*/





                /*=========================Job Board Panel Info==========================*/

                .state('dashboard.jobBoard', {
                    url: '/jobBoard',
                    templateUrl: 'views/writer/writer.html',
                    controller: 'newProjectController',
                    controllerAs: 'uc'
                })

                .state('dashboard.jobBoardProfileWirter', {
                    url: '/jobBoardProfileWirter',
                    templateUrl: 'views/writer/project-profile-writer.html',
                    controller: 'userController',
                    controllerAs: 'uc'
                })
                .state('dashboard.jobBoardUnassigned', {
                    url: '/jobBoardUnassigned',
                    templateUrl: 'views/writer/project-profile-writer.html',
                    controller: 'userController',
                    controllerAs: 'uc'
                })

                /*=========================End Job Board Panel Info==========================*/





                /*=========================Download & Export Panel Info==========================*/

                .state('dashboard.savedFile', {
                    url: '/savedFile',
                    templateUrl: 'views/writer/writer.html',
                    controller: 'userController',
                    controllerAs: 'uc'
                })

                /*=========================End Download & Export Panel Info==========================*/




                /*=========================My Project Panel Info==========================*/

                .state('dashboard.clientAdmin', {
                    url: '/clientAdmin',
                    templateUrl: 'views/writer/writer.html',
                    controller: 'newProjectController',
                    controllerAs: 'uc'
                })
                .state('dashboard.projectProfileAdmin', {
                    url: '/projectProfileAdmin',
                    templateUrl: 'views/writer/project-profile-writer.html',
                    controller: 'userController',
                    controllerAs: 'uc'
                })
                .state('dashboard.projectAdmin', {
                    url: '/projectAdmin',
                    templateUrl: 'views/writer/writer.html',
                    controller: 'newProjectController',
                    controllerAs: 'uc'
                })

                .state('dashboard.newProject', {
                    url: '/newProject',
                    templateUrl: 'views/projects/newProject.html',
                    controller: 'newProjectController',
                    controllerAs: 'uc',
                    params: {
                        type: null,
                        data: null
                    }
                })

                //=========================================================================//
                //social login
                .state('dashboard.linked-account', {
                    url: '/linked-account',
                    templateUrl: 'views/projects/connectSocialMedia.html',
                    controller: 'newProjectController',
                    controllerAs: 'uc'
                })


                .state('dashboard.setting', {
                    url: '/setting',
                    templateUrl: 'views/setting/messageSetting.html',
                    controller: 'settingCtrl'
                })


                //========================================================================//

                /*=========================End My Project Panel Info==========================*/



                /*=========================Register Stage Info==========================*/
                .state('confirmPassword', {
                    url: '/confirmPassword/:email',
                    templateUrl: 'views/users/confirmPassword.html',
                    controller: 'userController',
                    controllerAs: 'uc'
                })
                /*=========================End Register Stage Info==========================*/


                /*=========================Project By Id Info==========================*/
                .state('dashboard.projectById', {
                    url: '/project/:id',
                    templateUrl: 'views/writer/project-profile-writer.html',
                    controller: 'ProjectByIdController',
                    params:{
                        data:null
                    }
                })

                /*=========================End Project by Id Info==========================*/

                /*=========================Mail Info==========================*/
                .state('dashboard.mail', {
                    url: '/mail',
                    templateUrl: 'views/mail/mailbox.html',
                    controller: 'mailController'
                })

                /*=========================End Mail Info==========================*/



                /*=========================compose Mail==========================*/
                .state('dashboard.composeMail', {
                    url: '/composemail',
                    templateUrl: 'views/mail/mail_compose.html',
                    controller: 'mailController',
                    params: {
                        email: 'type',
                        data: null,
                        subject: null,
                        type: null,
                        FullName: null,
                        message: null
                    }
                })

                /*=========================End Mail Info==========================*/


                /*=========================Mail Info==========================*/
                .state('dashboard.sentmail', {
                    url: '/sentmail',
                    templateUrl: 'views/mail/sentmailbox.html',
                    controller: 'mailController'
                })

                /*=========================End Mail Info==========================*/


                /*=========================Mail Info==========================*/
                .state('dashboard.mailDetail', {
                    url: '/mail/:mailId',
                    templateUrl: 'views/mail/mail_detail.html',
                    controller: 'viewMailController',
                    params: {
                        type: 'inbox'
                    }
                })

                /*=========================End Mail Info==========================*/

                /*=========================Send Mail Info==========================*/
                .state('dashboard.sendMailDetail', {
                    url: '/sendmail/:mailId',
                    templateUrl: 'views/mail/mail_detail.html',
                    controller: 'viewMailController',
                    params: {
                        type: 'sent'
                    }
                })

                /*=========================End Mail Info==========================*/

                /*=========================Trash Mail Info==========================*/
                .state('dashboard.trashMail', {
                    url: '/trashmail',
                    templateUrl: 'views/mail/trashmail.html',
                    controller: 'mailController'
                })

                /*=========================End Mail Info==========================*/


                /*=========================trash Mail Info==========================*/
                .state('dashboard.trashMailDetail', {
                    url: '/trashmail/:mailId',
                    templateUrl: 'views/mail/mail_detail.html',
                    controller: 'viewMailController'
                })

                /*=========================End Mail Info==========================*/


                /*=========================Trash Mail Info==========================*/
                .state('dashboard.importantmail', {
                    url: '/important',
                    templateUrl: 'views/mail/important_mail.html',
                    controller: 'mailController',
                    params: {
                        type: 'important'
                    }
                })

                /*=========================End Mail Info==========================*/

                /*=========================Draft Mail Info==========================*/
                .state('dashboard.draftmail', {
                    url: '/draft',
                    templateUrl: 'views/mail/draft_mail.html',
                    controller: 'mailController',
                    params: {
                        type: 'draft'
                    }
                })

                /*=========================End Mail Info==========================*/

                /*=========================trash Mail Info==========================*/
                .state('dashboard.draftMailDetail', {
                    url: '/draftmail/:mailId',
                    templateUrl: 'views/mail/mail_detail.html',
                    controller: 'viewMailController'
                })

                /*=========================End Mail Info==========================*/



                /*=========================trash Mail Info==========================*/
                .state('dashboard.importantMailDetail', {
                    url: '/importantmail/:mailId',
                    templateUrl: 'views/mail/mail_detail.html',
                    controller: 'viewMailController'
                })

                /*=========================End Mail Info==========================*/



                /*=========================addressbook Mail==========================*/
                .state('dashboard.addressBook', {
                    url: '/addressBook',
                    templateUrl: 'views/mail/address_book.html',
                    controller: 'mailController'
                })

                /*=========================End Mail Info==========================*/

                /*=========================Stop Server==========================*/
                .state('dashboard.stopServer', {
                    url: '/stopServer',
                    templateUrl: 'views/stop_server/stop_server.client.html',
                    controller: 'startAndStopServerController'
                })

                //Oc logs
                .state('dashboard.ocLogs', {
                    url: '/captionLog',
                    templateUrl: 'views/stop_server/captionLog.html',
                    controller: 'startAndStopServerController'
                })

                //Payment logs
                .state('dashboard.PaymentLogs', {
                    url: '/paymentlog',
                    templateUrl: 'views/payment/paymentlog.html',
                    controller: 'paymentLogController'
                })

                /*=========================End Mail Info==========================*/


                /*=========================Project based status ==========================*/
                .state('dashboard.Complete', {
                    url: '/Complete',
                    templateUrl: '/views/status/status.client.html',
                    controller: 'labelController',
                    params: {
                        statusType: 'completed'
                    }
                })

                .state('dashboard.UnAssigned', {
                    url: '/UnAssigned',
                    templateUrl: '/views/status/status.client.html',
                    controller: 'labelController',
                    params: {
                        statusType: 'Unassigned'
                    }
                })

                /*=========================End Project based status==========================*/

                /*=========================Project based status==========================*/
                .state('dashboard.InProgress', {
                    url: '/InProgress',
                    templateUrl: '/views/status/status.client.html',
                    controller: 'labelController',
                    params: {
                        statusType: 'In Progress'
                    }
                })

                /*=========================Project based status==========================*/

                /*=========================Project based status==========================*/
                .state('dashboard.Cancelled', {
                    url: '/Cancelled',
                    templateUrl: '/views/status/status.client.html',
                    controller: 'labelController',
                    params: {
                        statusType: 'cancelled'
                    }
                })

                .state('dashboard.UserProfile', {
                    url: '/userProfile',
                    templateUrl: '/views/users/edit-user.html',
                    controller: 'UserEditController'
                })


                .state('dashboard.issueReport', {
                    url: '/issueReport',
                    templateUrl: '/views/issue/issueReport.html',
                    controller: 'issueController'
                })

                .state('dashboard.priceConfiguration', {
                    url: '/priceConfiguration',
                    templateUrl: '/views/priceConfiguration/priceSetting.html',
                    controller: 'serviceController'
                })
            // $locationProvider.html5Mode(true);
            /*=========================Project based status==========================*/

        }
    ]);

/* ===================================$End state definition=================================================*/
app.run(function (editableOptions) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});



/*=================================request auth controllers and logics==================================*/

app.controller('appController', ['$scope', '$rootScope', 'jwtHelper', '$state', 'userService', 'dataService', function ($scope, $rootScope, jwtHelper, $state, userService, dataService) {
    this.initUser = function (user) {
        $rootScope.currentUser = user || {};
        $rootScope.currentUser.token = JSON.parse(user.token);
        $rootScope.currentUser.data = jwtHelper.decodeToken($rootScope.currentUser.token);
        if (!$rootScope.currentUser.data.user.profile_picture) {
            $rootScope.currentUser.data.user.profile_picture = "css/design/img/avtar.png";
        };
        userService.getUserById($rootScope.currentUser.data.user.id, function (response) {
            $rootScope.currentUser.data.user.role = response.data.role.name;
            // if ($rootScope.currentUser.data.user.role == 'client_user') {
            //     $rootScope.currentUser.data.user.projectListState = 'dashboard.clientAdmin'
            // } else if ($rootScope.currentUser.data.user.role == 'project_admin') {
            //     $rootScope.currentUser.data.user.projectListState = 'ashboard.projectAdmin'
            // } else if ($rootScope.currentUser.data.user.role == 'writer') {
            //     $rootScope.currentUser.data.user.projectListState = 'dashboard.writer'
            // } else if ($rootScope.currentUser.data.user.role == 'client_admin') {
            //     $rootScope.currentUser.data.user.projectListState = 'dashboard.clientAdmin'
            // } else if ($rootScope.currentUser.data.user.role == 'project_user') {
            //     $rootScope.currentUser.data.user.projectListState = 'dashboard.projectAdmin'
            // } else if ($rootScope.currentUser.data.user.role == 'super_user') {
            //     $rootScope.currentUser.data.user.projectListState = 'dashboard.clientAdmin'
            // }
            // $rootScope.currentUser.data.user.projectListState = 'response.data.role.name';
            // console.log(response);
            // $rootScope.currentUser.data.user.driveToken = response.data.
            // console.log($rootScope);
        });
        $rootScope.loader = false;
        $rootScope.logout = function () {
            $rootScope = {};
            window.location.href = '/logout';
            console.log('rootscope:', $rootScope);
        }
        $('#projectlist').hide();
    };

    $scope.loginAsUser = (userId) => {
        $rootScope.realUser = $rootScope.currentUser.data.user.id;
        userService.getUserById(userId, function (response) {
            $rootScope.currentUser.data = jwtHelper.decodeToken(response.data.token);
            $rootScope.currentUser.token = response.data.token;
            if (!$rootScope.currentUser.data.user.profile_picture) {
                $rootScope.currentUser.data.user.profile_picture = "css/design/img/avtar.png";
            };
            $rootScope.currentUser.data.user.role = response.data.role.name;
        })
    }

    $scope.loginBackAsUser = (userId) => {
        userService.getUserById($rootScope.realUser, function (response) {
            $rootScope.currentUser.data = jwtHelper.decodeToken(response.data.token);
            if (!$rootScope.currentUser.data.user.profile_picture) {
                $rootScope.currentUser.data.user.profile_picture = "css/design/img/avtar.png";
            };
            $rootScope.realUser = ""
            $rootScope.currentUser.data.user.role = response.data.role.name;

        })
    }



}]);

app.factory('httpRequestInterceptor', ['$rootScope', function ($rootScope) {
    return {
        request: function ($config) {
            if ($rootScope.currentUser && $rootScope.currentUser.token) {
                $config.headers['x-access-token'] = $rootScope.currentUser.token;
            }
            return $config;
        }
    };
}]);


app.factory('socket', function (socketFactory) {

    return socketFactory({
        prefix: 'foo~',
        // ioSocket: io.connect('http://localhost:3000') //for enviroment
        ioSocket: io.connect('https://www.postcaponline.com') //for productio
    });
});
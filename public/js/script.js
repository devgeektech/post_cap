"use strict";

// Add reverse alsoResize method to jQuery UI
$.ui.plugin.add("resizable", "alsoResizeReverse", {

    start: function() {
        var that = $(this).resizable( "instance" ),
            o = that.options;

        $(o.alsoResizeReverse).each(function() {
            var el = $(this);
            el.data("ui-resizable-alsoresizeReverse", {
                width: parseInt(el.width(), 10), height: parseInt(el.height(), 10),
                left: parseInt(el.css("left"), 10), top: parseInt(el.css("top"), 10)
            });
        });
    },

    resize: function(event, ui) {
        var that = $(this).resizable( "instance" ),
            o = that.options,
            os = that.originalSize,
            op = that.originalPosition,
            delta = {
                height: (that.size.height - os.height) || 0,
                width: (that.size.width - os.width) || 0,
                top: (that.position.top - op.top) || 0,
                left: (that.position.left - op.left) || 0
            };

        $(o.alsoResizeReverse).each(function() {
            var el = $(this), start = $(this).data("ui-resizable-alsoresize-reverse"), style = {},
                css = el.parents(ui.originalElement[0]).length ?
                    [ "width", "height" ] :
                    [ "width", "height", "top", "left" ];

            $.each(css, function(i, prop) {
                var sum = (start[prop] || 0) - (delta[prop] || 0);
                if (sum && sum >= 0) {
                    style[prop] = sum || null;
                }
            });

            el.css(style);
        });
    },

    stop: function() {
        $(this).removeData("resizable-alsoresize-reverse");
    }
});

// Add resizable functions to elements
$('.caption-editor').resizable({
    handles: 'e',
    alsoResizeReverse: '.info-media',
    containment: '.row',
    minWidth: 250,
    maxWidth: $('.caption-info-wrap').width() - 250
});

$('.caption-info-wrap').resizable({
	handles: 's',
	minHeight: 130
});

$('.media-box').resizable({
	handles: 'n',
	alsoResizeReverse: '.info-box',
	minHeight: 65
})

// Click to expand/shrink section
$('.block-toggle-ctrl').click(function() {
	var el = $(this).parent(),
		target = $('.'+$(this).attr('data-target')),
		dataShrink = el.attr('data-shrink');

	if (el[0].hasAttribute('data-shrink')) {
		if ($(this).hasClass('btc-e') || $(this).hasClass('btc-w')) {
			el.width(el.width()-dataShrink);
			el.removeAttr('data-shrink');
		}
		else if ($(this).hasClass('btc-n') || $(this).hasClass('btc-s')) {
			el.height(el.height()-dataShrink);
			el.removeAttr('data-shrink');
		}

		target.removeClass('hide');
	}
	else {
		var	targetHeight = target.height(),
			targetWidth = target.width();

		target.addClass('hide');

		if ($(this).hasClass('btc-e') || $(this).hasClass('btc-w')) {
			el.width(el.width()+targetWidth);
			el.attr('data-shrink', targetWidth);
		}
		else if ($(this).hasClass('btc-n') || $(this).hasClass('btc-s')) {
			el.height(el.height()+targetHeight);
			el.attr('data-shrink', targetHeight);
		}
	}
});

/*---------------------------------------------------------------------------------
                                      MODAL
---------------------------------------------------------------------------------*/
var modalGlass = $('.modal-glass');

// Populate and show modal
$('[data-target-modal]').click(function() {
    var modalData = $('[data-modal="'+$(this).attr('data-target-modal')+'"]');

    modalGlass.find('.modal').attr('data-modal', $(this).attr('data-target-modal'));
    modalGlass.find('.modal-heading').text(modalData.find('[data-modal-heading]').text());
    modalGlass.find('.modal-body').html(modalData.find('[data-modal-body]').html());

    modalGlass.removeClass('hide');
    setTimeout(function() { modalGlass.find('.modal').addClass('show-modal'); }, 10);
});

function closeModal() {
    modalGlass.addClass('hide').find('.modal').removeClass('show-modal').removeAttr('data-modal').children().empty();
}

// Close modal
modalGlass.find('.modal-close').click(closeModal);
modalGlass.click(function(event) {
    if ($(event.target).hasClass('modal-glass')) {
        closeModal();
    }
});

// Additional modal cancel button
$('.modal-body').on('click', '.modal-cancel', closeModal);
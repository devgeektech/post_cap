$(document).ready(function() {	
	$("#able_printout_edit").jqte();
	//$( "#idinterval" ).spinner({ min: 1, step: 1 });
	$( "#speedrate" ).spinner({ min: 0.50, step: 0.25 });
	
	// $body = $("body");
	// $(document).on({
		// ajaxStart: function() { $body.addClass("loading");    },
		// ajaxStop: function() { $body.removeClass("loading"); }    
	// });
	
	// start/stop timer - custom algorithm -->
	$('#able_transcript_edit').wordsTracking({
		controlButtonId: 'time-type'
	});
	
	$("#btnAdd").click(function () {
		createnewrow(); 
	});
	
	$("#speedrate").on("spinstop", function(){
		var rate = $(this).spinner('value');
		wavesurfer.setPlaybackRate(rate)
		
		var player = document.getElementById("captionedvid");
		player.playbackRate = rate;	
		$("span.able-speed").html("Speed: "+rate+"x");
	});
	
	(function( func ) {
		$.fn.addClass = function() { // replace the existing function on $.fn
			func.apply( this, arguments ); // invoke the original function
			this.trigger('classChanged'); // trigger the custom event
			return this; // retain jQuery chainability
		}
	})($.fn.addClass); // pass the original function as an argument

	(function( func ) {
		$.fn.removeClass = function() {
			func.apply( this, arguments );
			this.trigger('classChanged');
			return this;
		}
	})($.fn.removeClass);
	
	// On opening the Open project dialog.
	$('.modal').on('classChanged', function() {
		if($(this).attr('data-modal') == "projects" && $(this).hasClass('show-modal')){
			$.ajax({url: "/listbucketmedia",
				success: function(result){
					var radiobtnControls = '';
					$.each(result, function (key, val) {
						var disableAttr = '';
						var cssAttr = '';
						var projectName = '';
						if(val == false){
							disableAttr = '';
							cssAttr = '';
							projectName = key;
						}else{
							disableAttr = 'disabled';
							cssAttr = 'style="color:red;"';
							projectName = key + '-inprogress';
						}
						
						if(radiobtnControls == "")
							radiobtnControls = '<input type="radio" name="project" value="'+key+'" '+disableAttr+'><span '+cssAttr+'>'+projectName+'</span><br />';
						else
							radiobtnControls = radiobtnControls + '<input type="radio" name="project" value="'+key+'" '+disableAttr+'><span '+cssAttr+'>'+projectName+'</span><br />';
					});
					
					$('#lstProjects').html(radiobtnControls);
				},
				error: function(xhr){
					console.log(xhr);
				}
			});
		}
	});
});

function createnewrow()
{
	var count = $('.tbltimecode tr').length;
	var starttimeId = count;
	var starttime = '00:00:00:000';
	var endtime = '00:00:00:000';
	var n = count;
	$('.tbltimecode > tbody:last-child').append('<tr><td><input name="checkbox" onclick=check_rows_lines($(this),"labeltxt_'+ starttimeId +'") type="checkbox">'+ n + '</td><td><input type="text" size="12" id="starttxt_'+starttimeId+'" value="'+starttime+'" /><input type="hidden" id="hdnstarttxt_'+starttimeId+'" value="" /></td><td><input type="text" size="12" id="endtxt_'+starttimeId+'" value="'+endtime+'" /><input type="hidden" id="hdnendtxt_'+starttimeId+'" value="" /></td><td><input type="text" size="60" ondrag=set_new_active_input("' + starttimeId + '") onmouseup=set_new_active_input("' + starttimeId + '") id="labeltxt_'+starttimeId+'" onkeyup=LineSyncWithTime(event,"'+starttimeId+'") value="" /></td></tr>');		
}

//Open Project Dialog -->
function loadProject() {
	var selectedVal = $('input[name=project]:checked').val();
	var postProcessVal = $('input[name=processRdBtn]:checked').val();
	selectedVal = selectedVal.replace("/", "");	
	//post processing for caption
	if(postProcessVal == "watson")
	{			
		$("#autocaptionDiv").html("Processed file will be opened in a moment!");
		$("#selectedMedia").val(selectedVal);
	}
	//Set the corresponding video and audio file to the players
	if(selectedVal != "")
	{	
		$("#waveId").val("http://d3l6fnyzzmn75t.cloudfront.net/" + selectedVal + "/wav-" + selectedVal + ".wav");
		loadwavesurfer();
		
		$("#player").show();
		$("#noplayer").hide();
		var player = document.getElementById('captionedvid');
		var mp4Vid = document.getElementById('mp4Source');
		mp4Vid.src = "http://d7to8ix29t7ij.cloudfront.net/" + selectedVal + ".mp4";
		$("#videoPlayerId").val("http://d7to8ix29t7ij.cloudfront.net/" + selectedVal + ".mp4");
		player.load();
		closeModal();
  	}
}
function addProject() {			
		$("#sthreeForm").submit();
		//closeModal();
}

function adjustTimestamp() {
	//alert(spinner.spinner( "value" ));
	loadIntervalRegions(JSON.parse($("#lblJsonId").val()));
	saveRegions();
	closeModal();
}
	
function loadmergecontent() {
	var transcriptContent = $("#able_transcript_edit").html();
	var transcriptHtml = $.parseHTML( transcriptContent );			
	var found = $(transcriptHtml).filter("span");
	if(found.length == 0){
		alert("No projects opened");
		closeModal();
	}
	else{
		var dynamicControls = '';
		found.each(function(){
			var $span = $(this);

			var starttime = $span.attr('data-start');
			var starttimeId = starttime.replace(".", "_");
			var endtime = $span.attr('data-end');
			var content = $span.html();
			
			if(dynamicControls == '')
				dynamicControls = '<div class="timestamp"><input type="checkbox" id="chk_'+starttimeId+'" /><span>Start Time:</span> <input type="text" id="starttxt_'+starttimeId+'" value="'+starttime+'" /><span>End Time:</span> <input type="text" id="endtxt_'+starttimeId+'" value="'+endtime+'" /><span>Content:</span> <input type="text" id="labeltxt_'+starttimeId+'" value="'+content+'" /></div>';
			else
				dynamicControls = dynamicControls + '<div class="timestamp"><input type="checkbox" id="chk_'+starttimeId+'" /><span>Start Time:</span> <input type="text" id="starttxt_'+starttimeId+'" value="'+starttime+'" /><span>End Time:</span> <input type="text" id="endtxt_'+starttimeId+'" value="'+endtime+'" /><span>Content:</span> <input type="text" id="labeltxt_'+starttimeId+'" value="'+content+'" /></div>';			
		});
		dynamicControls = dynamicControls + '<br><br><div class="text-center"><a href="javascript:;" onclick="return mergeTimestampDetails()" class="button">Merge</a><a href="javasript:;" onclick="return splitTimestampDetails()" class="button">Split</a><a href="#" class="button modal-cancel">Cancel</a></div>';
		$('[data-modal="merge_split"] .modal-body, [data-modal="merge_split"] [data-modal-body]').html(dynamicControls);
	}
}

function loadmanuladjustcontent() {
	var transcriptContent = $("#able_transcript_edit").html();
	var transcriptHtml = $.parseHTML( transcriptContent );			
	var found = $(transcriptHtml).filter("span");
	if(found.length == 0){
		alert("No projects opened");
		closeModal();
	}
	else{
		var dynamicControls = '';
		found.each(function(){
			var $span = $(this);

			var starttime = $span.attr('data-start');
			var starttimeId = starttime.replace(".", "_");
			starttimeId = starttimeId.replace(".", "_");
			var endtime = $span.attr('data-end');
			var content = $span.html();
			
			if(dynamicControls == '')
				dynamicControls = '<div class="timestamp"><span>Start Time:</span> <input type="text" id="starttxt_'+starttimeId+'" value="'+starttime+'" /><span>End Time:</span> <input type="text" id="endtxt_'+starttimeId+'" value="'+endtime+'" /><span>Content:</span> <input type="text" id="labeltxt_'+starttimeId+'" value="'+content+'" /></div>';
			else
				dynamicControls = dynamicControls + '<div class="timestamp"><span>Start Time:</span> <input type="text" id="starttxt_'+starttimeId+'" value="'+starttime+'" /><span>End Time:</span> <input type="text" id="endtxt_'+starttimeId+'" value="'+endtime+'" /><span>Content:</span> <input type="text" id="labeltxt_'+starttimeId+'" value="'+content+'" /></div>';			
		});
		dynamicControls = dynamicControls + '<br><br><div class="text-center"><a href="javascript:;" onclick="return manualAdjustTimestampDetails()"  class="button">Ok</a><a href="#" class="button modal-cancel">Cancel</a></div>';
		$('[data-modal="manual_text"] .modal-body, [data-modal="manual_text"] [data-modal-body]').html(dynamicControls);
	}
}	

function showIndicators() {
	if(ClickedTranscriptContent != ''){
		var watsonElem = $(ClickedTranscriptContent);
		var id = watsonElem.attr("id");
		var actualdata = watsonElem.html();
		var descriptiveData = $( "#tags" ).val();
		actualdata = actualdata + ' ' + descriptiveData;
		$("#" + id).html(actualdata);
		closeModal();
	}else{
		alert("Please choose a line where Descriptive Indicator to be placed!");
	}
	
	// var actualdata = $( "#tags" ).val();
	// var starttime = $( "#starttime" ).val();
	// var endtime = $( "#endtime" ).val();
	// var div_output = $("#able_transcript_edit").html() + '\n' + '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+starttime+'" data-end="'+endtime+'">' + actualdata + '</span>';
	
	// var plaintext_output = $("#able_printout_edit").html() + '\n' + actualdata;		
	
	// $("#able_transcript_edit").html(div_output);
	// $("#able_printout_edit").html(plaintext_output);
	// indicatorDialog.dialog( "close" );
}
			
function mergeTimestampDetails() {
	var transcriptContent = $("#able_transcript_edit").html();
	var transcriptHtml = $.parseHTML( transcriptContent );
	var found = $(transcriptHtml).filter("span");
	var combinedContent = '';
	var endTime = '';
	var selectedIdx = 0;
	var loopIdx = 0;
	var firstSelectedSpan;
	var firstSelectedJsonObject;
	
	//Waveform purpose.
	var jsonValue = JSON.parse($("#lblJsonId").val());
	
	found.each(function(){
		var $span = $(this);

		var starttime = $span.attr('data-start');
		var starttimeId = starttime.replace(".", "_");
		if($("#chk_"+starttimeId).prop('checked')==true){
			combinedContent = combinedContent + ' ' + $("#spanid_"+starttimeId).html();
			endTime = $span.attr('data-end');
			
			//remove second and further selected span tags.
			if(selectedIdx > 0){
				$("#spanid_"+starttimeId).remove();
				
				//remove json string for waveform.
				delete jsonValue[loopIdx];
			}
			else{
				firstSelectedSpan = $("#spanid_"+starttimeId);
				firstSelectedJsonObject = jsonValue[loopIdx];
			}
			selectedIdx++;
		}
		loopIdx++;
	});
	firstSelectedSpan.html(combinedContent);
	firstSelectedSpan.attr('data-end', endTime);
	firstSelectedSpan.attr('data-alternative', 'Not Applicable');
	
	firstSelectedJsonObject.end = endTime;
	firstSelectedJsonObject.attributes.label = combinedContent;

	//Reload Waveform.
	$("#lblJsonId").val(JSON.stringify(jsonValue));
	loadRegions(jsonValue, 'clear');
	
	closeModal();
}
			
function splitTimestampDetails() {
	var transcriptContent = $("#able_transcript_edit").html();
	var transcriptHtml = $.parseHTML( transcriptContent );
	var found = $(transcriptHtml).filter("span");
	var splitContent = '';
	var selectedIdx = 0;
	var loopIdx = 0;
	var firstSelectedSpan;
	var firstSelectedJsonObject;

	//Waveform purpose.
	var jsonValue = JSON.parse($("#lblJsonId").val());

	found.each(function(){
		var $span = $(this);

		var starttime = $span.attr('data-start');
		var starttimeId = starttime.replace(".", "_");				
		if($("#chk_"+starttimeId).prop('checked')==true){
			var endtime = $span.attr('data-end');
			var difftime = endtime - starttime;
			var halftime = parseFloat(starttime) + parseFloat((difftime/2).toFixed(2));
			
			var secondhalfStarttime = halftime;
			
			//split only one at a time.
			if(selectedIdx == 0){
				var selectedLabel = $("#spanid_"+starttimeId).html();
				var splitT = selectedLabel.split(" ");
				var halfIndex = Math.round(splitT.length / 2);
				var newText = "";
				var secondHalfText = "";
				for(var i = 0; i < splitT.length; i++) {
					if(i <= halfIndex) {
						newText += splitT[i] + " ";
					}
					else{
						secondHalfText += splitT[i] + " ";
					}
				}
				
				firstSelectedSpan = $("#spanid_"+starttimeId);
				firstSelectedSpan.attr('data-end', halftime);
				firstSelectedSpan.html(newText);
				
				firstSelectedJsonObject = jsonValue[loopIdx];
				firstSelectedJsonObject.end = halftime;
				firstSelectedJsonObject.attributes.label = newText;						
				
				//append second half text with new start and end time.
				$('<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+secondhalfStarttime+'" data-end="'+endtime+'" data-confidence="100" data-alternative="Not Applicable" onclick="DisplayWatsonData(this);">' + secondHalfText + '</span>').insertAfter("#spanid_"+starttimeId);
				
				//append new value to json.
				var newObj = {"confidence":'100',"start":secondhalfStarttime,"end":endtime,"data":{},"attributes":{"label": secondHalfText }};
				jsonValue.push(newObj);
			}
			
			selectedIdx++;
		}		
		
		loopIdx++;
	});
			
	//Reload Waveform.
	$("#lblJsonId").val(JSON.stringify(jsonValue));
	loadRegions(jsonValue, 'clear');
	
	mergeSplitTimestampDialog.dialog( "close" );
}
			
function manualAdjustTimestampDetails() {
	var transcriptContent = $("#able_transcript_edit").html();
	var transcriptHtml = $.parseHTML( transcriptContent );
	var found = $(transcriptHtml).filter("span");
	var loopIdx = 0;
	
	//Waveform purpose.
	var jsonValue = JSON.parse($("#lblJsonId").val());
	
	found.each(function(){
		var $span = $(this);
		var starttimeId = $span.attr('data-start').replace(".", "_");
		
		var startime = $("#starttxt_"+starttimeId).val();
		var endtime = $("#endtxt_"+starttimeId).val();
		var label = $("#labeltxt_"+starttimeId).val();
		
		var selectedSpan = $("#spanid_"+starttimeId);
		selectedSpan.attr('data-start', startime);
		selectedSpan.attr('data-end', endtime);
		selectedSpan.html(label);
		
		var selectedJsonObj = jsonValue[loopIdx];
		selectedJsonObj.start = startime;
		selectedJsonObj.end = endtime;
		selectedJsonObj.attributes.label = label;
		
		loopIdx++;
	});
	
	//Reload Waveform.
	$("#lblJsonId").val(JSON.stringify(jsonValue));
	loadRegions(jsonValue, 'clear');
	
	manualTextBasedTimestampDialog.dialog( "close" );
}
	
function EnableCaptionEdit()
{
	$("#plaintextstyle").hide();
	$("#timecodestyle").hide();
	$("#transcript").show();
	
	$("#lineButtons").hide();
	$("#wordButtons").hide();
	$("#clickSyncbutton").show();
}

function EnableTranscriptEdit()
{
	$("#plaintextstyle").show();
	$("#transcript").hide();
	$("#timecodestyle").hide();
	
	$("#lineButtons").hide();
	$("#wordButtons").hide();
	$("#clickSyncbutton").hide();
	
	var htmlcontent = $("#able_transcript_edit").html();
	var regex = /(<([^>]+)>)/ig;
	var result = htmlcontent.replace(regex, "");
	//alert($(".jqte_editor").html());
	//if($.trim($(".jqte_editor").html()) == '')
		$(".jqte_editor").html(result);	
}

function EnableTimecodeEdit()
{
	$("#plaintextstyle").hide();
	$("#transcript").hide();
	$("#timecodestyle").show();
	
	$("#lineButtons").show();
	$("#wordButtons").hide();
	$("#clickSyncbutton").hide();
	
	//apply the changes to json object so that changes retains
	SaveJsonObject("timecode");
		
	CreateTimeCodeEditorControls();	
}

function SaveJsonObject(mode)
{
	//Get the data from caption edit or mode editor window and save it back to watson object.
	var transcriptContent = $("#able_transcript_edit").html();
	var transcriptHtml = $.parseHTML( transcriptContent );			
	var found = $(transcriptHtml).filter("span");
	
	if(typeof(watsonObject)  !== "undefined"){
		//console.log(found);
		found.each(function(){
			var $span = $(this);
			var dataid = $span.attr('data-id');
			var content = $span.html();
			watsonObject.results.forEach(function(item, key){
				
				//update only first alternative word in the json object.
				var firstalternative = 0;
				item.alternatives.forEach(function(sentence, sentence_key) {
					if(dataid == sentence.id && firstalternative == 0)
						sentence.transcript = content;
					
					firstalternative++;
				});
			});
		});
		console.log(watsonObject);
	}
}

function CreateTimeCodeEditorControls()
{
	var transcriptContent = $("#able_transcript_edit").html();
	var transcriptHtml = $.parseHTML( transcriptContent );			
	var found = $(transcriptHtml).filter("span");
	//console.log(found);
	var n=0
	if(found.length == 0){
		//alert("No projects opened");
	}
	else{
		var dynamicControls = '<table class="tbltimecode"><tr class="tableheader"><th>Line&nbsp;#</th><th>Start</th><th>End</th><th>Text</th></tr>';
		found.each(function(){
			var $span = $(this);
			var starttimeId = $span.attr('data-start').replace(new RegExp("-", 'g'), "_");
			var starttime = $span.attr('data-start').replace(new RegExp("-", 'g'), ":");
			var endtime = $span.attr('data-end').replace(new RegExp("-", 'g'), ":");
			var confidence = $span.attr('data-confidence')
			var content = $span.html();
			
			// <table class="tbltimecode">
				// <tr class="tableheader"><th>Line#</th><th>Start</th><th>End</th><th>Text</th></tr>
				// <tr><td>1</td><td><input type="text" size="6" value="" /></td><td><input type="text" size="6" value="" /></td><td><input type="text" size="50" value="" /></td></tr>
			// </table>
			n = n + 1;
			if(dynamicControls == '')
				dynamicControls = '<tr><td><input name="check_name_'+ starttimeId +'" id="check_id_'+ starttimeId +'" onclick=check_rows_lines($(this),"labeltxt_'+ starttimeId +'","' + confidence +  '") type="checkbox">'+ n + '</td><td><input type="text" size="12" id="starttxt_'+starttimeId+'" value="'+starttime+'" /></td><td><input type="text" size="12" id="endtxt_'+starttimeId+'" value="'+endtime+'" /></td><td><input type="text" size="60" ondrag=set_new_active_input("' + starttimeId + '") onmouseup=set_new_active_input("' + starttimeId + '","' + confidence +  '") id="labeltxt_'+starttimeId+'" value="'+content+'" /></td></tr>';
			else
				dynamicControls = dynamicControls + '<tr><td><input name="check_name_'+ starttimeId +'" id="check_id_'+ starttimeId +'" onclick=check_rows_lines($(this),"labeltxt_'+ starttimeId +'","' + confidence +  '") type="checkbox">'+ n + '</td><td><input type="text" size="12" id="starttxt_'+starttimeId+'" value="'+starttime+'" /></td><td><input type="text" size="12" id="endtxt_'+starttimeId+'" value="'+endtime+'" /></td><td><input type="text" size="60" onmouseup=set_new_active_input("' + 'labeltxt_' + starttimeId + '","' + confidence +  '") id="labeltxt_'+starttimeId+'" value="'+content+'" /></td></tr>';
		});
		
		dynamicControls = dynamicControls + '</table><input type="button" class="button" value="Add New" id="btnAdd" onclick=createnewrow(); />';
		$("#timecodeeditor").html(dynamicControls);
	}
}
	
//get watsonInfo from the file
setInterval(function(){ 
	if($("#selectedMedia").val() != "" && typeof(watsonObject)  === "undefined")
	{	
		var selectedVal = $("#selectedMedia").val();
		$.ajax({url: "/getwatson?data=" + selectedVal,
			success: function(result){
				$("#autocaptionDiv").html("Processing completed!. Click on transcript for information.");
				if(result!= "no data")
				{
					watsonObject = jQuery.parseJSON(result);
					
					var linecount = 1;
					//Capitalize the transcript.
					watsonObject.results.forEach(function(item, key){						
						item.alternatives.forEach(function(sentence, sentence_key) {
							sentence.transcript = sentence.transcript.substr(0,1).toUpperCase() + sentence.transcript.substr(1);
							sentence.id = linecount;
						});
						linecount++;
					});					
					
					//console.log(watsonObject);
					fill_watson_based_waves(watsonObject, 'line');
				}
			},
			error: function(xhr){
				watsonObject = null;
			}
		});
	}
}, 10000);
	
//get json Object from Custom Start/Stop Timer.
setInterval(function(){ 
	if($("#StartStopTimer").val() != "" && typeof(customObject)  !== "undefined")
	{
		$("#StartStopTimer").val("");
		fill_custom_based_waves(customObject);			
	}
}, 10000);

function InsertSpeaker() {
	if(ClickedTranscriptContent != ''){
		var watsonElem = $(ClickedTranscriptContent);
		var id = watsonElem.attr("id");
		var actualdata = watsonElem.html();
		var speakerData = $( "#txtSpeaker" ).val();
		actualdata = "[" + speakerData + "] " + actualdata;
		$("#" + id).html(actualdata);
		closeModal();
	}else{
		alert("Please choose a line where Speaker Indicator to be placed!");
	}
}
app.directive('boardDirective', function () {
    function tableFunction($scope, element, attributes) {
        $scope.changeText = function (newText) {
            $scope.text = newText;
        }
    }
    return {
        link: tableFunction,
        templateUrl: 'js/directives/board/board.html',
        restrict: 'A',
        controller: ['$scope', '$state', '$stateParams', 'dataService', '$rootScope', '$location', '$anchorScroll', 'toaster', 'commanService', '$http', function ($scope, $state, $stateParams, dataService, $rootScope, $location, $anchorScroll, toaster, commanService, $http) {
            console.log($state.current.name); // state1 
            $scope.rowProperties = {};
            var allRowProperties = ["writer", "apply", "request", "newProject", "projectProfileAdmin", "alert", "changePayment", "report", "assign", "lable1", "submit", "download", "projectId", "projectName", "client", "user", "orderPlaced",
                "orderDue", "orderStatus", "requestService", "mediaType", "duration", "dueDate", "status", "invoice", "totalCost", "paymentStatus", "unassignedButton"
            ];
            allRowProperties.forEach(function (item) {
                $scope.rowProperties[item] = true;
            });
            var hideDetails = function (hide) {
                hide.forEach(function (item) {
                    $scope.rowProperties[item] = false;
                });
            }
            $scope.userLabels = commanService.label;

            console.log('dasd0', $stateParams)
            $scope.data = $stateParams.data


            $scope.scrollTo = function (id) {
                if (!$scope.productData.data.workingUser) {
                    $location.hash(id);
                    $anchorScroll();
                } else {
                    toaster.pop('warning', 'Assigned To:' + $scope.productData.data.workingUser.firstname + $scope.productData.data.workingUser.lastname);
                }
            }



            // issueReport

            $scope.issueReport = function (issue) {
                var postData = {
                    "userId": $rootScope.currentUser.data.user.id,
                    "issueWithDelivery": issue.delivery,
                    "issueWithMedia": issue.media,
                    "message": issue.message,
                    "projectId": $stateParams.id
                }

                dataService.issueSubmission(postData, function (response) {
                    issue.message = null;
                    issue.media = false;
                    issue.delivery = false;
                    $('#reportIssue').modal('hide');
                })

            }



            $scope.videoOptions = [
                {
                    key: "over_video",
                    value: "Over Video (Maintain Original Size)"
                },
                {
                    key: "beneath_video",
                    value: "Beneath Video (Resize Video and place captions underneath)"
                }
            ]

            $scope.convertVideo = function (procjectLink, body, email, option) {
                console.log(`clicked`)
                let check = body.attachment.includes('srt')
                if (!body.attachment.includes('srt')) return $scope.message = 'Please provide a srt format.'
                // let formatSrt = JSON.parse(body.attachment)
                dataService.downloadOpenCaptionVideo({ link: procjectLink, srt: formatSrt.attachmentLink, email: email, option }, function (response) {
                    console.log(`${response}`)
                    if (response.data.message.success) {
                        toaster.pop('success', response.data.message.message);
                        $('#OCserver').modal('hide');
                    } else {
                        toaster.pop('warning', response.data.message.message);
                    }
                })
               
            }

            $scope.addlabelToproject = (label, projectId) => {
                var newLabel = JSON.parse(label)
                var postData = {
                    projectId: projectId,
                    label: newLabel.name,
                    labelId: newLabel._id
                }
                dataService.addlabelToproject(postData, function (response) {
                    toaster.pop('success', response.data);
                    $('#labelModal').modal('hide');
                    $scope.getusersLabel();
                })
            }


            $scope.addNewlabel = function (projectId) {
                var postData = {
                    label: $scope.labelName,
                    projectId: projectId
                }
                dataService.addlabelbyUser(postData, function (response) {
                    toaster.pop('success', response.data);
                    $scope.labelName = "";
                    $scope.getusersLabel()
                })
            };

            $scope.getusersLabel = function () {
                dataService.getusersLabel(function (response) {
                    $scope.userLabels = response.data;
                })
            };


            $scope.removelabelbyUser = function (labelId) {
                var postData = {
                    labelId: labelId
                }
                dataService.removelabelbyUser(postData, function (response) {
                    toaster.pop('success', response.data);
                    $scope.getusersLabel();
                })
            };

            $scope.downloadLoader = false;

            $scope.downloadVideo = (link) => {
                var postData = {
                    link: link
                }
                $scope.downloadLoader = true;
                dataService.downloadVideo(postData, function (response) {
                    if (response.data.messege) {
                        toaster.pop('success', response.data.messege);

                    } else {
                        $scope.downloadLoader = false;
                        $scope.youtubeIn = response.data
                    }
                })
            }

            const saveData = (function () {
                const a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                return function (data, fileName) {
                    const blob = new Blob([data], {
                        type: "octet/stream"
                    }),
                        url = window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = fileName;
                    a.click();
                    window.URL.revokeObjectURL(url);
                };
            }());

            //cancelProject

            $scope.cancelProject = () => {
                var postData = {
                    projectId: $stateParams.id
                }
                dataService.cancelProject(postData, function (response) {
                    toaster.pop('warning', response.data);
                    $scope.initProjectById()
                })
            }

            $scope.updateWriterPrice = (price) => {

                dataService.editWriterPrice({
                    writerPrice: price,
                    id: $stateParams.id
                }, function (response) {
                    if (!response.data.success) {
                        toaster.pop('warning', response.data.message);
                    } else {
                        $scope.initProjectById()
                        $('#updateWriter').modal('hide');
                    }


                })
            }
            $scope.updatetotalPrice = (price) => {
                console.lof(price)
                dataService.updateWriterPrice({
                    totalPrice: price,
                    id: $stateParams.id
                }, function (response) {
                    console.log('====>', response)
                    $('#updateTotal').modal('hide');
                })
            }


            // updatedCostProject
            $scope.updatedCostProject = function (updatedPrice) {
                var postData = {
                    "userId": $rootScope.currentUser.data.user.id,
                    "newTotalPrice": updatedPrice,
                    "projectId": $stateParams.id
                }
                dataService.updatedCostProject(postData, function (response) {
                    $scope.updatedPrice = null;
                    toaster.pop('warning', response.data.message);
                    $('#updateCost').modal('hide');
                    $('#updateTotal').modal('hide');

                    $scope.initProjectById()
                })
            }
            //  End

            $scope.applied = false;

            $scope.acceptJobAndRejectByAdmin = function (jobId, userId, type) {
                var postData = {
                    id: jobId,
                    userId: userId,
                    type: type
                };
                dataService.acceptJobAndRejectByAdmin(postData, function (response) {
                    if (type == 'reject') {
                        dataService.getAppliedUser({
                            id: $scope.productData.data.job
                        }, function (response) {
                            $scope.appliedNames = response.data;
                        })
                    } else {
                        $scope.applied = true;
                        $('#appliedUser').modal('hide');
                        $scope.initProjectById();
                    }
                })
            }

            $scope.userCheck = () => {
                if ($rootScope.currentUser.data.user.role == 'writer') {
                    $scope.userCondition = true;
                } else {
                    $scope.userCondition = false;
                }
            }
            $scope.userCheck(); //check usertype

            $scope.getAllFile = data => {
                $scope.SubmitSrt = []
                for (let i = 0; i < data.attachment.length; i++) {
                    if (data.attachment[i].attachmentLink.includes('srt')) $scope.SubmitSrt.push(data.attachment[i].attachmentLink)
                }
                for (let j = 0; j < data.finalProjectSubmit.length; j++)
                    if (data.finalProjectSubmit[j].includes('srt')) $scope.SubmitSrt.push(data.finalProjectSubmit[j])
                console.log('dasdasdasd', $scope.SubmitSrt)
                // let attachment = data.attachment.map(function (item) {
                //     if (item.attachmentLink.includes('srt')) {
                //         return item.attachmentLink
                //     }

                // })
                // let submitLink = data.finalProjectSubmit.map(function(item){
                //     if (item.includes('srt')) {
                //         return item
                //     }
                // })
                // $scope.SubmitSrt = attachment.concat(submitLink)
                // // console.log($scope.SubmitSrt)
                // $scope.$apply()
                // $scope.$watch('aModel', function(newValue, oldValue) {
                //     //update the DOM with newValue
                //   });

                // $scope.$watch(function() {
                //     $scope.SubmitSrt = attachment.concat(submitLink)
                // });
            }

            
            $scope.userCheckForInvoice = () => {
                if ($rootScope.currentUser.data.user.role == 'writer') {
                    $scope.userConditionWriter = true;
                    $scope.invoiceCheck = false;
                }
                if ($rootScope.currentUser.data.user.role == 'project_admin' || $rootScope.currentUser.data.user.role == 'super_user') {
                    $scope.userConditionWriter = true;
                    $scope.invoiceCheck = true;
                }

            }
            $scope.userCheckForInvoice(); //check usertype
            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };
            $scope.popup1 = {
                opened: false
            };
            $scope.creds = {
                bucket: 'post-cap-projects-phase-2',
                access_key: 'AKIAJ3RYIKD5JYFSYGWQ',
                secret_key: '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'
            }

            // $scope.convertVideo = []
            $scope.initProjectById = function () {
                var id = $stateParams.id;
                dataService.getProjectById({
                    id: $stateParams.id
                }, function (response) {
                    // 
                    if (response.data.job.appliedUser.length > 0 && response.data.job.appliedUser.includes($rootScope.currentUser.data.user.id)) {
                        response.data.Applied = true
                    } else {
                        response.data.Applied = false
                    }
                    $scope.productData = response;

                });
            }



            $("#testClick").click(function () {
                dataService.getAppliedUser({
                    id: $scope.productData.data.job._id
                }, function (response) {
                    $scope.appliedNames = response.data;
                    $('#appliedUser').modal('show');
                })
            });




            // upload data
            $scope.loadProgressBar = '0';

            $scope.attachUploadData = function () {
                AWS.config.update({
                    accessKeyId: $scope.creds.access_key,
                    secretAccessKey: $scope.creds.secret_key
                });
                AWS.config.region = 'us-east-2';
                var bucket = new AWS.S3({
                    params: {
                        Bucket: $scope.creds.bucket
                    }
                });
                var params = {}

                var filedata = {}
                // var dataIndex;
                if ($scope.file == null) {
                    toaster.pop('error', "No File Selected");

                } else {
                    angular.forEach($scope.file, function (item, index) {
                        // console.log(item);
                        params.Key = item.name;
                        params.ContentType = item.type;
                        params.Body = item;
                        params.ServerSideEncryption = 'AES256';
                        filedata.data = item;
                        // dataIndex = 'project' + (index + 1).toString()
                    })

                    bucket.putObject(params, function (err, data) {
                        if (err) {
                            // There Was An Error With Your S3 Config
                            console.log(err);
                            return false;
                        } else {
                            var url = "https://s3-us-east-2.amazonaws.com/post-cap-projects-phase-2/";
                            var fileName = filedata.data.name.trim().replace(/ /g, "_");
                            $scope.completeFinalUrl = url + fileName;
                            console.log('final', $scope.completeUrl);

                            var postData = {
                                attachmentLink: $scope.completeFinalUrl,
                                userId: $rootScope.currentUser.data.user.id,
                                projectId: $stateParams.id
                            }
                            //  api call
                            dataService.attachmentToProject(postData, function (response) {
                                // $scope.notification = response;
                                console.log(response);
                                $scope.initProjectById()
                            })

                        }
                    })
                        .on('httpUploadProgress', function (progress) {
                            $scope.loadProgressBar = Math.round(progress.loaded / progress.total * 100);
                            // console.log($scope.loadProgress);
                            $scope.$apply();

                        });

                }

            }
            $scope.progressBarFinalSubmit = '0';
            $scope.projectSubmitMessageSubmit = (message) => {
                AWS.config.update({
                    accessKeyId: $scope.creds.access_key,
                    secretAccessKey: $scope.creds.secret_key
                });
                AWS.config.region = 'us-east-2';
                var bucket = new AWS.S3({
                    params: {
                        Bucket: $scope.creds.bucket
                    }
                });
                var params = {}

                var filedata = {}
                if ($scope.file == null) {
                    toaster.pop('error', "No File Selected");
                } else {
                    angular.forEach($scope.file, function (item, index) {
                        params.Key = item.name;
                        params.ContentType = item.type;
                        params.Body = item;
                        params.ServerSideEncryption = 'AES256';
                        filedata.data = item;
                    })

                    bucket.putObject(params, function (err, data) {
                        if (err) {
                            // There Was An Error With Your S3 Config
                            console.log(err);
                            return false;
                        } else {
                            var url = "https://s3-us-east-2.amazonaws.com/post-cap-projects-phase-2/";
                            var fileName = filedata.data.name.replace(/ /g, "+");
                            $scope.finalSubmitLink = url + fileName;
                            console.log('final', $scope.finalSubmitLink);
                            var postData = {
                                message: message,
                                projectId: $stateParams.id,
                                finalProjectSubmit: $scope.finalSubmitLink
                            }
                            dataService.projectSubmitMessageSubmit(postData, function (response) {
                                $scope.finalSubmitLink = "";
                                $scope.message = ""
                                $scope.initProjectById()
                                $('#myModal').modal('hide');
                            })
                        }
                    })
                        .on('httpUploadProgress', function (progress, response) {
                            $scope.progressBarFinalSubmit = Math.round(progress.loaded / progress.total * 100);
                            $scope.$apply();
                            console.log('-------------->', response)
                        });

                }
            }

            $scope.unAssignWorkingUser = () => {
                dataService.unAssignWriter({
                    id: $stateParams.id
                }, function (response) {
                    if (!response.data.success) {
                        toaster.pop('error', response.data.message);
                    } else {
                        toaster.pop('success', response.data.message);
                        $scope.initProjectById()
                    }

                })
            }

            $scope.applyThisProject = () => {
                var postData = {
                    id: $stateParams.id,
                    userId: $rootScope.currentUser.data.user.id
                }
                dataService.jobApplyById(postData, function (response) {
                    $scope.initProjectById()
                    toaster.pop('success', 'Applied to the User.');

                })
            }

            $scope.updateOrderDue = (orderDue) => {
                let postData = {
                    orderDue: orderDue,
                    id: $stateParams.id
                }
                dataService.updateOrderDue(postData, function (response) {
                    if (!response.data.success) {
                        toaster.pop('error', response.data.message);
                    } else {
                        toaster.pop('success', response.data.message);
                        $scope.initProjectById()
                        $('#updateDue').modal('hide');
                    }

                })
            }

            $scope.attachmentAdd = (link) => {
                if (!link) return $scope.error = 'No link Provided'
                // return $scope.error = 'Not a valid Url.'
                isURL(link, function (result) {
                    console.log('result', result)
                    if (result) {
                        let postData = {
                            attachmentLink: link,
                            userId: $rootScope.currentUser.data.user.id,
                            projectId: $stateParams.id
                        }
                        dataService.attachmentToProject(postData, function (response) {
                            $scope.initProjectById()
                        })
                    } else {
                        $scope.error = 'Not a valid Url.'
                    }
                })

            }

            function isURL(str, cb) {
                var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
                cb(regex.test(str))
            }





            switch ($state.current.name) {
                case 'dashboard.project-profile-writer':
                    var hideProperties = ["alert", "changePayment", "assign", "apply", "request", "newProject", "orderStatus", "user", "dueDate", "status", "totalCost", "unassignedButton"];
                    hideDetails(hideProperties);
                    $scope.heading = "Sample Project #1 (Project Profile)";
                    break;

                case 'dashboard.projectProfileAdmin':
                    var hideProperties = ["writer", "apply", "request", "newProject", "submit", "report", "dueDate", "status", "invoice", "unassignedButton", "projectProfileAdmin"];
                    hideDetails(hideProperties);
                    $scope.heading = "Sample Project #1 (Project Profile)";
                    break;

                case 'dashboard.project-profile-user':
                    var hideProperties = ["writer", "apply", "assign", "report", "submit", "alert", "changePayment", , "client", "dueDate", "status", "invoice", "unassignedButton"];
                    hideDetails(hideProperties);
                    $scope.heading = "Sample Project #1 (Project Profile)";
                    break;
                case 'dashboard.jobBoardUnassigned':
                    var hideProperties = ["writer", "alert", "changePayment", "report", "lable1", "submit", "apply", "request", "newProject", "orderDue", "orderStatus", "dueDate", "invoice", "totalCost", "paymentStatus"];
                    hideDetails(hideProperties);
                    $scope.heading = "Unassigned (Project Profile)";
                    break;
                case 'dashboard.jobBoardProfileWirter':
                    var hideProperties = ["writer", "download", "submit", "assign", "alert", "changePayment", "report", "lable1", "request", "newProject", , "orderDue", "orderStatus", "invoice", "totalCost", "paymentStatus", "unassignedButton"];
                    hideDetails(hideProperties);
                    $scope.heading = "Job Board (Project Profile Writer)";
                    break;
                case 'dashboard.projectById':
                    var hideProperties = [];

                    if ($rootScope.currentUser.data.user.role == 'writer') {
                        hideProperties = ["apply", "writer", "assign", "alert", "changePayment", "request", "newProject", "orderDue", "orderStatus", "invoice", "totalCost", "paymentStatus", "unassignedButton"]
                        $scope.heading = "writer ( Writer)";
                    } else if ($rootScope.currentUser.data.user.role == 'project_admin') {
                        hideProperties = ["writer", "apply", "request", "newProject", "submit", "report", "dueDate", "status", "invoice", "unassignedButton", "projectProfileAdmin"];
                        $scope.heading = "project admin ( Project Admin)";
                    } else if ($rootScope.currentUser.data.user.role == 'client_admin') {
                        hideProperties = ["writer", "apply", "request", "newProject", "submit", "report", "dueDate", "status", "invoice", "unassignedButton", "projectProfileAdmin"];
                        $scope.heading = "Client Admin ( Client Admin)";
                    } else if ($rootScope.currentUser.data.user.role == 'super_user') {
                        hideProperties = ["writer", "apply", "request", "newProject", "submit", "report", "dueDate", "status", "invoice", "unassignedButton", "projectProfileAdmin"];
                        $scope.heading = "super user ( Super user)";
                    } else if ($rootScope.currentUser.data.user.role == 'client_user') {
                        hideProperties = ["writer", "apply", "submit", "report", "dueDate", "status", "invoice", "unassignedButton", "projectProfileAdmin", "assign", "alert", "changePayment"];
                        $scope.heading = "client user ( Client user)";
                    } else if ($rootScope.currentUser.data.user.role == 'project_user') {
                        hideProperties = ["writer", "apply", "submit", "report", "dueDate", "status", "invoice", "unassignedButton", "projectProfileAdmin", "assign", "alert", "changePayment"];
                        $scope.heading = "project user ( Project user)";
                    }
                    hideDetails(hideProperties);
                    break;
            }

			
			$scope.processLoader = false;
            $scope.sendtoEditor = function () {
				//console.log($('#chkWatson').is(':checked'));
                $scope.processLoader = true;
				dataService.sendObjectToEditor({
					objectname: $scope.productData.data.name,
					canallowwatson: $('#chkWatson').is(':checked')
				}, function (response) {
					$scope.processLoader = false;
					$('#SendEditor').modal('hide');
					//console.log(response);
					if(response.data.success || response.status == 502){							
						toaster.pop('success', "File has been transferred successfully!");
					}else{
						toaster.pop('error', "Something went wrong. Please try again");
					}
				});
            }			
			
			$scope.downloadAttachment = function (attachmentType) {				
				if(attachmentType == "srt"){
					window.location.href = "/api/downloadAttachment?filename=SRT_" + $scope.productData.data.name.replace(".mp4", "").replace(".mp3", "") + ".srt";
				}
				if(attachmentType == "vtt"){
					window.location.href = "/api/downloadAttachment?filename=VTT_" + $scope.productData.data.name.replace(".mp4", "").replace(".mp3", "") + ".vtt";
				}
				if(attachmentType == "sami"){
					window.location.href = "/api/downloadAttachment?filename=SAMI_" + $scope.productData.data.name.replace(".mp4", "").replace(".mp3", "") + ".smi";
				}
				if(attachmentType == "ttml"){
					window.location.href = "/api/downloadAttachment?filename=TTML_" + $scope.productData.data.name.replace(".mp4", "").replace(".mp3", "") + ".ttml";
				}
                var r = confirm("You are about to send the file to Caption Editor. Are you sure to proceed?");
                if (r == true) {
                    dataService.sendObjectToEditor({
                        objectname: "TESTINGPROJECTSYNC.mp4"
                    }, function (response) {
                        console.log(response);
                    });
                }
            }
			
			$scope.processLoader = false;
            $scope.UploadToEditor = function () {
				//console.log($scope.youtubeIn);
				console.log($('#chkWatson').is(':checked'));
				$scope.processLoader = true;
				dataService.UploadToEditor({
					hrefUrl: $scope.youtubeIn,
					canallowwatson: $('#chkWatson').is(':checked'),
					projectName: $scope.productData.data.name
				}, function (response) {
					$scope.processLoader = false;
					$('#SendEditor').modal('hide');
					//console.log(response);
					if(response.data.success){							
						toaster.pop('success', "File has been transferred successfully!");
					}else{
						toaster.pop('error', "Something went wrong. Please try again");
					}
				});
			}
        }]
    };

});

app.filter('projectName', function () {
    return function (input) {
        if (input.includes('/post-cap-projects-phase-2/')) {
            var splittedString = input.split('/post-cap-projects-phase-2/')
            return splittedString[1];
        } else {
            return input
        }
    }

});
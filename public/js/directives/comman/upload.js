app.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function fn_link(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                    scope.$apply(function() {
                        modelSetter(scope, element[0].files[0]);
                    })
                })
                // // var onChange = $parse(attrs.ngFiles);
                // element.on('change', function(event) {
                //     onChange(scope, { $files: event.target.files });
                // });
        };
    }
    // function fn_link(scope, element, attrs) {
    //     var onChange = $parse(attrs.ngFiles);
    //     element.on('change', function(event) {
    //         onChange(scope, { $files: event.target.files });
    //     });
    // };

    // return {
    //     link: fn_link
    // }
    // // }

}]);
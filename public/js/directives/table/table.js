app.directive('tableDirective', function () {
    function writerFunction($scope, element, attributes) {
        $scope.changeText = function (newText) {
            $scope.text = newText;
        }
    }
    return {
        link: writerFunction,
        templateUrl: 'js/directives/table/table.html',
        restrict: 'A',
        controller: ['$scope', '$state', 'userService', 'toaster', 'getService', '$rootScope', 'Upload', 'dataService', '_', 'getService', function ($scope, $state, userService, toaster, getService, $rootScope, Upload, dataService, _, getService) {
            console.log('========>', $state.current.name); // state1
            $scope.currentState = $state.current.name
            $scope.properties = {};
            var allProperties = ["clientPanal", "project", "users", "null", "drop", "projectId", "projectName", "service",
                "duration", "dueDate", "status", "orderPlaced", "orderDue", "client",
                "firstName", "LastName", "department",
                "usertype", "email", "phone",
                "organization", "title", "skype",
                "technical", "invoice", "Apply",
                "newButton", "deleteButton", "applyButton",
                "downloadButton", "labelButton", "favouriteButton", "filtersButton", "filtersProjectButton", "projectUserModalButton", "clientUserModalButton", "writerPrice", "login"
            ];
            $scope.loader = false
            $scope.param = $state.current.name
            if ($state.current.name != 'dashboard.projectUser') {
                $scope.currentPage = 1;
            }
            $scope.filterData = [20,50,100]

           




           
            checkForSuperUser = function (data, callback) {

                data.forEach(function (element) {
                    if (element.role.name == 'super_user') {
                        element.superCheck = true
                    }
                })
                callback(data)
            }


            if (localStorage.getItem('filter') == null) {
                $scope.maxSize = getService.maxSize;
            } else {
                $scope.maxSize = parseInt(localStorage.getItem('filter'))
            }
    
    
            const setValueFilterLocalStorage = (data) => {
                localStorage.setItem("filter", data);
                return parseInt(localStorage.getItem('filter'))
            }

            $scope.setValue = function (data) {
                $scope.maxSize = setValueFilterLocalStorage(data)
                if ($state.current.name === `dashboard.jobBoard`) {
                    getAllProjectForJob()
                }else if($state.current.name === `dashboard.projectUser`){
                    getUserList()
                }
            }

            function getUserList() {
                var role = {
                    skip: $scope.currentPage,
                    limit: $scope.maxSize
                }
                $scope.loader = true
                getService.getUsers(role, function (response) {
                    $scope.loader = false
                    checkForSuperUser(response.data, function (data) {
                        $scope.uc.users = data;
                    })
                })
            }


           
            $scope.totalItems = getService.totalItems;
            function checkuser() {
                if ($rootScope.currentUser.data.user.role == 'project_admin' || $rootScope.currentUser.data.user.role == 'super_user') {
                    $scope.modalId = '#viewEditSuperUserAndPrjectAdmin';
                    $scope.modal2Id = '#myModal2Filter';

                } else {
                    $scope.modalId = '#editModalClientUser';
                    $scope.modal2Id = '#myModalFilter';
                }
            }
            checkuser();

            const getAllProjectForJob = () => {
                $scope.loader = true
                dataService.getProjects({
                    skip: $scope.currentPage,
                    limit: $scope.maxSize
                }, function (response) {
                    $scope.loader = false
                    if ($rootScope.currentUser.data.user.role == 'writer') {

                        dataService.getUserPriceTable({
                            id: $rootScope.currentUser.data.user.id
                        }, (customRates) => {
                            if (customRates.data.message === 'No user') {
                            } else {
                                // if(!customRates.data.message.transcription || !customRates.data.message.transcriptionAlignment) {
                                //     console.log('Normal flow')
                                // }
                                if (customRates.data.message.transcription || customRates.data.message.transcriptionAlignment) {
                                    let test = convertToCustomWriterPrice(response.data, customRates.data.message, 'transcription')
                                }
                            }


                            let temp = response.data;
                            $scope.uc.projects = []
                            if ($rootScope.currentUser.data.transcriber && !$rootScope.currentUser.data.transcriptionAlignment) {
                                temp.forEach((item) => {
                                    if (item.service.name == 'transcription') {
                                        $scope.uc.projects.push(item)
                                    }
                                })
                            } else if (!$rootScope.currentUser.data.transcriber && $rootScope.currentUser.data.transcriptionAlignment) {
                                temp.forEach((item) => {
                                    if (item.service.name == 'transcription-alignment') {
                                        $scope.uc.projects.push(item)
                                    }
                                })
                            } else if ($rootScope.currentUser.data.transcriber && $rootScope.currentUser.data.transcriptionAlignment) {
                                temp.forEach((item) => {
                                    if (item.service.name == 'transcription-alignment' || item.service.name == 'transcription') {
                                        $scope.uc.projects.push(item)
                                    }
                                })
                            }

                        })
                    } else {
                        $scope.uc.projects = response.data;
                    }
                });
            }

            getAllProjectForJob();

            if ($state.current.name != 'dashboard.projectUser') {
                $scope.pageChanged = function () {
                    getAllProjectForJob();
                };
            }



            const convertToCustomWriterPrice = async (projects, customPrice, type) => {
                let calculatedData = await projects.map(function (item) {
                    let serviceSelected = {}

                    if (item.selectedService.turnaroundSelected) serviceSelected.turnAround = JSON.parse(item.selectedService.turnaroundSelected)

                    if (item.selectedService.techniqueSelected) serviceSelected.techniqueSelected = JSON.parse(item.selectedService.techniqueSelected)

                    if (item.selectedService.accentSelected && item.service.name == 'transcription-alignment') serviceSelected.accentSelected = JSON.parse(item.selectedService.accentSelected)

                    if (customPrice.transcription.writerPrice && item.service.name == 'transcription') {
                        customPrice.transcription.price = customPrice.transcription.writerPrice;

                        serviceSelected.serviceCharge = customPrice.transcription
                    } else if (customPrice.transcriptionAlignment.writerPrice && item.service.name == 'transcription-alignment') {
                        customPrice.transcriptionAlignment.price = customPrice.transcriptionAlignment.writerPrice;

                        serviceSelected.serviceCharge = customPrice.transcriptionAlignment
                    }

                    let writerPrice = 0.00
                    angular.forEach(serviceSelected, function (value, key) {
                        let val = parseFloat(value.price.replace(/[^0-9\.]+/g, ""))
                        var price = val * parseInt(item.duration);
                        writerPrice = writerPrice + price;
                        item.writerPrice[item.writerPrice.length - 1] = writerPrice.toFixed(2);
                    });
                    return item;
                })
                return calculatedData
            }

            $scope.applyJob = function (id, type) {
                if (type == 'apply') {
                    var postData = {
                        id: id,
                        userId: $rootScope.currentUser.data.user.id
                    }
                    dataService.jobApplyById(postData, function (response) {
                        getAllProjectForJob();
                    });
                } else if (type = 'unapply') {
                    dataService.unapplyFromJob({
                        id: id
                    }, function (response) {
                        getAllProjectForJob();
                    });
                }

            }

            var serviceArray = [];
            $scope.userTypeArray = [];

            allProperties.forEach(function (item) {
                $scope.properties[item] = true;
            });

            $scope.getUserById = function (id) {
                userService.getUserById(id, function (response) {
                    $scope.userData = response.data;
                });
            }



            $scope.mail_notifications = [
                {
                    "key": "weekly",
                    "value": "Weekly"
                },
                {
                    "key": "byWeekly",
                    "value": "ByWeekly"
                },
                {
                    "key": "monthly",
                    "value": "Monthly"
                }
            ];

            $scope.superAndprojectAdmin = false
            var checkuserRole = function () {
                if ($rootScope.currentUser.data.user.role == 'super_user' || $rootScope.currentUser.data.user.role == 'project_admin') {
                    $scope.superAndprojectAdmin = true
                }
            }
            checkuserRole()

            $scope.submit = function (filedata) {
                Upload.upload({
                    url: 'api/uploadtoAmazon', //webAPI exposed to upload the file
                    data: {
                        file: filedata
                    } //pass file as data, should be user ng-model
                }).then(function (resp) { //upload function returns a promise
                    console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    $scope.userData.profile_picture = resp.data;

                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            };



            $scope.getUsers = () => {
                var role = {
                    roleType: $rootScope.currentUser.data.user.role
                }
                getService.getUsers(role, function (responseData) {
                    $scope.uc.users = responseData.data
                })
            }



            $scope.editUser = function (userDetail) {

                userDetail.id = userDetail._id;
                userService.updateUser(userDetail, function (response) {
                    var role = {
                        roleType: $rootScope.currentUser.data.user.role
                    }
                    getService.getUsers(role, function (responseData) {
                       
                        $scope.uc.users = responseData.data
                        $('#editModalClientUser').modal('hide');
                        $('#viewEditSuperUserAndPrjectAdmin').modal('hide');
                    })
                });
            }


            $scope.showProjectById = function (id) {
                $state.go('dashboard.projectById', {
                    id: id
                });
            }

            /*=============filterUser===========*/

            $scope.allOptions = [{
                "key": "false",
                "data": "Client User",
                "value": "client_user"
            },
            {
                "key": "false",
                "data": "Client Admin",
                "value": "client_admin"
            }
            ];

            $scope.allOptionsProjectAdmin = [{
                "key": "false",
                "data": "Client User",
                "value": "client_user"
            },
            {
                "key": "false",
                "data": "Client Admin",
                "value": "client_admin"
            },
            {
                "key": "false",
                "data": "Writer",
                "value": "writer"
            },
            {
                "key": "false",
                "data": "Project Admin",
                "value": "project_admin"
            }
            ];

            $scope.clearFilter = () => {
                $scope.userTypeArray = []
                $scope.allOptionsProjectAdmin.forEach(function (item) {
                    item.key = false
                })
                $scope.allService.forEach(function (item) {
                    item.key = false
                })
                $scope.date = {}
                $scope.getUsers();
            }

            $scope.allStatus = [{
                "key": true,
                "data": "Unassigned",
                "value": "Unassigned"
            }];

            $scope.allService = [{
                "key": "false",
                "data": "Transcription",
                "value": "transcription"
            },
            {
                "key": "false",
                "data": "Captioning",
                "value": "captioning"
            },
            {
                "key": "false",
                "data": "Translation",
                "value": "translation"
            },
            {
                "key": "false",
                "data": "Transcription Alignment",
                "value": "transcription-alignment"
            }
            ];




            $scope.selectServiceTypeForProject = function (check) {
                if (check.key) {
                    serviceArray.push(check.value);
                } else {
                    var index = serviceArray.indexOf(check);
                    serviceArray.splice(index);
                }
            }




            $scope.selectUserTypeForFilter = function (check) {
                if (check.key) {
                    $scope.userTypeArray.push(check.value);
                } else {
                    var index = userTypeArray.indexOf(check);
                    $scope.userTypeArray.splice(index);
                }

            }

            $scope.ApplyFilter = () => {
                var postData = {
                    userTypeData: $scope.userTypeArray
                }
                dataService.getUserAccordingUserType(postData, function (response) {
                    $scope.uc.users = response.data
                    $scope.userTypeArray = []
                    $('#myModalFilter').modal('hide');
                    $('#myModal2Filter').modal('hide');
                })
            }
            $scope.disableButton = false
            $scope.resendPassword = (id) => {
                $scope.disableButton = true;
                dataService.resendPassword({ id }, function (response) {
                    if (response.data.success) {
                        $scope.disableButton = false;
                        toaster.pop('success', response.data.message)
                    } else {
                        toaster.pop('error', response.data.message)
                    }
                })
            }
            console.log('===', $state)
            $scope.applyProjectFilter = (date) => {
                if (!date) {
                    date = {}
                }
                var postData = {
                    projectStatus: $scope.userTypeArray || '',
                    serviceType: serviceArray || '',
                    startDate: date.startDate || '',
                    lastDate: date.endDate || '',
                    skip: $scope.currentPage,
                    limit: $scope.maxSize
                }
                dataService.getProjectAccordingProjectTypeJobBoard(postData, function (response) {
                    $scope.uc.projects = response.data
                    $('#myModalProjFilter').modal('hide');
                })
            }

            /*=======select and delete user=========*/

            $scope.data = {
                ids: []
            };



            $scope.checkUser = function (check, id) {
                if (check) {
                    $scope.data.ids.push(id);
                } else {
                    var index = data.ids.indexOf(id);
                    $scope.data.ids.splice(index);
                }
            }

            $scope.accending = true;
            $scope.messageClass = 'fa fa-angle-double-up'

            $scope.setSort = function (projectData, type) {
                if ($scope.accending) {
                    sortByDate(projectData, type);

                } else {
                    accending(projectData, type);
                }

            }


            $scope.accendingByDate = function (project, type) {
                accending(project, type);
            }


            function sortByDate(arr, type) {

                if (type == 'totalPrice') {
                    arr.sort(function (a, b) {
                        return (b.totalPrice) - (a.totalPrice);
                    });
                }

                if (type == 'projectName') {

                    arr.sort(function (a, b) {
                        return a.name < b.name ? -1 : 1
                    })
                }

                if (type == 'service') {

                    arr.sort(function (a, b) {
                        return a.service < b.service ? -1 : 1
                    })
                }

                if (type == 'writerPrice') {
                    arr.sort(function (a, b) {
                        return (b.writerPrice) - (a.writerPrice);
                    });
                }
                if (type == 'order') {
                    arr.sort(function (a, b) {
                        return Number(new Date(b.createdAt)) - Number(new Date(a.createdAt));
                    });
                }
                if (type == 'totalPrice') {
                    arr.sort(function (a, b) {
                        return (b.totalPrice) - (a.totalPrice);
                    });
                }

                if (type == 'duration') {
                    arr.sort(function (a, b) {
                        return (b.duration) - (a.duration);
                    });
                }



                if (type == 'firsname') {
                    arr.sort(function (a, b) {
                        return a.firstname < b.firstname ? -1 : 1
                    })
                }

                if (type == 'lastname') {
                    arr.sort(function (a, b) {
                        return a.lastname < b.lastname ? -1 : 1
                    })
                }

                if (type == 'userType') {
                    arr.sort(function (a, b) {
                        return a.role.name < b.role.name ? -1 : 1
                    })
                }

                if (type == 'email') {
                    arr.sort(function (a, b) {
                        return a.email < b.email ? -1 : 1
                    })
                }

                if (type == 'phone') {
                    arr.sort(function (a, b) {
                        return a.phone < b.phone ? -1 : 1
                    })
                }

                if (type == 'organization') {
                    arr.sort(function (a, b) {
                        return a.organization < b.organization ? -1 : 1
                    })
                }

                if (type == 'title') {
                    arr.sort(function (a, b) {
                        return a.title < b.title ? -1 : 1
                    })
                }

                $scope.accending = false;
                $scope.messageClass = 'fa fa-angle-double-down'

                if (uc.projects) {
                    uc.projects = arr;
                } else {
                    $scope.uc.users = arr
                }

            }

            function accending(arr, type) {

                if (type == 'totalPrice') {
                    arr.sort(function (a, b) {
                        return (a.totalPrice) - (b.totalPrice);
                    });
                }

                if (type == 'projectName') {
                    arr.sort(function (a, b) {
                        return a.name > b.name ? -1 : 1
                    })
                }

                if (type == 'service') {
                    arr.sort(function (a, b) {
                        return a.service > b.service ? -1 : 1
                    })
                }

                if (type == 'totalPrice') {
                    arr.sort(function (a, b) {
                        return (a.totalPrice) - (b.totalPrice);
                    });
                }

                if (type == 'writerPrice') {
                    arr.sort(function (a, b) {
                        return (a.writerPrice) - (b.writerPrice);
                    });
                }

                if (type == 'order') {
                    arr.sort(function (a, b) {
                        return Number(new Date(a.createdAt)) - Number(new Date(b.createdAt));
                    });
                }

                if (type == 'duration') {
                    arr.sort(function (a, b) {
                        return (a.duration) - (b.duration);
                    });
                }

                if (type == 'firsname') {
                    arr.sort(function (a, b) {
                        return a.firstname > b.firstname ? -1 : 1
                    })
                }

                if (type == 'lastname') {
                    arr.sort(function (a, b) {
                        return a.lastname > b.lastname ? -1 : 1
                    })
                }

                if (type == 'userType') {
                    arr.sort(function (a, b) {
                        return a.role.name > b.role.name ? -1 : 1
                    })
                }

                if (type == 'email') {
                    arr.sort(function (a, b) {
                        return a.email > b.email ? -1 : 1
                    })
                }

                if (type == 'phone') {
                    arr.sort(function (a, b) {
                        return a.phone > b.phone ? -1 : 1
                    })
                }

                if (type == 'organization') {
                    arr.sort(function (a, b) {
                        return a.organization > b.organization ? -1 : 1
                    })
                }

                if (type == 'title') {
                    arr.sort(function (a, b) {
                        return a.title > b.title ? -1 : 1
                    })
                }

                $scope.accending = true;
                $scope.messageClass = 'fa fa-angle-double-up'
                if (uc.projects) {
                    uc.projects = arr;
                } else {
                    $scope.uc.users = arr
                }
            }



            $scope.delete = function () {
                if ($scope.data.ids.length > 0) {
                    console.log('DATA TO DELETE', $scope.data.ids.indexOf($rootScope.currentUser.data.user.id));
                    if ($scope.data.ids.indexOf($rootScope.currentUser.data.user.id) == -1) {
                        userService[method]($scope.data, function (res) {

                            var message = res.data.message;
                            var role = {
                                roleType: $rootScope.currentUser.data.user.role
                            }
                            getService.getUsers(role, function (responseData) {
                                $scope.uc.users = responseData.data
                                if (!res.data.message) {
                                    message = 'no user selected';
                                    toaster.pop('error', module, message)
                                } else {
                                    toaster.pop('success', res.data.message)
                                }
                            })
                        });
                    } else {
                        toaster.pop('error', module, 'cannot delete yourself');
                    }
                } else {
                    toaster.pop('error', module, 'no user selected');
                }

            }



            /*=======select and delete user=========*/


            var hideDetails = function (hide) {
                hide.forEach(function (item) {
                    $scope.properties[item] = false;
                });
            }
            $scope.modals = {
                client: '#myModalClientUser',
                project: '#myModalProjectUser'
            }
            switch ($state.current.name) {
                case 'dashboard.writer':
                    var hideProperties = ["users", "drop", "orderPlaced", "orderDue", "client",
                        "firstName", "LastName", "department",
                        "usertype", "email", "phone", "organization",
                        "title", "skype", "technical",
                        "Apply", "newButton", "deleteButton", "applyButton", "projectUserModalButton", "clientUserModalButton", "filtersButton"
                    ];
                    hideDetails(hideProperties);
                    break;

                case 'dashboard.clientUser':
                    var hideProperties = ["project", "downloadButton", "drop", "email", "service", "duration", "dueDate", "status", "projectName", "orderPlaced", "orderDue", "client",
                        "title", "skype", "technical", "invoice", "organization",
                        "Apply", "deleteButton", "applyButton", "projectUserModalButton", "filtersProjectButton", "writerPrice"
                    ];
                    hideDetails(hideProperties);
                    break;

                case 'dashboard.clientAdmin':
                    var hideProperties = ["users", "drop", "usertype", "email", "phone",
                        "organization", "title", "skype",
                        "technical", "invoice", "Apply", "firstName", "LastName", "department", "client", "service", "email", "service", "dueDate",
                        "newButton", "deleteButton", "applyButton", "projectUserModalButton", "filtersButton", "writerPrice"
                    ];
                    hideDetails(hideProperties);
                    var modal = '#myModalClientUser';
                    break;

                case 'dashboard.projectAdmin':
                    var hideProperties = ["clientPanal", "users", "drop", "usertype", "email", "phone",
                        "technical", "invoice", "Apply", "firstName", "LastName", "department", "email", "dueDate",
                        "title", "skype", "technical", "invoice", "organization", "filtersButton",
                        "Apply", "newButton", "deleteButton", "applyButton", "projectUserModalButton", "clientUserModalButton", "favouriteButton"
                    ];
                    hideDetails(hideProperties);
                    var modal = '#myModalProjectUser';
                    break;

                case 'dashboard.lable1':
                    var hideProperties = ["users", "drop", "filtersButton", "usertype", "email", "phone",
                        "organization", "title", "skype",
                        "technical", "invoice", "Apply", "firstName", "LastName", "department", "client", "service", "email", "service", "dueDate", "client",
                        "title", "skype", "technical", "invoice", "organization",
                        "Apply", "newButton", "deleteButton", "applyButton", "projectUserModalButton"
                    ];
                    hideDetails(hideProperties);
                    break;

                case 'dashboard.lable2':
                    var hideProperties = ["users", "drop", "filtersButton", "usertype", "email", "phone",
                        "organization", "title", "skype",
                        "technical", "invoice", "Apply", "firstName", "LastName", "department", "client", "service", "email", "service", "dueDate", "client",
                        "title", "skype", "technical", "invoice", "organization",
                        "Apply", "newButton", "deleteButton", "applyButton", "projectUserModalButton", "clientUserModalButton"
                    ];
                    hideDetails(hideProperties);
                    break;
                case 'dashboard.lable3':
                    var hideProperties = ["users", "drop", "filtersButton", "usertype", "email", "phone",
                        "organization", "title", "skype",
                        "technical", "invoice", "Apply", "firstName", "LastName", "department", "client", "service", "email", "service", "dueDate", "client",
                        "skype", "technical", "invoice",
                        "Apply", "newButton", "deleteButton", "applyButton", "projectUserModalButton", "clientUserModalButton"
                    ];
                    hideDetails(hideProperties);
                    break;
                case 'dashboard.lable4':
                    var hideProperties = ["users", "drop", "filtersButton", "usertype", "email", "phone",
                        "organization", "title", "skype",
                        "technical", "invoice", "Apply", "firstName", "LastName", "department", "client", "service", "email", "service", "dueDate", "client",
                        "title", "skype", "technical", "invoice", "organization",
                        "Apply", "newButton", "deleteButton", "applyButton", "projectUserModalButton", "clientUserModalButton"
                    ];
                    hideDetails(hideProperties);
                    break;

                case 'dashboard.projectUser':
                    var hideProperties = ["project", "service", "status", "dueDate", "duration", "projectName", "drop", "orderPlaced", "orderDue", "client",
                        "department", "technical", "invoice",
                        "Apply", "downloadButton", "labelButton", "favouriteButton", "applyButton", "clientUserModalButton", "filtersProjectButton", "writerPrice"
                    ];
                    hideDetails(hideProperties);
                    var method = 'deleteUser';
                    var module = 'User';
                    var modal = '#myModalProjectUser';

                    break;

                case 'dashboard.jobBoard':
                    var hideProperties = ["users", "drop", "filtersButton", "title", "skype", "email", "client", "firstName", "LastName",
                        "phone", "organization", "department", "dueDate", "status",
                        "newButton", "deleteButton", "applyButton", "projectUserModalButton", "clientUserModalButton", "usertype", "Update", "login",
                    ];
                    hideDetails(hideProperties);
                    break;


            }


            $scope.userCheck = () => {
                if ($rootScope.currentUser.data.user.role == 'writer') {
                    $scope.userCondition = true;
                    $scope.invoice = false;
                }
                if ($rootScope.currentUser.data.user.role == 'project_admin' || $rootScope.currentUser.data.user.role == 'super_user') {
                    $scope.userCondition = true;
                    $scope.invoice = true;
                }

            }
            $scope.userCheck(); //check usertype



            $scope.selectUserForm = function (selectedRole) {
                var forms = ['project_admin', 'writer', 'client_admin', 'client_user', 'project_user', 'super_user']
                forms.forEach(function (item) {
                    $scope[item] = false;
                });
                $scope[JSON.parse(selectedRole).name] = true;
            };

        }]


    };
});
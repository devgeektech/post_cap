app.directive('mailDirective', function () {
    function mailFunction($scope, element, attributes) {
        $scope.changeText = function (newText) {
            $scope.text = newText;
        }
    }
    return {
        link: mailFunction,
        templateUrl: 'js/directives/mailView/viewMail.html',
        restrict: 'A',
        controller: ['$scope', '$state', '$stateParams', 'dataService', '$rootScope', function ($scope, $state, $stateParams, dataService, $rootScope) {
            console.log($state.current.name); // state1
            $scope.paramsData = $stateParams;

            $scope.oneAtATime = true;

            $scope.groups = [
                {
                    title: 'Dynamic Group Header - 1',
                    content: 'Dynamic Group Body - 1'
                },
                {
                    title: 'Dynamic Group Header - 2',
                    content: 'Dynamic Group Body - 2'
                }
            ];

            $scope.items = ['Item 1', 'Item 2', 'Item 3'];

            $scope.addItem = function () {
                var newItemNo = $scope.items.length + 1;
                $scope.items.push('Item ' + newItemNo);
            };

            $scope.status = {
                isCustomHeaderOpen: false,
                isFirstOpen: true,
                isFirstDisabled: false
            };


            switch ($state.current.name) {
                case 'dashboard.mailDetail':
                    // var hideProperties = ["users", {sent : true}
                    // ];
                    $scope.inbox = true;

                    // hideDetails(hideProperties);
                    break;

                case 'dashboard.sendMailDetail':
                    $scope.inbox = false;
                    break;
            }


        }]
    };

});
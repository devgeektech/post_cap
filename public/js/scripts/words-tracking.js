(function () {

    var index = 0;
    var words = [];
    var currentWord;
    var wordsTimer;
    var timerStatus = false;

    // init jquery plugin
    $.fn.wordsTracking = function (userOptions) {

        // set defult options
        var options = {};

        // override default options by user options
        options = $.extend(options, userOptions);

        // set local element object
        var element = $(this);
		$('#' + options.controlButtonId).css({ color: 'green' });
		
        // control button.
        $('#' + options.controlButtonId).change(function () {
           // if($(this).is(':checked')){
				// alert("yes");
			// } else {
				// alert("no");
			// }
	
            timerStatus = !timerStatus;

            if (timerStatus) {
                
                // style the timer control button
                $(this).css({
                    color: '#f00'
                });
                
                // clear
                words = [];
                currentWord = '';
                
                // set start time.
                wordsTimer = Date.now();
                // start tracking user input.
                //$(element).on('input', trackWords);
				document.getElementById("able_transcript_edit").addEventListener("input", trackWords);
            }
            else {
                
                // style the timer control button
                $(this).css({
                    color: 'green'
                });
                
                // stop tracking user input
                //$(element).off('input', trackWords);
				document.getElementById("able_transcript_edit").removeEventListener("input", trackWords);
                stopWordsTracking();
            }
            
        });

        // return element object for seek of Chaining
        return element;

    };

    // track words in user input and timestamp it.
    function trackWords () {
		
        var input = $(this).text();                                // full input
        var lastChar = input.charAt(input.length - 2);            // last user input
        var currentChar = input.charAt(input.length - 1);         // current user input

        // if current user input not empty or white space
        if (currentChar.length > 0 && currentChar !== ' ') {

            // case 1 : last user input was nothing or white space
            if (lastChar === '' || lastChar === ' ') {
                // make sure there's a current tracked word
                if (currentWord) {
                    // append End Time
                    currentWord.end = getTimerTime();
                    // append the last word to the object
                    currentWord.text = input.slice(index, input.length).split(' ')[0];
                    // push to words array
                    if (currentWord.text.length > 0 && currentWord.text !== ' '){
                        words.push(currentWord);
                    }

                    //updateResults(words); // -------- DEBUG

                    // create a new object to store the next word
                    currentWord = new TimedWord(getTimerTime());
                    index = input.length - 1;
                }
                // if no tracked words.
                else {
                    currentWord = new TimedWord(getTimerTime());
                    index = input.length - 1;
                }
            }

        }

    }

    // finish words tracking.
    function stopWordsTracking () {
        var inputBox = $("#able_transcript_edit").text();
        if (currentWord) {
            // append End Time
            currentWord.end = getTimerTime();
            // append the last word to the object
            currentWord.text = inputBox.slice(index, inputBox.length).trim();
            // push to words array
            if (currentWord.text.length > 0 && currentWord.text !== ' '){
                words.push(currentWord);
            }

            // remove any duplicates 
            var inputAsWords = inputBox.split(' ');
            var results = [];
            for (var i = 0; i < words.length; i++) {
                if (inputAsWords.indexOf(words[i].text) !== -1){
                    results.push(words[i]);
                }
            }
            words = results;
            
            var jsonFile = formatResults();
			customObject = jsonFile;
            $("#StartStopTimer").val("json");
			
            // download the ressults in json format
            //var blob = new Blob([JSON.stringify(jsonFile)], {type: "text/plain;charset=utf-8"});
            //saveAs(blob, "words-tracking-results.json");
            
            //updateResults(words); // -------- DEBUG

        }
    }
    
    function formatResults () {
        var json = {
            "results": [
                {
                    "word_alternatives": [],
                    "keywords_result": {},
                    "alternatives": [],
                    "final": true
                }
            ],
            "result_index": 0
        }; 
        
        // keywords result
        for (var i = 0; i < words.length; i++) {
            var normalize = words[i].text.toLowerCase();
            
            var wordObject = {
                "normalized_text": words[i].text,
                "start_time": words[i].start,
                "confidence": 1,
                "end_time": words[i].end
            };
            
            if (Array.isArray(json.results[0].keywords_result[normalize])) {
                json.results[0].keywords_result[normalize].push(wordObject);
            }
            else {
                json.results[0].keywords_result[normalize] = [wordObject];
            }
        }
        return json;
    }

    // get timestamp from the current working timer.
    function getTimerTime () {

        // get total elapsed seconds.
        var totalMilliseconds = Date.now() - wordsTimer;

        var hours = Math.floor(totalMilliseconds / (60 * 60 * 1000));
        var minutes = Math.floor( totalMilliseconds / (60 * 1000) % 60 );
        var seconds = Math.floor(( totalMilliseconds / 1000 ) % 60);
        var millis = totalMilliseconds % 1000;

        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        if (millis < 10) { millis = "0" + millis; }

        return hours + ':' + minutes + ':' + seconds + ':' + millis;

    }

    // Word Object.
    function TimedWord(start) {
        this.start = start || null;
    }

    // for debuging only
    function updateResults (words) {
        console.log(words);
        $('#results .words span').text(words.length);
    }

})( jQuery );
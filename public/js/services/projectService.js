var app = angular.module('app')
app.service('projectService',['ajaxService', function(ajaxService) {
    this.get = function(callback) {
        ajaxService.get(null, '/api/project', callback);
    }
}]);
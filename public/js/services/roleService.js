var app = angular.module('app')
app.service('getService', ['ajaxService', function(ajaxService) {
    this.getRoles = function(callback) {
        ajaxService.get(null, '/api/role', callback);
    };
    this.getOrganizations = function(callback) {
        ajaxService.get(null, '/api/organization', callback);
    };
    this.getUsers = function(data, callback) {
        ajaxService.get(null, '/api/user?skip=' + data.skip +`&limit=${data.limit}`, callback);
    }
    // for Pagination
    this.maxSize = 5;
    this.totalItems = 100;

}]);
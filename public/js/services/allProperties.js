var app = angular.module('app')
app.service('getProperties', ['$scope', function ($scope) {
    this.allProperties = ["projectId", "projectName", "service",
        "duration", "dueDate", "status",
        "orderPlaced", "orderDue", "client",
        "firstName", "LastName", "department",
        "usertype", "email", "phone",
        "Orgenization", "title", "skype",
        "technical", "invoice", "Apply",
        "newButton", "deleteButton", "applyButton",
        "downloadButton", "labelButton", "favouriteButton"];

    this.minutes = [
        {
            key: '1',
            value: 1
        },
        {
            key: '2',
            value: 2
        },
        {
            key: '3',
            value: 3
        },
        {
            key: '4',
            value: 4
        }, {
            key: '5',
            value: 5
        },
        {
            key: '6',
            value: 6
        },
        {
            key: '7',
            value: 7
        },
        {
            key: '8',
            value: 8
        }, {
            key: '9',
            value: 9
        }, {
            key: '10',
            value: 10
        },
    ]

    this.cents = [
        {
            key: '00',
            value: 00
        },
        {
            key: '05',
            value: 05
        },
        {
            key: '10',
            value: 10
        },
        {
            key: '15',
            value: 15
        }, {
            key: '20',
            value: 20
        },
        {
            key: '25',
            value: 25
        },
        {
            key: '30',
            value: 33
        },
        {
            key: '40',
            value: 40
        }, {
            key: '50',
            value: 50
        }, {
            key: '60',
            value: 60
        },
        {
            key: '70',
            value: 70
        },
        {
            key: '80',
            value: 80
        },
        {
            key: '90',
            value: 90
        }
    ]

}]);
// app.service('fileUpload', ['$http', '$rootScope', function($http, $rootScope) {
//     this.uploadFileToUrl = function(file, uploadUrl, callback) {
//         var fd = new FormData();
//         fd.append('file', file);
//         fd.append('user', $rootScope.currentUser.data.email);
//         $http.post(uploadUrl, fd, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } })
//             .then(function successCallback(response) {
//                     callback(response);
//                 },
//                 function errorCallback(response) {
//                     callback(response);
//                 }
//             );
//     }
// }]);

// app.factory('socket', ['$rootScope', function($rootScope) {
//     var socket = io.connect();

//     return {
//         on: function(eventName, callback) {
//             socket.on(eventName, function() {
//                 var args = arguments;
//                 $rootScope.$apply(function() {
//                     callback.apply(socket, args);
//                 });
//             });
//         },
//         emit: function(eventName, data, callback) {
//             socket.emit(eventName, data, function() {
//                 var args = arguments;
//                 $rootScope.$apply(function() {
//                     if (callback) {
//                         callback.apply(socket, args);
//                     }
//                 });
//             })
//         }
//     };
// }]);


        var app = angular.module('app');
        app.service('ajaxService', ['$http','$rootScope', function($http,$rootScope) {
            this.post = function (data, route, callback) {
                let Header = {
                    headers:
                    {
                        'x-access-token': $rootScope.currentUser.data.token || $rootScope.currentUser.token
                    }
                }
                $http.post(route, data,Header).then(function successCallback(response) {
					callback(response);
				}, function errorCallback(response) {
					callback(response);
				});
            };

            this.get = function (data, route, callback) {
                $http({
                    method: 'GET',
                    url: route,
                    params: data,
                    headers: {
                        'x-access-token': $rootScope.currentUser.token
                      }
                }).then(function successCallback(response) {
					callback(response);
				}, function errorCallback(response) {
					callback(response);
				});
            };

            this.put = function (data, route, callback) {
                $http({
                    method: 'PUT',
                    url: route,
                    data: data,
                    headers: {
                        'x-access-token': $rootScope.currentUser.token
                      }
                }).then(function successCallback(response) {
					callback(response);
				}, function errorCallback(response) {
					callback(response);
				});
            };

            this.delete = function (data, route, callback) {
                $http.delete(route, data).then(function successCallback(response) {
					callback(response);
				}, function errorCallback(response) {
					callback(response);
				});
            };
        }]);


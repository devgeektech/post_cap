var app = angular.module('app')
app.service('dataService', ['ajaxService', function (ajaxService) {
    this.get = function (callback) {
        ajaxService.get(null, 'https://api.totallister.net/hello', callback);
    };
    this.getTechniques = function (callback) {
        ajaxService.get(null, '/api/technique', callback);
    };
    this.getAccents = function (callback) {
        ajaxService.get(null, '/api/accent', callback);
    };
    this.getCaptionServices = function (callback) {
        ajaxService.get(null, '/api/getCaptionServices', callback);
    };
    this.getTurnarounds = function (callback) {
        ajaxService.get(null, '/api/turnaround', callback);
    };
    this.getServices = function (callback) {
        ajaxService.get(null, '/api/services', callback);
    };
    this.getProjectById = function (data, callback) {
        ajaxService.get(null, '/api/projectbyid/' + data.id, callback);
    }
    this.getProjects = function (data, callback) {
        ajaxService.get(null, '/api/project/?skip=' + data.skip + `&limit=${data.limit}`, callback);
    };
    this.jobApplyById = function (data, callback) {
        ajaxService.put(null, '/api/job/' + data.id + '/user/' + data.userId, callback);
    }
    this.getNotificationListById = function (userId, callback) {
        ajaxService.get(null, '/api/notifications/user/' + userId, callback);
    }
    this.acceptJob = function (data, callback) {
        ajaxService.put(data, '/api/job/accept/' + data.jobId + '/user/' + data.userId, callback);
    }
    this.rejectJob = function (data, callback) {
        var postData = {
            notificationId: data.notificationId
        }
        ajaxService.put(postData, '/api/job/reject/' + data.jobId + '/user/' + data.userId, callback);
    }
    this.getJobs = function (data, callback) {
        ajaxService.get(null, '/api/jobs/' + data.userId, callback);
    }
    // mail service sendmail
    this.sendmail = function (data, callback) {
        ajaxService.post(data, '/api/createmail', callback);
    }
    // get mail  
    this.getMail = function (data, callback) {
        ajaxService.get(null, '/api/user/' + data.userId + '/mail/' + data.data, callback);
    }
    // getMailById
    this.getMailById = function (mailId, callback) {
        ajaxService.get(null, '/api/mail/' + mailId, callback);
    }
    // moveToTrash
    this.moveToTrash = function (data, callback) {
        var postData = {
            "messageId": data.messageId,
            "locationType": data.locationType
        }
        ajaxService.put(postData, '/api/user/trash/' + data.userId, callback);
    }
    // importantMail
    this.importantMail = function (data, callback) {
        var postData = {
            "messageId": data.messageId,
            "check": data.check
        }
        ajaxService.put(postData, '/api/user/important/' + data.userId, callback);
    }
    // projectSubmitMessageSubmit
    this.projectSubmitMessageSubmit = function (data, callback) {
        var postData = {
            finalProjectSubmit: data.finalProjectSubmit,
            messageBody: data.message
        }
        ajaxService.put(postData, '/api/projectSubmission/' + data.projectId, callback);
    }
    // issueSubmission
    this.issueSubmission = function (data, callback) {
        var postData = {
            "userId": data.userId,
            "issueWithDelivery": data.issueWithDelivery,
            "issueWithMedia": data.issueWithMedia,
            "message": data.message
        }
        ajaxService.post(postData, '/api/issueReport/' + data.projectId, callback);
    }
    // updatedCostProject
    this.updatedCostProject = function (data, callback) {
        var postData = {
            "userId": data.userId,
            newTotalPrice: data.newTotalPrice
        }
        ajaxService.put(postData, '/api/updatedCostProject/' + data.projectId, callback);
    }
    this.getWriterList = function (callback) {
        ajaxService.get(null, '/api/getWriterListUser', callback);
    }
    // assignJobToWriter
    this.assignJobToWriter = function (data, callback) {
        var postData = {
            "userId": data.userId,
            "writerId": data.writerId
        }
        ajaxService.put(postData, '/api/assignIc/' + data.projectId, callback);
    }

    // productAttachment
    this.attachmentToProject = function (data, callback) {
        var postData = {
            "userId": data.userId,
            "attachmentLink": data.attachmentLink
        }
        ajaxService.put(postData, '/api/attachmentToProject/' + data.projectId, callback);
    }
    //google drive video info
    this.getVideoLinkInfo = function (data, callback) {
        ajaxService.post(data, '/api/getVideoLinkInfo', callback);
    }

    //link google account
    this.getDriveDetails = function (data, callback) {
        ajaxService.get(data, '/api/getDriveDetails', callback);
    }

    //link youtube Data
    this.getYoutubeData = function (data, callback) {
        ajaxService.get(data, '/api/getYoutubeData', callback);
    }
    // getVimeoDetail
    this.getVimeoDetail = function (data, callback) {
        ajaxService.get(data, '/api/getVimeoDetail', callback);
    }
    //stop server
    this.stopServer = function (callback) {
        ajaxService.post(null, '/api/stopServer', callback);
    }
    //start server
    this.startServer = function (callback) {
        ajaxService.post(null, '/api/startServer', callback);
    }
    // getMyProjects
    this.getMyProjects = function (data, callback) {
        ajaxService.post(data, '/api/getUserProject', callback);
    }
    //add label project
    this.addlabelToproject = (data, callback) => {
        ajaxService.post(data, '/api/addlabelToproject', callback);
    }
    //addlabelbyUser
    this.addlabelbyUser = (data, callback) => {
        ajaxService.post(data, '/api/addlabelbyUser', callback);
    }
    //getusersLabel
    this.getusersLabel = (callback) => {
        ajaxService.get(null, 'api/getusersLabel', callback);
    }
    //removelabelbyUser
    this.removelabelbyUser = (data, callback) => {
        ajaxService.put(data, '/api/removelabelbyUser', callback);
    }
    //getProjectAccordingToLabel
    this.getProjectAccordingToLabel = (data, callback) => {
        // /api/getProjectAccordingToLabel/label/:labelId
        ajaxService.get(null, '/api/getProjectAccordingToLabel/label/' + data.labelData, callback);
    }
    //getUserAccordingUserType
    this.getUserAccordingUserType = (data, callback) => {
        ajaxService.get(null, '/api/getUserAccordingUserType?userType=' + data.userTypeData, callback);
    }
    //cancelProject
    this.cancelProject = (data, callback) => {
        ajaxService.put(null, '/api/cancelProject/' + data.projectId, callback);
    }
    //getUserAccordingUserType
    this.getProjectAccordingProjectType = (data, callback) => {
        ajaxService.get(null, '/api/getProjectAccordingProjectType?projectStatus=' + data.projectStatus + '&serviceType=' + data.serviceType + '&startDate=' + data.startDate + '&lastDate=' + data.lastDate + '&writerName=' + data.writerName + '&clientName=' + data.clientName, callback);
    }

    this.getProjectAccordingProjectTypeJobBoard = (data, callback) => {
        ajaxService.get(null, '/api/getProjectAccordingProjectTypeJobBoard?projectStatus=' + data.projectStatus + '&serviceType=' + data.serviceType + '&startDate=' + data.startDate + '&lastDate=' + data.lastDate + '&clientName=' + data.clientName + `&skip=${data.skip}&limit=${data.limit}`, callback);
    }
    // addToDraft
    this.addToDraft = (data, callback) => {
        ajaxService.post(data, '/api/addToDraft', callback);
    }
    //uploadToAmazon
    this.uploadtoAmazon = (data, callback) => {
        ajaxService.post(data, '/api/uploadtoAmazon', callback);
    }
    // /api/getAllUserForMessage/:role
    this.getAllUserForMessage = (data, callback) => {
        ajaxService.post(data, '/api/getAllUserForMessage/' + data.role, callback);
    }
    // dataService.downloadVideo
    this.downloadVideo = (data, callback) => {
        ajaxService.post(data, '/api/downloadYoutube', callback);
    }
    this.downloadCsv = (data, callback) => {
        ajaxService.post(data, '/api/downloadCsv', callback);
    }

    // dataService.getAppliedUser
    this.getAppliedUser = (data, callback) => {
        ajaxService.get(null, '/api/getAppliedUser/' + data.id, callback);
    }
    //acceptJobByAdmin
    this.acceptJobAndRejectByAdmin = (data, callback) => {
        ajaxService.post(data, '/api/acceptJobAndRejectByAdmin', callback);
    }
    this.getProjectAccordingToStatus = (data, callback) => {
        ajaxService.get(null, '/api/getProjectAccordingToStatus/' + data.statusType, callback);
    }
    this.outputLog = (callback) => {
        ajaxService.post(null, '/api/outputListCaptionServer', callback);
    }
    this.inputLog = (callback) => {
        ajaxService.post(null, '/api/inputListCaptionServer', callback);
    }
    this.captionLog = (callback) => {
        ajaxService.post(null, '/api/captionListCaptionServer', callback);
    }
    this.serverLog = (callback) => {
        ajaxService.post(null, '/api/serverLogs', callback);
    }
    this.restartFile = (data, callback) => {
        ajaxService.post(data, '/api/databaseConnectionAndRestart', callback);
    }
    this.getuserInfoByName = (data, callback) => {
        ajaxService.get(null, '/api/getuserInfoByName/' + data.name, callback);
    }
    this.getQuickBookToken = (callback) => {
        ajaxService.get(null, '/api/getQuickBookToken', callback)
    }
    this.renderPaymentPage = (callback) => {
        ajaxService.post(null, '/api/renderPaymentPage', callback)
    }
    this.getAllWriter = (callback) => {
        ajaxService.get(null, '/api/getAllWriter', callback)
    }

    this.deleteProject = (data, callback) => {
        ajaxService.put(data, '/api/deleteProject', callback)
    }

    this.updateOrderDue = (data, callback) => {
        ajaxService.put(data, '/api/orderDueUpdate/' + data.id, callback)
    }
    this.getPaymentLogs = (callback) => {
        ajaxService.post(null, '/api/getPaymentLogs', callback)
    }
    this.getPaymentDetail = (data, callback) => {
        ajaxService.post(data, '/api/getPaymentDetail', callback)
    }
    // notificationHide
    this.notificationHide = (data, callback) => {
        ajaxService.post(data, '/api/notificationHide', callback)
    }
    this.editWriterPrice = (data, callback) => {
        ajaxService.post(data, '/api/editWriterPrice', callback);
    }
    this.updateWriterPrice = (data, callback) => {
        ajaxService.post(data, '/api/updateWriterPrice', callback);
    }
    this.unapplyFromJob = (data, callback) => {
        ajaxService.post(data, '/api/jobUnapply', callback);
    }

    this.getAllIssue = (data, callback) => {
        ajaxService.post(data, '/api/getIssue', callback);
    }

    this.editService = (data, callback) => {
        ajaxService.put(data, '/api/editService/' + data._id, callback);
    }

    this.editTurnaround = (data, callback) => {
        ajaxService.put(data, '/api/editTurnaround/' + data._id, callback);
    }
    this.editAccent = (data, callback) => {
        ajaxService.put(data, '/api/editAccent/' + data._id, callback);
    }
    this.editTecnique = (data, callback) => {
        ajaxService.put(data, '/api/editTecnique/' + data._id, callback);
    }

    this.markArchieve = (data, callback) => {
        ajaxService.post(data, '/api/markArchieve', callback);
    }

    this.resendPassword = (data, callback) => {
        ajaxService.post(data, '/api/resendPassword', callback);
    }

    this.getUsers = function (callback) {
        ajaxService.get(null, '/api/user', callback);
    }
    this.addPriceForUser = (data, callback) => {
        ajaxService.post(data, '/api/addPriceForUser', callback);
    }
    this.getUserPriceTable = (data, callback) => {
        ajaxService.post(data, '/api/getUserPriceTable', callback);
    }
    this.getUserPriceTableByreqUser = (callback) => {
        ajaxService.get(null, '/api/getUserPriceTableByreqUser', callback);
    }

    this.getWriterAndClientList = (data, callback) => {
        ajaxService.get(null, '/api/getWriterAndClientList?skip=' + data.skip + `&limit=${data.limit}`, callback);
    }

    this.updatePriceIndividual = (data, callback) => {
        ajaxService.post(data, '/api/updatePriceIndividual', callback);
    }

    this.messageSettingTime = (data, callback) => {
        ajaxService.post(data, '/api/messageSettingTime', callback);
    }

    this.autoAssigner = (data, callback) => {
        ajaxService.post(data, '/api/autoAssigner', callback);
    }

    this.getSetting = (callback) => {
        ajaxService.get(null, '/api/getSetting', callback);
    }

    this.editSetting = (data, callback) => {
        ajaxService.post(data, '/api/editSetting', callback);
    }

    this.disconnectLinkAccount = (data, callback) => {
        ajaxService.post(data, '/api/disconnectLinkAccount', callback);
    }

    this.unAssignWriter = (data, callback) => {
        ajaxService.post(data, '/api/unAssignWriter', callback);
    }

    this.getMessageTime = (callback) => {
        ajaxService.get(null, '/api/getMessageTime', callback);
    }

    this.verifyPassword = (data, callback) => {
        ajaxService.post(data, '/api/verifyPassword', callback);
    }

    this.changePassword = (data, callback) => {
        ajaxService.post(data, '/api/changePassword', callback);
    }
    this.getUserByid = (data, callback) => {
        ajaxService.get(null, `/api/getUserByid/${data.id}`, callback);
    }


    /* TODO  remove this */
    this.downloadOpenCaptionVideo = (data, callback) => {
        ajaxService.post(data, `/api/downloadOpenCaptionVideo`, callback);
    }

    //sendObjectToEditor
    this.sendObjectToEditor = (data, callback) => {
        ajaxService.get(null, '/api/sendObjectToEditor?objectname=' + data.objectname + '&canallowwatson=' + data.canallowwatson, callback);
    }
	
	//downloadAttachment
    this.downloadAttachment = (data, callback) => {
        ajaxService.get(null, '/api/downloadAttachment?filename=' + data.filename, callback);
    }
	
	//Upload to Editor
	this.UploadToEditor = (data, callback) => {
        ajaxService.post(data, '/api/UploadToEditor', callback);
    }

    //cardSave
    this.saveCardsDetail = (data, callback) => {
        ajaxService.post(data, '/api/saveCardsDetail', callback);
    }
    // getuserCards
    this.getuserCards = (callback) => {
        ajaxService.post(null, '/api/getuserCards', callback);
    }
    // /api/deleteCard
    this.deleteCard = (data, callback) => {
        ajaxService.post(data, '/api/deleteCard', callback);
    }
    this.makePaymentWithSaved = (data, callback) => {
        ajaxService.post(data, '/api/makePaymentWithSaved', callback);
    }

    // makePaymentWithSaved
}]);
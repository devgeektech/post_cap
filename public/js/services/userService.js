var app = angular.module('app')
app.service('userService', ['ajaxService', function(ajaxService) {
    this.get = function(callback) {
        ajaxService.get(null, '/api/project', callback);
    };
    this.getUserById = function(id, callback) {
        ajaxService.get(null, '/api/user/' + id, callback);
    };
    this.getUserInfo = function(callback) {
        ajaxService.get(null, '/api/getUserInfo', callback);
    };
    this.create = function(data, route, callback) {
        ajaxService.post(data, route, callback);
    };
    this.setPassword = function(data, callback) {
        ajaxService.post(data, '/api/user/setPassword', callback);
    };
    this.upload = function(data, callback) {
        ajaxService.post(data, '/api/user/uploadPhotos', callback);
    };
    this.updateUser = function(data, callback) {
        ajaxService.put(data, '/api/userUpdate/' + data.id, callback);
    };
    this.deleteUser = function(data, callback) {
        ajaxService.delete(data, '/api/user/' + data.ids, callback);
    };
    this.getUsers = function(callback) {
        ajaxService.get(null, '/api/users', callback);
    }

    /* =============================== project controller calls=================================*/

    this.getVideoInfo = function(data, callback) {
        ajaxService.post(data, '/api/getVideoInfo', callback);
    };

    this.projectSubmit = function(data, callback) {
        ajaxService.post(data, '/api/project', callback);
    };






    /* ========================================================================================*/




}]);
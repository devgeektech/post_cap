/**
 * Create a WaveSurfer instance.
 */
var wavesurfer = Object.create(WaveSurfer);
var last_timestamp;
var last_str_time;

/**
 * Init & load.
 */
document.addEventListener('DOMContentLoaded', function () {

    // Init wavesurfer
    wavesurfer.init({
        container: '#waveform',
        height: 100,
        pixelRatio: 1,
        scrollParent: true,
        normalize: true,
        minimap: true,
        backend: 'MediaElement',
		waveColor: 'orange'
    });
	
	//wavesurfer.load($("#waveId").val());

    /* Regions */
    wavesurfer.enableDragSelection({
        color: randomColor(0.3)
    });

    wavesurfer.on('ready', function () {		
        //if (localStorage.regions) {
            //loadRegions(JSON.parse(localStorage.regions));
        //} else {
			//alert($("#lblJsonId").val());
			loadRegions(JSON.parse($("#lblJsonId").val()));
            saveRegions();
        //}
    });
    wavesurfer.on('region-click', function (region, e) {
        e.stopPropagation();
        // Play on click, loop on shift click
        e.shiftKey ? region.playLoop() : region.play();
    });
    wavesurfer.on('region-click', editAnnotation);
    wavesurfer.on('region-updated', saveRegions);
    wavesurfer.on('region-removed', saveRegions);
    wavesurfer.on('region-in', showNote);

    wavesurfer.on('region-play', function (region) {
        region.once('out', function () {
            wavesurfer.play(region.start);
            wavesurfer.pause();
        });
    });

    /* Timeline plugin */
    wavesurfer.on('ready', function () {
        var timeline = Object.create(WaveSurfer.Timeline);
        timeline.init({
            wavesurfer: wavesurfer,
            container: "#wave-timeline"
        });
    });


    /* Toggle play/pause buttons. */
    // var playButton = document.querySelector('#play');
    // var pauseButton = document.querySelector('#pause');
    // wavesurfer.on('play', function () {
        // playButton.style.display = 'none';
        // pauseButton.style.display = '';
    // });
    // wavesurfer.on('pause', function () {
        // playButton.style.display = '';
        // pauseButton.style.display = 'none';
    // });
});

function playwavesurfer()
{
	wavesurfer.playPause();
}

function loadwavesurfer()
{
	wavesurfer.load($("#waveId").val());
}

function stopwavesurfer()
{
	wavesurfer.stop();
}

/**
 * Save annotations to localStorage.
 */
function saveRegions() {
    localStorage.regions = JSON.stringify(
        Object.keys(wavesurfer.regions.list).map(function (id) {
            var region = wavesurfer.regions.list[id];
            return {
                start: region.start,
                end: region.end,
                attributes: region.attributes,
                data: region.data,
				drag: false
            };
        })
    );
}

function fill_waves(event) {
	var text_content = $.trim($("#able_transcript_edit").text());
	var offset = $("#able_transcript_edit").offset();

	if(text_content != "") {
		var text_full_length = text_content.length;
		var start_time = 1;
		if(last_timestamp != null) {
			start_time = last_timestamp;
		}
		var end_time = wavesurfer.getCurrentTime();
		last_timestamp = end_time;
			
		var start_str = 1;
		if(last_str_time != null) {
			start_str = last_str_time;
		}
		var selection = window.getSelection();
		console.log(selection.focusNode.data[selection.focusOffset]);
		var end_str = selection.focusOffset;
		last_str_time  = end_str;
		
		hidden_content = $("#audiocontent").val();
		
		audio_content = $.trim($("#able_transcript_edit").text()).substring(start_str, end_str);
		//alert($.trim($("#able_transcript_edit").text()));
	
		output = "";		
		if(hidden_content != "") {		
			span_out = '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+start_time+'" data-end="'+end_time+'">'+audio_content+'</span>';
			div_output = hidden_content+' '+span_out;
						
			//audio_text += audio_content;
			//var html_content = $.trim($("#able_transcript_edit").html());
			
			$("#able_transcript_edit").html(text_content.replace(audio_content, div_output));
			
			output = output.concat('[{"start":'+start_time+',"end":'+end_time+',"data":{},"attributes":{"label": "'+audio_content+'"}}]');
			$("#audiocontent").val(text_content.replace(audio_content, div_output));
		}
		else {
			div_output = '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+start_time+'" data-end="'+end_time+'">'+audio_content+'</span>';
			
			$("#able_transcript_edit").html(text_content.replace(audio_content, div_output));
			
			output = output.concat('[{"start":'+start_time+',"end":'+end_time+',"data":{},"attributes":{"label": "'+audio_content+'"}}]');					
			$("#audiocontent").val(text_content.replace(audio_content, div_output));
		}
						
		 $("#lblJsonId").val(output);
		 loadRegions(JSON.parse($("#lblJsonId").val()), 'none');
	}		 
}

function font_famil() {
	$('.able-transcript-container').css("font-family", $('#setfamily').val());
	return false;
}

function font_increase() {
	var currentSize = $('.able-transcript-container').css('font-size');
	var currentSize = parseFloat(currentSize)+1;
	$('.able-transcript-container').css('font-size', currentSize);
	return false;
}
// Decrease Font Size
function font_decrease() {
	var currentFontSize = $('.able-transcript-container').css('font-size');
	var currentSize = $('.able-transcript-container').css('font-size');
	var currentSize = parseFloat(currentSize)-1;
	$('.able-transcript-container').css('font-size', currentSize);
	return false;
}
//Fill transcript and waves using watson object; change to line or word highlight;
function fill_watson_based_waves(obj, wordorline) {
	//alert(obj.results[0].alternatives[0].confidence)
	var output = "";
	var div_output = "";
	$.each(obj.results, function (i) {
		$.each(obj.results[i], function (key, val) {
			$.each(val, function (index, alternative) {
				if(index == 0 && wordorline == "line"){
					var actualdata = alternative.transcript;
					var confidence = (alternative.confidence * 100);
					var start_time = alternative.timestamps[0][1];
					var end_time = alternative.timestamps[alternative.timestamps.length-1][2];

					div_output = div_output + ' ' + '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+start_time+'" data-end="'+end_time+'">' + actualdata + '</span>';					
					
					if(output == '')
						output = output.concat('[{"confidence":'+confidence+',"start":'+start_time+',"end":'+end_time+',"data":{},"attributes":{"label": "'+actualdata+'" }}');
					else
						output = output.concat(',{"confidence":'+confidence+',"start":'+start_time+',"end":'+end_time+',"data":{},"attributes":{"label": "'+actualdata+'" }}');  
				}
				else if(index == 0 && wordorline == "word"){
					
					for(var idx = 0; idx < alternative.timestamps.length; idx++){
						var actualdata = alternative.timestamps[idx][0];						
						var start_time = alternative.timestamps[idx][1];
						var end_time = alternative.timestamps[idx][2];
						var confidence = (alternative.word_confidence[idx][1] * 100);

						div_output = div_output + ' ' + '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+start_time+'" data-end="'+end_time+'">' + actualdata + '</span>';					
						
						if(output == '')
							output = output.concat('[{"confidence":'+confidence+',"start":'+start_time+',"end":'+end_time+',"data":{},"attributes":{"label": "'+actualdata+'" }}');
						else
							output = output.concat(',{"confidence":'+confidence+',"start":'+start_time+',"end":'+end_time+',"data":{},"attributes":{"label": "'+actualdata+'" }}');
					}					
				}
			});
			
			$("#able_transcript_edit").html(div_output);
		});
	});	
	//alert(output);
	
	$("#lblJsonId").val(output + "]");
	loadRegions(JSON.parse($("#lblJsonId").val()), 'watson');			
}

/**
 * Load regions from localStorage.
 */
function loadRegions(regions, watsonornot) {
	if(watsonornot == "watson")
		wavesurfer.clearRegions();
    regions.forEach(function (region) {
		region.color = fixedConfidenceColor(0.4, region.confidence);
        //region.color = randomColor(0.3);
        wavesurfer.addRegion(region);
    });
}

function loadIntervalRegions(regions) {
	wavesurfer.clearRegions();
    regions.forEach(function (region) {
		region.start = region.start + parseInt($("#idinterval").val());
		region.end = region.end + parseInt($("#idinterval").val());
        region.color = randomColor(0.3);
        wavesurfer.addRegion(region);
    });
}


/**
 * Extract regions separated by silence.
 */
function extractRegions(peaks, duration) {
    // Silence params
    var minValue = 0.0015;
    var minSeconds = 0.25;

    var length = peaks.length;
    var coef = duration / length;
    var minLen = minSeconds / coef;

    // Gather silence indeces
    var silences = [];
    Array.prototype.forEach.call(peaks, function (val, index) {
        if (Math.abs(val) <= minValue) {
            silences.push(index);
        }
    });

    // Cluster silence values
    var clusters = [];
    silences.forEach(function (val, index) {
        if (clusters.length && val == silences[index - 1] + 1) {
            clusters[clusters.length - 1].push(val);
        } else {
            clusters.push([ val ]);
        }
    });

    // Filter silence clusters by minimum length
    var fClusters = clusters.filter(function (cluster) {
        return cluster.length >= minLen;
    });

    // Create regions on the edges of silences
    var regions = fClusters.map(function (cluster, index) {
        var next = fClusters[index + 1];
        return {
            start: cluster[cluster.length - 1],
            end: (next ? next[0] : length - 1)
        };
    });

    // Add an initial region if the audio doesn't start with silence
    var firstCluster = fClusters[0];
    if (firstCluster && firstCluster[0] != 0) {
        regions.unshift({
            start: 0,
            end: firstCluster[firstCluster.length - 1]
        });
    }

    // Filter regions by minimum length
    var fRegions = regions.filter(function (reg) {
        return reg.end - reg.start >= minLen;
    });

    // Return time-based regions
    return fRegions.map(function (reg) {
        return {
            start: Math.round(reg.start * coef * 10) / 10,
            end: Math.round(reg.end * coef * 10) / 10
        };
    });
}


/**
 * Random RGBA color.
 */
function randomColor(alpha) {
    return 'rgba(' + [
        ~~(Math.random() * 255),
        ~~(Math.random() * 255),
        ~~(Math.random() * 255),
        alpha || 1
    ] + ')';

}

function fixedConfidenceColor(alpha, confidence) {
	var number = 0.75;
	if(confidence > 90)
		number = 0.35;
    return 'rgba(' + [
        ~~(number * 255),
        ~~(number * 255),
        ~~(number * 255),
        alpha || 1
    ] + ')';

}


/**
 * Edit annotation for a region.
 */
function editAnnotation (region) {
    var form = document.forms.edit;
    form.style.opacity = 1;
    form.elements.start.value = Math.round(region.start * 10) / 10,
    form.elements.end.value = Math.round(region.end * 10) / 10;
    form.elements.note.value = region.data.note || '';
    form.onsubmit = function (e) {
        e.preventDefault();
        region.update({
            start: form.elements.start.value,
            end: form.elements.end.value,
            data: {
                note: form.elements.note.value
            }
        });
        form.style.opacity = 0;
    };
    form.onreset = function () {
        form.style.opacity = 0;
        form.dataset.region = null;
    };
    form.dataset.region = region.id;
}


/**
 * Display annotation.
 */
function showNote (region) {
    if (!showNote.el) {
        showNote.el = document.querySelector('#subtitle');
    }
    showNote.el.textContent = region.data.note || '–';
}

/**
 * Bind controls.
 */
GLOBAL_ACTIONS['delete-region'] = function () {
    var form = document.forms.edit;
    var regionId = form.dataset.region;
    if (regionId) {
        wavesurfer.regions.list[regionId].remove();
        form.reset();
    }
};

GLOBAL_ACTIONS['export'] = function () {
    window.open('data:application/json;charset=utf-8,' +
        encodeURIComponent(localStorage.regions));
};
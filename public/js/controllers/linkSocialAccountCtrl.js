var app = angular.module('app')
    .controller('labelController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', '$state', function($scope, $http, $rootScope, dataService, $stateParams, $state) {
        $scope.getProjectAccordingToLabel = function() {
            var postData = {
                labelData: $state.params.labelId
            }
            dataService.getProjectAccordingToLabel(postData, function(response) {
                console.log(response.data)
                $scope.productData = response.data.projectId;

            })
        }

        $scope.showProjectById = function(id) {
            $state.go('dashboard.projectById', { id: id });
        }
        $scope.accending = true;
        $scope.messageClass = 'fa fa-angle-double-up';

        $scope.getProjectAccordingToStatus = function() {
            var postData = {
                statusType: $state.params.statusType
            }
            dataService.getProjectAccordingToStatus(postData, function(response) {
                $scope.productData = response.data;
            })
        }

      

        $scope.setSort = function(projectData,type) {
            if($scope.accending){
                sortByDate(projectData,type);
                
            }else{
                accending(projectData,type);
            }
                
        }

        $scope.accendingByDate = function(project) {
            accending(project);
        }


        function sortByDate(arr,type) {
               
            if(type == 'totalPrice'){
                arr.sort(function(a, b) {
                    return (b.totalPrice) - (a.totalPrice);
                });
            }

            if(type == 'projectName'){ 

                arr.sort(function(a, b){
                    return a.name < b.name ? -1 : 1
                })
            }

            if(type == 'client'){ 

                arr.sort(function(a, b){
                    return a.clientUser.firstname < b.clientUser.firstname ? -1 : 1
                })
            }

            if(type == 'service'){ 
                arr.sort(function(a, b){
                    return a.service.name < b.service.name ? -1 : 1
                })
            }

            if(type == 'Status'){ 

                arr.sort(function(a, b){
                    return a.status < b.status ? -1 : 1
                })
            }

            if(type == 'writerPrice'){
                arr.sort(function(a, b) {
                    return (b.writerPrice) - (a.writerPrice);
                });
            }
            if(type == 'order'){
                arr.sort(function(a, b) {
                return Number(new Date(b.createdAt)) - Number(new Date(a.createdAt));
            });
            }
            if(type == 'totalPrice'){
                arr.sort(function(a, b) {
                    return (b.totalPrice) - (a.totalPrice);
                });
            }

            if(type == 'duration'){
                arr.sort(function(a, b) {
                    return (b.duration) - (a.duration);
                });
            }
           
            $scope.accending = false;
            $scope.messageClass = 'fa fa-angle-double-down'
            uc.projects = arr;
        }

        

        function accending(arr,type) {
            
            if(type == 'totalPrice'){
                arr.sort(function(a, b) {
                    return (a.totalPrice) - (b.totalPrice);
                });
            }

            if(type == 'projectName'){
                arr.sort(function(a, b){
                    return a.name > b.name ? -1 : 1
                })
            }

            if(type == 'client'){ 

                arr.sort(function(a, b){
                    return a.clientUser.firstname > b.clientUser.firstname ? -1 : 1
                })
            }

            if(type == 'service'){ 
                arr.sort(function(a, b){
                    return a.service.name > b.service.name ? -1 : 1
                })
            }

            if(type == 'Status'){
                arr.sort(function(a, b){
                    return a.status > b.status ? -1 : 1
                })
            }

            if(type == 'totalPrice'){
                arr.sort(function(a, b) {
                    return (a.totalPrice) - (b.totalPrice);
                });
            }

            if(type == 'writerPrice'){
                arr.sort(function(a, b) {
                    return (a.writerPrice) - (b.writerPrice);
                });
            }

            if(type == 'order'){
               arr.sort(function(a, b) {
                return Number(new Date(a.createdAt)) - Number(new Date(b.createdAt));
            });
            }
            
            if(type == 'duration'){
                arr.sort(function(a, b) {
                    return (a.duration) - (b.duration);
                });
            }
            $scope.messageClass = 'fa fa-angle-double-up'
            $scope.accending = true;
            uc.projects = arr;
            
        }

      

    }]);
var app = angular.module('app')
    .controller('issueController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', 'toaster', 'userService', 'Upload','$state', function ($scope, $http, $rootScope, dataService, $stateParams, toaster, userService, Upload,$state) {
        // console.log('dasdasda')

        (getAllIssue = () => {
            dataService.getAllIssue({ archieve: false }, response => {
                console.log(response)
                $scope.getAllIssue = response.data.message;
            })
        })()

        getAllArchievedIssue = () => {
            dataService.getAllIssue({ archieve: true }, response => {
                console.log(response)
                $scope.getAllIssueArcheived = response.data.message;
            })
        }
        getAllArchievedIssue()

        $scope.replyToWriter = function(email,issue){
            $state.go('dashboard.composeMail', {data: email,subject:"Issue Report : "+issue.project.name,message:issue.message,FullName:issue.from.firstname + ' '+issue.from.lastname});
        }

        $scope.oneAtATime = true;

        $scope.showProjectById = function (projectData) {
            $state.go('dashboard.projectById', {
                id: projectData._id
            });
        }


        $scope.markArchieve = (id) => {
            dataService.markArchieve({ id: id, type: 'Archieve' }, response => {
                getAllIssue();
                getAllArchievedIssue();
            })
        }


        $scope.status = {
            isCustomHeaderOpen: false,
            isFirstOpen: true,
        };

    }]);
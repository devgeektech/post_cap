var app = angular.module('app')
    .controller('paymentLogController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', 'toaster', '$state', function($scope, $http, $rootScope, dataService, $stateParams, toaster, $state) {
        
        var getAllLogs = ()=>{
             dataService.getPaymentLogs( function(response) {
                    $scope.logsList = response.data
             })
        }

        getAllLogs();

        $scope.getChargeDetail = (chargeId)=>{
            let postData = {
                chargeId :chargeId
            }
            dataService.getPaymentDetail(postData, function(response) {
                $scope.PaymentDetail = response.data
         })
        }

    }]);
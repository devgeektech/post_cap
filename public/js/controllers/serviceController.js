var app = angular.module('app')
    .controller('serviceController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', 'toaster', 'userService', 'Upload', function ($scope, $http, $rootScope, dataService, $stateParams, toaster, userService, Upload) {
       

        (getServices=()=>{
            dataService.getServices(response => {
                $scope.services = response.data;
            })
        })()

        getTurnarounds=()=>{
            dataService.getTurnarounds(response => {
                $scope.turnarounds = response.data;
            })
        }
        getTurnarounds();

        getAccents=()=>{
            dataService.getAccents(response => {
                $scope.Accents = response.data;
            })
        }
        getAccents();

        getTechniques=()=>{
            dataService.getTechniques(response => {
                $scope.Techniques = response.data;
            })
        }
        getTechniques();


        // user for all the modals
        $scope.serviceData ={}
        $scope.getServiceForEdit =(data)=>{      
            $scope.serviceData = data
        }

        $scope.editService = (data)=>{
            dataService.editService(data,response => {
                if(response.data.success){
                    toaster.pop('success', response.data.message);
                    $('#serviceModal').modal('hide');
                }
            })
        }

        $scope.editTurnaround = (data)=>{
            dataService.editTurnaround(data,response => {
                if(response.data.success){
                    toaster.pop('success', response.data.message);
                    $('#turnAroundModal').modal('hide');
                }
            })
        }

        $scope.editTecnique = (data)=>{
            dataService.editTecnique(data,response => {
                if(response.data.success){
                    toaster.pop('success', response.data.message);
                    $('#AccentModal').modal('hide');
                }
            })
        }
        $scope.editAccent = (data)=>{
            dataService.editAccent(data,response => {
                if(response.data.success){
                    toaster.pop('success', response.data.message);
                    $('#TecniqueModal').modal('hide');
                }
            })
        }

    }]);
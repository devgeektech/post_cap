angular.module('app', []).controller('PlayerController', function($scope) {

	var SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
	var fs = require('fs');
	console.log("Loading Watson Information");
	var speech_to_text = new SpeechToTextV1({
	  username: '32d30f2c-8b67-439b-90d5-a28dba999520',
	  password: 'OYK2HIiJymiG'
	});
	
	var params = {
	  // From file 
	  audio: fs.createReadStream('http://d3l6fnyzzmn75t.cloudfront.net/shortclip/wav-shortclip.wav'),
	  content_type: 'audio/l16; rate=44100'
	};
	 
	speech_to_text.recognize(params, function(err, res) {
	  if (err)
		console.log(err);
	  else
		console.log(JSON.stringify(res, null, 2));
	});
	 
	// or streaming 
	fs.createReadStream('http://d3l6fnyzzmn75t.cloudfront.net/shortclip/wav-shortclip.wav')
	  .pipe(speech_to_text.createRecognizeStream({ content_type: 'audio/l16; rate=44100' }))
	  .pipe(fs.createWriteStream('./transcription.txt'));

});
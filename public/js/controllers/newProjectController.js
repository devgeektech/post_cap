angular.module('app').controller('newProjectController', ['$scope', '$http', 'userService', 'dataService', '$rootScope', '$stateParams', '$window', 'toaster', '$q', '$state', 'DropBoxSettings', '$timeout', '$document', 'socket', '$window', function ($scope, $http, userService, dataService, $rootScope, $stateParams, $window, toaster, $q, $state, DropBoxSettings, $timeout, $document, socket, $window) {

    /* ==============================initialization====================================*/

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    var uc = this;
    uc.step1 = true;
    uc.step2 = false;
    uc.step3 = false;

    uc.paramsData = $state.params;

    $scope.autoHideVideo = false;
    $scope.hideVideo = function () {
        $scope.autoHideVideo = true;
        $timeout(function () {
            $scope.loginAlertMessage = false;
        }, 4000);
    };


    // @ google picker

    $scope.files = [];

    $scope.onLoaded = function () {
        console.log('Google Picker loaded!');
    }

    $scope.onPicked = function (docs) {
        angular.forEach(docs, function (file, index) {
            if (file.type != 'video') return toaster.pop('warning', file.name + ' cannot be added because of the format.');
            $scope.files.push(file);
            var projectModelData = {}
            projectModelData.name = file.name;
            projectModelData.url = file.url;
            projectModelData.size = file.sizeBytes;
            //convert milli sec to sec
            var durationToSeconds = file.duration // round to nearest second
            projectModelData.decimalLength = durationToSeconds / 60;
            // to convert sec to hr
            var h = Math.floor(durationToSeconds / 3600);
            var m = Math.floor(durationToSeconds % 3600 / 60);
            var s = Math.floor(durationToSeconds % 3600 % 60);

            var mins = Math.floor(projectModelData.decimalLength); // extract the hours (in 24 hour format)
            var sec = 60 * (projectModelData.decimalLength - mins);
            var milliseconSplit = s.toString().split('.')
            // projectModelData.stringLength = h.toString() + ':' +
            //     m.toString() + ':' + s.toString();
            projectModelData.stringLength = mins.toString()

            // }
            projectModelData.totalPrice = 0.00;
            uc.projectModel.projects.push(projectModelData);

        });
    }

    function removeDoller(data) {
        data = data.slice(1);
        data = data.replace("/min", '')
        return data
    }

    $scope.onCancel = function () {
        console.log('Google picker close/cancel!');
    }

    $scope.isValidUrl = (link) => {
        isURL(link, function (result) {
            if (result) {
                $scope.valid = true
            } else {
                $scope.valid = false
            }
        })
    }

    function isURL(str, cb) {
        var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
        cb(regex.test(str))
    }

    $scope.videoDisabel = true
    $scope.onDropboxSuccess = function (files) {
        $scope.dataAtDropbox = files;
        $rootScope.loader = true;
        $scope.hideVideo();

        files.forEach(function (item) {
            // var elements = document.getElementsByName('test');

            var documentResult = document.getElementsByClassName("drpoboxVideo");
            $timeout(function () {
                var dta = Array.from(documentResult);
                $scope.videoDisabel = false
                if (dta.length > 0) {
                    $scope.duration = dta[0].duration;
                    $rootScope.loader = false;
                }
            }, 3000)
        })
    }



    $scope.calculateCheckValue = function () {
        uc.projectModel.captioning.addTranscript = document.getElementById('captioningAddTranscipt').checked;
        uc.projectModel.captioning.addOpenCaption = document.getElementById('captioningOpenCaption').checked;


        uc.projectModel['transcription'].addTranscript = document.getElementById('captioningAddTranscipt').checked;
        uc.projectModel['transcription-alignment'].addOpenCaption = document.getElementById('captioningAddTranscipt').checked;
    };


    socket.on('notification', function (data) {
        if (data.user == $rootScope.currentUser.data.user.id) {
            let audio = new Audio('assets/NotificationTone/to-the-point.ogg');
            audio.play();
            toaster.pop('warning', 'You got a notification.');
            $scope.getNotification();
        }

    });

    $scope.disconnectAccount = function (type) {
        dataService.disconnectLinkAccount({
            type: type
        }, function (response) {
            // $scope.notification = response;
            $window.location.reload();
        })
    }



    $scope.getNotification = function () {
        var userId = $rootScope.currentUser.data.user.id;
        dataService.getNotificationListById(userId, function (response) {
            $scope.notification = response;
        })
    }
    $scope.getNotification();

    $scope.notificationHide = id => {
        dataService.notificationHide({
            id: id
        }, function (response) {
            $scope.getNotification();
        })
    }




    $scope.addCaptionService = (check, data) => {
        if (check) {
            uc.projectModel.CaptionFileOutput.push(data);
        } else {
            var index = uc.projectModel.CaptionFileOutput.indexOf(data);
            uc.projectModel.CaptionFileOutput.splice(index);
        }
    }


    $scope.acceptJob = function (notification) {

        var postData = {
            jobId: notification.extraData,
            userId: notification.from._id,
            notificationId: notification._id
        };
        dataService.acceptJob(postData, function (response) {

            $scope.getNotification();
            if (!response.data.success) {
                toaster.pop('error', response.data.message);
            } else {
                toaster.pop('success', response.data.message);
            }
        })
    }

    $scope.rejectJob = function (notification) {
        var postData = {
            jobId: notification.extraData,
            userId: $rootScope.currentUser.data.user.id,
            notificationId: notification._id
        };
        dataService.rejectJob(postData, function (response) {
            $scope.getNotification();
            if (!response.data.success) {
                toaster.pop('error', response.data.message);
            } else {
                toaster.pop('success', response.data.message);
            }
        })
    }


    /* ==============================initialization====================================*/




    /* ==========================project data model======================*/

    uc.projectModel = {
        upload: {
            project1: {}
        },
        projects: [],
        currentService: "",
        transcription: {
            addOpenCaption: false,
            addTranscript: false
        },
        "transcription-alignment": {
            addOpenCaption: false,
            addTranscript: false
        },
        captioning: {
            addOpenCaption: false,
            addTranscript: false
        },
        CaptionFileOutput: [],
        translation: {},
        selectedService: '',
        specialInstruction: ''
    };

    if ((uc.paramsData.type == 'social')) {
        uc.paramsData.data.forEach(function (item) {
            var projectModelData = {}
            projectModelData.name = item.name;
            projectModelData.url = item.url;
            projectModelData.size = item.size;
            projectModelData.decimalLength = item.decimalLength;
            projectModelData.stringLength = item.stringLength;
            uc.projectModel.projects.push(projectModelData);
        })
    }

    /* ==========================project data model end======================*/




    /* ==========================show/hide of templates======================*/

    $scope.selectTask = function (taskId) {
        uc.projectModel.currentService = taskId;
        var services = ['transcription', 'captioning', 'transcription-alignment', 'translation'];
        for (i = 0; i <= 3; i++) {
            uc[services[i]] = false;
        };
        uc[taskId] = true;
    }

    $scope.tostep = function (step) {
        var nextStateAllow = false;
        switch (step) {
            case 2:
                if (uc.projectModel.projects.length > 0) {
                    nextStateAllow = true;
                } else {
                    toaster.pop('error', "nextStep", "No File Uploaded");
                }
                break;
            case 3:

                if (uc.projectModel.currentService == 'transcription') {
                    uc.projectModel.selectedService = uc.projectModel.transcription
                }
                if (uc.projectModel.currentService == 'transcription-alignment') {
                    uc.projectModel.selectedService = uc.projectModel['transcription-alignment']
                }
                if (uc.projectModel.currentService == 'translation') {
                    uc.projectModel.selectedService = uc.projectModel.translation
                }
                if (uc.projectModel.currentService == 'captioning') {
                    uc.projectModel.selectedService = uc.projectModel.captioning
                }
                calculateRates(uc.projectModel.currentService);
                break;

        }
        if (uc.projectModel.currentService) {
            nextStateAllow = $('#' + uc.projectModel.currentService + 'Form')[0].checkValidity();
        }

        if (nextStateAllow) {
            for (i = 1; i <= 3; i++) {
                uc['step' + i] = false;
            }
            uc['step' + step] = true;

        };


    };



    $scope.selectTask();


    /* ==========================show/hide of templates end======================*/


    var defaultService
    /* ===============get lists ========================*/
    var getLists = function () {
        dataService.getTechniques(function (response) {
            uc.techniques = response.data;
        });
        dataService.getAccents(function (response) {
            uc.accents = response.data;
        });
        dataService.getTurnarounds(function (response) {
            uc.turnarounds = response.data;
        });
        dataService.getCaptionServices(function (response) {
            uc.CaptionServices = response.data;
        });
        dataService.getServices(function (response) {
            defaultService = response.data;
            dataService.getUserPriceTableByreqUser(function (Price) {
                if (Price.data.message == 'No user') return uc.services = response.data;
                if (Price.data.message.transcription || Price.data.message.transcriptionAlignment || Price.data.message.captioning) {
                    uc.services = response.data.map(function (item) {
                        if (item.name == 'transcription' && Price.data.message.transcription) {
                            item.price = Price.data.message.transcription.totalPrice
                        }
                        if (item.name == 'transcription-alignment' && Price.data.message.transcriptionAlignment) {
                            item.price = Price.data.message.transcriptionAlignment.totalPrice
                        }
                        if (item.name == 'captioning' && Price.data.message.captioning) {

                            let query ={}

                            if (Price.data.message.captioning.transcription) {
                             
                                Price.data.message.captioning.transcription.totalPrice = Price.data.message.captioning.transcription.totalPrice.slice(1);

                                Price.data.message.captioning.transcription.totalPrice = Price.data.message.captioning.transcription.totalPrice.replace("/min", '')

                                query.transcriptionPrice = parseFloat(Price.data.message.captioning.transcription.totalPrice);
                            }

                            if (Price.data.message.captioning.transcriptionAlignment) {


                                Price.data.message.captioning.transcriptionAlignment.totalPrice = Price.data.message.captioning.transcriptionAlignment.totalPrice.slice(1);

                                Price.data.message.captioning.transcriptionAlignment.totalPrice = Price.data.message.captioning.transcriptionAlignment.totalPrice.replace("/min", '')

                                query.transcriptionAlignmentPrice = parseFloat(Price.data.message.captioning.transcriptionAlignment.totalPrice)  ;
                            }

                            let transcriptionAlignmentPriceCopy = query.transcriptionAlignmentPrice || 0;

                            let transcriptionPriceCopy = query.transcriptionPrice || 0
                            
                            item.price = transcriptionAlignmentPriceCopy + transcriptionPriceCopy

                            item.price.toString()
                            item.price = '$' + item.price + "/min"

                        }
                        // if (item.name == 'captioning' && Price.data.message.captioning) {

                        //     if( Price.data.message.captioning.caption.totalPrice){
                        //         item.price = Price.data.message.captioning.caption.totalPrice
                        //     }else{
                        //         console.log(item)
                        //     }
                        // }
                        return item
                    })
                } else {
                    uc.services = response.data;
                }
            })
        });
        dataService.getUserPriceTableByreqUser(function (response) {
            $scope.customPrice = response.data
        })

        // dataService.getProjects({
        //     userId: $rootScope.currentUser.data.user.id
        // }, function (response) {

        //     if ($rootScope.currentUser.data.user.role == 'writer') {
        //         let temp = response.data;
        //         uc.projects = []
        //         if ($rootScope.currentUser.data.transcriber && !$rootScope.currentUser.data.transcriptionAlignment) {
        //             temp.forEach((item) => {
        //                 if (item.service.name == 'transcription') {
        //                     uc.projects.push(item)
        //                 }
        //             })
        //         } else if (!$rootScope.currentUser.data.transcriber && $rootScope.currentUser.data.transcriptionAlignment) {
        //             temp.forEach((item) => {
        //                 if (item.service.name == 'transcription-alignment') {
        //                     uc.projects.push(item)
        //                 }
        //             })
        //         } else if ($rootScope.currentUser.data.transcriber && $rootScope.currentUser.data.transcriptionAlignment) {
        //             temp.forEach((item) => {
        //                 if (item.service.name == 'transcription-alignment' || item.service.name == 'transcription') {
        //                     uc.projects.push(item)
        //                 }
        //             })
        //         }
        //     } else {
        //         uc.projects = response.data;
        //     }
        // });

    }

    getLists();

    $scope.getMyProjects = function () {
        dataService.getProjects({
            userId: $rootScope.currentUser.data.user.id
        }, function (response) {
            $scope.myProjects = response.data;
        });
    }

    $scope.initProject = function () {
        dataService.getProjects({
            userId: $rootScope.currentUser.data.user.id
        }, function (response) {
            uc.projects = response.data;
        });
    }


    /* ===============get lists end ========================*/


    // $scope.creds = {
    //     bucket: 'post-cap-phase-2-input-project',
    //     access_key: 'AKIAIZWLV3WCWDDXWLBA',
    //     secret_key: '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'
    // }

    // new credential
    $scope.creds = {
        bucket: 'post-cap-projects-phase-2',
        access_key: 'AKIAJ3RYIKD5JYFSYGWQ',
        secret_key: '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'
    }

    uc.fileUploadStatus1 = '0%';



    /* single file upload*/
    // if($scope.file) {
    //     var params = { Key: $scope.file.name, ContentType: $scope.file.type, Body: $scope.file, ServerSideEncryption: 'AES256' };
    //     uc.projectModel.upload.project1.name = $scope.file.name;
    //     bucket.putObject(params, function(err, data) {
    //         if(err) {
    //             // There Was An Error With Your S3 Config
    //             return false;
    //         }
    //         else {
    //             // Success!
    //             var url = "https://s3-us-west-2.amazonaws.com/post-cap-phase-2-input-project/";
    //             var fileName = $scope.file.name.replace(/ /g , "+");
    //             var completeUrl = url + fileName;
    //             var projectModelData = {};
    //             projectModelData.url = completeUrl;
    //             projectModelData.name = fileName;
    //             projectModelData.size = $scope.file.size;
    //             projectModelData.type = $scope.file.type;
    //             var data = {
    //                 link: completeUrl
    //             };
    //             userService.getVideoInfo(data, function(response) {
    //                 projectModelData.decimalLength = response.data.length/60;   
    //                 var mins = Math.floor(projectModelData.decimalLength); // extract the hours (in 24 hour format)
    //                 var sec = 60 * (projectModelData.decimalLength - mins);
    //                 var milliseconSplit = sec.toString().split('.')
    //                 projectModelData.stringLength = mins.toString() +  ':' + 
    //                  milliseconSplit[0].toString() + ':' + milliseconSplit[1].toString();      
    //                 projectModelData.totalPrice = 0.00; 
    //                 uc.projectModel.projects.push(projectModelData);
    //                 toaster.pop('success', "Upload", "Project Uploaded");
    //                 $scope.$apply();
    //             });
    //         };
    //     })
    //     .on('httpUploadProgress',function(progress) {
    //         // Log Progress Information
    //         uc.projectModel.upload.project1.fileUploadStatus = Math.round(progress.loaded / progress.total * 100) + '%'
    //         document.getElementById("fileUploadStatus1").style.width = uc.projectModel.upload.project1.fileUploadStatus;
    //         console.log(Math.round(progress.loaded / progress.total * 100) + '%');
    //         $scope.$apply();
    //     });
    // }
    // else {
    //     // No File Selected
    //     toaster.pop('error', "Upload", "No File Selected");
    //     $scope.$apply();
    // }

    /* end single file upload*/





    /* ===============multiple file upload========================================*/
    $scope.loader = false;
    $scope.upload = function () {
        // Configure The S3 Object 
        $scope.loader = true;
        AWS.config.update({
            accessKeyId: $scope.creds.access_key,
            secretAccessKey: $scope.creds.secret_key,
            httpOptions: {
                timeout: 43200000 //your timeout value 12 hours timeout
            }
        });
        AWS.config.region = 'us-east-2';

        var bucket = new AWS.S3({
            params: {
                Bucket: $scope.creds.bucket
            }
        });

        if ($scope.file.length > 0) {
            var paramsArray = [];
            var fileArray = [];
            angular.forEach($scope.file, function (fileData, index) {
                if (fileData.size > 0) {
                    var params = {
                        Key: fileData.name,
                        ContentType: fileData.type,
                        Body: fileData,
                        ServerSideEncryption: 'AES256'
                    };
                    uc.projectModel.upload['project' + (index + 1).toString()] = {};
                    uc.projectModel.upload['project' + (index + 1).toString()].name = fileData.name;
                    uc.projectModel.upload['project' + (index + 1).toString()].size = fileData.size;
                    var data = {
                        params: params,
                        fileData: fileData
                    };
                    fileArray.push(data);
                }

            });

            fileArray.forEach(function (data, index) {
                var dataIndex = 'project' + (index + 1).toString()

                var fileNameFormat = data.fileData.name.replace(/ /g, "_");
                // if (fileNameFormat.includes('.')) {
                //     fileNameFormat = fileNameFormat.split(".").join("")
                // }
                let fileName = data.fileData
                fileName.name = fileNameFormat
                data.params.Key = fileNameFormat
                data.params.Body.name = fileNameFormat
                uploadFile(bucket, fileName, data.params, dataIndex);
            })
        } else {
            toaster.pop('error', "Upload", "No File Selected");
            $scope.$apply();
        }
        /* ===============multiple file upload end========================================*/


    }



    /*============================================lastcalculationStep3==================================================*/
    var deferred = $q.defer();
    var promise = deferred.promise


    var parsePriceToDollar = function (priceString) {
        var price = priceString.substring(priceString.lastIndexOf("$") + 1, priceString.lastIndexOf("/"));
        return price;
    };

    var arr2 = []

    const totalPriceGreaterThantwo = (price, callback) => {
        let convertedPrice = parseInt(price)
        if (convertedPrice > 2) {
            callback(price)
        } else {
            let newprice = '2.00'
            callback(newprice)
        }
    }

    var calculateAllPrices = function (charges, service, writerCharge, selectedService) {

        uc.projectModel.projects.forEach(function (project) {
            project.totalPrice = 0.00;
            angular.forEach(charges, function (value, key) {
                var price = parseFloat(value) * project.decimalLength;
                project.serviceName = service.name;
                project.serviceId = service._id;
                project.totalPrice = project.totalPrice + price;
                project.totalPrice = project.totalPrice.toFixed(2);
                totalPriceGreaterThantwo(project.totalPrice, function (cost) {
                    project.totalPrice = parseFloat(cost);
                })
            });

            project.writerPrice = 0.00;
            angular.forEach(writerCharge, function (value, key) {
                var price = parseFloat(value) * project.decimalLength;
                project.serviceName = service.name;
                project.serviceId = service._id;
                project.writerPrice = project.writerPrice + price;
                project.writerPrice = project.writerPrice.toFixed(2);
                totalPriceGreaterThantwo(project.writerPrice, function (cost) {
                    project.writerPrice = parseFloat(cost);
                })
            });

        })

        $scope.price = 0;
        for (index in uc.projectModel.projects) {
            $scope.price = uc.projectModel.projects[index].totalPrice + $scope.price
        }

    };


    function addCaptioningCharges(refinedProjectArray, project, service,
        writerCharge, charges, callback) {
        project.totalPrice = 0.00;
        angular.forEach(charges, function (value, key) {
            var price = parseFloat(value) * project.decimalLength;
            project.serviceName = service.name;
            project.serviceId = service._id;
            project.totalPrice = project.totalPrice + price;
            project.totalPrice = project.totalPrice.toFixed(2);
            project.totalPrice = parseFloat(project.totalPrice);
        });

        angular.forEach(writerCharge, function (value, key) {
            var price = parseFloat(value) * project.decimalLength;
            project.serviceName = service.name;
            project.serviceId = service._id;
            project.writerPrice = project.writerPrice + price;
            project.writerPrice = project.writerPrice.toFixed(2);
            project.writerPrice = parseFloat(project.writerPrice);
        });
        callback(project)

    }


    uc.firstClear = false


    var calculateRates = function (selectedService) {
        var charges = {};
        var writerCharge = {};
        switch (selectedService) {
            case 'transcription':

                var service = uc.services.filter(function (key, index) {
                    return key.name == 'transcription';
                });
                if ($scope.customPrice.message.transcription) {
                    if ($rootScope.currentUser.data.user.role == 'writer') {
                        service[0].writerPrice = $scope.customPrice.message.transcription.writerPrice
                    } else {
                        service[0].price = $scope.customPrice.message.transcription.totalPrice
                    }
                }
                service[0].addTranscript = true;
                if (service.length > 0) {
                    charges.serviceCharge = parsePriceToDollar(service[0].price);
                    writerCharge.serviceCharge = parsePriceToDollar(service[0].writerPrice);

                    service[0].paidFeatures.forEach(function (feature, key) {
                        if (uc.projectModel[selectedService][feature]) {
                            var parsedFeature = JSON.parse(uc.projectModel[selectedService][feature]);
                            promise
                                .then(charges[feature] = parsePriceToDollar(parsedFeature.price));
                            var writerParsedFeature = JSON.parse(uc.projectModel[selectedService][feature]);
                            promise
                                .then(writerCharge[feature] = parsePriceToDollar(parsedFeature.price));
                        };
                    });

                }
                calculateAllPrices(charges, service[0], writerCharge);

                break;
            case 'captioning':
                if (uc.firstClear) return console.log('return')
                var copyProject = uc.projectModel.projects

                var arr = []
                var copyProjectArray = [];
                var chargeArr = []
                var service = uc.services.filter(function (key, index) {
                    return key.name == 'captioning';
                });
                if ($scope.customPrice.message == 'No user') {
                    var transcription = defaultService.filter(function (key, index) {
                        return key.name == 'transcription';
                    });
                    var transcriptionAlignment = defaultService.filter(function (key, index) {
                        return key.name == 'transcription-alignment';
                    });
                }

                if ($scope.customPrice.message !== 'No user' && $scope.customPrice.message.captioning) {
                    if ($scope.customPrice.message.captioning.transcription) {
                        var transcription = uc.services.filter(function (key, index) {
                            if (key.name === 'transcription') {
                                if ($scope.customPrice.message.captioning.transcription) {
                                    key.price = $scope.customPrice.message.captioning.transcription.totalPrice
                                    return key
                                } else {
                                    return key
                                }
                            }
                        });
                    } else {
                        var transcription = defaultService.filter(function (key, index) {
                            return key.name == 'transcription';
                        });
                    }
                    if ($scope.customPrice.message.captioning.transcriptionAlignment) {
                        var transcriptionAlignment = uc.services.filter(function (key, index) {
                            if (key.name === 'transcription-alignment') {
                                if ($scope.customPrice.message.captioning.transcriptionAlignment) {
                                    key.price = $scope.customPrice.message.captioning.transcriptionAlignment.totalPrice
                                    return key
                                } else {
                                    return key
                                }
                            }
                        });
                    } else {
                        var transcriptionAlignment = defaultService.filter(function (key, index) {
                            return key.name == 'transcription-alignment';
                        });
                    }

                }


                //PUSHING ALL THE SERVICE
                arr.push(transcription)
                arr.push(transcriptionAlignment)
                arr.push(service)
                uc.firstClear = true
                arr.forEach(function (item, key) {
                    charges.serviceCharge = parsePriceToDollar(item[0].price);
                    writerCharge.serviceCharge = parsePriceToDollar(item[0].writerPrice);
                    item[0].paidFeatures.forEach(function (feature, key) {
                        var parsedFeature = JSON.parse(uc.projectModel[selectedService][feature]);
                        if (item[0].name == "transcription" &&
                            uc.projectModel['transcription']['addTranscript']) {
                            writerCharge.addTranscript = '$1.00/min';
                            charges.addTranscript = '$2.00/min'

                            promise
                                .then(charges['addTranscript'] = parsePriceToDollar(charges.addTranscript));
                            promise
                                .then(writerCharge['addTranscript'] = parsePriceToDollar(writerCharge.addTranscript));

                        }
                        if (item[0].name == "transcription-alignment" &&
                            uc.projectModel['transcription-alignment']['addOpenCaption']) {
                            writerCharge.addOpenCaption = '$1.00/min';
                            charges.addOpenCaption = '$2.00/min'

                            promise
                                .then(charges['addOpenCaption'] = parsePriceToDollar(charges.addOpenCaption));
                            promise
                                .then(writerCharge['addOpenCaption'] = parsePriceToDollar(writerCharge.addOpenCaption));
                        }

                        if (item[0].name != "captioning") {

                            promise
                                .then(charges[feature] = parsePriceToDollar(parsedFeature.price));
                            var writerParsedFeature = JSON.parse(uc.projectModel[selectedService][feature]);
                            promise
                                .then(writerCharge[feature] = parsePriceToDollar(parsedFeature.price));

                        }

                    });

                    if (item[0].name != "captioning") {
                        uc.projectModel.projects.forEach(function (project, index) {
                            var tempProject = Object.assign({}, project);
                            tempProject.writerPrice = 0.00;
                            addCaptioningCharges(copyProject, tempProject, item[0],
                                writerCharge, charges,
                                function (returnedProject) {
                                    copyProjectArray.push(returnedProject);
                                })
                        })
                    } else {
                        uc.projectModel.projects.forEach(function (project, index) {
                            var tempProject = Object.assign({}, project);

                            tempProject.writerPrice = 0.00;
                            tempProject.serviceName = item[0].name;
                            tempProject.serviceId = item[0]._id;
                            tempProject.writerPrice = copyProjectArray[copyProjectArray.length - 1].writerPrice + copyProjectArray[copyProjectArray.length - 2].writerPrice
                            tempProject.totalPrice = copyProjectArray[copyProjectArray.length - 1].totalPrice + copyProjectArray[copyProjectArray.length - 2].totalPrice
                            copyProjectArray.push(tempProject);
                        })
                    }
                })
                uc.projectModel.projects = copyProjectArray;

                this.$apply = $scope.$apply;

                break;
            case 'transcription-alignment':
                var service = uc.services.filter(function (key, index) {
                    return key.name == 'transcription-alignment';
                });
                if ($scope.customPrice.message.transcriptionAlignment) {
                    if ($rootScope.currentUser.data.user.role == 'writer') {
                        service[0].writerPrice = $scope.customPrice.message.transcriptionAlignment.writerPrice
                    } else {
                        service[0].price = $scope.customPrice.message.transcriptionAlignment.totalPrice
                    }
                }

                if (service.length > 0) {
                    writerCharge.serviceCharge = parsePriceToDollar(service[0].writerPrice);
                    charges.serviceCharge = parsePriceToDollar(service[0].price);
                    service[0].paidFeatures.forEach(function (feature, key) {
                        if (uc.projectModel[selectedService][feature]) {
                            var parsedFeature = JSON.parse(uc.projectModel[selectedService][feature]);
                            promise
                                .then(charges[feature] = parsePriceToDollar(parsedFeature.price));
                            var writerParsedFeature = JSON.parse(uc.projectModel[selectedService][feature]);
                            promise
                                .then(writerCharge[feature] = parsePriceToDollar(parsedFeature.price));
                        };
                    });
                }
                calculateAllPrices(charges, service[0], writerCharge);
                break;
            case 'translation':
                var service = uc.services.filter(function (key, index) {
                    return key.name == 'translation';
                });
                if (service.length > 0) {
                    writerCharge.serviceCharge = parsePriceToDollar(service[0].writerPrice);
                    charges.serviceCharge = parsePriceToDollar(service[0].price);
                    service[0].paidFeatures.forEach(function (feature, key) {
                        if (uc.projectModel[selectedService][feature]) {
                            var parsedFeature = JSON.parse(uc.projectModel[selectedService][feature]);
                            promise
                                .then(charges[feature] = parsePriceToDollar(parsedFeature.price));
                            promise
                                .then(writerCharge[feature] = parsePriceToDollar(parsedFeature.price));
                        };
                    });
                }
                calculateAllPrices(charges, service[0], writerCharge);
                break;
        }
    }

    /*=================================================================================================================*/

    $scope.setSort = function (projectData) {
        sortByDate(projectData);
    }

    $scope.accendingByDate = function (project) {
        accending(project);
    }


    function sortByDate(arr) {
        arr.sort(function (a, b) {
            return Number(new Date(b.createdAt)) - Number(new Date(a.createdAt));
        });

        $scope.userProjectData = arr;
    }

    function accending(arr) {
        arr.sort(function (a, b) {
            return Number(new Date(a.createdAt)) - Number(new Date(b.createdAt));
        });

        $scope.userProjectData = arr;
    }



    $scope.next = false;
    /*=========================upload project function===================================================================*/
    var uploadFile = function (bucket, fileData, params, dataIndex) {
        bucket.putObject(params, function (err, data) {
            if (err) {
                // There Was An Error With Your S3 Config
                return false;
            } else {
                // Success!
                var url = "https://s3-us-east-2.amazonaws.com/post-cap-projects-phase-2/";
                if (fileData.name.includes('.')) {
                    fileData.name = fileData.name.split(".").join("")
                }
                var fileName = fileData.name.replace(/ /g, "_");
                var completeUrl = url + fileName;
                var projectModelData = {};
                projectModelData.url = completeUrl;
                projectModelData.name = fileName;
                projectModelData.size = fileData.size;
                projectModelData.type = fileData.type;
                var data = {
                    link: completeUrl
                };
                userService.getVideoInfo(data, function (response) {
                    projectModelData.decimalLength = Math.round(response.data.length.format.duration / 60);
                    var mins = Math.floor(projectModelData.decimalLength); // extract the hours (in 24 hour format)
                    var sec = 60 * (projectModelData.decimalLength - mins);
                    var h = Math.floor(response.data.length.format.duration / 3600);
                    var m = Math.floor(response.data.length.format.duration % 3600 / 60);
                    var s = Math.floor(response.data.length.format.duration % 3600 % 60);
                    if (s > 1) {
                        m = m + 1;
                    }
                    m.toString();
                    var milliseconSplit = sec.toString().split('.')
                    projectModelData.totalPrice = 0.00;
                    projectModelData.writerPrice = 0.00;
                    projectModelData.stringLength = mins.toString()
                    uc.projectModel.projects.push(projectModelData);
                    toaster.pop('success', "Upload", "Project Uploaded");
                    if ($scope.file.length == uc.projectModel.projects.length) {
                        $scope.loader = false;
                        $scope.next = true;
                    }
                    $scope.$apply();
                });
            };
        }).on('httpUploadProgress', function (progress) {
            // Log Progress Information

            uc.projectModel.upload[dataIndex].fileUploadStatus = Math.round(progress.loaded / progress.total * 100) + '%';
            document.getElementById("fileUploadStatus" + dataIndex).style.width = uc.projectModel.upload[dataIndex].fileUploadStatus;
            $scope.$apply();
        });
    };
    /*=========================upload project function end=================================================*/



    $scope.projectStatus = true;
    /* =================================submitProjct=============================*/
    $scope.makepayment = false;
    $scope.createdProjects = []
    $scope.projectIds = []
    uc.submitProject = function () {
        $rootScope.loader = true;
        var captionReference;

        var projectArray = []

        uc.projectModel.projects.forEach(function (project, key) {
            var mainCaptionFlag = false;
            if (uc.projectModel.currentService == "captioning") { /* stop seeing other projects to client admin*/
                if (project.serviceName == "captioning") { // stop job creation
                    mainCaptionFlag = true;
                }
                if (uc.projectModel.CaptionFileOutput.length > 0) {
                    uc.projectModel.selectedService.CaptionFileOutput = uc.projectModel.CaptionFileOutput
                }
                var postData = {
                    name: project.name,
                    duration: project.stringLength,
                    projectUrl: project.url,
                    totalPrice: [project.totalPrice],
                    writerPrice: [project.writerPrice],
                    size: project.size,
                    mediaType: project.type || 'video',
                    service: project.serviceId,
                    mainCaptionFlag: mainCaptionFlag,
                    clientUser: $rootScope.currentUser.data.user.id,
                    selectedService: uc.projectModel.selectedService,
                    specialInstruction: uc.projectModel.specialInstruction
                };
                projectArray.push(postData)
            } else {
                var form = {
                    name: project.name,
                    duration: project.stringLength,
                    projectUrl: project.url,
                    totalPrice: [project.totalPrice],
                    writerPrice: [project.writerPrice],
                    size: project.size,
                    mediaType: project.type || 'video',
                    service: project.serviceId,
                    mainCaptionFlag: mainCaptionFlag,
                    clientUser: $rootScope.currentUser.data.user.id,
                    selectedService: uc.projectModel.selectedService,
                    specialInstruction: uc.projectModel.specialInstruction
                };
                userService.projectSubmit(form, function (response) {
                    $scope.createdProjects.push(response.data)
                    for (i in $scope.createdProjects) {
                        $scope.projectIds.push($scope.createdProjects[i].project._id)
                    }
                    if (response.data.project.paymentStats == true) {
                        $scope.makepayment = false;
                        $scope.projectStatus = false;
                        toaster.pop('success', "Project", "Project Submitted And Payment Done!");
                        $state.go('dashboard.overview');

                    } else {
                        $scope.makepayment = true;
                        $scope.projectStatus = false;
                        toaster.pop({
                            type: 'success',
                            title: 'Project',
                            body: 'Project Submitted',
                            timeout: 1000
                        });
                        // toaster.pop('success', "Project", "Project Submitted");
                    }
                    if (uc.projectModel.projects.length == (key + 1)) {
                        $rootScope.loader = false;

                        // $scope.$apply();
                    };
                });
            }
            // }
        })
        if (uc.projectModel.currentService == "captioning") {
            projectArray.reverse();
            var postData = {
                projects: projectArray,
                service: "captioning"
            }
            userService.projectSubmit(postData, function (response) {
                // response.forEach
                $scope.createdProjects.push(response.data)
                for (i in $scope.createdProjects) {
                    $scope.projectIds.push($scope.createdProjects[i].project._id)
                }

                if (response.data.project[0].paymentStats == true) {
                    $scope.makepayment = false;
                    $scope.projectStatus = false;
                    toaster.pop('success', "Project", "Project Submitted And Payment Done!");
                    $state.go('dashboard.overview');
                } else {
                    $scope.makepayment = true;
                    $scope.projectStatus = false;
                    toaster.pop('success', "Project", "Project Submitted");
                }




                if (uc.projectModel.projects.length == (key + 1)) {
                    toaster.pop('success', "Project", "Project Submitted");
                    $rootScope.loader = false;

                    $scope.$apply();
                };
            });
        }

    };

    /* =================================submitProjct End=========================*/


    $scope.isLoading = false
    $scope.getuserCards = () => {
        $scope.isLoading = true
        dataService.getuserCards(response => {
            $scope.isLoading = false
            $scope.Cards = response.data.cards;
        })
    }

    $scope.makepaymentWithSavedCard = (card, amount) => {
        // makePaymentWithSaved
        card.amount = 10
        dataService.makePaymentWithSaved(card, response => {
            $scope.isLoading = false
            $scope.Cards = response.data.cards;
        })
    }

    uc.getvideoInfo = function (completeUrl) {
        var data = {
            link: completeUrl
        };
        userService.getVideoInfo(data, function (response) {

        })
    }

    // // convert minutes to seaconds
    function convert(input) {
        var parts = input.split(':');
        var minutes = +parts[parts.length - 2];
        var seconds = +parts[parts.length - 1];
        var hours = +parts[parts.length - 3];
        if (parts.length == 1) {
            return input;
        } else if (parts.length == 2) {
            return (minutes * 60 + seconds).toFixed(3);
        } else if (parts.length == 3) {

            return (hours * 3600 + minutes * 60 + seconds).toFixed(3);
        }

    }


    $scope.videoList = [];

    $scope.videoAdd = function (video) {
        $scope.videoList.push({
            videoLink: video,
            done: false
        });
        $scope.uc.videoLink = "";
    };
    $scope.done = false

    $scope.eventCheck = function (item) {
        $scope.done = true
    }

    $scope.remove = function () {
        var oldList = $scope.videoList;
        $scope.videoList = [];
        angular.forEach(oldList, function (x) {
            if (!x.done) $scope.videoList.push(x);
        });
    };

    /* =================================video End=========================*/
    // create projects by link
    // @outsideLink
    $scope.newLoader = false;

    uc.submitLink = function () {

        var postData = {
            link: $scope.videoList
        }
        $scope.newLoader = true;
        dataService.getVideoLinkInfo(postData, function (response) {
            console.log('', response)
            if (response.data.success) {
                angular.forEach(response.data.data, function (item) {
                    var projectModelData = {}
                    projectModelData.name = item.fulltitle;
                    projectModelData.url = item.webpage_url;
                    projectModelData.mediaType = item.ext;
                    var duration = convert(item.duration);
                    projectModelData.decimalLength = Math.round(duration / 60);
                    // to convert sec to hr
                    var h = Math.floor(duration / 3600);
                    var m = Math.floor(duration % 3600 / 60);
                    var s = Math.floor(duration % 3600 % 60);
                    if (s >= 1) {
                        m = m + 1;
                    }
                    var mins = Math.floor(projectModelData.decimalLength); // extract the hours (in 24 hour format)
                    var sec = 60 * (projectModelData.decimalLength - mins);
                    var milliseconSplit = s.toString().split('.')
                    projectModelData.stringLength = mins.toString()
                    projectModelData.totalPrice = 0.00;
                    uc.projectModel.projects.push(projectModelData);
                    uc.linkInformation = item;
                });
                $scope.newLoader = false;
                $('#addLinkModal').modal('hide');
            } else {
                toaster.pop('warning', response.data.message);
            }

        })
    }

    $scope.projectType = function (project) {
        $state.go('dashboard.newProject', {
            type: 'social',
            data: project
        });
        uc.paramsData = $state.params;
    }


    $scope.oneAtATime = true;

    $scope.initUser = function () {
        userService.getUserById($rootScope.currentUser.data.user.id, function (response) {
            $rootScope.currentUser.data.user.driveToken = response.data.drive;
            $rootScope.currentUser.data.user.youTubeToken = response.data.youTube;
            $rootScope.currentUser.data.user.VimeoToken = response.data.vimeo
            // console.log('root',$rootScope);
        });
    };


    $scope.loaderForDrive = false

    $scope.getVideosForSocial = function () {
        var postData = {
            userId: $rootScope.currentUser.data.user.id
        }
        $scope.loaderForDriver = true;
        dataService.getDriveDetails(postData, function (response) {

            if (response.data.code == 400) {
                toaster.pop('warning', "Your token is expired, Please re-connect.");
                $rootScope.loader = false;
            } else {
                $scope.videoData = response.data;
                $scope.loaderForDrive = false;
            }
        })
    }

    $scope.getYoutubeData = function () {

        var postData = {
            'userId': $rootScope.currentUser.data.user.id
        }
        $rootScope.loader = true;
        dataService.getYoutubeData(postData, function (response) {
            if (response.data.code == 400) {
                toaster.pop('warning', "Your token is expired, Please re-connect.");
                $rootScope.loader = false;
            } else {
                $scope.youtubeData = response.data;
                $rootScope.loader = false;
            }
        })
    }

    //vimeo data
    $scope.vimeoLoader = false
    $scope.getVimeoDetail = function () {
        var postData = {
            'userId': $rootScope.currentUser.data.user.id
        }
        $rootScope.loader = true;
        dataService.getVimeoDetail(postData, function (response) {
            $scope.vimeoData = response.data;
            $scope.vimeoLoader = true;
        })
    }

    $scope.nextStep = function (durationMillis, webViewLink, size, name) {
        var projectModelData = {}
        projectModelData.name = name;
        projectModelData.url = webViewLink;
        projectModelData.size = size;
        //convert milli sec to sec
        var durationToSeconds = (durationMillis / 1000) // round to nearest second
        projectModelData.decimalLength = durationToSeconds / 60;
        // to convert sec to hr
        var h = Math.floor(durationToSeconds / 3600);
        var m = Math.floor(durationToSeconds % 3600 / 60);
        var s = Math.floor(durationToSeconds % 3600 % 60);

        var mins = Math.floor(projectModelData.decimalLength); // extract the hours (in 24 hour format)
        var sec = 60 * (projectModelData.decimalLength - mins);
        var milliseconSplit = s.toString().split('.')
        // projectModelData.stringLength = h.toString() + ':' +
        //     m.toString() + ':' + s.toString();
        projectModelData.stringLength = mins.toString()

        // }
        projectModelData.totalPrice = 0.00;
        uc.projectModel.projects.push(projectModelData);
    }

    $scope.vimeoNextStep = function (duration, link, filesize, name, type) {
        var projectModelData = {}
        projectModelData.name = name;
        projectModelData.url = link;
        projectModelData.size = filesize;
        //convert milli sec to sec
        // var durationToSeconds = (duration / 1000) // round to nearest second
        projectModelData.decimalLength = Math.round(duration / 60);
        projectModelData.type = type;
        // to convert sec to hr
        var h = Math.floor(duration / 3600);
        var m = Math.floor(duration % 3600 / 60);
        var s = Math.floor(duration % 3600 % 60);
        // end
        // sir logic
        var mins = Math.floor(projectModelData.decimalLength); // extract the hours (in 24 hour format)
        var sec = 60 * (projectModelData.decimalLength - mins);
        var milliseconSplit = s.toString().split('.')
        // sir logic
        // if (milliseconSplit.length == 1) {
        //     projectModelData.stringLength = 0 + ':' + milliseconSplit[0].toString() + ':' + 00;
        // } else {
        // projectModelData.stringLength = h.toString() + ':' +
        //     m.toString() + ':' + s.toString();
        // }
        projectModelData.stringLength = mins.toString()

        projectModelData.totalPrice = 0.00;
        uc.projectModel.projects.push(projectModelData);
    }

    $scope.youtubeNextStep = function (duration, webpage_url, fulltitle) {
        var projectModelData = {}
        projectModelData.name = fulltitle;
        projectModelData.url = webpage_url;

        // var durationToSeconds = (duration / 1000) // round to nearest second
        projectModelData.decimalLength = Math.round(duration / 60);
        // to convert sec to hr
        var h = Math.floor(duration / 3600);
        var m = Math.floor(duration % 3600 / 60);
        var s = Math.floor(duration % 3600 % 60);
        if (s > 1) {
            m = m + 1;
        }
        var mins = Math.floor(projectModelData.decimalLength); // extract the hours (in 24 hour format)
        var sec = 60 * (projectModelData.decimalLength - mins);
        var milliseconSplit = s.toString().split('.')
        // projectModelData.stringLength = h.toString() + ':' +
        //     m.toString();
        projectModelData.stringLength = mins.toString()
        projectModelData.totalPrice = 0.00;
        uc.projectModel.projects.push(projectModelData);
    }

    // dropbox
    $scope.nextStepDropBox = function (name, link, filesize, duration) {
        var projectModelData = {}
        projectModelData.name = name;
        projectModelData.url = link;
        projectModelData.size = filesize;
        // var durationToSeconds = (duration / 1000) // round to nearest second
        projectModelData.decimalLength = Math.round(duration / 60);
        // to convert sec to hr
        var h = Math.floor(duration / 3600);
        var m = Math.floor(duration % 3600 / 60);
        var s = Math.floor(duration % 3600 % 60);
        if (s > 1) {
            m = m + 1;
        }
        var mins = Math.floor(projectModelData.decimalLength); // extract the hours (in 24 hour format)
        var sec = 60 * (projectModelData.decimalLength - mins);
        var milliseconSplit = s.toString().split('.')
        // projectModelData.stringLength = h.toString() + ':' +
        //     m.toString();
        projectModelData.stringLength = mins.toString()
        projectModelData.totalPrice = 0.00;
        projectModelData.writerPrice = 0.00;
        uc.projectModel.projects.push(projectModelData);
    }

    $scope.connectToQuickbooks = () => {
        $http({
            method: 'GET',
            url: '/api/paymentIntegrationQuickbooks'
        }).then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.makePaymentPageRender = () => {
        dataService.renderPaymentPage(function (response) {
            // $rootScope.loader = false;
        })
    }

    $scope.submitCardForm = (card) => {
        // console.log(card);
        let cardObject = {
            "card": {
                "expYear": "2020",
                "expMonth": "02",
                "address": {
                    "region": "CA",
                    "postalCode": "94086",
                    "streetAddress": "1130 Kifer Rd",
                    "country": "US",
                    "city": "Sunnyvale"
                },
                "name": "emulate=0",
                "cvc": "123",
                "number": "4111111111111111"
            }
        }
        // /api/genrateToken
        // var req = {
        //     method: 'POST',
        //     url: 'https://sandbox.api.intuit.com/quickbooks/v4/payments/tokens',
        //     headers: {
        //       'Content-Type': 'application/json'
        //     },
        //     data: cardObject
        //    }

        var req = {
            method: 'POST',
            url: 'http://localhost:3000/api/genrateToken',
            headers: {
                'Content-Type': 'application/json'
            },
            data: cardObject
        }



        $http(req).then(function successCallback(response) {
            $scope.token = response.data;
        }, function errorCallback(response) {});
    }

}]);


/* ==============================file select directive====================================*/

angular.module('app').directive('file', function () {
    return {
        restrict: 'AE',
        scope: {
            file: '@'
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                // var file = files[0];
                scope.file = files;
                scope.$parent.$parent.file = scope.file;
                scope.$apply();
            });
        }
    };
});

/* ==============================file select directive end====================================*/
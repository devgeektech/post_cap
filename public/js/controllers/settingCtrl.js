var app = angular.module('app')
    .controller('settingCtrl', ['$scope', '$http', '$rootScope', 'dataService', 'toaster', '$state', function ($scope, $http, $rootScope, dataService, toaster, $state) {
        const convertTimeToHours = (time) => {
            let newTime = time.toString().split(" ")[4];
            return newTime;
        }

        function settingTime() {
            dataService.getSetting(response => {
                $scope.isCreated = response.data
            })
        }
        settingTime()

        function getUserNotificationTime() {
            dataService.getMessageTime(response => {
                $scope.notificationTime = response.data.data.notificationTime
            })
        }

        getUserNotificationTime()

        $scope.saveTime = (time) => {
            dataService.messageSettingTime({
                time: JSON.parse(time)
            }, response => {
                toaster.pop('success', response.data.message);
                getUserNotificationTime()
                $state.go('dashboard.overview');
            })
        }

        $scope.autoAssigner = (time) => {
            // let newTime = convertTimeToHours(time)
            console.log(time)
            dataService.autoAssigner({
                time: JSON.parse(time)
            }, response => {
                toaster.pop('success', response.data.message);
                $state.go('dashboard.overview');
            })
        }


        $scope.timeObject = [{
                "key": "600",
                "value": "10 minutes"
            },
            {
                "key": "1200",
                "value": "20 minutes"
            },
            {
                "key": "1800",
                "value": "30 minutes"
            },
            {
                "key": "2700",
                "value": "45 minutes"
            },
            {
                "key": "3600",
                "value": "60 minutes"
            },
            {
                "key": "7200",
                "value": "120 minutes"
            }
        ]


        $scope.userNotificationDelay = [{
                "key": "300",
                "value": "5 minute"
            },
            {
                "key": "600",
                "value": "10 minutes"
            },
            {
                "key": "900",
                "value": "15 minutes"
            },
            {
                "key": "1200",
                "value": "20 minutes"
            },
            {
                "key": "1600",
                "value": "25 minutes"
            },
            {
                "key": "2000",
                "value": "30 minutes"
            },
            {
                "key": "3600",
                "value": "1 hours"
            },
            {
                "key": "7200",
                "value": "2 hours"
            },
            {
                "key": "10800",
                "value": "3 hours"
            },
            {
                "key": "14400",
                "value": "4 hours"
            },
            {
                "key": "18000",
                "value": "5 hours"
            },
            {
                "key": "21600",
                "value": "6 hours"
            },
            {
                "key": "43200",
                "value": "12 hours"
            },
            {
                "key": "86400",
                "value": "1 day"
            }
        ]
        $scope.edit = false
        $scope.EditAssignTime = function () {
            $scope.edit = true
        }

        $scope.editStaus = (status) => {
            // let timeFormat = time.toString().split(" ")[4];
            dataService.editSetting({
                status: status,
                id: $scope.isCreated.message._id
            }, response => {
                toaster.pop('success', response.data.message);
                // $state.go('dashboard.overview');
                $scope.edit = false
                settingTime()
            })
        }

        $scope.editSetting = (time) => {
            // let timeFormat = time.toString().split(" ")[4];
            console.log(time)
            dataService.editSetting({
                time: JSON.parse(time),
                id: $scope.isCreated.message._id
            }, response => {
                toaster.pop('success', response.data.message);
                // $state.go('dashboard.overview');
                $scope.edit = false
                settingTime()
            })
        }
        $scope.message = ''
        $scope.nextStep = false
        $scope.validatePassword = (password) => {
            dataService.verifyPassword({
                password
            }, response => {
                if (response.data.success) {
                    $scope.nextStep = true
                    password = ""
                    $scope.message = ''
                } else {
                    $scope.message = response.data.message
                }
            })
        }

        $scope.setPassword = (newPassword,confirm)=>{
            if(newPassword.length < 6) return $scope.message = 'Your password must be at least 6 characters'
            if(newPassword !== confirm) return $scope.message = 'Password Do Not Match!'
            dataService.changePassword({
                newPassword,
                confirm
            }, response => {
                if(response.data.success){
                    toaster.pop('success', response.data.message);
                    $state.go('dashboard.overview');
                }else{
                    $scope.message = response.data.message
                }
            })

        }


        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };


    }]);
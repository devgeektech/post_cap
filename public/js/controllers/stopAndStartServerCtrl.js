var app = angular.module('app')
    .controller('startAndStopServerController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', 'toaster', function($scope, $http, $rootScope, dataService, $stateParams, toaster) {
        // stop server move to new controller
        $rootScope.loader = false;
        $scope.stopServer = function() {
            $rootScope.loader = true;
            dataService.stopServer(function(response) {
                $scope.status = 'Server Stopped'
                $rootScope.loader = false;
                toaster.pop('success', "Server is stopped.");
            });
        }

        $scope.startServer = function() {
            $rootScope.loader = true;
            dataService.startServer(function(response) {
                $scope.status = 'Server Started.'
                $rootScope.loader = false;
                toaster.pop('success', "Server is started.");
            });
        }
        $scope.newloader = false;
        $scope.outputLog = (type) => {
            if (type == 'output') {
                $scope.newloader = true;
                $scope.sameName = ''
                dataService.outputLog(function(response) {
                    $scope.newloader = false;
                    $scope.outputdata = response.data.data;
                });
            } else if (type == 'input') {
                $scope.newloader = true;
                $scope.sameName = ''
                dataService.inputLog(function(response) {
                    $scope.newloader = false;
                    $scope.outputdata = response.data.data;
                });
            } else if (type == 'caption') {
                $scope.sameName = ''
                $scope.newloader = true;
                dataService.captionLog(function(response) {
                    $scope.newloader = false;
                    $scope.outputdata = response.data.data;
                });
            } else if (type == 'server') {
                $scope.outputdata = ''
                $scope.newloader = true;
                dataService.serverLog(function(response) {
                    $scope.newloader = false;
                    // $scope.serverData = response.data.data;
                    var data = response.data.data;
                    $scope.sameName = [];
                    var objectData = {}
                    data.inputDataArray.forEach(function(input, i) {
                        var found = false;
                        data.outputDataArray.forEach(function(output, j) {
                            if (input.name == output.name) {
                                var finalObject = {
                                    name: input.name,
                                    inputSize: input.dateAndSize,
                                    outputSize: output.dateAndSize
                                }
                                $scope.sameName.push(finalObject);
                                found = true
                            }
                        })
                        if (found == false) {
                            var finalObject = {
                                name: input.name,
                                inputSize: input.dateAndSize,
                                outputSize: '-'
                            }
                            $scope.sameName.push(finalObject);
                        }
                    })

                });
            }
        }


        $scope.retartFile = (name) => {
            var postData = {
                filename: "output_" + name
            }
            dataService.restartFile(postData, function(response) {
                $scope.newloader = false;
                // $scope.outputdata = response.data.data;
            });
        }



    }]);
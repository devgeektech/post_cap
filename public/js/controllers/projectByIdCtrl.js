var app = angular.module('app')
    .controller('ProjectByIdController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', 'toaster', '$state', '$timeout', function ($scope, $http, $rootScope, dataService, $stateParams, toaster, $state, $timeout) {
        var serviceRequested;
        $scope.initProjectById = function () {
            var id = $stateParams.id;

            dataService.getProjectById({
                id: $stateParams.id
            }, function (response) {
                console.log('==========>', response)
                if (response.data.job.appliedUser.length > 0 && response.data.job.appliedUser.includes($rootScope.currentUser.data.user.id)) {
                    response.data.Applied = true
                } else {
                    response.data.Applied = false
                }

                if ($rootScope.currentUser.data.user.role == 'writer') {
                    dataService.getUserPriceTable({
                        id: $rootScope.currentUser.data.user.id
                    }, (customRates) => {
                        if(customRates.data.message == "No user"){
                            return $scope.productData = response;
                        } 
                        if(!customRates.data.message.transcription || !customRates.data.message.transcriptionAlignment) return $scope.productData = response;
                        if (customRates.data.message.transcription.writerPrice || customRates.data.message.transcriptionAlignment.writerPrice) {
                            let returnData ={}
                            let returnArray = convertToCustomWriterPrice([response.data], customRates.data.message)
                            returnData.data = returnArray[0]
                            $scope.productData = returnData
                        };
                    })
                }else{
                    $scope.productData = response;
                }



                
                let serviceSelected = response.data.selectedService
                $scope.serviceSelected = {}
                serviceRequested = response.data.service.name
                if (response.data.service.name == 'transcription') {
                    $scope.serviceSelected.addTranscript = serviceSelected.addTranscript;
                    $scope.serviceSelected.addOpenCaption = serviceSelected.addOpenCaption;
                    $scope.serviceSelected.turnaroundSelected = JSON.parse(serviceSelected.turnaroundSelected)
                    $scope.serviceSelected.techniqueSelected = JSON.parse(serviceSelected.techniqueSelected)
                    $scope.serviceSelected.accentSelected = JSON.parse(serviceSelected.accentSelected)
                    $scope.serviceSelected.captionServiceSelected = serviceSelected.CaptionFileOutput
                }
                if (response.data.service.name == 'captioning') {
                    $scope.serviceSelected.addTranscript = serviceSelected.addTranscript;
                    $scope.serviceSelected.addOpenCaption = serviceSelected.addOpenCaption;
                    $scope.serviceSelected.quality = serviceSelected.quality
                    $scope.serviceSelected.accentSelected = JSON.parse(serviceSelected.accentSelected)
                    $scope.serviceSelected.techniqueSelected = JSON.parse(serviceSelected.techniqueSelected)
                    $scope.serviceSelected.turnaroundSelected = JSON.parse(serviceSelected.turnaroundSelected)
                    $scope.serviceSelected.captionServiceSelected = serviceSelected.CaptionFileOutput
                }
                if (response.data.service.name == 'translation') {
                    $scope.serviceSelected.accentSelected = JSON.parse(serviceSelected.accentSelected)
                    $scope.serviceSelected.turnaroundSelected = JSON.parse(serviceSelected.turnaroundSelected)
                }
                if (response.data.service.name == 'transcription-alignment') {
                    $scope.serviceSelected.addTranscript = serviceSelected.addTranscript;
                    $scope.serviceSelected.addOpenCaption = serviceSelected.addOpenCaption;
                    $scope.serviceSelected.quality = serviceSelected.quality
                    $scope.serviceSelected.accentSelected = JSON.parse(serviceSelected.accentSelected)
                    $scope.serviceSelected.turnaroundSelected = JSON.parse(serviceSelected.turnaroundSelected)
                    $scope.serviceSelected.captionServiceSelected = serviceSelected.CaptionFileOutput
                }
            });
        }
        $scope.initProjectById();

        const convertToCustomWriterPrice =  (projects, customPrice) => {
            let calculatedData =  projects.map(function (item) {
                let serviceSelected = {}
                if(item.assignTime) return item
                if (item.selectedService.turnaroundSelected) serviceSelected.turnAround = JSON.parse(item.selectedService.turnaroundSelected)

                if (item.selectedService.techniqueSelected) serviceSelected.techniqueSelected = JSON.parse(item.selectedService.techniqueSelected)

                if (item.selectedService.accentSelected && item.service.name == 'transcription-alignment') serviceSelected.accentSelected = JSON.parse(item.selectedService.accentSelected)

                if (customPrice.transcription.writerPrice && item.service.name == 'transcription') {
                    customPrice.transcription.price = customPrice.transcription.writerPrice;

                    serviceSelected.serviceCharge = customPrice.transcription
                } else if (customPrice.transcriptionAlignment.writerPrice && item.service.name == 'transcription-alignment') {
                    customPrice.transcriptionAlignment.price = customPrice.transcriptionAlignment.writerPrice;

                    serviceSelected.serviceCharge = customPrice.transcriptionAlignment
                }

                let writerPrice = 0.00
                angular.forEach(serviceSelected, function (value, key) {
                    let val = parseFloat(value.price.replace(/[^0-9\.]+/g, ""))
                    var price = val * parseInt(item.duration);
                    writerPrice = writerPrice + price;
                    item.writerPrice[item.writerPrice.length - 1] = writerPrice.toFixed(2);
                });
                return item;
            })
            return calculatedData
        }

        //  $scope.assignCheck = false;
        $scope.getWriterList = function () {
            dataService.getWriterList(function (response) {
                $scope.WritersData = []
                response.data.userArray.forEach(element => {
                    if (element.transcriber && serviceRequested == 'transcription') {
                        $scope.WritersData.push(element)
                    }
                    if (element.transcriptionAlignment && serviceRequested == 'transcription-alignment') {
                        $scope.WritersData.push(element)
                    }
                    // }
                });
            });
        }
        $scope.getWriterList();

        $scope.deleteProject = function () {
            dataService.deleteProject({
                id: [$stateParams.id]
            }, function (response) {
                if (!response.data.success) {
                    toaster.pop('warning', response.data.message);
                } else {
                    toaster.pop('success', response.data.message);
                    $timeout(function () {
                        $state.go('dashboard.overview')
                    }, 2000);
                }
            });
        }

        $scope.assignJobToWriter = function (writer) {
            var postData = {
                projectId: $stateParams.id,
                userId: $rootScope.currentUser.data.user.id,
                writerId: writer._id
            };

            dataService.assignJobToWriter(postData, function (response) {
                toaster.pop('success', "Assign to User: " + writer.firstname + " " + writer.lastname);
                $scope.initProjectById();
            })
        }

    }]);
var app = angular.module('app')
    .controller('mailController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', '$state', 'toaster', '$timeout', function ($scope, $http, $rootScope, dataService, $stateParams, $state, toaster, $timeout) {
        $scope.loadProgress = "0";


        // $scope.stateData = $state

        if ($stateParams.data) {
            $scope.emailAddress = $stateParams.data;
            $scope.fullname = $stateParams.FullName
            console.log('state', $stateParams)
        }

        if ($stateParams.subject) {
            $scope.subject = $stateParams.subject;
            $scope.emailAddress = $stateParams.data;
            $scope.mailBody = $stateParams.message
        }

        if ($stateParams.type == 'MuLtiPleEmails') {
            $scope.emailAddress = '';
            $scope.fullname = ''
            angular.forEach($stateParams.data, (item, index) => {
                $scope.emailAddress = item.email + ", " + $scope.emailAddress
                $scope.fullname = item.fullname + ", " + $scope.fullname
            })
        }

        $scope.goToAddressBook = function (user) {
            $state.go('dashboard.composeMail', {
                data: user.email,
                FullName: user.firstname + " " + user.lastname
            });
        }

        $scope.getAllUserForMessage = function (mailId, check) {
            var query = {
                'role': $rootScope.currentUser.data.user.role,
            }
            dataService.getAllUserForMessage(query, function (response) {
                $scope.userList = response.data;
            });
        }

        $scope.discardMessage = () => {
            $scope.subject = ''
            $scope.mailBody = ''
            $scope.emailAddress = '';
            $scope.fullname = ''
        }

        //sendmail
        $scope.sent = true;

        $scope.sendMail = function (data) {
            var postdata = {
                'userId': $rootScope.currentUser.data.user.id,
                'subject': $scope.subject,
                'body': $scope.mailBody,
                'senderEmail': [$scope.emailAddress],
                'attachment': $scope.completeUrl
            };
            var flag = true;
            if (flag) {
                var emailAddress = $scope.emailAddress.split(', ')
                emailAddress.pop()
                $scope.userList.forEach(function (item) {
                    if ($stateParams.type == 'MuLtiPleEmails') {

                        angular.forEach(emailAddress, function (email, key) {
                            if (item.email == email) {
                                postdata.senderEmail = []
                                postdata.senderEmail.push(email)
                                dataService.sendmail(postdata, function (response) {
                                    if (response.data.error) {
                                        toaster.pop('error', "User Not Found");
                                    } else {
                                        $scope.sent = false;
                                        toaster.pop('success', "Message is sent");
                                        postdata.senderEmail = []
                                        // $timeout(function () {
                                        $state.go('dashboard.mail');
                                        // }, 1000);
                                    }
                                });
                            }

                        })
                    } else {
                        if (item.email == $scope.emailAddress) {
                            dataService.sendmail(postdata, function (response) {
                                if (response.data.error) {
                                    toaster.pop('error', "User Not Found");
                                } else {
                                    $scope.sent = false;
                                    toaster.pop('success', "Message is sent");
                                    $timeout(function () {
                                        $state.go('dashboard.mail');
                                    }, 1000);
                                }

                            });
                        }
                    }
                })
            }
        }

        $scope.addToDraft = function (data) {
            var postdata = {
                'subject': data.subject,
                'body': data.body,
                'senderEmail': data.to || '',
                'attachment': $scope.completeUrl
            };
            dataService.addToDraft(postdata, function (response) {
                toaster.pop('success', response.data);
            });
        }
        $scope.discard = function (data) {
            console.log(data)
            data.body = "";
            data.to = "";
            data.subject = "";
        }

        //    get mails

        $scope.initGetMail = function () {
            var query = {
                'userId': $rootScope.currentUser.data.user.id,
                'data': 'inbox'
            }
            dataService.getMail(query, function (response) {
                console.log(response);
                $scope.mailData = response.data;

            });
        }

        $scope.initGetsendMail = function () {
            var query = {
                'userId': $rootScope.currentUser.data.user.id,
                'data': 'sent'
            }
            dataService.getMail(query, function (response) {
                $scope.sentData = response.data;
            });
        }

        $scope.initGetTrashMail = function () {
            var query = {
                'userId': $rootScope.currentUser.data.user.id,
                'data': 'trash'
            }
            dataService.getMail(query, function (response) {
                $scope.trashData = response.data;
            });
        }

        $scope.initGettrashMail = function () {
            var query = {
                'userId': $rootScope.currentUser.data.user.id,
                'data': 'important'
            }
            dataService.getMail(query, function (response) {
                $scope.importantData = response.data;
            });
        }

        $scope.initDraftMail = function () {
            var query = {
                'userId': $rootScope.currentUser.data.user.id,
                'data': $state.params.type
            }
            dataService.getMail(query, function (response) {
                $scope.draftData = response.data;
            });
        }
        $scope.disableAddressbook = false;
        $scope.addressbookEmails = []



        $scope.selectMultipleEmail = (user, check) => {
            if (check) {
                $scope.addressbookEmails.push({
                    email: user.email,
                    fullname: user.firstname + " " + user.lastname
                });
            } else {
                var index = $scope.addressbookEmails.indexOf({
                    email: user.email,
                    fullname: user.firstname + " " + user.lastname
                });
                $scope.addressbookEmails.splice(index);
            }
            if ($scope.addressbookEmails.length == 0) {
                $scope.disableAddressbook = false;
            } else {
                $scope.disableAddressbook = true;
            }
        }

        $scope.mailSelected = () => {
            $state.go('dashboard.composeMail', {
                data: $scope.addressbookEmails,
                type: 'MuLtiPleEmails'
            });
        }

        //  get mails by id

        $scope.getMailById = function () {
            var mailId = $stateParams.mailId;
            dataService.getMailById(mailId, function (response) {
                console.log(response)
                $scope.mailInfo = response.data;
            });
        }

        //importantMail
        $scope.importantMail = function (messageId, check) {
            var query = {
                'check': check,
                'userId': $rootScope.currentUser.data.user.id,
                'messageId': messageId
            }
            dataService.importantMail(query, function (response) {

            });
        }

        // mail attachment

        $scope.creds = {
            bucket: 'post-cap-projects-phase-2',
            access_key: 'AKIAJ3RYIKD5JYFSYGWQ',
            secret_key: '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'
        }

        $scope.projectModel = {
            upload: {
                project1: {}
            },
            projects: [],
            currentService: "",
            transcription: {},
            captioning: {
                addOpenCaption: false,
                addTranscript: false
            },
            translation: {}
        };
        var messageIdArray = [];

        $scope.multipleMails = function (mailId, check) {
            if (check) {
                messageIdArray.push(mailId);
            } else {
                var index = messageIdArray.indexOf(mailId);
                messageIdArray.splice(index);
            }

        }



        $scope.moveToTrashForBulk = function (location) {

            messageIdArray.forEach(function (item) {
                var dataPost = {
                    'userId': $rootScope.currentUser.data.user.id,
                    'messageId': item,
                    'locationType': location
                }
                dataService.moveToTrash(dataPost, function (response) {
                    console.log(response);
                    // $scope.mailInfo = response.data;
                });
            })
            if (location == 'sent') {
                $scope.initGetsendMail();
            } else if (location == 'important') {
                $scope.initGettrashMail();
            } else {
                $scope.initGetMail();
            }
        }



        $scope.attachUpload = function () {
            AWS.config.update({
                accessKeyId: $scope.creds.access_key,
                secretAccessKey: $scope.creds.secret_key
            });
            AWS.config.region = 'us-east-2';
            var bucket = new AWS.S3({
                params: {
                    Bucket: $scope.creds.bucket
                }
            });
            var params = {}

            var filedata = {}
            var dataIndex;
            if ($scope.file == null) {
                toaster.pop('error', "No File Selected");
            } else {
                angular.forEach($scope.file, function (item, index) {
                    console.log(item);
                    params.Key = item.name;
                    params.ContentType = item.type;
                    params.Body = item;
                    params.ServerSideEncryption = 'AES256';
                    filedata.data = item;
                    dataIndex = 'project' + (index + 1).toString()
                })

                bucket.putObject(params, function (err, data) {
                        if (err) {
                            // There Was An Error With Your S3 Config
                            console.log(err);
                            return false;
                        } else {
                            var url = "https://s3-us-east-2.amazonaws.com/post-cap-projects-phase-2/";
                            var fileName = filedata.data.name.replace(/ /g, "+");
                            $scope.completeUrl = url + fileName;
                        }
                    })
                    .on('httpUploadProgress', function (progress) {
                        $scope.loadProgress = Math.round(progress.loaded / progress.total * 100);
                        $scope.$apply();
                    });

            }

        }


    }])
    .controller('viewMailController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', '$timeout', 'toaster', '$state', function ($scope, $http, $rootScope, dataService, $stateParams, $timeout, toaster, $state) {
        //  get mails by id
        $scope.getMailById = function () {
            var mailId = $stateParams.mailId;
            dataService.getMailById(mailId, function (response) {
                console.log(response);
                $scope.mailInfo = response.data;
            });
        }
        $scope.getMailById();

        //  reply to the email
        $scope.disable = false
        $scope.replyMail = function (body, to, id, subject) {
            var postData = {
                'senderEmail': [to],
                'body': body,
                'userId': $rootScope.currentUser.data.user.id,
                "messageId": id,
                "type": 'reply',
                "subject": subject
            }
            $scope.disable = true

            dataService.sendmail(postData, function (response) {
                $scope.disable = false
                if (response.data.error) {
                    toaster.pop('error', "User Not Found");
                } else {
                    toaster.pop('success', "Message is sent");
                    $state.go('dashboard.mail');
                }
            });


        }




        $scope.printDiv = function (divName) {
            console.log(divName)
            console.log('clicked')
            var printContents = document.getElementById(divName).innerHTML;
            var popupWin = window.open('', '_blank', 'width=800,height=800');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
            popupWin.document.close();
        }



        // forward the email to the particular Id
        $scope.forwardMail = function (forwardMailId, id, subject, body) {
            var postData = {
                'senderEmail': [forwardMailId],
                'body': body,
                'userId': $rootScope.currentUser.data.user.id,
                "messageId": id,
                "subject": subject,

            }

            dataService.sendmail(postData, function (response) {
                console.log(response.data)
                if (response.data.error) {
                    $timeout(function () {
                        toaster.pop('error', "User Not Found");
                    });
                } else {
                    console.log(response);
                    toaster.pop('success', "Mail is forward");
                    // $timeout(function () {
                    $state.go('dashboard.mail');
                    // }, 3000);
                }
            });
        }

        $scope.moveTotrash = function (messageId, locationType) {
            var dataPost = {
                'userId': $rootScope.currentUser.data.user.id,
                'messageId': messageId,
                'locationType': locationType
            }
            dataService.moveToTrash(dataPost, function (response) {
                toaster.pop('success', "Mail Is Moved To Trash.");
                $timeout(function () {
                    $state.go('dashboard.mail');
                }, 3000);
            });
        }
        $scope.status = false;
        // stop server move to new controller
        $scope.stopServer = function () {
            console.log('test the server');
            dataService.stopServer(function (response) {
                $scope.status = true
                toaster.pop('success', "Server is stopped.");
            });
        }

        $scope.startServer = function () {
            dataService.startServer(function (response) {
                console.log(response.data)
                $scope.status = false
                toaster.pop('success', "Server is started.");
            });
        }


    }]);
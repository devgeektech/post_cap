var app = angular.module('app')
    .controller('sidebarController', ['$scope', '$http', '$rootScope', 'dataService', '$state', 'commanService', function($scope, $http, $rootScope, dataService, $state, commanService) {
       
        $scope.userLabels = commanService.label;
        $scope.getusersLabel = function() {
            dataService.getusersLabel(function(response) {
                $scope.userLabels = response.data;
            })
        };
        $scope.getusersLabel();
    }]);
var app = angular.module('app')
    .controller('cardController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', 'toaster', 'userService', 'Upload', '$state', function ($scope, $http, $rootScope, dataService, $stateParams, toaster, userService, Upload, $state) {
        // console.log('dasdasda')
        $scope.message = null;
        $scope.isLoading = false
        $scope.saveCard = (cardDetails) => {
            console.log(`@@@@@@@@@`, cardDetails)
            $scope.isLoading = true
            if (!cardDetails.number) {
                return $scope.message = 'Please provide card No.'
            } else if (!cardDetails.expMonth) {
                return $scope.message = 'Please provide exp Month.'
            } else if (!cardDetails.Year) {
                return $scope.message = 'Please provide Year.'
            } else if (!cardDetails.name) {
                return $scope.message = 'Please provide name.'
            } else if (!cardDetails.street) {
                return $scope.message = 'Please provide street.'
            } else if (!cardDetails.city) {
                return $scope.message = 'Please provide city.'
            }
            else if (!cardDetails.region) {
                return $scope.message = 'Please provide region.'
            } else if (!cardDetails.country) {
                return $scope.message = 'Please provide country.'
            } else if (!cardDetails.postalcode) {
                return $scope.message = 'Please provide postalcode.'
            }
            let postData = {
                "number": cardDetails.number,
                "expMonth": cardDetails.expMonth,
                "expYear": cardDetails.Year,
                "name": cardDetails.name,
                "address": {
                    "streetAddress": cardDetails.street,
                    "city": cardDetails.city,
                    "region": cardDetails.region,
                    "country": cardDetails.country,
                    "postalCode": cardDetails.postalcode
                }
            }
            dataService.saveCardsDetail(postData, response => {
                $scope.isLoading = false
                console.log(`test`, response)
                if (response.success) {
                    toaster.pop('success', response.data.message);
                     $scope.message =  response.data.message
                    $state.go('dashboard.overview');
                } else {
                     $scope.message =  response.data.message
                    toaster.pop('error', response.data.message);
                }
            })
        }

        $scope.getuserCards = () => {
            $scope.isLoading = true
            dataService.getuserCards(response => {
                $scope.isLoading = false
                $scope.Cards = response.data.cards;
            })
        }
        $scope.getuserCards()


        $scope.deleteCard = detail => {
            $scope.isLoading = true
            dataService.deleteCard({id:detail},response => {
                if (response.success) {
                    toaster.pop('success', response.data.message);
                    $scope.getuserCards()
                } else {
                    toaster.pop('error', response.data.message);
                }
               
            })
        }


    }]);
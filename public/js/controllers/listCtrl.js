var app = angular.module('app')
    .controller('listUserController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', 'toaster', 'userService', 'Upload', '$state', 'getService','$window', function ($scope, $http, $rootScope, dataService, $stateParams, toaster, userService, Upload, $state, getService,$window) {

        const serviceData = getService
        $scope.loader = false;
        $scope.currentPage = 1;
        $scope.maxSize = serviceData.maxSize;
        $scope.totalItems = serviceData.totalItems;
        ($scope.getUsers = () => {
            $scope.loader = true;
            dataService.getWriterAndClientList({
                skip: $scope.currentPage,
                limit: $scope.maxSize
            }, (response) => {
                $scope.loader = false;
                $scope.usersData = response.data.message;
            })
        })()

        $scope.pageChanged = function () {
            $scope.getUsers()
        };

        const remove_duplicates = (actual, userSpecific) => {
            for (var i = 0, len = actual.length; i < len; i++) {
                for (var j = 0, len = userSpecific.length; j < len; j++) {
                    if (actual[i].name == userSpecific[j].name) {
                        // actual.splice(j, 1);
                        actual[i].currentPrice = userSpecific[j].price
                    }
                }
            }
            console.log(actual)
            return actual
        }

        var getLists = function () {
            dataService.getTechniques(function (response) {
                dataService.getUserPriceTable({
                    id: $stateParams.id
                }, (userPrice) => {
                    if (userPrice.data.message == "No user") return $scope.techniques = response.data
                    let data = remove_duplicates(response.data, userPrice.data.message.technique)

                    $scope.techniques = data;
                })

            });
            dataService.getAccents(function (response) {

                dataService.getUserPriceTable({
                    id: $stateParams.id
                }, (userPrice) => {
                    console.log('dasd', userPrice)
                    if (userPrice.data.message == "No user") return $scope.accents = response.data
                    let data = remove_duplicates(response.data, userPrice.data.message.accent)
                    $scope.accents = data
                })
            });
            dataService.getTurnarounds(function (response) {
                dataService.getUserPriceTable({
                    id: $stateParams.id
                }, (userPrice) => {
                    if (userPrice.data.message == "No user") return $scope.turnarounds = response.data
                    let data = remove_duplicates(response.data, userPrice.data.message.turnAround)
                    $scope.turnarounds = data
                })
            });
            dataService.getCaptionServices(function (response) {
                $scope.CaptionServices = response.data;
            });
            dataService.getUserPriceTable({
                id: $stateParams.id
            }, (response) => {
                $scope.userPriceDetail = response.data.message
            })
        }
        getLists();



        $scope.getInvidualUser = (user) => {
            $scope.IndividualUser = user
        }

        $scope.gotoupdatePage = (id) => {
            $state.go('dashboard.updatePrice', {
                id: id
            })
        }
        $scope.doTheBack = function () {
            window.history.back();
        };
        $scope.disable = true
        const convertToSaveFormat = (doller, cent) => {
            let changePrice = `$${doller}.${cent}/min`
            return changePrice
        }
        $scope.setTransctiption = (price, type) => {
            if (!price.doller) return $scope.message = 'Please provide a correct Format'
            if (!price.cent) return $scope.message = 'Please provide a correct Format'
            $scope.disable = false
            const doller = JSON.parse(price.doller)
            const cent = JSON.parse(price.cent)

            const transcription = convertToSaveFormat(parseInt(doller.key), cent.key)
            dataService.addPriceForUser({
                transcription,
                type,
                userId: $stateParams.id
            }, (response) => {
                toaster.pop('success', response.data.message);
                $window.location.reload();
            })
        }

        /* captioning */


        $scope.captioning = (price, type,subtype) => {
            if (!price.doller) return $scope.message = 'Please provide a correct Format'
            if (!price.cent) return $scope.message = 'Please provide a correct Format'
            $scope.disable = false
            const doller = JSON.parse(price.doller)
            const cent = JSON.parse(price.cent)

            const caption = convertToSaveFormat(parseInt(doller.key), cent.key)
            dataService.addPriceForUser({
                caption,
                type,
                userId: $stateParams.id,
                subtype:subtype
            }, (response) => {
                toaster.pop('success', response.data.message);
                $window.location.reload();
            })
        }

        $scope.setTranscriptionAlignment = (price, type) => {
            const transcriptionAlignment = convertToSaveFormat(price)
            dataService.addPriceForUser({
                transcriptionAlignment,
                type,
                userId: $stateParams.id
            }, (response) => {
                toaster.pop('success', response.data.message);
                $window.location.reload();
            })
        }

        $scope.getUserByid = () => {
            dataService.getUserByid({
                id: $stateParams.id
            }, (response) => {
                $scope.IndividualUser = response.data.data
            })
        }

        $scope.minutes = [
            {
                key: '0',
                value: 0
            },
            {
                key: '1',
                value: 1
            },
            {
                key: '2',
                value: 2
            },
            {
                key: '3',
                value: 3
            },
            {
                key: '4',
                value: 4
            }, {
                key: '5',
                value: 5
            },
            {
                key: '6',
                value: 6
            },
            {
                key: '7',
                value: 7
            },
            {
                key: '8',
                value: 8
            }, {
                key: '9',
                value: 9
            }, {
                key: '10',
                value: 10
            },
        ]

        $scope.cents = [
            {
                key: '00',
                value: 00
            },
            {
                key: '05',
                value: 05
            },
            {
                key: '10',
                value: 10
            },
            {
                key: '15',
                value: 15
            }, {
                key: '20',
                value: 20
            },
            {
                key: '25',
                value: 25
            },
            {
                key: '30',
                value: 33
            },
            {
                key: '40',
                value: 40
            }, {
                key: '50',
                value: 50
            }, {
                key: '60',
                value: 60
            },
            {
                key: '70',
                value: 70
            },
            {
                key: '80',
                value: 80
            },
            {
                key: '90',
                value: 90
            }
        ]

        const convertToSaveFormatAccent = (doller, cent) => {
            let changePrice = `+$${doller}.${cent}/min`
            return changePrice
        }

        $scope.saveAccent = (price, accent, type) => {
            if (!price.doller) return $scope.message = 'Please provide a correct Format'
            if (!price.cent) return $scope.message = 'Please provide a correct Format'
            const doller = JSON.parse(price.doller)
            const cent = JSON.parse(price.cent)

            accent.price = convertToSaveFormatAccent(parseInt(doller.key), cent.key)

            let postData = {
                type: type,
                saveObject: accent,
                userId: $stateParams.id
            }
            dataService.addPriceForUser(postData, (response) => {
                toaster.pop('success', response.data.message);
               
                $window.location.reload();
            })
        }

        $scope.saveTurnAround = (price, name, userId, time) => {

            var changePrice = convertToSaveFormat(price)
            let postData = {
                type: 'turnAround',
                turnAround: {
                    name: name,
                    price: changePrice,
                    time: time
                },
                userId: userId
            }
            dataService.addPriceForUser(postData, (response) => {
                toaster.pop('success', response.data.message);
            })
        }

        $scope.savetechnique = (price, name, userId) => {
            var changePrice = convertToSaveFormat(price)
            let postData = {
                type: 'technique',
                technique: {
                    name: name,
                    price: changePrice
                },
                userId: userId
            }
            dataService.addPriceForUser(postData, (response) => {
                toaster.pop('success', response.data.message);
            })
        }

        $scope.addPriceForUser = (type) => {
            let postData = {
                type: type
            }
            dataService.addPriceForUser(postData, (response) => { })
        }


        $scope.getUserPriceDetail = () => {
            dataService.getUserPriceTable({
                id: $stateParams.id
            }, (response) => {
                console.log(response)
                $scope.priceInfo = response.data.message
            })
        }

        $scope.updateAccent = (data, id) => {
            dataService.updatePriceIndividual({
                user: data,
                id: id,
                type: 'accent'
            }, (response) => {
                console.log(response)
                $scope.getUserPriceDetail()
            })
        }

        $scope.updateturnAround = (data, id) => {
            dataService.updatePriceIndividual({
                user: data,
                id: id,
                type: 'turnAround'
            }, (response) => {
                console.log(response)
                $scope.getUserPriceDetail()
                toaster.pop('success', response.data.message);
            })
        }

        $scope.updateTechnique = (data, id) => {
            dataService.updatePriceIndividual({
                user: data,
                id: id,
                type: 'technique'
            }, (response) => {
                console.log(response)
                $scope.getUserPriceDetail()
                toaster.pop('success', response.data.message);
            })
        }

    }]);
var app = angular.module('app')
    .controller('userController', ['$scope', '$http', 'userService', 'getService', '$rootScope', '$stateParams', '$window', 'toaster', '$timeout', function ($scope, $http, userService, getService, $rootScope, $stateParams, $window, toaster, $timeout) {
        var uc = this;
        uc.test = "test"
        uc.selectedRoles = [];
        $scope.currentPage = 1;

        if (localStorage.getItem('filter') == null) {
            $scope.maxSize = 20;
        } else {
            $scope.maxSize = parseInt(localStorage.getItem('filter'))
        }

     

        const setValueFilterLocalStorage = (data) => {
            localStorage.setItem("filter", data);
            return parseInt(localStorage.getItem('filter'))
        }
        // $scope.maxSize = 5;

        $scope.totalItems = 64;
        getService.getRoles(function (response) {
            uc.roles = response.data;
            switch ($rootScope.currentUser.data.user.role) {
                case 'project_admin':
                    uc.selectedRoles = _.filter(uc.roles, function (item) {
                        return item.name == 'writer';
                    });
                    break;
                case 'client_admin':
                    uc.selectedRoles = _.filter(uc.roles, function (item) {
                        return item.name == 'client_user';
                    });
                    break;
                default:
                    uc.selectedRoles = uc.roles;
                    break;
            }
        })
        getService.getOrganizations(function (response) {
            uc.organizations = response.data;
        })


        $scope.loader = false
        function getUserList() {
            var role = {
                skip:  $scope.currentPage,
                limit: $scope.maxSize
            }
            $scope.loader = true
            getService.getUsers(role, function (response) {
                $scope.loader = false
                checkForSuperUser(response.data, function (data) {
                    uc.users = data;
                })
            })
        }

        $scope.pageChanged = function () {
            console.log('clicked')
            $scope.maxSize = setValueFilterLocalStorage(data)
            getUserList()
        };


        checkForSuperUser = function (data, callback) {

            data.forEach(function (element) {
                if (element.role.name == 'super_user') {
                    element.superCheck = true
                }
            })
            callback(data)
        }


        uc.setPassword = function () {
            uc.setPasswordData.email = $stateParams.email;
            userService.setPassword(uc.setPasswordData, function (response) {
                console.log(response);
                $window.location.href = 'http://ec2-18-216-203-72.us-east-2.compute.amazonaws.com/login';
            });
        }
        uc.photoUpload = function (model) {
            userService.upload(model, function (response) {
                console.log(response);
            })
        }
        uc.createUser = function (userModel) {
            userModel.created_by = $rootScope.currentUser.data.user.id;
            userService.create(userModel, '/api/user', function (response) {
                getUserList();
                $('#myModalProjectUser').modal('toggle');
            })
        }

        $('#upload').change(function () {
            var filename = $(this).val();
            var lastIndex = filename.lastIndexOf("\\");
            if (lastIndex >= 0) {
                filename = filename.substring(lastIndex + 1);
            };
            $('#filename').val(filename);
        });

        getUserList();


        $scope.updateUser = function (user, index) {
            var userUpdate = {
                id: user._id,
                firstname: user.firstname,
                lastname: user.lastname,
                phone: user.phone,
                organization: user.organization,
                title: user.title,
                skype: user.skype,
                department: user.department
            };
            userService.updateUser(userUpdate, function (response) {
                console.log(response)
                getService.getUsers(function (response) {
                    uc.users = response.data;
                    //10 seconds delay
                    $timeout(function () {
                        toaster.pop('success', "User", "user updated successfully");
                    });
                });
            });
        };



        uc.selectUserType = function (type) {
            uc.usermodel = {};
            if (type == 'admin') {
                var roleName = 'client_admin';
                var selectedRole = uc.roles.filter(function (role) {
                    return role.name == roleName
                });
                uc.usermodel.usertype = selectedRole[0];
            } else {
                var roleName = 'client_user';
                var selectedRole = uc.roles.filter(function (role) {
                    return role.name == roleName
                });
                uc.usermodel.usertype = selectedRole[0]
            };
        }





    }]);
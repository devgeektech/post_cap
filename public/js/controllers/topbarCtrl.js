var app = angular.module('app')
    .controller('topBarController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', 'toaster', '$state', function ($scope, $http, $rootScope, dataService, $stateParams, toaster, $state) {
        // stop server move to new controller
        var serviceArray = [];
        $scope.disable = false
        $scope.userTypeArray = [];
        $scope.newloader = false
        $scope.currentPage = 1;

        $scope.totalItems = 100;
        $scope.filterData = [ 20, 50, 100]
       
        if (localStorage.getItem('filter') == null) {
            $scope.maxSize = 20;
        } else {
            $scope.maxSize = parseInt(localStorage.getItem('filter'))
        }

        $scope.setValue = function (data) {
            $scope.maxSize = setValueFilterLocalStorage(data)
            $scope.getMyProjects()
        }

        const setValueFilterLocalStorage = (data) => {
            localStorage.setItem("filter", data);
            return parseInt(localStorage.getItem('filter'))
        }




        $scope.getMyProjects = function () {
            $scope.newloader = true
            $scope.showProjectById = function (id) {
                $state.go('dashboard.projectById', {
                    id: id
                });
            }
            let postData = {
                skip: $scope.currentPage,
                limit: $scope.maxSize
            }

            dataService.getMyProjects(postData, function (response) {
                $scope.newloader = false
                $scope.userProjectData = response.data;
            });
        }

        $scope.pageChanged = function () {
            $scope.getMyProjects()
        };

        $scope.clearFilter = (data) => {
            $scope.date = {}
            $scope.userTypeArray = [];
            $scope.userArray = []
            serviceArray = [];
            $scope.writerArray = []
            $scope.allStatus.forEach(function (item) {
                item.key = false
            })
            $scope.allService.forEach(function (item) {
                item.key = false
            })
            $scope.getMyProjects();

        }

        $scope.getAllWriter = () => {
            dataService.getAllWriter(function (response) {
                $scope.writers = []
                response.data.message.forEach(element => {
                    let fullName = {}
                    fullName.name = element.firstname + ' ' + element.lastname;
                    fullName.id = element._id
                    $scope.writers.push(fullName)
                });
            })
        }

        $scope.getAllWriter();

        $scope.getMyProjects();

        $scope.accending = true;
        $scope.messageClass = 'fa fa-angle-double-up'

        $scope.setSort = function (projectData, type) {
            if ($scope.accending) {
                sortByDate(projectData, type);

            } else {
                accending(projectData, type);
            }

        }

        $scope.accendingByDate = function (project, type) {
            accending(project, type);
        }



        function sortByDate(arr, type) {

            if (type == 'totalPrice') {
                arr.sort(function (a, b) {
                    return (b.totalPrice) - (a.totalPrice);
                });
            }

            if (type == 'projectName') {

                arr.sort(function (a, b) {
                    return a.name < b.name ? -1 : 1
                })
            }

            if (type == 'client') {

                arr.sort(function (a, b) {
                    return a.clientUser.firstname < b.clientUser.firstname ? -1 : 1
                })
            }

            if (type == 'service') {
                arr.sort(function (a, b) {
                    return a.service.name < b.service.name ? -1 : 1
                })
            }

            if (type == 'Status') {

                arr.sort(function (a, b) {
                    return a.status < b.status ? -1 : 1
                })
            }

            if (type == 'writerPrice') {
                arr.sort(function (a, b) {
                    return (b.writerPrice) - (a.writerPrice);
                });
            }
            if (type == 'order') {
                arr.sort(function (a, b) {
                    return Number(new Date(b.createdAt)) - Number(new Date(a.createdAt));
                });
            }
            if (type == 'totalPrice') {
                arr.sort(function (a, b) {
                    return (b.totalPrice) - (a.totalPrice);
                });
            }

            if (type == 'duration') {
                arr.sort(function (a, b) {
                    return (b.duration) - (a.duration);
                });
            }

            $scope.accending = false;
            $scope.messageClass = 'fa fa-angle-double-down'
            uc.projects = arr;
        }



        function accending(arr, type) {

            if (type == 'totalPrice') {
                arr.sort(function (a, b) {
                    return (a.totalPrice) - (b.totalPrice);
                });
            }

            if (type == 'projectName') {
                arr.sort(function (a, b) {
                    return a.name > b.name ? -1 : 1
                })
            }

            if (type == 'Status') {
                arr.sort(function (a, b) {
                    return a.status > b.status ? -1 : 1
                })
            }

            if (type == 'client') {

                arr.sort(function (a, b) {
                    return a.clientUser.firstname > b.clientUser.firstname ? -1 : 1
                })
            }

            if (type == 'service') {
                arr.sort(function (a, b) {
                    return a.service.name > b.service.name ? -1 : 1
                })
            }

            if (type == 'totalPrice') {
                arr.sort(function (a, b) {
                    return (a.totalPrice) - (b.totalPrice);
                });
            }

            if (type == 'writerPrice') {
                arr.sort(function (a, b) {
                    return (a.writerPrice) - (b.writerPrice);
                });
            }

            if (type == 'order') {
                arr.sort(function (a, b) {
                    return Number(new Date(a.createdAt)) - Number(new Date(b.createdAt));
                });
            }

            if (type == 'duration') {
                arr.sort(function (a, b) {
                    return (a.duration) - (b.duration);
                });
            }
            $scope.messageClass = 'fa fa-angle-double-up'
            $scope.accending = true;
            uc.projects = arr;

        }
        var removeObjectProperties = function (obj, props) {

            for (var i = 0; i < props.length; i++) {
                if (obj.hasOwnProperty(props[i])) {
                    delete obj[props[i]];
                }
            }

        };

        $scope.downloadCsv = (projects) => {
            dataService.downloadCsv({ projects: projects }, function (response) {
                var fileName = "Projects-Data.csv";
                saveData(response.data, fileName);
            });
        }

        // download the link

        const saveData = (function () {
            const a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            return function (data, fileName) {
                const blob = new Blob([data], {
                    type: "octet/stream"
                }),
                    url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = fileName;
                a.click();
                window.URL.revokeObjectURL(url);
            };
        }());

        $scope.allStatus = [{
            "key": "false",
            "data": "Attention Required",
            "value": "attentionRequired"
        },
        {
            "key": "false",
            "data": "Cancelled",
            "value": "cancelled"
        },
        {
            "key": "false",
            "data": "Completed",
            "value": "completed"
        },
        {
            "key": "false",
            "data": "In Progress",
            "value": "In Progress"
        },
        {
            "key": "false",
            "data": "Unassigned",
            "value": "Unassigned"
        }
        ];

        $scope.allService = [{
            "key": "false",
            "data": "Transcription",
            "value": "transcription"
        },
        {
            "key": "false",
            "data": "Captioning",
            "value": "captioning"
        },
        {
            "key": "false",
            "data": "Translation",
            "value": "translation"
        },
        {
            "key": "false",
            "data": "Transcription Alignment",
            "value": "transcription-alignment"
        }
        ];




        $scope.selectServiceTypeForProject = function (check) {
            if (check.key) {
                serviceArray.push(check.value);
            } else {
                var index = serviceArray.indexOf(check);
                serviceArray.splice(index);
            }
        }




        $scope.selectUserTypeForFilter = function (check) {
            if (check.key) {
                $scope.userTypeArray.push(check.value);
            } else {
                var index = userTypeArray.indexOf(check);
                $scope.userTypeArray.splice(index);
            }

        }

        $scope.applyProjectFilter = (date) => {
            writerArray
            console.log('0', userArray)
            if (!date) {
                date = {}
            }
            if (date.Writer) {
                var postData = {
                    projectStatus: $scope.userTypeArray || '',
                    serviceType: serviceArray || '',
                    startDate: date.startDate || '',
                    lastDate: date.endDate || '',
                    writerName: writerArray || '',
                    clientName: userArray || ''
                }
                dataService.getProjectAccordingProjectType(postData, function (response) {
                    $scope.userProjectData = response.data;
                    $scope.userTypeArray = [];
                    serviceArray = []
                    $scope.data = {}
                    $('#myModalProjFilter').modal('hide');
                })
                // })
            } else {
                var postData = {
                    projectStatus: $scope.userTypeArray || '',
                    serviceType: serviceArray || '',
                    startDate: date.startDate || '',
                    lastDate: date.endDate || '',
                    writerName: writerArray || '',
                    clientName: userArray || ''
                }
                dataService.getProjectAccordingProjectType(postData, function (response) {
                    $scope.userProjectData = response.data;
                    $scope.userTypeArray = [];
                    serviceArray = []

                    $('#myModalProjFilter').modal('hide');
                })
            }
        }

        // $scope.users = []

        $scope.getUserAutoComplete = (name) => {
            let data = {
                name: name
            }
            dataService.getuserInfoByName(data, function (response) {
                $scope.userList = response.data
            })
        }
        $scope.userArray = []
        let userArray = []
        $scope.writerArray = [];
        let writerArray = []
        $scope.userDetailCopy = (id) => {
            $scope.userArray.push(id)
            userArray.push(id._id)
        }

        $scope.writerPush = (id) => {
            $scope.writerArray.push(id)
            writerArray.push(id.id)
        }
        $scope.removeFromWriter = (id) => {
            var index = $scope.writerArray.indexOf(id);
            var index2 = writerArray.indexOf(id.id);
            $scope.writerArray.splice(index);
            writerArray.splice(index2);
            console.log(writerArray)
        }

        $scope.removeFromArray = function (mailId) {
            var index = $scope.userArray.indexOf(mailId);
            var index2 = userArray.indexOf(mailId._id);
            $scope.userArray.splice(index);
            userArray.splice(index2);
            console.log(userArray)
        }



        $scope.deleteProject = function () {
            if (!projectList.length) return toaster.pop('error', module, "NO project Added")
            let projectIds = {
                id: projectList
            }
            dataService.deleteProject(projectIds, function (response) {
                if (!response.data.success) {
                    toaster.pop('warning', response.data.message);
                } else {
                    toaster.pop('success', response.data.message);
                }
                $scope.getMyProjects();
            });
        }


        let projectList = []

        $scope.checkProject = function (check, id) {
            $scope.disable = true
            if (check) {
                projectList.push(id);
            } else {
                var index = projectList.indexOf(id);
                projectList.splice(index);
            }
        }

        // $scope.totalItems = 64;
        // $scope.currentPage = 1;

        // $scope.setPage = function (pageNo) {
        //     $scope.currentPage = pageNo;
        // };

        // $scope.pageChanged = function () {
        //     console.log($scope.currentPage)
        // };

        // $scope.nextPage = (page) => {
        //     console.log(page)
        // }

        // $scope.maxSize = 5;
        // $scope.bigTotalItems = 175;
        // $scope.bigCurrentPage = 1;

    }]);
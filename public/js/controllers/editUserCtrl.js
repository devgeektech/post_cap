var app = angular.module('app')
    .controller('UserEditController', ['$scope', '$http', '$rootScope', 'dataService', '$stateParams', 'toaster', 'userService', 'Upload', function ($scope, $http, $rootScope, dataService, $stateParams, toaster, userService, Upload) {
        $scope.getUserByIdinit = function () {
            userService.getUserInfo(function (response) {
                $scope.userData = response.data;
            })
        }

        //edit user

        $scope.editUser = function (userDetail) {
            userDetail.id = userDetail._id;
            userService.updateUser(userDetail, function (response) {
                if(response.data.message == 'user updated'){
                    toaster.pop('warning', response.data.message);
                    userService.getUserInfo(function (response) {
                        $scope.userData = response.data;
                    })
                }
            });
        }


        $scope.mail_notifications = [
            {
                "key": "weekly",
                "value": "Weekly"
            },
            {
                "key": "byWeekly",
                "value": "ByWeekly"
            },
            {
                "key": "monthly",
                "value": "Monthly"
            }
        ];



        $scope.submit = function (filedata) {
            Upload.upload({
                url: 'api/uploadtoAmazon', //webAPI exposed to upload the file
                data: { file: filedata } //pass file as data, should be user ng-model
            }).then(function (resp) { //upload function returns a promise
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                $scope.userData.profile_picture = resp.data;

            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

    }]);
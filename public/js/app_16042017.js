/**
 * Create a WaveSurfer instance.
 */
var wavesurfer = Object.create(WaveSurfer);
var last_timestamp;
var audio_text = "";

/**
 * Init & load.
 */
document.addEventListener('DOMContentLoaded', function () {

    // Init wavesurfer
    wavesurfer.init({
        container: '#waveform',
        height: 100,
        pixelRatio: 1,
        scrollParent: true,
        normalize: true,
        minimap: true,
        backend: 'MediaElement',
		waveColor: 'orange'
    });
	
	//wavesurfer.load($("#waveId").val());

    /* Regions */
    wavesurfer.enableDragSelection({
        color: randomColor(0.3)
    });
	
	var slider = document.querySelector('#slider');

	slider.oninput = function () {
	  var zoomLevel = Number(slider.value);
	  wavesurfer.zoom(zoomLevel);
	};

    wavesurfer.on('ready', function () {		
        //if (localStorage.regions) {
            //loadRegions(JSON.parse(localStorage.regions));
        //} else {
			//alert($("#lblJsonId").val());
			loadRegions(JSON.parse($("#lblJsonId").val()));
            saveRegions();
        //}
    });
    wavesurfer.on('region-click', function (region, e) {
        e.stopPropagation();
        // Play on click, loop on shift click
        e.shiftKey ? region.playLoop() : region.play();
    });
    wavesurfer.on('region-click', editAnnotation);
    wavesurfer.on('region-updated', saveRegions);
    wavesurfer.on('region-removed', saveRegions);
    //wavesurfer.on('region-in', showNote);

    wavesurfer.on('region-play', function (region) {
        region.once('out', function () {
            wavesurfer.play(region.start);
            wavesurfer.pause();
        });
    });

    /* Timeline plugin */
    wavesurfer.on('ready', function () {
        var timeline = Object.create(WaveSurfer.Timeline);
        timeline.init({
            wavesurfer: wavesurfer,
            container: "#wave-timeline"
        });
    });


    /* Toggle play/pause buttons. */
    // var playButton = document.querySelector('#play');
    // var pauseButton = document.querySelector('#pause');
    // wavesurfer.on('play', function () {
        // playButton.style.display = 'none';
        // pauseButton.style.display = '';
    // });
    // wavesurfer.on('pause', function () {
        // playButton.style.display = '';
        // pauseButton.style.display = 'none';
    // });
});

function playwavesurfer()
{
	wavesurfer.playPause();
	var player = document.getElementById("captionedvid");
	player.paused ? player.play() : player.pause();
}

function loadwavesurfer()
{
	wavesurfer.clearRegions();
	wavesurfer.load($("#waveId").val());
}

function stopwavesurfer()
{
	wavesurfer.stop();
}

/**
 * Save annotations to localStorage.
 */
function saveRegions() {
    localStorage.regions = JSON.stringify(
        Object.keys(wavesurfer.regions.list).map(function (id) {
            var region = wavesurfer.regions.list[id];
            return {
                start: region.start,
                end: region.end,
                attributes: region.attributes,
                data: region.data,
				drag: false
            };
        })
    );
}

var clicktosync = false;
function clicksync()
{
	if(clicktosync == false){
		clicktosync = true;
		alert("Click to assign timecode enabled!");	
		last_str_time = 0;
	}
	else{
		clicktosync = false;
		alert("Click to assign timecode disabled!");
		$("#able_transcript_edit").html($("#audiocontent").val());
	}
}

var linesync = false;
function clicklinesync()
{
	if(linesync == false){
		linesync = true;
		alert("Click to assign timecode enabled!");	
		last_str_time = 0;
	}
	else{
		linesync = false;
		alert("Click to assign timecode disabled!");
		//$("#able_transcript_edit").html($("#audiocontent").val());
	}
}

var last_str_time;
function fill_waves() {
	if(clicktosync == true)
	{
		var text_content = $.trim($("#able_transcript_edit").text());
		var offset = $("#able_transcript_edit").offset();

		if(text_content != "") {
			var text_full_length = text_content.length;
			var start_time = 0;
			if(last_timestamp != null) {
				start_time = last_timestamp;
			}
			var end_time = wavesurfer.getCurrentTime();
			last_timestamp = end_time;
				
			var start_str = 0;
			if(last_str_time != null) {
				start_str = last_str_time;
			}
			
			var selection = window.getSelection();
			//console.log(selection.focusNode.data[selection.focusOffset]);
			var end_str = selection.focusOffset;
			last_str_time  = end_str;
			audiocontent = $("#audiocontent").val();
			
			selectedcontent = $.trim($("#able_transcript_edit").text()).substring(start_str, end_str);
			//alert($.trim($("#able_transcript_edit").text()));
		
			var starttimeforid = start_time.toString().replace(".","_");
			output = "";		
			if(audiocontent != "") {		
				span_out = '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+start_time+'" data-end="'+end_time+'" data-confidence="" data-alternative="" id="spanid_'+starttimeforid+'" onclick="DisplayWatsonData(this);">'+selectedcontent+'</span>';
				div_output = audiocontent+span_out;
				
				var restofthecontent = $.trim($("#able_transcript_edit").text()).substring(end_str, $.trim($("#able_transcript_edit").text()).length);				
				//$("#able_transcript_edit").html(div_output + ' ' + restofthecontent);
				
				output = output.concat('[{"start":'+start_time+',"end":'+end_time+',"data":{},"attributes":{"label": "'+selectedcontent+'"}}]');
				//$("#audiocontent").val(text_content.replace(selectedcontent, div_output));
				$("#audiocontent").val(div_output);
			}
			else {
				div_output = '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+start_time+'" data-end="'+end_time+'" data-confidence="" data-alternative="" id="spanid_'+starttimeforid+'" onclick="DisplayWatsonData(this);">'+selectedcontent+'</span>';
				
				var restofthecontent = $.trim($("#able_transcript_edit").text()).substring(end_str, $.trim($("#able_transcript_edit").text()).length);	
				
				//$("#able_transcript_edit").html(div_output + ' ' + restofthecontent);
				
				output = output.concat('[{"start":'+start_time+',"end":'+end_time+',"data":{},"attributes":{"label": "'+selectedcontent+'"}}]');					
				//$("#audiocontent").val(text_content.replace(selectedcontent, div_output));
				$("#audiocontent").val(div_output);
			}
							
			 $("#lblJsonId").val(output);
			 loadRegions(JSON.parse($("#lblJsonId").val()), 'none');
		}
	}	
}

var split_strings = {};
split_strings.selection_times = {};
split_strings.checked_row_times = []
split_strings.merge_lines_ids = [];
split_strings.merge_lines_values = [];

document.addEventListener("mouseup", function(){
	var selectedTextArea = document.activeElement;
	if(selectedTextArea.value) {
		var length =  selectedTextArea.value.length;
	}
	split_strings.selection = window.getSelection().toString();
	var row = $('#' + selectedTextArea.id).closest('tr');
	if(window.getSelection().toString() != '') {
		split_strings.selection = window.getSelection().toString();
		console.log(split_strings.selection);
	}
	split_strings.active_id = selectedTextArea.id;
}, false);


var calculate_object_checked = function (id, confidence, callback) {
	var row = $('#' + id).closest('tr');
		var row_times = {};
		row_times.confidence = confidence;
		row_times.id = id;
		row_times.value = $('#' + id).val().toLowerCase();
		if(row[0].childNodes) {
			var start_time_id = row[0].childNodes[1].firstChild.id;
			var end_time_id = row[0].childNodes[2].firstChild.id;
			// $('#' + end_time_id).val($('#' + end_time_id).val() + ' ' + split_strings.selection.toLowerCase());
			var start_time_value = $('#' + start_time_id).val();
			var end_time_value = $('#' + end_time_id).val();
			var a = start_time_value.split(':'); // split it at the colons

			// minutes are worth 60 seconds. Hours are worth 60 minutes.
			row_times.start_time_value = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]) +  (+a[3]/1000); 

			var a = end_time_value.split(':'); // split it at the colons
			row_times.end_time_value = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]) +  (+a[3]/1000); 
		};
		callback(row_times);
};

function check_rows_lines(t, id, confidence) {
	split_strings.confidence = confidence;
	split_strings.checked_id = t[0].id;
    if (t.is(':checked')) {
		calculate_object_checked(id, confidence, function(row_data) {
			split_strings.merge_lines_ids.push(row_data);
		});
    } else {
		calculate_object_checked(id, confidence, function(row_data) {
			var index = split_strings.merge_lines_ids.indexOf(row_data);
			split_strings.merge_lines_ids.splice(index, 1);
		});	
    };
	console.log(split_strings);
}

var SecondsTohhmmss = function(totalSeconds, callback) {
  var hours   = Math.floor(totalSeconds / 3600);
  var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
  var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

  // round seconds
  seconds = Math.round(seconds * 100) / 100

  var result = (hours < 10 ? "0" + hours : hours);
	result += "-" + (minutes < 10 ? "0" + minutes : minutes);
	result += "-" + (seconds  < 10 ? "0" + seconds : seconds);
  callback(result);
}

var formatTime = function (time) {
	var splittime = time.toString().split('.');
	var totalSeconds = splittime.length > 1 ? splittime[0] : splittime;
	var hours   = Math.floor(totalSeconds / 3600);
	var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
	var seconds = totalSeconds - (hours * 3600) - (minutes * 60);
	
    var result = (hours < 10 ? "0" + hours : hours);
    result += ":" + (minutes < 10 ? "0" + minutes : minutes);
    result += ":" + (seconds  < 10 ? "0" + seconds : seconds);
	if(splittime.length > 1)
		result += ":" + splittime[1].substr(0,3);	
	return result;
};

function LineSyncWithTime(e,id)
{
	if(linesync == true){		
		if(e.which == 13) {
			if($("#starttxt_"+id).val() == '00:00:00.000' || $("#starttxt_"+id).val() == '')
				$("#starttxt_"+id).val(formatTime(wavesurfer.getCurrentTime()));
			else if($("#starttxt_"+id).val() != '00:00:00.000' && ($("#endtxt_"+id).val() == '00:00:00.000' || $("#endtxt_"+id).val() == ''))
				$("#endtxt_"+id).val(formatTime(wavesurfer.getCurrentTime()));
		}
	}
}

function set_new_active_input (id, confidence) {
	var selectedTextArea = document.activeElement || split_strings.selectedTextArea;
	var length =  selectedTextArea.value.length;
	/*=================================split word===================================*/ 
	var row = $('#' + selectedTextArea.id).closest('tr');
	split_strings.confidence = confidence;
	
	if(row[0].childNodes) {
		var start_time_id = row[0].childNodes[1].firstChild.id;
		var end_time_id = row[0].childNodes[2].firstChild.id;
		// $('#' + end_time_id).val($('#' + end_time_id).val() + ' ' + split_strings.selection.toLowerCase());
		var start_time_value = $('#' + start_time_id).val();
		var end_time_value = $('#' + end_time_id).val();
		var a = start_time_value.split(':'); // split it at the colons

		// minutes are worth 60 seconds. Hours are worth 60 minutes.
		split_strings.selection_times.start_time_value = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]) +  (+a[3]/1000); 

		var a = end_time_value.split(':'); // split it at the colons
		split_strings.selection_times.end_time_value = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]) +  (+a[3]/1000); 
	};

	/*==============================================================================*/ 

	
	split_strings.original_value = selectedTextArea.value.toLowerCase();
	split_strings.initial_string = selectedTextArea.value.slice(0, selectedTextArea.selectionStart);
	split_strings.end_string = selectedTextArea.value.slice(selectedTextArea.selectionStart, length);
	split_strings.active_id = id;
	find_splited_word(split_strings.initial_string, split_strings.end_string, function(splited_word) {
		split_strings.line_splited_word = splited_word;
		console.log(split_strings);
	})
}


var find_splited_word = function(string_one, string_two, callback) {
	var splitted_string_one = string_one.split(' ');
	var splitted_string_two = string_two.split(' ');
	var lengths = {};
	lengths.splitted_string_one = splitted_string_one.length;
	lengths.splitted_string_two = splitted_string_two.length;
	var splitted_word = splitted_string_one[splitted_string_one.length-1] + splitted_string_two[0];
	callback(splitted_word);
}

function mergeline (watsonObject) {
	if(split_strings.merge_lines_ids.length > 0) {
		split_strings.merge_lines_ids.forEach(function(item){
			var index = split_strings.merge_lines_values.indexOf($('#' + item.id).val().toLowerCase());
			if(index < 0) {
				split_strings.merge_lines_values.push($('#' + item.id).val().toLowerCase());
			}
		});
		
		watsonObject.results.forEach(function(item, key){
			item.alternatives.forEach(function(sentence) {
				var spaceremoving = $.trim(sentence.transcript);
				if(split_strings.merge_lines_values[0] === spaceremoving) {
					split_strings.merge_lines_values.forEach(function(line, key) {
						if(key > 0) {
							sentence.transcript = sentence.transcript + ' ' + line;
						}
					});
					// var second_splited_string_object = jQuery.extend(true, {}, item);
					// watsonObject.results.splice((index),0,second_splited_string_object)
					// sentence.transcript = split_strings.initial_string;
					// var first_splited_string_object = jQuery.extend(true, {}, item);
					// watsonObject.results.splice(index,0,first_splited_string_object);
				};
			});

		});


		split_strings.merge_lines_values.forEach(function(line, key) {
			if(key > 0) {
				watsonObject.results.forEach(function(item, key){
					item.alternatives.forEach(function(sentence) {
						var spaceremoving = $.trim(sentence.transcript);
						if(line === spaceremoving) {
							var index = watsonObject.results.indexOf(item);
							watsonObject.results.splice(index, 1);
						};
					});
				});
			};
		});

		fill_watson_based_waves(watsonObject, 'line');
		watsonObject = split_strings.original_object;
		split_strings.merge_lines_values = [];
		split_strings.merge_lines_ids = [];
	}

};

function splitword (watsonObject) {
	if(!split_word_watson_object) {
		var split_word_watson_object = jQuery.extend(true, {}, watsonObject);
	};	
	if(split_strings) {
		watsonObject.results.forEach(function(item, key){
			item.alternatives.forEach(function(line, line_key) {
				var flag_timestamp = true,
				flag_confidence = true
				if(line.timestamps) {
					line.timestamps.forEach(function(word, word_key) {
						if(flag_timestamp) {split_strings.merge_lines_ids[0]
							if(word[0] == split_strings.merge_lines_ids[0].value.toLowerCase() && word[1] == split_strings.merge_lines_ids[0].start_time_value && word[2] == split_strings.merge_lines_ids[0].end_time_value) {
								flag_timestamp = false;
								var word_instance = jQuery.extend(true, {}, word);
								var original_word = jQuery.extend(true, {}, word);

								word_instance[1] = word_instance[1] + (jQuery.extend(true, {}, word)[2]-jQuery.extend(true, {}, word)[1])/2;
								word_instance[1] = word_instance[1].toFixed(3);
								word_instance[1] = parseFloat(word_instance[1]);
								word_instance[2] = (jQuery.extend(true, {}, word)[2]);
							
								var index = line.timestamps.indexOf(word);
								line.timestamps.splice((index+1),0,word_instance);
								word[2] = (word[2] - (jQuery.extend(true, {}, word)[2]-jQuery.extend(true, {}, word)[1])/2);
								word[2] = word[2].toFixed(3);
								word[2] = parseFloat(word[2]);
								console.log(word);
							};	
						};
									
					});
				};
				if(line.word_confidence) {					
					line.word_confidence.forEach(function(word, word_key) {
						if(flag_confidence) {
							if(word[0] == split_strings.merge_lines_ids[0].value.toLowerCase() && word[1]*100 == (split_strings.merge_lines_ids[0].confidence) ) {	
								flag_confidence = false;
								var index = line.word_confidence.indexOf(word);
								line.word_confidence.splice((index+1),0,word);
							};
						}
					});
				};
			});
			flag_timestamp =true; 
			flag_confidence = true;
		});
		split_strings.merge_lines_ids = [];
		fill_watson_based_waves(watsonObject, 'word');
	};
}


var mergeword = function(watsonObject) {
	split_strings.original_object = jQuery.extend(true, {}, watsonObject);
	if(split_strings.merge_lines_ids.length > 0) {
		var main_word = jQuery.extend(true, {}, split_strings.merge_lines_ids[0]);
		var timeStamp= 0.000;
		
			watsonObject.results.forEach(function(val, key){
				val.alternatives.forEach(function(line, line_key) {
					if(line.timestamps) {
						line.timestamps.forEach(function(word, word_key) {
							split_strings.merge_lines_ids.forEach(function(item, key){
								if(key > 0) {
									if(word[0] == item.value.toLowerCase() && word[2] == item.end_time_value && word[1] == item.start_time_value) {
										timeStamp = timeStamp + (word[2] - word[1]);
										// main_word.value = main_word.value + ' ' + word[0];
										// var index = line.timestamps.indexOf(word);
										// line.timestamps.splice(index, 1);
									};
								} 
							})
							split_strings.merge_lines_ids.forEach(function(item, key){
								if(key > 0) {
									if(word[0] == item.value.toLowerCase() && word[2] == item.end_time_value && word[1] == item.start_time_value) {
										var index = line.timestamps.indexOf(word);
										line.timestamps.splice(index, 1);
									};
								};
							})
						});
					};
				});
			});
		
			watsonObject.results.forEach(function(val, key){
				val.alternatives.forEach(function(line, line_key) {
					if(line.timestamps) {
						line.timestamps.forEach(function(word, word_key) {
							split_strings.merge_lines_ids.forEach(function(item, key){
								if(key < 1) {
									if(word[0] == item.value.toLowerCase() && word[2] == item.end_time_value && word[1] == item.start_time_value) {
										word[0] = main_word.value;
										word[2] = timeStamp + word[2];
									};
								};
							});
						});
					};
				});
			});
		
		 

	};
	fill_watson_based_waves(watsonObject, 'word');
	split_strings.merge_lines_ids = [];

}

function splitline (watsonObject) {
	split_strings.original_object = jQuery.extend(true, {}, watsonObject);
	var delete_flag = 0;
	var first_timestamps = [];
	var second_timestamps = [];
	if(split_strings) {
		watsonObject.results.forEach(function(item, key){
			item.alternatives.forEach(function(sentence) {
				var spaceremoving = $.trim(sentence.transcript);
				if(split_strings.original_value === spaceremoving) {
					split_strings.removing_property = item;
					var index = watsonObject.results.indexOf(item);
  					watsonObject.results.splice(index, 1);
					sentence.transcript = split_strings.end_string;
					var second_splited_string_object = jQuery.extend(true, {}, item);
					watsonObject.results.splice((index),0,second_splited_string_object)
					sentence.transcript = split_strings.initial_string;
					var first_splited_string_object = jQuery.extend(true, {}, item);
					watsonObject.results.splice(index,0,first_splited_string_object)	
				}
			})
		});
		fill_watson_based_waves(watsonObject, 'line');
		watsonObject = split_strings.original_object;
	}
}

function nextline (watsonObject) {
	if(split_strings.merge_lines_ids[0]) {
		var row = $('#' + split_strings.merge_lines_ids[0].id).closest('tr');
		var next = row.next();
		if(next['0'].childNodes) {
			var next_end_time_id = next['0'].childNodes[2].firstChild.id;
						
			var a = $('#' + next_end_time_id).val().split(':'); // split it at the colons

			// minutes are worth 60 seconds. Hours are worth 60 minutes.
			var end_value = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]) +  (+a[3]/1000); 
			end_value = end_value + split_strings.merge_lines_ids[0].end_time_value - split_strings.merge_lines_ids[0].start_time_value;
			
			conver_float_to_time(end_value, function(formatted){
				$('#' + next_end_time_id).val(formatted).val()
			});

			var next_id = next['0'].childNodes[3].firstChild.id;
			$('#' + next_id).val($('#' + next_id).val() + ' ' + split_strings.merge_lines_ids[0].value.toLowerCase());
		}
		 var checkid = $('#' + split_strings.checked_id);
		 checkid.prop("checked", false);
		
	};
	split_strings.merge_lines_ids = [];
}

var conver_float_to_time = function (seconds, callback) {
	var time_string = seconds.toFixed(3);
	var time_string_timestamps = time_string.split('.')
	SecondsTohhmmss(seconds, function(end_hhmmss){
		var formatted = end_hhmmss.split(".");
		switch (time_string_timestamps[1].length) {
			case 1:
				time_string_timestamps[1] = time_string_timestamps[1] + '00'
				break;
			case 2:
				time_string_timestamps[1] = time_string_timestamps[1] + '0'
				break;
		}
		formatted[0] = formatted[0] + '-' + time_string_timestamps[1];
		var starttimeforid = formatted[0].replace(new RegExp('-', 'g'), ':');
		callback(starttimeforid);					
	});
}

function prevline (watsonObject) {
	if(split_strings.merge_lines_ids[0]) {
		var row = $('#' + split_strings.merge_lines_ids[0].id).closest('tr');
		var prev = row.prev();
		if(prev['0'].childNodes) {
			var previous_end_time_id = prev['0'].childNodes[2].firstChild.id;
						
			var a = $('#' + previous_end_time_id).val().split(':'); // split it at the colons

			// minutes are worth 60 seconds. Hours are worth 60 minutes.
			var end_value = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]) +  (+a[3]/1000); 
			end_value = end_value + split_strings.merge_lines_ids[0].end_time_value - split_strings.merge_lines_ids[0].start_time_value;
			
			conver_float_to_time(end_value, function(formatted){
				$('#' + previous_end_time_id).val(formatted).val()
			});

			var previous_id = prev['0'].childNodes[3].firstChild.id;
			$('#' + previous_id).val($('#' + previous_id).val() + ' ' + split_strings.merge_lines_ids[0].value.toLowerCase());
		};
		var checkid = $('#' + split_strings.checked_id);
		checkid.prop("checked", false);
	};
	split_strings.merge_lines_ids = [];
}
//Fill transcript and waves using watson object; change to line or word highlight;
function fill_watson_based_waves(obj, wordorline) {
	//Enable buttons.
	if(wordorline == "line" && $('#timecodestyle').is(':visible')){
		$("#lineButtons").show();
		$("#wordButtons").hide();
		$("#clickSyncbutton").hide();
	}
	else if(wordorline == "word" && $('#timecodestyle').is(':visible')){
		$("#lineButtons").hide();
		$("#wordButtons").show();
		$("#clickSyncbutton").hide();
	}
	else{
		$("#lineButtons").hide();
		$("#wordButtons").hide();
		$("#clickSyncbutton").show();
	}
	//console.log(obj);
	//obj = JSON.parse(obj);
	
	var output = "";
	var div_output = "";
	$.each(obj.results, function (i) {
		var wordAlternatives = '';
		$.each(obj.results[i], function (key, val) {	
			var listOfAlternatives = '';				
			if(key == "alternatives"){						
				$.each(val, function (index, alternative) {
					var data = $.trim(alternative.transcript);
					data = data.substr(0,1).toUpperCase() + data.substr(1);
					if(listOfAlternatives == ''){
						listOfAlternatives = data;
					}
					else{
						listOfAlternatives = listOfAlternatives + "@@" + data;
					}
				});
			}
			
			if(key == "word_alternatives"){						
				$.each(val, function (index, alternative) {
					$.each(alternative.alternatives, function (idx, altword) {
						var data = $.trim(altword.word);
						data = data.substr(0,1).toUpperCase() + data.substr(1);
						if(wordAlternatives == ''){
							wordAlternatives = alternative.start_time + "@~" + data;
						}
						else{
							wordAlternatives = wordAlternatives + "@@" + alternative.start_time + "@~" + data;
						}
					});
				});
			}
			
			var line = 1;
			$.each(val, function (index, alternative) {				
				if(index == 0 && wordorline == "line" && key == "alternatives"){
					//alert(line);
					var actualdata = $.trim(alternative.transcript);
					actualdata = actualdata.substr(0,1).toUpperCase() + actualdata.substr(1);
					var confidence = (alternative.confidence * 100);
					//console.log("timestamp ==>" + alternative.timestamps);
					//console.log("timestamp[0] ==>" + alternative.timestamps[0]);
					//console.log("timestamp[0][1] ==>" + alternative.timestamps[0][1]);
					if(alternative.timestamps != "" && alternative.timestamps[0] != "undefined")
					{
						var start_time = alternative.timestamps[0][1];										
						var end_time = alternative.timestamps[alternative.timestamps.length-1][2];
						
						var startimeforJson = start_time.toFixed(3);
						var endtimeforJson = end_time.toFixed(3);
						
						var start_time_string_timestamps = start_time.toString().split('.');
						var end_time_string_timestamps = end_time.toString().split('.')
						var startformatted,endformatted;
						SecondsTohhmmss(start_time_string_timestamps.length > 1 ? start_time_string_timestamps[0] : start_time_string_timestamps, function(start_hhmmss){
							SecondsTohhmmss(end_time_string_timestamps.length > 1 ? end_time_string_timestamps[0] : end_time_string_timestamps, function(end_hhmmss){
								startformatted = start_hhmmss;
								endformatted = end_hhmmss; 
							});
						});
						
						var startlen = start_time_string_timestamps.length > 1 ? start_time_string_timestamps[1].length : 0; 
						switch (startlen) {
							case 1:
								start_time_string_timestamps[1] = start_time_string_timestamps[1] + '00'
								break;
							case 2:
								start_time_string_timestamps[1] = start_time_string_timestamps[1] + '0'
								break;
							default:
								start_time_string_timestamps[1] = start_time_string_timestamps.length > 1 ? start_time_string_timestamps[1] : '000';
								break;
						}
						
						var endlen = end_time_string_timestamps.length > 1 ? end_time_string_timestamps[1].length : 0;
						switch (endlen) {
							case 1:
								end_time_string_timestamps[1] = end_time_string_timestamps[1] + '00'
								break;
							case 2:
								end_time_string_timestamps[1] = end_time_string_timestamps[1] + '0'
								break;
							default:
								end_time_string_timestamps[1] = end_time_string_timestamps.length > 1 ? end_time_string_timestamps[1] : '000';
								break;
						}
							
						start_time = startformatted +"-" + start_time_string_timestamps[1];
						end_time = endformatted +"-" + end_time_string_timestamps[1];
						
						var starttimeforid = start_time.toString().replace("-","_");
						var starttimeforid = starttimeforid.toString().replace("-","_");
						
						//for caption
						div_output = div_output + ' ' + '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+start_time+'" data-end="'+end_time+'" data-confidence="'+confidence+'" data-alternative="'+listOfAlternatives+'" id="spanid_'+starttimeforid+'" onclick="DisplayWatsonData(this);">' + actualdata + '</span>';
						
						if(actualdata.indexOf(".") == -1){
							div_output = div_output + ".";
						}
						
						//for waveform
						if(output == '')
							output = output.concat('[{"confidence":'+confidence+',"start":"'+startimeforJson+'","end":"'+endtimeforJson+'","data":{},"attributes":{"label": "'+actualdata+'" }}');
						else
							output = output.concat(',{"confidence":'+confidence+',"start":"'+startimeforJson+'","end":"'+endtimeforJson+'","data":{},"attributes":{"label": "'+actualdata+'" }}');  
					}
				}
				else if(index == 0 && wordorline == "word" && key == "alternatives"){
					var firstword = 0;
					for(var idx = 0; idx < alternative.timestamps.length; idx++){
						
						var actualdata = alternative.timestamps[idx][0];
						if(firstword == 0)
							actualdata = actualdata.substr(0,1).toUpperCase() + actualdata.substr(1);						
						if(idx == alternative.timestamps.length - 1){
							actualdata = alternative.timestamps[idx][0];
							if(actualdata.indexOf(".") == -1){
								actualdata = actualdata + ".";
							}
						}
						if(alternative.timestamps.length == 1){
							actualdata = actualdata.substr(0,1).toUpperCase() + actualdata.substr(1);						
							if(actualdata.indexOf(".") == -1){
								actualdata = actualdata + ".";
							}
						}
					
						if(alternative.timestamps != "" && alternative.timestamps[idx] != "undefined")
						{						
							var start_time = alternative.timestamps[idx][1];
							var end_time = alternative.timestamps[idx][2];
							var confidence = (alternative.word_confidence[idx][1] * 100);
							
							var arrAlternative = wordAlternatives.split('@@');
							var curralternativeWord = '';
							$.each(arrAlternative, function (index, alternative) {
								var timealternativeSplit = alternative.split('@~');
								if(start_time == timealternativeSplit[0]){
									if(curralternativeWord == ''){
										curralternativeWord = alternative.replace(start_time + '@~', '');
									}
									else{
										curralternativeWord = curralternativeWord + "@@" + alternative.replace(start_time + '@~', '');
									}
								}								
							});
							//alert(curralternativeWord);

							var startimeforJson = start_time.toFixed(3);
							var endtimeforJson = end_time.toFixed(3);


							var start_time_string_timestamps = start_time.toString().split('.');
							var end_time_string_timestamps = end_time.toString().split('.')
							var startformatted,endformatted;
							SecondsTohhmmss(start_time_string_timestamps.length > 1 ? start_time_string_timestamps[0] : start_time_string_timestamps, function(start_hhmmss){
								SecondsTohhmmss(end_time_string_timestamps.length > 1 ? end_time_string_timestamps[0] : end_time_string_timestamps, function(end_hhmmss){
									startformatted = start_hhmmss;
									endformatted = end_hhmmss; 
								});
							});

							var startlen = start_time_string_timestamps.length > 1 ? start_time_string_timestamps[1].length : 0; 
							switch (startlen) {
								case 1:
									start_time_string_timestamps[1] = start_time_string_timestamps[1] + '00'
									break;
								case 2:
									start_time_string_timestamps[1] = start_time_string_timestamps[1] + '0'
									break;
								default:
									start_time_string_timestamps[1] = start_time_string_timestamps.length > 1 ? start_time_string_timestamps[1] : '000';
									break;
							}
							
							var endlen = end_time_string_timestamps.length > 1 ? end_time_string_timestamps[1].length : 0;
							switch (endlen) {
								case 1:
									end_time_string_timestamps[1] = end_time_string_timestamps[1] + '00'
									break;
								case 2:
									end_time_string_timestamps[1] = end_time_string_timestamps[1] + '0'
									break;
								default:
									end_time_string_timestamps[1] = end_time_string_timestamps.length > 1 ? end_time_string_timestamps[1] : '000';
									break;
							}
							
							start_time = startformatted +"-" + start_time_string_timestamps[1];
							end_time = endformatted +"-" + end_time_string_timestamps[1];
							
							var starttimeforid = start_time.toString().replace("-","_");
							var starttimeforid = starttimeforid.toString().replace("-","_");
							
							//for caption
							div_output = div_output + ' ' + '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+start_time+'" data-end="'+end_time+'" data-confidence="'+confidence+'" data-alternative="'+curralternativeWord+'" id="spanid_'+starttimeforid+'" onclick="DisplayWatsonData(this);">' + actualdata + '</span>';
							wordAlternatives = '';
							
							//for waveform
							if(output == '')
								output = output.concat('[{"confidence":'+confidence+',"start":"'+startimeforJson+'","end":"'+endtimeforJson+'","data":{},"attributes":{"label": "'+actualdata+'" }}');
							else
								output = output.concat(',{"confidence":'+confidence+',"start":"'+startimeforJson+'","end":"'+endtimeforJson+'","data":{},"attributes":{"label": "'+actualdata+'" }}');
						}
						
						firstword++;
					}					
				}
				//line++;
			});
			
			
			$("#able_transcript_edit").html(div_output);
		
			if($('#timecodestyle').is(':visible')){
				CreateTimeCodeEditorControls();
			}			
		});
	});	
	//alert(output);
	
	$("#lblJsonId").val(output + "]");
	loadRegions(JSON.parse($("#lblJsonId").val()), 'clear');	
}

var ClickedTranscriptContent = '';
function DisplayWatsonData(data)
{
	ClickedTranscriptContent = data;
	var watsonElem = $(data);
	var confidence = watsonElem.attr("data-confidence");
	var alternativeData = watsonElem.attr("data-alternative");
	var id = watsonElem.attr("id");
	var start_time = watsonElem.attr("data-start");
	var end_time = watsonElem.attr("data-end");
	start_time = start_time.replace(new RegExp('-', 'g'), ':');
	end_time = end_time.replace(new RegExp('-', 'g'), ':');
	var watsonData = "<b>Information:</b> " + watsonElem.html() + "<br /><b>Start time:</b> " + start_time + "<br /><b>End time:</b> " + end_time + "<br /><b>Confidence:</b> " + confidence;
	watsonData = watsonData + "<br /><b>Alternatives:</b> <br />";
	
	if(confidence < 90 && alternativeData != ""){
		var arrAlternative = alternativeData.split('@@');
		$.each(arrAlternative, function (index, alternative) {
			watsonData = watsonData + "<a onclick=ReplaceWithAltContent('"+id+"',this); href=# data-alternative='"+alternative+"'>" + (index + 1) + "- " + alternative + "</a><br />";
		});	
	}
	else {
		watsonData = watsonData + "Not Applicable";
	}
		
	$("#autocaptionDiv").html(watsonData);
}

function ReplaceWithAltContent(id, data)
{
	var replacedData = $(data).attr("data-alternative");
	$("#"+id).html(replacedData);
}

//Fill transcript and waves using custom object;
function fill_custom_based_waves(obj) {
	var output = "";
	var div_output = "";
	//alert("called");
	$.each(obj.results, function (i) {
		$.each(obj.results[i], function (key, val) {
			if(key ==  "keywords_result"){
				$.each(val, function (index, keyword) {				
					$.each(keyword, function (idx, innerKey) {	
						var confidence = (innerKey.confidence * 100);
						var start_time = innerKey.start_time;
						var end_time = innerKey.end_time;
						var starttimeforid = start_time.toString().replace(".","_");
						var actualdata = innerKey.normalized_text;
						
						var modifiedStarttime = parseFloat(start_time.split(':')[2]) + parseFloat("0." + start_time.split(':')[3]);
						var modifiedEndtime = parseFloat(end_time.split(':')[2]) + parseFloat("0." + end_time.split(':')[3]);
						
						//for caption
						div_output = div_output + ' ' + '<span class="able-transcript-seekpoint able-transcript-caption able-block-temp" data-start="'+modifiedStarttime+'" data-end="'+modifiedEndtime+'" data-confidence="'+confidence+'" data-alternative="" id="spanid_'+starttimeforid+'" onclick="DisplayWatsonData(this);">' + actualdata + '</span>';
						
						// //for waveform
						if(output == '')
							output = output.concat('[{"confidence":'+confidence+',"start":'+modifiedStarttime+',"end":'+modifiedEndtime+',"data":{},"attributes":{"label": "'+actualdata+'" }}');
						else
							output = output.concat(',{"confidence":'+confidence+',"start":'+modifiedStarttime+',"end":'+modifiedEndtime+',"data":{},"attributes":{"label": "'+actualdata+'" }}'); 
					});
					 
				});
				
				$("#able_transcript_edit").html(div_output);
			}
		});
	});	
	//alert(output);

	$("#lblJsonId").val(output + "]");
	loadRegions(JSON.parse($("#lblJsonId").val()), 'clear');			
}

/**
 * Load regions from localStorage.
 */
function loadRegions(regions, isclear) {
	if(isclear == "clear")
		wavesurfer.clearRegions();
    regions.forEach(function (region) {
		region.color = fixedConfidenceColor(0.4, region.confidence);
        //region.color = randomColor(0.3);
        wavesurfer.addRegion(region);
    });
}

function loadIntervalRegions(regions) {
	wavesurfer.clearRegions();
	regions.forEach(function (region) {
		//alert(region.start);
		region.start = region.start + parseInt($("#idinterval").val());
		region.end = region.end + parseInt($("#idinterval").val());
        //region.color = randomColor(0.3);
        wavesurfer.addRegion(region);
    });
}


/**
 * Extract regions separated by silence.
 */
function extractRegions(peaks, duration) {
    // Silence params
    var minValue = 0.0015;
    var minSeconds = 0.25;

    var length = peaks.length;
    var coef = duration / length;
    var minLen = minSeconds / coef;

    // Gather silence indeces
    var silences = [];
    Array.prototype.forEach.call(peaks, function (val, index) {
        if (Math.abs(val) <= minValue) {
            silences.push(index);
        }
    });

    // Cluster silence values
    var clusters = [];
    silences.forEach(function (val, index) {
        if (clusters.length && val == silences[index - 1] + 1) {
            clusters[clusters.length - 1].push(val);
        } else {
            clusters.push([ val ]);
        }
    });

    // Filter silence clusters by minimum length
    var fClusters = clusters.filter(function (cluster) {
        return cluster.length >= minLen;
    });

    // Create regions on the edges of silences
    var regions = fClusters.map(function (cluster, index) {
        var next = fClusters[index + 1];
        return {
            start: cluster[cluster.length - 1],
            end: (next ? next[0] : length - 1)
        };
    });

    // Add an initial region if the audio doesn't start with silence
    var firstCluster = fClusters[0];
    if (firstCluster && firstCluster[0] != 0) {
        regions.unshift({
            start: 0,
            end: firstCluster[firstCluster.length - 1]
        });
    }

    // Filter regions by minimum length
    var fRegions = regions.filter(function (reg) {
        return reg.end - reg.start >= minLen;
    });

    // Return time-based regions
    return fRegions.map(function (reg) {
        return {
            start: Math.round(reg.start * coef * 10) / 10,
            end: Math.round(reg.end * coef * 10) / 10
        };
    });
}


/**
 * Random RGBA color.
 */
function randomColor(alpha) {
    return 'rgba(' + [
        ~~(Math.random() * 255),
        ~~(Math.random() * 255),
        ~~(Math.random() * 255),
        alpha || 1
    ] + ')';

}

function fixedConfidenceColor(alpha, confidence) {
	var number = 0.75;
	if(confidence > 90)
		number = 0.35;
    return 'rgba(' + [
        ~~(number * 255),
        ~~(number * 255),
        ~~(number * 255),
        alpha || 1
    ] + ')';
}

/**
 * Edit annotation for a region.
 */
function editAnnotation (region) {
    var form = document.forms.edit;
    form.style.opacity = 1;
    form.elements.start.value = Math.round(region.start * 10) / 10,
    form.elements.end.value = Math.round(region.end * 10) / 10;
    form.elements.note.value = region.data.note || '';
    form.onsubmit = function (e) {
        e.preventDefault();
        region.update({
            start: form.elements.start.value,
            end: form.elements.end.value,
            data: {
                note: form.elements.note.value
            }
        });
        form.style.opacity = 0;
    };
    form.onreset = function () {
        form.style.opacity = 0;
        form.dataset.region = null;
    };
    form.dataset.region = region.id;
}


/**
 * Display annotation.
 */
// function showNote (region) {
    // if (!showNote.el) {
        // showNote.el = document.querySelector('#subtitle');
    // }
    // showNote.el.textContent = region.data.note || '–';
// }

/**
 * Bind controls.
 */
GLOBAL_ACTIONS['delete-region'] = function () {
    var form = document.forms.edit;
    var regionId = form.dataset.region;
    if (regionId) {
        wavesurfer.regions.list[regionId].remove();
        form.reset();
    }
};

GLOBAL_ACTIONS['export'] = function () {
    window.open('data:application/json;charset=utf-8,' +
        encodeURIComponent(localStorage.regions));
};
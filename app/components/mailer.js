var config = require('./../settings/config');
var nodemailer = require('nodemailer');
var ejs = require('ejs');
const Email = require('email-templates');
var fs = require("fs");
const sgMail = require('@sendgrid/mail');

// const transport = sgMail.config.sendGrid.key

// var transporter = nodemailer.createTransport({
//     // service: 'gmail',
//     // auth: {
//     //     user: 'userservices@postcapllc.com',
//     //     pass: '5yTR5CaRMuzZy8QVk2'
//     // }
//     service: 'gmail',
//     smtp: 'smtp.sendgrid.net',
//     port: 587,
//     auth: {
//         user: 'userservices@postcapllc.com',
//         pass: '5yTR5CaRMuzZy8QVk2'
//     }
// });

exports.sender = function (toEmail, subject, params, callback) {

    // setup email data with unicode symbols
    var mailOptions = {
        from: 'userservices@postcapllc.com', // sender address
        to: toEmail, // list of receivers
        subject: subject, // Subject line
        text: 'Download your file', // plain text body
        // html: '<a type="button" href="http://ec2-54-191-51-9.us-west-2.compute.amazonaws.com/download?Key=' + params.Key + '&Bucket=' + params.Bucket + '">' + params.Key + '</a>' // html body
        html: '<a type="button" href="' + params.Location + '">' + params.outputName + '</a>' // html body
    };
    sgMail.send(mailOptions, function (err, success) {
       
        if (err) {
            return callback(err);
        }
        callback(null, success, '');
    });
    // send mail with defined transport object
    // transporter.sendMail(mailOptions, (error, info) => {
    //     if (error) {
    //         return callback(error);
    //     }
    //     callback(null, info.messageId, info.response);
    // });

}


exports.post_cap_sender = function (toEmail, token, subject, usertype, callback) {
    let postData = {
        url: config.server.url,
        token: token,
        link: config.server.url + 'confirmPassword?secret=' + token,
        userType: usertype.name
    }

    ejs.renderFile(__dirname + "/template/confirmEmail" + "/confirmPasswordTemplate.ejs", postData, function (err, data) {
        if (err) {
            return callback(err);
        } else {
            let mailOptions = {
                from: 'userservices@postcapllc.com', // sender address
                to: toEmail, // list of receivers
                subject: subject, // Subject line
                text: 'confirm your email', // plain text body
                html: data
            };
            sgMail.setApiKey(config.sendGrid.key)
            sgMail.send(mailOptions, function (err, success) {
               
                if (err) {
                    return callback(err);
                }
                callback(null, success, '');
            });

            // transporter.sendMail(mailOptions, (error, info) => {
            //     if (error) {
            //         return callback(error);
            //     }
            //     callback(null, info.messageId, info.response);
            // });
        }
    });

}
// after payment

exports.paymentDetail = function (toEmail, paymentinfo, callback) {

    // setup email data with unicode symbols
    var mailOptions = {
        from: 'userservices@postcapllc.com', // sender address
        to: toEmail, // list of receiver
        subject: 'Payment Detail', // Subject line
        text: 'Payment Details', // plain text body
        html: '<div><p><h1>ProjectName</h1>' + paymentinfo.projectName + '</p><p><h1>Charge Id</h1>' + paymentinfo.chargeId + '</p><p><h1>Amount</h1>' + paymentinfo.amount + '</p></div>' // html body
    };

    // send mail with defined transport object
    // transporter.sendMail(mailOptions, (error, info) => {
    //     if (error) {
    //         return callback(error);
    //     }
    //     callback(null, info.messageId, info.response);
    // });
    sgMail.send(mailOptions, function (err, success) {
       
        if (err) {
            return callback(err);
        }
        callback(null, success, '');
    });

}
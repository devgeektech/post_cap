var jwt = require('jsonwebtoken');
var models = require('mongoose').models;
//var User = requ;ire('../models/user');
var config = require('../settings/config');



exports.requiresToken = function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(!token) {
        return res.status(403).send({
            success: false,
            message: 'Token is required.'
        });
    }
    jwt.verify(token, config.auth.secret, {
            ignoreExpiration: true
        }
        ,function(err, claims) {
        if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {  
            req.user = claims;
            next();
        }
    });
};

exports.getToken = function(email, user) {
    var claims = {
        email: email,
        _id:user._id || user.id,
        role:user.role.name || user.role,
        paymentBalance: user.paymentBalance ,
        transcriber:user.transcriber || null,
        transcriptionAlignment:user.transcriptionAlignment || null,
        user: {
            id: user.id,
            profile_picture: user.profile_picture,
            company: user.organization_name,
            firstname: user.firstname,
            lastname: user.lastname,
            role: user.rolename,
            role_id: user.role,
            organization: user.organization_name
        },
        permissions: 'basic'
    };

    return jwt.sign(claims, config.auth.secret, { expiresIn: '1h' });
};

exports.requireLogin = function(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/login');
};
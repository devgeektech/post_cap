var forms = require('forms');
var fields = forms.fields;
var validators = forms.validators;
var handler = require('../helpers/inputHandler');

var projectForm = forms.create({
    name: fields.string({
        required: validators.required('name is required')
    }),
    projectUrl: fields.string({
        required: validators.required('projectUrl is required')
    }),
    totalPrice: fields.string({
        required: validators.required('totalPrice is required')
    }),
    clientUser: fields.string({
        required: validators.required('clientUser is required')
    }),
    mediaType: fields.string({
        required: validators.required('mediaType is required')
    }),
    
});

module.exports.projectForm = handler.validations(projectForm);
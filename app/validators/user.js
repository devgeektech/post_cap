var forms = require('forms');
var fields = forms.fields;
var validators = forms.validators;
var handler = require('../helpers/inputHandler');

var login_form = forms.create({
    email: fields.string({
        required: validators.required('email is required')
    }),
    firstname: fields.string({
        required: validators.required('firstname is required')
    }),
    userType: fields.string({
        required: validators.required('usertype is required')
    }),
    skype: fields.string({
        required: validators.required('skype is required')
    })
});

module.exports.login_form = handler.validations(login_form);
var request = require('request');
var Tokens = require('csrf')
var ClientOAuth2 = require('client-oauth2')
var csrf = new Tokens();
var tools = require('./tool');
var QuickBookAcc = require('node-quickbooks');
// var QuickBooks = require('./quickbooks');
var jwt = require('./jwt');
var session = require('express-session');
var Guid = require('guid');
var models = require('mongoose').models;
var async = require('async');
var _ = require('lodash');
var config = require('./../settings/config');
const validator = require('validator');
const nodemailer = require('./../components/mailer');
const quickBook = require('quickbooks');
const quickBookLocalModule = require('./quickModule/quickBookLocal')

// OAUTH 2 makes use of redirect requests
function generateAntiForgery(session) {
    session.secret = csrf.secretSync();
    return csrf.create(session.secret);
};

exports.paymentIntegrationQuickbooks = function (req, res) {

    var redirecturl = 'https://appcenter.intuit.com/connect/oauth2' +
        '?client_id=' + config.quickbooks.consumerKey +
        '&redirect_uri=' + encodeURIComponent(config.quickbooks.redirectUrl) +
        '&scope=com.intuit.quickbooks.payment openid email profile' +
        '&response_type=code' +
        '&state=' + generateAntiForgery(req.session);
    res.redirect(redirecturl);
}


// var card = {
//     "card": {
//         "expYear": "2020",
//         "expMonth": "02",
//         "address": {
//           "region": "CA",
//           "postalCode": "94086",
//           "streetAddress": "1130 Kifer Rd",
//           "country": "US",
//           "city": "Sunnyvale"
//         },
//         "name": "emulate=0",
//         "cvc": "123",
//         "number": "4111111111111111"
//       }
//   }
exports.genrateToken = (req, res) => {
    if (!req.user._id) return res.json({
        success: false,
        message: 'Not Authorized.'
    })
    async.waterfall([
        function (callback) {
            if (!req.body.tokenize_cc_address_city) {
                return callback('Please fill card city.');
            } else if (!req.body.tokenize_cc_address_country) {
                return callback('Please fill card country.');
            } else if (!req.body.tokenize_cc_address_postalcode) {
                return callback('Please fill card postalCode.');
            } else if (!req.body.tokenize_cc_address_region) {
                return callback('Please fill card region.');
            } else if (!req.body.tokenize_cc_name) {
                return callback('Please fill name.');
            } else if (!req.body.tokenize_cc_number) {
                return callback('Please fill number.');
            } else if (!req.body.tokenize_cc_expyear) {
                return callback('Please fill expYear.');
            } else if (!req.body.tokenize_cc_expmonth) {
                return callback('Please fill expMonth.');
            } else if (!req.body.tokenize_cc_cvc) {
                return callback('Please fill cvc.');
            }
            // else if(!req.body.Amount){
            //     return callback('Amount is Not Provided');
            // } 
            // else if(!req.body.ProjectIds){
            //     return callback('projectsId not provided')
            // } 
            else if (req.body.tokenize_cc_address_country.length != 2) {
                return callback('Country code size must be 2')
            } else {
                callback(null);
            }

        },
        function (callback) {
            let projectIds = JSON.parse(req.body.ProjectIds);
            callback(null, projectIds)
        },
        function (ProjectIds, callback) {
            models.User.findOne({
                _id: req.user._id
            }, (err, user) => {
                if (err) return callback(err);
                callback(null, user);
            })
        },
        function (user, callback) {
            // add the amount
            var totalPrice = 0;
            user.transaction.forEach(function (item) {
                totalPrice = totalPrice + parseInt(item.amount);
            })
            callback(null, user, totalPrice);
        },
        function (user, totalPrice, callback) {
            var charge = {
                // "amount": req.body.amount,
                "card": {
                    "expYear": req.body.tokenize_cc_expyear,
                    "expMonth": req.body.tokenize_cc_expmonth,
                    "address": {
                        "region": req.body.tokenize_cc_address_region,
                        "postalCode": req.body.tokenize_cc_address_postalcode,
                        "streetAddress": req.body.tokenize_cc_address_street,
                        "country": req.body.tokenize_cc_address_country,
                        "city": req.body.tokenize_cc_address_city
                    },
                    "name": req.body.tokenize_cc_name,
                    "cvc": req.body.tokenize_cc_cvc,
                    "number": req.body.tokenize_cc_number
                },


            }



            models.QuickBook.find({}, function (err, quickbook) {
                if (err) return callback(err);
                var qbo = new quickBook(config.quickbooks.consumerKey,
                    config.quickbooks.consumerSecret,
                    quickbook[0].accessToken, /* oAuth access token */
                    false, /* no token secret for oAuth 2.0 */
                    quickbook[0].realmId,
                    quickbook[0].refreshToken, /* use a sandbox account */
                    '2.0', /* oauth version */
                    true,
                    true /* refresh token */);

                qbo.createToken(charge, function (err, cardToken) {
                    if (err) return callback(err)
                    let token = cardToken.value
                    //  refreshToken(quickbook[0].accessToken,quickbook[0].refreshToken,quickbook[0].realmId,function(data){
                    qbo.charge({
                        amount: totalPrice,
                        token,
                        "currency": "USD",
                        "context": {
                            "mobile": "false",
                            "isEcommerce": "true"
                        }
                    }, function (err, charged) {
                        if (err) {
                            if (err.code == "AuthenticationFailed") {
                                refreshToken(quickbook[0].accessToken, quickbook[0].refreshToken, quickbook[0].realmId, function (data) {
                                    if (data.success) {
                                        qbo.token = data.data
                                        qbo.charge({
                                            amount: totalPrice,
                                            token,
                                            "currency": "USD",
                                            "context": {
                                                "mobile": "false",
                                                "isEcommerce": "true"
                                            }
                                        }, function (err, charged) {
                                            if (err) return callback(err)
                                            callback(null, user, charged)
                                        })
                                    }
                                })
                            } else {
                                return callback(err)
                            }
                        } else {
                            callback(null, user, charged);
                        }
                    })
                })
            })
            // })

        },

        function (user, charge, callback) {
            var paymentArr = []
            async.eachSeries(user.transaction, function (transaction, next) {
                models.Project.findOne({
                    _id: transaction.projectId
                })
                    .exec(function (err, currentProject) {
                        if (err) return callback(err);
                        if (!currentProject) callback('No project found!')
                        var payment = new models.Payment({
                            status: charge.status,
                            amount: transaction.amount,
                            currency: charge.currency,
                            chargeId: charge.id,
                            // cardNO: req.body.tokenize_cc_number,
                            postalCode: req.body.tokenize_cc_address_postalcode,
                            authCode: charge.authCode,
                            user: req.user._id,
                            project: transaction.projectId,
                            // RealmId: req.session.realmId,
                            // name: req.body.tokenize_cc_name,
                            projectname: currentProject.name
                        });
                        currentProject.paymentStats = true;
                        currentProject.save(function (errr, savedProject) {
                            if (err) return callback(errr);
                            payment.save(function (err, paymentObject) {
                                if (err) return callback(err);
                                paymentArr.push(paymentObject)
                                next();
                            })
                        })

                    })
            }, function (err) {
                callback(null, paymentArr, user);
            })
        },
        // toEmail,  paymentinfo,
        function (payment, user, callback) {
            var paymentInfo = {}
            async.eachSeries(payment, function (item, next) {
                paymentInfo.projectName = item.projectname;
                paymentInfo.chargeId = item.chargeId;
                paymentInfo.amount = item.amount;
                // paymentInfo.status = payment.status;
                nodemailer.paymentDetail(req.user.email, paymentInfo, function (err, messageId, response) {
                    if (err) return callback(err);
                    // callback(null, user);
                    next();
                });
            }, function (err) {
                callback(null, user);
            })
        },
        function (user, callback) {
            user.transaction = [];
            user.updatedAt = Date.now();
            user.save(function (err, updatedUser) {
                if (err) return callback(err);
                callback(null, user);
            })
        }
    ],
        function (err, user) {
            if (err) {
                res.render('paymentGateway', {
                    message: err,
                    Amount: req.body.Amount,
                    ProjectIds: req.body.ProjectIds
                });
            } else {
                res.redirect('/');
            }
        });


}

const refreshToken = (accessToken, refreshToken, realmId, cb) => {

    var quick = new QuickBookAcc(config.quickbooks.consumerKey,
        config.quickbooks.consumerSecret,
        accessToken, /* oAuth access token */
        false, /* no token secret for oAuth 2.0 */
        realmId,
        config.useSandbox, /* use a sandbox account */
        true, /* turn debugging on */
        4, /* minor version */
        '2.0', /* oauth version */
        refreshToken /* refresh token */);

    quick.refreshAccessToken(function (err, token) {
        console.log(`err in token`,err)
        console.log(`\ token`,token)
        if (err) {
            cb({
                succss: false,
                error: err
            })
        } else {
            models.QuickBook.find({}, function (err, quickbook) {
                if (err) return res.json({
                    success: false,
                    message: err
                });
                quickbook[0].accessToken = token.access_token;
                quickbook[0].refreshToken = token.refresh_token;
                quickbook[0].updatedAt = Date.now();
                quickbook[0].save(function (err, done) {
                    if (err) cb({
                        succss: false,
                        error: err
                    })
                    cb({
                        success: true,
                        data: token.access_token
                    });
                })

            })
        }
    })

}


exports.makePaymentWithSaved = (req, res) => {
    models.QuickBook.find({}, function (err, quickbook) {
        if (err) return res.json({
            success: false,
            message: err
        })
        var qbo = new quickBookLocalModule(config.quickbooks.consumerKey,
            config.quickbooks.consumerSecret,
            quickbook[0].accessToken, /* oAuth access token */
            false, /* no token secret for oAuth 2.0 */
            quickbook[0].realmId,
            quickbook[0].refreshToken, /* use a sandbox account */
            '2.0', /* oauth version */
            true,
            true /* refresh token */);

        refreshToken(quickbook[0].accessToken, quickbook[0].refreshToken, quickbook[0].realmId, function (data) {
            if (data.success) {
                qbo.token = data.data

                // qbo.charge({
                //     amount: totalPrice,
                //     token,
                //     "currency": "USD",
                //     "context": {
                //         "mobile": "false",
                //         "isEcommerce": "true"
                //     }
                // }, function (err, charged) {

                req.body.currency = "USD";
                req.body.context = {
                    "mobile": "false",
                    "isEcommerce": "true"
                }
                req.body.cvc = 123
                qbo.charge(req.body, function (err, cards) {
                    if (err) return res.json({ success: false, message: err.errors[0].message })
                    res.json({ success: true, cards })
                })
            }
        })
    })
}


exports.saveCardsDetail = (req, res) => {
    if (!req.body.number) {
        return res.json({ success: false, message: 'please provide card Number' })
    } else if (!req.body.expMonth) {
        return res.json({ success: false, message: 'please provide card expMonth' })
    } else if (!req.body.expYear) {
        return res.json({ success: false, message: 'please provide card expYear' })
    } else if (!req.body.name) {
        return res.json({ success: false, message: 'please provide card holder name' })
    } else if (!req.body.address.streetAddress) {
        return res.json({ success: false, message: 'please provide card streetAddress.' })
    } else if (!req.body.address.city) {
        return res.json({ success: false, message: 'please provide card city' })
    }
    else if (!req.body.address.region) {
        return res.json({ success: false, message: 'please provide card region' })
    } else if (!req.body.address.country) {
        return res.json({ success: false, message: 'please provide card postalCode' })
    } else if (!req.body.address.postalCode) {
        return res.json({ success: false, message: err.errors[0].message })
    }
    models.QuickBook.find({}, function (err, quickbook) {
        if (err) return res.json({
            success: false,
            message: err
        })
        var qbo = new quickBookLocalModule(config.quickbooks.consumerKey,
            config.quickbooks.consumerSecret,
            quickbook[0].accessToken, /* oAuth access token */
            false, /* no token secret for oAuth 2.0 */
            quickbook[0].realmId,
            quickbook[0].refreshToken, /* use a sandbox account */
            '2.0', /* oauth version */
            true,
            true /* refresh token */);

        refreshToken(quickbook[0].accessToken, quickbook[0].refreshToken, quickbook[0].realmId, function (data) {
            if (data.success) {
                qbo.token = data.data
                /*testing purpose */

                // let sample = {
                //     "number": "4408041234567893",
                //     "expMonth": "12",
                //     "expYear": "2026",
                //     "name": "Test User",
                //     "address": {
                //         "streetAddress": "1245 Hana Rd",
                //         "city": "Richmond",
                //         "region": "VA",
                //         "country": "US",
                //         "postalCode": "44112"
                //     }
                // }

                qbo.createCard(req.user._id, req.body, function (err, charge) {
                    if (err) return res.json({ success: false, message: err })
                    res.json({ success: true, message: `your card is saved.` })

                })
            }
        })


    })
}

exports.getuserCards = (req, res) => {

    models.QuickBook.find({}, function (err, quickbook) {
        if (err) return res.json({
            success: false,
            message: err
        })
        var qbo = new quickBookLocalModule(config.quickbooks.consumerKey,
            config.quickbooks.consumerSecret,
            quickbook[0].accessToken, /* oAuth access token */
            false, /* no token secret for oAuth 2.0 */
            quickbook[0].realmId,
            quickbook[0].refreshToken, /* use a sandbox account */
            '2.0', /* oauth version */
            true,
            true /* refresh token */);

        refreshToken(quickbook[0].accessToken, quickbook[0].refreshToken, quickbook[0].realmId, function (data) {
            if (data.success) {
                console.log(`quickbook data`,data)
                qbo.token = data.data
                qbo.getCustomerCard(req.user._id, function (err, cards) {
                    if (err) return res.json({ success: false, message: err.errors[0].message })
                    res.json({ success: true, cards })
                })
            }
        })
    })
}


exports.deleteCard = (req, res) => {
    models.QuickBook.find({}, function (err, quickbook) {
        if (err) return res.json({
            success: false,
            message: err
        })
        var qbo = new quickBookLocalModule(config.quickbooks.consumerKey,
            config.quickbooks.consumerSecret,
            quickbook[0].accessToken, /* oAuth access token */
            false, /* no token secret for oAuth 2.0 */
            quickbook[0].realmId,
            quickbook[0].refreshToken, /* use a sandbox account */
            '2.0', /* oauth version */
            true,
            true /* refresh token */);

        refreshToken(quickbook[0].accessToken, quickbook[0].refreshToken, quickbook[0].realmId, function (data) {
            if (data.success) {
                qbo.token = data.data
                qbo.deleteCard(req.user._id, req.body.id, function (err, cards) {
                    if (err) return res.json({ success: false, message: err.errors[0].message })
                    res.json({ success: true, message: 'card deleted.' })
                })
            }
        })
    })
}

exports.getPaymentLogs = (req, res) => {
    if (!req.user._id) return res.json({
        success: false,
        message: 'Not Logged In!'
    })
    models.User.findOne({
        _id: req.user._id
    })
        .populate({
            path: 'role',
            select: 'name'
        })
        .exec((err, user) => {
            if (err) return res.json({
                success: false,
                message: err
            })
            if (!user) return res.json({
                success: false,
                message: 'NO USER WITH THAT ID.'
            })
            if (user.role.name == !'super_user') return res.json({
                success: false,
                message: 'YOU ARE NOT AUTHORIZED'
            })
            models.Payment.find({})
                .exec(function (err, logs) {
                    if (err) return res.json({
                        success: false,
                        message: err
                    })
                    if (!logs) return res.json({
                        success: false,
                        message: 'NO LOGS CREATED YET.'
                    })
                    res.json({
                        success: false,
                        message: 'Logs Found',
                        data: logs
                    })
                })
        })
}

exports.createCardForQuickbook = (req, res) => {
    if (!req.user._id) return res.json({ success: false, message: 'NOT AUTHORIZED!.' })


}

exports.getPaymentDetail = (req, res) => {
    if (!req.body.chargeId) return res.json({
        success: false,
        message: 'ChargeId is not Provided.'
    })
    models.QuickBook.find({}, function (err, quickbook) {
        if (err) return res.json({
            success: false,
            message: err
        })
        var qbo = new quickBook(config.quickbooks.consumerKey,
            config.quickbooks.consumerSecret,
            quickbook[0].accessToken, /* oAuth access token */
            false, /* no token secret for oAuth 2.0 */
            quickbook[0].realmId,
            quickbook[0].refreshToken, /* use a sandbox account */
            '2.0', /* oauth version */
            true,
            true /* refresh token */);

        qbo.getCharge(req.body.chargeId, function (err, charge) {
            if (err) {
                if (err.code == "AuthenticationFailed") {
                    refreshToken(quickbook[0].accessToken, quickbook[0].refreshToken, quickbook[0].realmId, function (data) {
                        if (data.success) {
                            qbo.token = data.data
                            qbo.getCharge(req.body.chargeId, function (err, charged) {
                                if (err) return res.json({
                                    success: false,
                                    message: err
                                })
                                res.json({
                                    success: true,
                                    message: charge
                                })
                            })
                        }
                    })
                } else {
                    return res.json({
                        success: false,
                        message: err
                    })
                }
            } else {
                res.json({
                    success: true,
                    message: charge
                })
            }
        })

    })

}

exports.getQuickBookToken = (req, res) => {
    models.QuickBook.find({}, function (err, quickbook) {
        if (err) return res.json({
            success: false,
            message: err
        });
        res.json({
            success: false,
            data: quickbook[0]
        })
    })
}

exports.renderPaymentPage = (req, res) => {
    console.log('<<<<<<<<<<<<<<<---------render')
    res.render('paymentGateway', {
        Amount: req.body.amount,
        message: '',
        ProjectIds: req.body.ProjectIds
    })
}


exports.callbackToQuickBook = function (req, res) {

    tools.intuitAuth.code.getToken(req.originalUrl).then(function (token) {
        console.log(token)
        tools.saveToken(req.session, token)
        req.session.realmId = req.query.realmId
        models.QuickBook.find({}, function (err, quickbook) {
            if (err) return res.json({
                success: false,
                message: err
            });
            if (!quickbook.length) {
                var quickbook = new models.QuickBook({
                    accessToken: token.accessToken,
                    refreshToken: token.refreshToken,
                    realmId: req.query.realmId
                });
                quickbook.save(function (err, quick) {
                    if (err) return res.json({
                        success: false,
                        data: err
                    });
                    res.render('paymentGateway', {
                        Amount: "",
                        message: token.accessToken,
                        ProjectIds: ""
                    })
                })
            } else {
                quickbook[0].accessToken = token.accessToken;
                quickbook[0].refreshToken = token.refreshToken;
                quickbook[0].save(function (err, done) {
                    if (err) return res.json({
                        success: false,
                        data: err
                    });
                    res.render('paymentGateway', {
                        Amount: "",
                        message: token.accessToken,
                        ProjectIds: ""
                    })
                })
            }
        })


    },
        function (err) {
            console.log(err)
            res.send(err)
        })

}

exports.makePayment = (req, res) => {
    // var charge = {
    //     "amount": totalPrice,
    //     "card": {
    //         "expYear": "2020",
    //         "expMonth": "02",
    //         "address": {
    //             "region": "CA",
    //             "postalCode": "94086",
    //             "streetAddress": "1130 Kifer Rd",
    //             "country": "US",
    //             "city": "Sunnyvale"
    //         },
    //         "name": "emulate=0",
    //         "cvc": "123",
    //         "number": "4111111111111111"
    //     },
    //     "currency": "USD"
    // }
    // var RequestId = Guid.create();



    // var charge = {
    //     "amount": totalPrice,
    //     "card": {
    //         "expYear": "2020",
    //         "expMonth": "02",
    //         "address": {
    //             "region": "CA",
    //             "postalCode": "94086",
    //             "streetAddress": "1130 Kifer Rd",
    //             "country": "US",
    //             "city": "Sunnyvale"
    //         },
    //         "name": "emulate=0",
    //         "cvc": "123",
    //         "number": "4111111111111111"
    //     },
    //     "currency": "USD"
    // }

    async.waterfall([
        function (callback) {

            if (!req.body.tokenize_cc_address_city) {
                return callback('Please fill card city.');
            } else if (!req.body.tokenize_cc_address_country) {
                return callback('Please fill card country.');
            } else if (!req.body.tokenize_cc_address_postalcode) {
                return callback('Please fill card postalCode.');
            } else if (!req.body.tokenize_cc_address_region) {
                return callback('Please fill card region.');
            } else if (!req.body.tokenize_cc_name) {
                return callback('Please fill name.');
            } else if (!req.body.tokenize_cc_number) {
                return callback('Please fill number.');
            } else if (!req.body.tokenize_cc_expyear) {
                return callback('Please fill expYear.');
            } else if (!req.body.tokenize_cc_expmonth) {
                return callback('Please fill expMonth.');
            } else if (!req.body.tokenize_cc_cvc) {
                return callback('Please fill cvc.');
            } else {
                callback(null);
            }


            // var emailValid = validator.isEmail(req.body.email);
            // if (!emailValid) { return cb('Email address not valid.')); }
        },
        function (callback) {
            models.User.findOne({
                _id: req.user._id
            }, (err, user) => {
                if (err) return callback(err);
                callback(null, user);
            })
        },
        function (user, callback) {
            // add the amount
            var totalPrice = 0;
            user.transaction.forEach(function (item) {
                totalPrice = totalPrice + parseInt(item.amount);
            })
            callback(null, user, totalPrice);
        },
        function (user, totalPrice, callback) {
            var charge = {
                "amount": totalPrice,
                "card": {
                    "expYear": req.body.tokenize_cc_expyear,
                    "expMonth": req.body.tokenize_cc_expmonth,
                    "address": {
                        "region": req.body.tokenize_cc_address_region,
                        "postalCode": req.body.tokenize_cc_address_postalcode,
                        "streetAddress": req.body.tokenize_cc_address_street,
                        "country": req.body.tokenize_cc_address_country,
                        "city": req.body.tokenize_cc_address_city
                    },
                    "name": req.body.tokenize_cc_name,
                    "cvc": req.body.tokenize_cc_cvc,
                    "number": req.body.tokenize_cc_number
                },
                "currency": 'USD',
                "context": {
                    "mobile": "false",
                    "isEcommerce": "true"
                }
            }
            //DUMMY OBJECT
            var charge1 = {
                "amount": "20",
                "card": {
                    "expYear": "2020",
                    "expMonth": "02",
                    "address": {
                        "region": "CA",
                        "postalCode": "94086",
                        "streetAddress": "1130 Kifer Rd",
                        "country": "US",
                        "city": "Sunnyvale"
                    },
                    "name": "emulate=0",
                    "cvc": "123",
                    "number": "4111111111111111"
                },
                "currency": "USD",
                "context": {
                    "mobile": "false",
                    "isEcommerce": "true"
                }
            }

            var RequestId = Guid.create();

            request({
                url: config.quickbooks.payment_Link,
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + req.session.data.access_token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Request-Id': RequestId.value
                },
                body: JSON.stringify(charge)
            }, function (err, response, body) {
                var bodyConvertedToJson = JSON.parse(body);
                if (err) {
                    return callback(err);
                } else if (bodyConvertedToJson.errors) {
                    return callback('Cards Details are incorrect');
                }
                callback(null, user, totalPrice, body, charge);
            })

        },
        function (user, totalPrice, body, charge, callback) {
            var bodyConvertedToJson = JSON.parse(body);
            var paymentArr = []
            async.eachSeries(user.transaction, function (transaction, next) {
                models.Project.findOne({
                    _id: transaction.projectId
                })
                    .exec(function (err, currentProject) {
                        if (err) return callback(err);
                        if (!currentProject) callback('No project found!')
                        var payment = new models.Payment({
                            status: bodyConvertedToJson.status,
                            amount: transaction.amount,
                            currency: bodyConvertedToJson.currency,
                            chargeId: bodyConvertedToJson.id,
                            cardNO: charge.card.number,
                            postalCode: charge.card.address.postalCode,
                            authCode: bodyConvertedToJson.authCode,
                            user: req.user._id,
                            project: transaction.projectId,
                            RealmId: req.session.realmId,
                            name: req.body.tokenize_cc_name,
                            projectname: currentProject.name
                        });
                        currentProject.paymentStats = true;
                        currentProject.save(function (err, savedProject) {
                            payment.save(function (err, paymentObject) {
                                if (err) return callback(err);
                                paymentArr.push(paymentObject)
                                next();
                            })
                        })

                    })
            }, function (err) {
                callback(null, paymentArr, user);
            })
        },
        // toEmail,  paymentinfo,
        function (payment, user, callback) {
            var paymentInfo = {}
            async.eachSeries(payment, function (item, next) {
                paymentInfo.projectName = item.projectname;
                paymentInfo.chargeId = item.chargeId;
                paymentInfo.amount = item.amount;
                // paymentInfo.status = payment.status;
                nodemailer.paymentDetail(req.user.email, paymentInfo, function (err, messageId, response) {
                    if (err) return callback(err);
                    // callback(null, user);
                    next();
                });
            }, function (err) {
                callback(null, user);
            })

            // nodemailer.paymentDetail(req.user.email, payment, function(err, messageId, response) {
            //     if (err) return callback(err);
            //     callback(null, user);
            // });
        },
        function (user, callback) {
            user.transaction = [];
            user.updatedAt = Date.now();
            user.save(function (err, updatedUser) {
                if (err) return callback(err);
                callback(null, user);
            })
        }
    ],
        function (err, user) {
            // result now equals 'done'
            if (err) {
                // user.transaction = [];
                // user.updatedAt = Date.now();
                // user.save(function(err, updatedUser) {
                return res.json({
                    success: false,
                    message: err
                });
                // })
            } else {
                // res.json({ success: true, message: 'Payment done' })
                res.redirect('/');
                // res.writeHead(301, { Location: config.server.productionUrl });
                // res.end();
            }

            // res.redirect('/');
            // res.writeHead(301, { Location: 'http://ec2-54-202-155-239.us-west-2.compute.amazonaws.com:4000/' });
            // res.end();
        });


}
var async = require('async');
var models = require('mongoose').models;
var AWS = require('aws-sdk');
var config = require('./../settings/config');
var formidable = require('formidable');
var fs = require('fs');
var  moment = require('moment');
var nodemailer = require('./../components/mailer');
var ffmpeg = require('ffmpeg');
var exec = require('child_process').exec;
AWS.config.credentials = config.awsCredentials;
var s3 = new AWS.S3({
    apiVersion: '2012–09–25'
});
var eltr = new AWS.ElasticTranscoder({
    apiVersion: '2012–09–25',
    region: 'us-west-2'
});



var elastictranscoder = new AWS.ElasticTranscoder();


module.exports.all = function(req, res) {
    models.Organization.find()
        .select('-__v')
        .exec(function(err, org) {
            if(err) return res.json({ err: err });
            res.json(org);
        });
};


module.exports.subscribe = function(req, res) {
    console.log(req.text);
};




module.exports.s3download = function(req, res) {
    // console.log(req.text);
    var params = {
        Bucket: 'trascoder-output-videos', /* required */
        Key: 'test8.mp4', /* required */
    };
    s3.getObject(params, function(err, data) {
        if (err) return (err); // an error occurred
        data.pipe(res);         // successful response
    });
};


module.exports.download = function(req, res) {
    // console.log(req.text);
    var params = {
        Bucket: req.query.Bucket, /* required */
        Key: req.query.Key, /* required */
    };
    var fileStream = s3.getObject(params).createReadStream();     
    res.header('Content-Disposition', 'attachment; filename=' + params.Key);
       
    fileStream.pipe(res); // successful response
};



module.exports.transcodeAcknowledgement = function(req, res) {
    var notification = JSON.parse(req.text);
    var message = JSON.parse(notification.Message);
    if(message.state == "COMPLETED") {
         console.log('message:', message)
        async.eachSeries(message.outputs, function(item, next) {
            var params = {
                Bucket: 'trascoder-output-videos', /* required */
                Key: item.key, /* required */
            };
            nodemailer.sender(message.userMetadata.user, 'transcoded video', params, function(err, messageId, response) {
                console.log('mailed:', response);
                console.log('err:', err)
                if(err) return res.json(err);
                next()
            })
        }, function(err) {
            console.log('err:', err)
            if(err) return res.json(err);
            return res.json({
                success: true
            });
        })
        
    }
};


module.exports.transcode = function(req, res) {
    var params = {
        PipelineId:'1495491618755-iw02xq',    /*required*/
        Input: {
            Key: req.body.video || 'vsshort-aac.mkv',
            InputCaptions: {
                CaptionSources:[{
                    Key: req.body.subtitle,
                    Label: 'en',
                    Language: 'en'
                }],
                MergePolicy: 'MergeOverride'
            }
        },
        Output: {
            Key: req.body.video + '_transcoded' || 'test28.mp4',
            PresetId: '1351620000001-100020',
            Rotate: 'auto',
            Captions: {
                CaptionFormats: [
                    {
                        Format: 'mov-text'
                    }
                    /* more items */
                ]
            }
        }
    }
    eltr.createJob(params, function(err, data){
        if (err){
            return console.log(err);
        } else {
            return console.log(data);
        }
        context.succeed('Job well done');
    });
};

var uploadDoc = function(name, path, callback) {
    var read = fs.createReadStream(path);
    var params = {Bucket: 'transcoding-video-caption', Key: name, Body: read};
    s3.upload(params, function(err, data) {
        if(err) return callback (err);
        callback(null, data);
    })
}


module.exports.s3upload = function(req, res) {
    var data = {};
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        uploadDoc(files.subtitles.name, files.subtitles.path, function(err, subtitleData) {
            if(err) return res.json({error : err});
            uploadDoc(files.video.name, files.video.path, function(err, videoData) {
                if(err) return res.json({err:err});
                var extension = videoData.Key.split('.');
                var params = {
                    PipelineId:'1495491618755-iw02xq',    /*required*/
                    Input: {
                        Key: videoData.Key,
                        InputCaptions: {
                            CaptionSources:[{
                                Key: subtitleData.Key,
                                Label: 'en',
                                Language: 'en'
                            }],
                            MergePolicy: 'MergeOverride'
                        }
                    },
                    Output: {
                        Key: extension[0]+ '-' + moment().unix().toString() + '-transcoded.mp4',
                        PresetId: '1351620000001-100020',
                        Rotate: 'auto',
                        Captions: {
                            CaptionFormats: [
                                {
                                    Format: 'mov-text'
                                }
                                /* more items */
                            ]
                        }
                    },
                    UserMetadata: {
                        'user': fields.email,
                        /* '<String>': ... */
                    }
                }
                eltr.createJob(params, function(err, data){
                    if (err){
                        return console.log(err);
                    } else {
                        res.writeHead(301,
                        {Location: 'http://ec2-54-202-155-239.us-west-2.compute.amazonaws.com:4000/'}
                        );
                        res.end();
                    };
                });
            });
        });
    });  
};

// Changes for upload file

module.exports.uploadFiles = function(req, res) {
    var localAppPath = '/home/nodejs/Downloads/Rahul_Beniwal_new/Rahul_Beniwal_Codes/daniel_sommer/code/post_cap/'
    var serverAppPath = '/home/ubuntu/phase_2/post_cap/';
    // var serverAppPath = '/home/lenovo02/Documents/post_cap/'
    var form = new formidable.IncomingForm();
    console.log(serverAppPath)
    form.parse(req, function (err, fields, files) {
        var subtitleOldpath = files.subtitles.path;
        var subtitleNewpath = serverAppPath + 'app/media/captions/' +  moment().unix().toString() + '_' +  files.subtitles.name;
        var splitFile = files.subtitles.name.split('.');
        var outputSubtitlePath = serverAppPath + 'app/media/captions/' + 'output_'  + moment().unix().toString() + '_' +  splitFile[1] + '.ass';
        var videoOldpath = files.video.path;
        var videoNewpath = serverAppPath + 'app/media/inputVideos/' +  moment().unix().toString() + '_' +  files.video.name;
        var outputName = 'output_' + moment().unix().toString() + '_' +  files.video.name;
        var outputVideoPath = serverAppPath+ 'app/media/outputVideos/' + outputName;
        if(fields.beneath_video) {
            var transcodType = fields.beneath_video;
        };
        if(fields.over_video) {
            var transcodType = fields.over_video;
        }
        console.log(files);
        fs.rename(subtitleOldpath, subtitleNewpath, function (err) {
            if (err) throw err;
            fs.rename(videoOldpath, videoNewpath, function (err) {
                if (err) throw err;
                var logModel = new models.UploadLogs({
                    video: videoNewpath,
                    outputVideo: outputVideoPath,
                    subtitle: subtitleNewpath,
                    subtitleAlias: outputSubtitlePath,
                    outputName: outputName,
                    email: fields.email,
                    status: 'waiting',
                    transcodingType: transcodType
                });
                logModel.save(function(err, log) {
                    // if(err) return res.json({ error: err });
                    res.writeHead(301,
                        {Location: 'http://ec2-18-220-238-26.us-east-2.compute.amazonaws.com:4000/'}
                        );
                        res.end();
                });        
            });
        });   
    }); 
}

module.exports.ffmpeg = function(req, res) {
    var child = exec('ffmpeg -i /home/nodejs/Downloads/Rahul_Beniwal/Rahul_Beniwal_Codes/david_somer/code/stagin_version_1may/app/media/captions/1497386789_captions.srt /home/nodejs/Downloads/Rahul_Beniwal/Rahul_Beniwal_Codes/david_somer/code/stagin_version_1may/app/media/captions/subtitles.ass', function(err, stdout, stderr) {
        console.log(stdout);
        // console.log(stderr);
        var child = exec('ffmpeg -i /home/nodejs/Downloads/Rahul_Beniwal/Rahul_Beniwal_Codes/david_somer/code/stagin_version_1may/app/media/inputVideos/1497386789_BWV248-6-11.mp4 -vf ass=/home/nodejs/Downloads/Rahul_Beniwal/Rahul_Beniwal_Codes/david_somer/code/stagin_version_1may/app/media/captions/subtitles.ass /home/nodejs/Downloads/Rahul_Beniwal/Rahul_Beniwal_Codes/david_somer/code/stagin_version_1may/app/media/inputVideos/mysubtitledmovie.mp4', function(err, stdout, stderr) {
            console.log('opencaptions', stdout);
            // console.log('opencaptions', stderr);
        });
    });
};
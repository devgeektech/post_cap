const mongoose = require('mongoose');
const async = require('async');
const models = require('mongoose').models;
const isEmpty = require('./../validators/isEmpty')
const Validator = require('validator');

exports.settingAutoAssigner = (req, res) => {
    if (req.user.role !== 'super_user') return res.json({ succes: false, message: 'Not Authorized!' })
    if (!req.body.time) return res.json({ succes: false, message: 'Please provide Time.' })
    req.body.time = !isEmpty(req.body.time) ? req.body.time : '';
    // if (Validator.isEmpty(req.body.time)) {
    //     return res.json({ succes: false, message: 'Please provide Time.' })
    // }
    // let timeArray = req.body.time.split(':')
    // let convetedTime = timeArray[0] * 3600 + timeArray[1] * 60 + timeArray[2] * 60;

    new models.Setting({
        time: req.body.time.key,
        createdBy: req.user._id
    }).save()
        .then(user => res.json({ success: true, message: 'Timing Saved.' }))
        .catch(err => res.status(400).json({ success: false, message: err }))
}

exports.getSetting = (req, res) => {

    if (req.user.role !== 'super_user') return res.json({ succes: false, message: 'Not Authorized!' })
    models.Setting.find()
        .then(setting => {
            if (setting.length == 0) return res.json({ success: false, found:false, message: 'No setting Found' })
            res.json({ success: true,found:true, message: setting[0] })
        }).catch(err => { res.json({ success: false, message: err }) })

}

exports.editSetting = (req, res) => {
    // if(!req.body.status) return  res.json({succes:false,message:'Status is not provided.'})
    if (req.user.role !== 'super_user') return res.json({ succes: false, message: 'Not Authorized!' })
    let editedData= {}
    if(req.body.hasOwnProperty("status")){
        editedData.status = req.body.status
    } 
    if(req.body.hasOwnProperty("time")){
        editedData.time = req.body.time.key
    } 
    models.Setting.findOneAndUpdate({
        _id:req.body.id
    },
        editedData
    )
        .then(setting => {
            if (!setting) return res.json({ success: false, found:false, message: 'No setting Found' })
            res.json({ success: true,found:true, message: 'Data Updated!.'})
        }).catch(err => { res.json({ success: false, message: err }) })

}


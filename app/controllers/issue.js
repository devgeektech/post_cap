var models = require('mongoose').models;
var async = require('async');
var notificationRefer = require('./notifications');

exports.getAllIssue = (req, res) => {
    if (req.user.role === 'super_user' || req.user.role === 'project_admin') {
       
        models.IssueReport.find(req.body)
            .populate([{ path: 'from', select: 'firstname lastname email' }, { path: 'project', select: 'name',populate:{
                path:'service',
                select:'name'
            } }])
            .sort({ createdAt: -1 })
            .then(issues => {
                if (!issues) return res.json({ success: false, message: 'No Issues Found' })
                res.json({ success: false, message: issues })
            }).catch(err => { res.json({ success: false, message: err }) })
    } else {
        res.json({ success: false, message: 'Not Authorized.' })
    }

}


exports.markArchieve = (req, res) => {
    if(req.body.type !== 'Archieve') return res.json({success:false,message:'Please provide a correct Parameter'})
    if (req.user.role === 'super_user' || req.user.role === 'project_admin') {
        
        models.IssueReport.findByIdAndUpdate({ _id: req.body.id }, {
            archieve:true,updatedAt:Date.now()
        })
            .then(issue => {
                if (!issue) return res.json({ success: false, message: 'No Issues Found' })
                res.json({ success: false, message: 'Issue Archieved.' })
            }).catch(err => { res.json({ success: false, message: err }) })
    } else {
        res.json({ success: false, message: 'Not Authorized.' })
    }
}
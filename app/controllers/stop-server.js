var node_ssh = require('node-ssh');
var fs = require('fs');
var ssh = new node_ssh();
var path = require('path');
var models = require('mongoose').models;
var config = require('./../settings/config');
const async = require('async');
//creating the new instance of the server
const mongoose = require('mongoose');

function wordInString(s, word) {
    return new RegExp('\\b' + word + '\\b', 'i').test(s);
}



exports.stopServer = function(req, res) {
    ssh.connect({
        host: 'ec2-18-221-180-214.us-east-2.compute.amazonaws.com',
        username: 'ubuntu',
        privateKey: config.server.stopServerPath
    }).then(function() {
        console.log('connected')
        ssh.execCommand('cd /home/ubuntu/phase_2/post_cap && sudo forever stop server.js')
            .then(function(result) {
                res.json('server is stoped.')
            })
    })
}


exports.startServer = (req, res) => {
    ssh.connect({
        host: 'ec2-18-221-180-214.us-east-2.compute.amazonaws.com',
        username: 'ubuntu',
        privateKey: config.server.stopServerPath
    }).then(function() {
        console.log('connected')
        ssh.execCommand('cd /home/ubuntu/phase_2/post_cap && sudo forever start server.js')
            .then(function(result) {
                res.json('server is started.')
            })
    })
}

exports.outputListCaptionServer = (req, res) => {
    ssh.connect({
        host: '18.221.180.214',
        username: 'ubuntu',
        privateKey: config.server.stopServerPath
    }).then(function() {
        console.log('connected')
        ssh.execCommand('cd /home/ubuntu/phase_2/post_cap/app/media/outputVideos && sudo ls -lhS')
            .then(function(result) {
                var finalData = []
                var outputData = result.stdout.split("-rw-r--r-- 1 root root")
                outputData.shift()
                async.eachSeries(outputData, function(item, next) {
                    var listObject = {}
                    var fullName = item.substring(item.lastIndexOf(" output_") + 1, item.lastIndexOf("\n"));
                    var name = item.split('output_');
                    var nameFormat = name[1].split('\n');
                    listObject.dateAndSize = name[0];
                    listObject.name = nameFormat[0];
                    finalData.push(listObject);
                    next();
                }, function(err) {
                    if (err) return res.json({ success: false, message: err })
                    res.json({ success: true, data: finalData })
                });
            })
    })
}

exports.inputListCaptionServer = (req, res) => {
    ssh.connect({
        host: 'ec2-18-221-180-214.us-east-2.compute.amazonaws.com',
        username: 'ubuntu',
        privateKey: config.server.stopServerPath
    }).then(function() {
        console.log('connected')
        ssh.execCommand('cd /home/ubuntu/phase_2/post_cap/app/media/inputVideos && sudo ls -lhS')
            .then(function(result) {
                var finalData = []
                var outputData = result.stdout.split("-rw-r--r-- 1 root root")
                outputData.shift()
                async.eachSeries(outputData, function(item, next) {
                    var listObject = {}
                    var inputname = item.split(':');
                    var inputData = (inputname.length == 1) ? inputname[0].split(/ (.*)/) : inputname[1].split(/ (.*)/);
                    listObject.dateAndSize = inputname[0] + ':' + inputData[0];
                    listObject.name = inputData[1];
                    finalData.push(listObject);
                    next();
                }, function(err) {
                    if (err) return res.json({ success: false, message: err })
                    res.json({ success: true, data: finalData })
                });
            })
    })
}

exports.captionListCaptionServer = (req, res) => {
    ssh.connect({
        host: 'ec2-18-221-180-214.us-east-2.compute.amazonaws.com',
        username: 'ubuntu',
        privateKey: config.server.stopServerPath
    }).then(function() {
        console.log('connected')
        ssh.execCommand('cd /home/ubuntu/phase_2/post_cap/app/media/captions && sudo ls -lhS')
            .then(function(result) {
                var finalData = []
                var outputData = result.stdout.split("-rw-r--r-- 1 root root")
                outputData.shift()
                async.eachSeries(outputData, function(item, next) {
                    var listObject = {}
                    var inputname = item.split(':');
                    var inputData = (inputname.length == 1) ? inputname[0].split(/ (.*)/) : inputname[1].split(/ (.*)/);
                    // var inputData = inputname[1].split(/ (.*)/);
                    listObject.dateAndSize = inputname[0] + ':' + inputData[0];

                    listObject.name = inputData[1];
                    finalData.push(listObject);
                    next();
                }, function(err) {
                    if (err) return res.json({ success: false, message: err })
                    res.json({ success: true, data: finalData })
                });
            })
    })
}

exports.serverLogs = (req, res) => {
    ssh.connect({
        host: 'ec2-18-221-180-214.us-east-2.compute.amazonaws.com',
        username: 'ubuntu',
        privateKey: config.server.stopServerPath
    }).then(function() {
        console.log('connected')
        ssh.execCommand(`cd /home/ubuntu/phase_2/post_cap/app/media/outputVideos && sudo ls -lhS`)
            .then(function(outputData) {

                var outputArray = outputData.stdout.split("-rw-r--r-- 1 root root")
                outputArray.shift()
                ssh.execCommand('cd /home/ubuntu/phase_2/post_cap/app/media/inputVideos && sudo ls -lhS')
                    .then(function(inputData) {
                        var inputArray = inputData.stdout.split("-rw-r--r-- 1 root root")
                        var foundArray = [];
                        var notFound = []
                        inputArray.shift()
                        var inputArrayData = [];
                        var outputArrayData = [];
                        var finalObject = {
                            inputDataArray: inputArrayData,
                            outputDataArray: outputArrayData
                        }

                        async.eachSeries(inputArray, function(item, next) {
                            var listObject = {}
                            var inputname = item.split(':');
                            // var inputData = inputname[1].split(/ (.*)/);
                            var inputData = (inputname.length == 1) ? inputname[0].split(/ (.*)/) : inputname[1].split(/ (.*)/);
                            listObject.dateAndSize = inputname[0] + ':' + inputData[0];
                            listObject.name = inputData[1];
                            inputArrayData.push(listObject);
                            next();
                        }, function(err) {
                            if (err) return res.json({ success: false, message: err })
                                // res.json({ success: true, data: finalData })
                            async.eachSeries(outputArray, function(item, next) {
                                var listObject = {}
                                var fullName = item.substring(item.lastIndexOf(" output_") + 1, item.lastIndexOf("\n"));
                                var name = item.split('output_');
                                var nameFormat = name[1].split('\n');
                                listObject.dateAndSize = name[0];
                                listObject.name = nameFormat[0];
                                outputArrayData.push(listObject);
                                next();
                            }, function(err) {
                                if (err) return res.json({ success: false, message: err })
                                res.json({ success: true, data: finalObject })
                            });
                        });
                    })
            })
    })
}


exports.databaseConnectionAndRestart = (req, res) => {
    ssh.connect({
        host: 'ec2-18-221-180-214.us-east-2.compute.amazonaws.com',
        username: 'ubuntu',
        privateKey: config.server.stopServerPath
    }).then(function() {
        ssh.execCommand('cd /home/ubuntu/phase_2/post_cap/app/media/outputVideos && sudo rm -r req.body.filename')
            .then(function(output) {
        var MongoClient = require('mongodb').MongoClient,
            format = require('util').format;
        MongoClient.connect('mongodb://127.0.0.1:6666/uw45mypu', function(err, db) {
            if (err) return res.json({ success: false, message: err })
            var collection = db.collection('uploadlogs');
            collection.findOneAndUpdate({ outputName: req.body.filename }, {
                $set: { status: "waiting" }
            }, { returnOriginal: false, upsert: true }, function(err, log) {
                console.dir(log);
                if (err) return res.json({ success: false, message: err })
                res.json({ success: true, message: 'file is processed' })
            });
        });
        })

    })
}
var models = require('mongoose').models;
const socket = require('./../settings/socket')
var config = require('./../settings/config');
var webSocket = require('./../../server')
const mongoose = require('mongoose')
const Notification = require('./../models/Notification')
const moment = require('moment')

exports.createNotification = function (req, res) {
    var notification = new models.Notification({
        type: req.body.type,
        to: req.body.to,
        from: req.body.from,
        text: req.body.text,
        image: req.body.image,
        status: req.body.status
    });
    notification.save(function (err, newNotification) {
        if (err) {
            console.log(err);
        } else {
            res.send(newNotification)
        }
    })
}

exports.createNotificationForUser = function (type, to, from, text, extraData, image, callback) {

    models.User.findById(to)
        .exec((err, user) => {
            if (err) callback(err)
            if (!user) callback('User Not found.')
            let time = user.notificationTime

           
            new models.Notification({
                type: type,
                to: to,
                from: from,
                text: text,
                extraData: extraData,
                image: image
            }).save(function (err, newNotification) {
                if (err) {
                    return callback(err)
                } else {
                    webSocket.io.emit("notification", {
                        user: newNotification.to.toString()
                    })
                    return callback(null, true)
                }
            })
            // }
        })
}
exports.socketConfiguration = (io) => {
    io.emit("notification", {
        user: ''
    });
}

// useful if the req is sent by Id in params
exports.getNotificationListById = function (req, res) {
    models.Notification.find({
            to: req.user._id,
            status: "true"
        })
        .populate({
            path: 'from',
            select: 'firstname lastname'
        })
        .sort('-createdAt')
        .exec(function (err, notifications) {
            if (err) {
                res.json(err)
            } else {
                if (!notifications) return res.json({
                    success: false,
                    message: 'No Notification Found.'
                })
                res.json(notifications);
            }
        })
};

exports.updateNotificationById = function (req, res) {
    var userId = req.param.id;
    models.Notification.updateOne({
        _id: userId
    }, function (err, notification) {
        if (err) return res.json({
            err
        })
        notification.Status = false;
        res.status(200).send(notification);
    })
}

exports.notificationHide = (req, res) => {
    if (!req.body.id) res.json({
        success: false,
        message: 'Id not provided'
    })
    models.Notification.findOneAndUpdate({
        _id: req.body.id
    }, {
        status: false
    }, {
        new: true
    }, function (err, notification) {
        if (err) return res.json({
            success: false,
            message: err
        })
        if (!notification) return res.json({
            success: false,
            message: 'No notification Found!.'
        })
        res.status(200).json({
            success: true,
            message: 'Notification Read.'
        });
    })
}

exports.deleteNotificationById = function (req, res) {
    var notificationId = req.param.id;
    models.Notification.findOneAndRemove({
        _id: notificationId
    }, function (err, notification) {
        if (err) {
            console.log(err);
        } else {
            res.json({
                message: "Notification deleted"
            });
        }
    });
}
var models = require('mongoose').models;
var async = require('async');
var notificationRefer = require('./notifications');

exports.createMail = function (req, res) {
    var userId = req.body.userId //from
    var to = req.body.senderEmail; //to
    var subject = req.body.subject;
    var cc = req.body.cc;
    var body = req.body.body;
    var type = req.body.type; // if reply
    var messageId = req.body.messageId; // if reply
    var attachment = req.body.attachment;
    // async waterfall fall

    async.waterfall([
        function (cb) {
            var userArray = [];
            async.eachSeries(to, function (usermail, next) { // to check user is present
                models.User.findOne({
                    email: usermail
                }, function (err, userData) {
                    if (err) return cb(err);
                    if (userData) {
                        userArray.push(userData._id);
                        next();
                    } else {
                        next();
                    }
                })
            },
                function (err) {
                    if (userArray.length <= 0)
                        return cb('user not found');
                    cb(null, userArray)
                }
            )
        },
        function (userArray, cb) {

            if (type == 'reply') {
                var message = new models.Message({
                    to: userArray,
                    from: req.user._id,
                    subject: subject,
                    cc: cc || null,
                    body: body
                });

                message.save(function (err, message) {
                    if (err) return cb(err);
                    models.Message.findOneAndUpdate({
                        _id: messageId
                    }, {
                            $addToSet: {
                                relatedEmails: message._id
                            }
                        }, {
                            new: true
                        }
                        , function (err, messageData) {
                            if (err) return cb(err);
                            cb(null, message, userArray)
                        })
                })
            } else {
                var message = new models.Message({
                    to: userArray,
                    from: userId,
                    subject: subject,
                    cc: cc,
                    body: body,
                    attachment: attachment,
                    primary:true
                });

                message.save(function (err, message) {
                    if (err) return cb(err);
                    cb(null, message, userArray)
                })
            }
        },
        function (message, userArray, cb) {
            if (type == 'reply') {
                cb(null, message, userArray)
            } else {
                async.eachSeries(userArray, function (item, next) {
                    models.User.findOneAndUpdate({
                        _id: item
                    }, {
                            $addToSet: {
                                inbox: [message._id]
                            }
                        }, function (err, user) {
                            if (err) return cb(err);
                            next();
                        })
                }, function (err) {
                    cb(null, message, userArray)
                });
            }

        },
        function (message, userArray, cb) {
            async.eachSeries(userArray, function (element, next) {
                models.User.findOneAndUpdate({
                    _id: userId
                }, {
                        $addToSet: {
                            sentMail: [message._id]
                        }
                    }, function (err, sentUser) {
                        if (err) return cb(err);
                        next();
                    })
            }, function (err) {
                cb(null, message, userArray)
            });
        },

        function (message, userArray, cb) {
            //  type,to,from,text,extraData,image, callback
            models.User.findOne({
                _id: userId
            }, function (err, mainUser) {
                var notificationText = "New Mail is Issued from :" + mainUser.email;
                if (err) return cb(err);
                async.eachSeries(userArray, function (item, next) {
                    models.User.findOne({
                        _id: item
                    }, function (err, user) {
                        if (err) return cb(err);
                       
                        notificationRefer.createNotificationForUser('Mail created.', item, userId, notificationText, "", 'image', function (err, success) {
                            if (err) return cb(err);
                            console.log('notification created');
                        })
                        next();
                    })
                }, function (err) {
                    cb(null, message)
                });
            })
        }

    ], function (err, message) {
        if (err) return res.json({
            error: err
        });
        res.json({
            id: message._id
        });
    })

}

exports.getAllUserForMessage = (req, res) => {
    var userArr = []
    var userId = req.user._id.toString()
    if (req.params.role == 'super_user' || req.params.role == 'project_admin') {
        models.User.find()
            .populate({
                path: 'role',
                select: 'name'
            })
            .select('firstname lastname email role profile_picture')
            .exec(function (err, users) {
                if (err) return res.json({ success: false, message: err });
                if (!users) return res.json({ success: false, message: 'No users found!' })
                async.eachSeries(users, function (item, next) {
                    var compare = item._id.toString();
                    if (compare != userId) {
                        userArr.push(item)
                    }
                    next();
                }, function (err) {
                    res.json(userArr)
                });
            })
    } else {

        models.User.find()
            .populate({
                path: 'role',
                select: 'name'
            })
            .select('firstname lastname email role profile_picture')
            .exec(function (err, users) {
                if (err) return err;
                if (!users) return res.json({ success: false, message: 'No users found!' })
                async.eachSeries(users, function (item, next) {
                    if (item.role.name == "super_user" || item.role.name == "project_admin") {
                        userArr.push(item)
                    }
                    next();
                }, function (err) {
                    if (err) return res.json({ success: false, message: err })
                    res.json(userArr)
                });
            })
    }

}


// getMails

exports.getMailsAndSentMails = function (req, res) {
    var userId = req.params.userId;
    var type = req.params.data;
    models.User.findOne({
        _id: userId
    }, function (err, user) {
        if (err) return res.json(err);
        if (!user) return res.json('No such user')
        if (type == 'inbox') {
            models.Message.find()
                .where('_id')
                .in(user.inbox)
                .where('primary').equals(true)
                .populate({
                    path: 'from',
                    select: 'firstname lastname'
                })
                // .populate('relatedEmails')
                .sort([
                    ['createdAt', 'descending']
                ])
                .exec(function (err, mails) {
                    if (err) return res.json(err);
                    var tempMails = [];
                    async.eachSeries(mails, function (item, next) {
                        if (user.important.indexOf(item._doc._id.toString()) == -1) {
                            item._doc.importantCheck = false;
                        } else {
                            item._doc.importantCheck = true;
                        }
                        tempMails.push(item);
                        next();
                    }, function (err) {
                        res.json(tempMails);
                    })

                })
        } else if (type == 'sent') {
            models.Message.find()
                .where('_id')
                .in(user.sentMail)
                .where('primary').equals(true)
                .populate({
                    path: 'to',
                    select: 'firstname lastname'
                })
                .sort([
                    ['createdAt', 'descending']
                ])
                .exec(function (err, mails) {
                    if (err) return res.json(err);
                    var tempMails = [];
                    async.eachSeries(mails, function (item, next) {
                        if (user.important.indexOf(item._doc._id.toString()) == -1) {
                            item._doc.importantCheck = false;
                        } else {
                            item._doc.importantCheck = true;
                        }
                        tempMails.push(item);
                        next();
                    }, function (err) {
                        res.json(tempMails);
                    })

                    // res.json(mails)
                })
        } else if (type == 'trash') {
            models.Message.find()
                .where('_id')
                .in(user.trash)
                .populate({
                    path: 'from',
                    select: 'firstname lastname'
                })
                .sort([
                    ['createdAt', 'descending']
                ])
                .exec(function (err, mails) {
                    if (err) return res.json(err);
                    // console.log(mails);
                    res.json(mails)
                })
        } else if (type == 'important') {
            models.Message.find()
                .where('_id')
                .in(user.important)
                .populate({
                    path: 'from',
                    select: 'firstname lastname'
                })
                .sort([
                    ['createdAt', 'descending']
                ])
                .exec(function (err, mails) {
                    if (err) return res.json(err);
                    res.json(mails)
                })
        } else if (type == 'draft') {
            models.Message.find()
                .where('_id')
                .in(user.draft)
                .populate({
                    path: 'from',
                    select: 'firstname lastname'
                })
                .sort([
                    ['createdAt', 'descending']
                ])
                .exec(function (err, mails) {
                    if (err) return res.json(err);
                    res.json(mails)
                })
        }

    })

}


// get mail byId

exports.getMailById = function (req, res) {
    if(!req.params.mailId) return res.json({success:false,message:'Please provide mail Id.'})
    var populateQuery = [{
        path: 'from',
        select: 'email'
    }, {
        path: 'to',
        select: 'email'
    }];
    var populateQueryTest = [{
        path: 'relatedEmails',
        populate:{
            path:'from to',
            select:'email'
        }
    }];
    models.Message.findOne({
        _id: req.params.mailId
    })
        .populate(populateQuery)
        .populate(populateQueryTest)
        .exec(function (err, mail) {
            if (err) return res.json({ success: false, message: err });
            if (!mail) return res.json({ success: false, message: 'No such message' })
            res.json(mail);
        })
}


exports.moveTotrash = function (req, res) {

    var messageId = req.body.messageId;
    var userId = req.params.userId;
    var locationType = req.body.locationType;
    if (locationType == 'inbox') {
        models.User.findOneAndUpdate({
            _id: userId
        }, {
                $addToSet: {
                    trash: [messageId]
                },
                $pull: {
                    inbox: [messageId]
                }
            },
            function (err, user) {
                if (err) return res.json(err);
                res.json(user);
            })
    } else if (locationType == 'sent') {
        models.User.findOneAndUpdate({
            _id: userId
        }, {
                $addToSet: {
                    trash: [messageId]
                },
                $pull: {
                    sentMail: [messageId]
                }
            },
            function (err, user) {
                if (err) return res.json(err);
                res.json(user);
            })

    } else {
        models.User.findOneAndUpdate({
            _id: userId
        }, {
                $addToSet: {
                    trash: [messageId]
                },
                $pull: {
                    important: [messageId]
                }
            },
            function (err, user) {
                if (err) return res.json(err);
                res.json(user);
            })
    }

}

exports.addToImportant = function (req, res) {
    var messageId = req.body.messageId;
    var userId = req.params.userId;
    var ischecked = req.body.check;

    if (ischecked) {
        models.User.findOneAndUpdate({
            _id: userId
        }, {
                $addToSet: {
                    important: [messageId]
                }
            },
            function (err, user) {
                if (err) return res.json(err);
                res.json(user);
            })
    } else {
        models.User.findOneAndUpdate({
            _id: userId
        }, {
                $pull: {
                    important: [messageId]
                }
            },
            function (err, user) {
                if (err) return res.json(err);
                res.json(user);
            })
    }
}

exports.addToDraft = (req, res) => {
    var to = req.body.senderEmail
    async.waterfall([
        function (cb) {
            var userArray = [];
            if (to) {
                async.eachSeries(to, function (usermail, next) {
                    models.User.findOne({
                        email: usermail
                    }, function (err, userData) {
                        if (err) return cb(err);
                        if (userData) {
                            userArray.push(userData._id);
                            next();
                        } else {
                            next();
                        }
                    })
                },
                    function (err) {
                        if (userArray.length <= 0)
                            return cb('user not found');
                        cb(null, userArray)
                    }
                )
            } else {
                cb(null, userArray)
            }
        },
        function (userArray, cb) {
            var message = new models.Message({
                to: userArray || '',
                from: req.user._id || '',
                subject: req.body.subject || '',
                // cc: req.body.cc || '',
                body: req.body.body || '',
                attachment: req.body.attachment || ''
            });

            message.save(function (err, messag) {
                if (err) return cb(err);
                cb(null, messag)
            })
        },
        function (messag, cb) {
            models.User.findOneAndUpdate({
                _id: req.user._id
            }, {
                    $addToSet: {
                        draft: [messag._id]
                    }
                },
                function (err, user) {
                    if (err) return res.json(err);
                    cb(null, messag)
                })
        }
    ],
        function (err, messag) {
            if (err) return res.json({
                error: err
            });
            res.json('Message is sent To Draft.');
        }
    )

}
var models = require('mongoose').models;
var cron = require('node-cron');
var async = require('async');
var AWS = require('aws-sdk');
var fs = require('fs');
var config = require('./../settings/config');
var localAdd = '/home/nodejs/Downloads/Rahul_Beniwal_new/Rahul_Beniwal_Codes/daniel_sommer/code/post_cap/';
var exec = require('child_process').exec;
var nodemailer = require('./../components/mailer');
AWS.config.credentials = config.awsCredentials;
var s3 = new AWS.S3({
    apiVersion: '2012–09–25'
});
var eltr = new AWS.ElasticTranscoder({
    apiVersion: '2012–09–25',
    region: 'us-west-2'
});




// var localAdd = '/home/ubuntu/post_cap/';
var localCaptionsAdd = 'app/media/captions/';
var inputVideosAdd = 'app/media/inputVideos/';
var outputVideosAdd = 'app/media/outputVideos/';
var space = ' ';
var comma = '"';

var findLog = function(statusKey, callback) {
    models.UploadLogs.findOne({
            status: statusKey
        })
        .exec(function(err, log) {
            if (err) return callback(err);
            callback(null, log);
        })
}

var deletefiles = function(path, callback) {
    exec('rm -r ' + comma + path.subtitle + comma + ' ' + comma + path.video + comma + ' ' + comma + path.outputVideo + comma, function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        callback();
    });
}

function transcoder() {
    async.waterfall([
        function(cb) {
            findLog('waiting', function(err, log) {
                if (err) return cb(err);
                if (!log) return cb('no task to process');
                log.status = 'processing';
                log.save(function(err, newlog) {
                    if (err) return cb(err);
                    exec('rm -r ' + comma + log.outputVideo + comma, function(err, stdout, stderr) {
                        console.log('response output');
                        console.log('stdout',stdout)
                        console.log('stderr',stderr)
                        cb(null, log);
                    });
                });
            });
        },
        function(log, cb) {
            // console.log('running task ' + log._doc.outputName + ' at ' + new Date());
            if (log._doc.transcodingType == 'beneath_video') {
                console.log('1')
                var videoVariable = "sudo ffmpeg -i" + space + "'" + log._doc.video + "'" + space + "-vf 'pad=iw:ih + ih/4:0:0:0x000000,subtitles=" + log._doc.subtitle + "'" + space + '-strict -2' + space + "'" + log._doc.outputVideo + "'";
            } else {
                console.log('2')
                var videoVariable = "sudo ffmpeg -i" + space + "'" + log._doc.video + "'" + space + "-vf 'subtitles=" + log._doc.subtitle + "'" + space + '-strict -2' + space + "'" + log._doc.outputVideo + "'";
            }
            var child = exec(videoVariable, { maxBuffer: 1024 * 1024 * 64 }, function(err, stdout, stderr) {
                if (err) {
                    console.log(err);
                    log.status = 'waiting';
                    log.save(function(err, newlog) {
                        exec('rm -r ' + comma + log.outputVideo + comma, function(err, stdout, stderr) {
                            console.log('response output');
                            if (err) return cb(err);
                        });
                    });
                };
                console.log('execution result', stdout);
                var read = fs.createReadStream(log._doc.outputVideo);
                var params = { Bucket: 'trascoder-output-videos', Key: log._doc.outputName, Body: read };
                s3.upload(params, function(err, data) {
                    if (err) return cb(err);
                    console.log('upload result', data);
                    data.outputName = log._doc.outputName;
                    nodemailer.sender(log._doc.email, 'transcoded video', data, function(err, messageId, response) {
                        console.log('err:', err);
                        if (err) return cb(err);
                        console.log('mailed:', response);
                        console.log('err:', err);
                        log.status = 'done';
                        log.save(function(err, newlog) {
                            if (err) return cb(err);
                            deletefiles(newlog, function() {
                                console.log('response output');
                                cb(err);
                            });
                        });
                    });
                    console.log('s3 response output', data);
                });
            });
        }
    ], function(err) {
        // console.log(err);
        // console.log('finishing the task'  + ' at ' + new Date());
        process.nextTick(transcoder);

    });
};

transcoder();
var async = require('async');
var models = require('mongoose').models;
var moment = require('moment');

//finds all the specific types of users
exports.getUserAccordingUserType = (req, res) => {
    var userType = req.query.userType;
    var specificType = userType.split(',');

    var userArray = []
    models.User.findOne({ _id: req.user._id })
        .populate({
            path: 'role',
            select: 'name'
        })
        .exec(function(err, user) {
            if (user.role.name == 'super_user' || user.role.name == 'project_admin') {
                models.User.find()
                    .populate({
                        path: 'role',
                        select: 'name'
                    })
                    .exec(function(err, users) {
                        if (err) return res.json(err);
                        async.eachSeries(users, function(user, next) { //async to find specific type of user
                            if (user.role.name == specificType[0] || user.role.name == specificType[1] || user.role.name == specificType[2] || user.role.name == specificType[3]) {
                                userArray.push(user);
                                next();
                            } else {
                                next();
                            }
                        }, function(err) {
                            res.json(userArray);
                        })
                    })
            } else {
                models.User.find({ createdBy: req.user._id })
                    .populate({
                        path: 'role',
                        select: 'name'
                    })
                    .exec(function(err, users) {
                        if (err) return res.json(err);
                        async.eachSeries(users, function(user, next) { //async to find specific type of user
                            if (user.role.name == specificType[0] || user.role.name == specificType[1] || user.role.name == specificType[2] || user.role.name == specificType[3]) {
                                userArray.push(user);
                                next();
                            } else {
                                next();
                            }
                        }, function(err) {
                            res.json(userArray);
                        })
                    })
            }

        })

}


exports.getProjectAccordingProjectType = (req, res) => {
    var projectStatus = req.query.projectStatus;
    var serviceType = req.query.serviceType;
    // var writerName = req.query.writer;
    var clientName = req.query.clientName;
    var statusType = projectStatus.split(',');
    let clientArray = req.query.clientName.split(',');
    let writerArray = req.query.writerName.split(',');

    var serviceType = serviceType.split(',');
    var projectArray = []
    var mongoQuery = {};

    if (req.query.startDate && statusType[0] == '') {
        mongoQuery.createdAt = {
            $gt: req.query.startDate,
            $lt: req.query.lastDate
        }
    } else if (statusType[0] != '' && !req.query.startDate) {
        mongoQuery.status = { $in: statusType }
    } else if (req.query.startDate && statusType[0] != '') {
        mongoQuery.createdAt = {
            $gt: req.query.startDate,
            $lt: req.query.lastDate
        }
        mongoQuery.status = { $in: statusType }
    } else if (statusType[0] == '' && !req.query.startDate) {
        mongoQuery
    }
    if (req.query.writerName){
        // mongoQuery.workingUser = req.query.writerName || '';
        if(writerArray[0] == ""){
            mongoQuery.workingUser =  ''
        }else{
            mongoQuery.workingUser = { $in: writerArray } || ''
        }
    } 
    if (req.query.clientName){
        // mongoQuery.clientUser = req.query.clientName || '';
        if(clientArray[0] == ""){
            mongoQuery.clientUser =  ''
        }else{
            mongoQuery.clientUser = { $in: clientArray } || ''
        }
       
    } 

    var populateQuery = [{
        path: 'service',
        select: 'name pricessss'
    }, {
        path: 'job',
        select: 'appliedUser'
    },{
        path:'clientUser',
        select:'firstname lastname'
    },{
        path: 'workingUser',
        select: 'firstname lastname'
    }];
    
    models.User.findOne({ _id: req.user._id })
        .populate({
            path: 'role',
            select: 'name'
        })
        .exec(function(err, user) {
            if (user.role.name == 'super_user' || user.role.name == 'project_admin') {
                models.Project.find(mongoQuery)
                    .populate(populateQuery)
                    .where('mainCaptionFlag').equals(false)
                    .exec(function(err, projects) {
                        if (err) return res.json(err);
                        if (serviceType[0] != '') {
                            async.eachSeries(projects, function(project, next) { //async to find 
                                var cond2 = project.service.name == serviceType[0] || project.service.name == serviceType[1] || project.service.name == serviceType[2] || project.service.name == serviceType[3];
                                if (cond2) {
                                    projectArray.push(project);
                                    next();
                                } else {
                                    next();
                                }
                            }, function(err) {
                                res.json(projectArray);
                            })
                        } else {
                            res.json(projects)
                        }
                    })
            } else if (user.role.name == 'client_admin') {
                models.Project.find(mongoQuery)
                    .populate(populateQuery)
                    .where('clientUser').equals(req.user._id)
                    .where('captionReference').equals(null)
                    .exec(function(err, projects) {
                        if (err) return res.json(err);
                        if (serviceType[0] != '') {
                            async.eachSeries(projects, function(project, next) { //async to find 
                                var cond2 = project.service.name == serviceType[0] || project.service.name == serviceType[1] || project.service.name == serviceType[2] || project.service.name == serviceType[3];
                                if (cond2) {
                                    projectArray.push(project);
                                    next();
                                } else {
                                    next();
                                }
                            }, function(err) {
                                res.json(projectArray);
                            })
                        } else {
                            res.json(projects)
                        }
                    })
            } else if (user.role.name == 'writer') {
                models.Project.find(mongoQuery)
                    .populate(populateQuery)
                    .where('mainCaptionFlag').equals(false)
                    .where('workingUser').equals(req.user._id)
                    .exec(function(err, projects) {
                        if (err) return res.json(err);
                        if (serviceType[0] != '') {
                            async.eachSeries(projects, function(project, next) { //async to find 
                                var cond2 = project.service.name == serviceType[0] || project.service.name == serviceType[1] || project.service.name == serviceType[2] || project.service.name == serviceType[3];
                                if (cond2) {
                                    projectArray.push(project);
                                    next();
                                } else {
                                    next();
                                }
                            }, function(err) {
                                res.json(projectArray);
                            })
                        } else {
                            res.json(projects)
                        }
                    })
            }

        })

}




exports.getProjectAccordingProjectTypeJobBoard = (req, res) => {
    var projectStatus = req.query.projectStatus;
    var serviceType = req.query.serviceType;
    var statusType = projectStatus.split(',');
    var serviceType = serviceType.split(',');
    var projectArray = []
    var mongoQuery = {}
    const skip = parseInt(req.query.skip)
    const limit = parseInt(req.query.limit)
    if (req.query.startDate && statusType[0] == '') {
        mongoQuery.createdAt = {
            $gt: req.query.startDate,
            $lt: req.query.lastDate
        }
    } else if (statusType[0] != '' && !req.query.startDate) {
        mongoQuery.status = { $in: statusType }
    } else if (req.query.startDate && statusType[0] != '') {
        mongoQuery.createdAt = {
            $gt: req.query.startDate,
            $lt: req.query.lastDate
        }
        mongoQuery.status = { $in: statusType }
    } else if (statusType[0] == '' && !req.query.startDate) {
        mongoQuery
    }

    var populateQuery = [{
        path: 'service',
        select: 'name'
    }, {
        path: 'job',
        select: 'appliedUser'
    }];


    // models.User.findOne({ _id: req.user._id })
    //     .populate({
    //         path: 'role',
    //         select: 'name'
    //     })
    //     .exec(function(err, user) {
            if (req.user.role === 'client_admin') {
                models.Project.find(mongoQuery)
                    .populate(populateQuery)
                    // .where('status').equals('Unassigned')
                    .skip((skip - 1) * skip)
                    .limit(limit)
                    .where('mainCaptionFlag').equals(true)
                    .exec(function(err, projects) {
                        if (err) return res.json(err);
                        if (serviceType[0] != '') {
                            async.eachSeries(projects, function(project, next) { //async to find 
                                var cond2 = project.service.name == serviceType[0] || project.service.name == serviceType[1] || project.service.name == serviceType[2] || project.service.name == serviceType[3];
                                if (cond2) {
                                    projectArray.push(project);
                                    next();
                                } else {
                                    next();
                                }
                            }, function(err) {
                                res.json(projectArray);
                            })
                        } else {
                            res.json(projects)
                        }
                    })
            } else {
                models.Project.find(mongoQuery)
                    .populate(populateQuery)
                    .where('mainCaptionFlag').equals(false)
                    // .where('status').equals('Unassigned')
                    .skip((skip - 1) * skip)
                    .limit(limit)
                    .exec(function(err, projects) {
                        if (err) return res.json(err);
                        if (serviceType[0] != '') {
                            async.eachSeries(projects, function(project, next) { //async to find 
                                var cond2 = project.service.name == serviceType[0] || project.service.name == serviceType[1] || project.service.name == serviceType[2] || project.service.name == serviceType[3];
                                if (cond2) {
                                    projectArray.push(project);
                                    next();
                                } else {
                                    next();
                                }
                            }, function(err) {
                                // res.json(projectArray);
                                async.eachSeries(projectArray, function(item, next) {
                                    if (item.job.appliedUser.length > 0) {
                                        async.eachSeries(item.job.appliedUser, function(applied, nextUser) {
                                            if (applied == req.user._id) {
                                                item._doc.jobApplied = true;
                                                nextUser();
                                            } else {
                                                item._doc.jobApplied = false;
                                                nextUser();
                                            }
                                        }, function(err) {
                                            next();
                                        });
                                    } else {
                                        item._doc.jobApplied = false;
                                        next();
                                    }
                                }, function(err) {
                                    res.json(projectArray);
                                });
                            })
                        } else {
                            // res.json(projects)
                            async.eachSeries(projects, function(item, next) {
                                if (item.job.appliedUser.length > 0) {
                                    async.eachSeries(item.job.appliedUser, function(applied, nextUser) {
                                        if (applied.toString() == req.user.id.toString()) {
                                            item._doc.jobApplied = true;
                                            nextUser();
                                        } else {
                                            item._doc.jobApplied = false;
                                            nextUser();
                                        }
                                    }, function(err) {
                                        next();
                                    });
                                } else {
                                    item._doc.jobApplied = false;
                                    next();
                                }
                            }, function(err) {
                                res.json(projects);
                            });
                        }
                    })
            }
}

exports.getProjectAccordingToStatus = (req, res) => {
    if(!req.params.statusType) return res.json({success:false,message:'No status Provided.'})
    var populateQuery = [{
        path: 'service',
        select: 'name pricessss'
    }, {
        path: 'job',
        select: 'appliedUser'
    },{
        path:'clientUser',
        select:'firstname lastname'
    },{
        path:'workingUser',
        select:'firstname lastname'
    }];
    models.User.findById({_id:req.user._id})
        .populate({
            path: 'role',
            select: 'name'
        })
        .exec((err,userInfo)=>{
            if (err) return res.json({success:false,message:err})
            if(!userInfo) return res.json({success:false,message:'User Does Not Exist.'})
            if(userInfo.role.name == 'writer'){
                models.Project.find()
                .populate(populateQuery)
                .where('workingUser').equals(req.user._id)
                .where('status').equals(req.params.statusType)
                .where('mainCaptionFlag').equals(false)
                .sort('-createdAt')
                .exec(function(err, projects) {
                    if (err) return res.json(err);
                    if(!projects) return res.json({success:false,message:`You dont have any project in :${req.params.statusType}`})
                    res.json(projects)
                })
            }else if(userInfo.role.name == 'client_admin'){
                models.Project.find()
                .populate(populateQuery)
                .where('createdBy').equals(req.user._id)
                .where('status').equals(req.params.statusType)
                .sort('-createdAt')
                .exec(function(err, projects) {
                    if (err) return res.json(err);
                    if(!projects) return res.json({success:false,message:`You dont have any project in :${req.params.statusType}`})
                    res.json(projects)
                })
            }
            else{
                models.Project.find()
                .populate(populateQuery)
                .where('status').equals(req.params.statusType)
                .sort('-createdAt')
                .where('mainCaptionFlag').equals(false)
                .exec(function(err, projects) {
                    if (err) return res.json(err);
                    if(!projects) return res.json({success:false,message:`You dont have any project in :${req.params.statusType}`})
                    res.json(projects)
                })
            }
        })
}

exports.getuserInfoByName = function(req, res) {
    if(!req.params.name) return res.json({success:false,message:'Params not Provided.'})
    models.User.find({
            firstname:  { $regex:req.params.name }
        })
        .select('_id firstname lastname')
        .exec(function(err, user) {
            if (err) return res.json(err);
            res.json(user);
        });
};

exports.getAllWriter = (req,res)=>{
    models.User.find()
    .select('firstname lastname role')
    .populate({path:'role',select:'name'})
    .exec(function(err, users) {
        if (err) return res.json({sucess:false,message:err});
        if(!users) return res.json({success:false,message:'No writer is in our record.'})
        var writerList = []
        async.eachSeries(users, function(user, next) {
         if(user.role.name == 'writer'){
             writerList.push(user)
             next()
         }else{
            next()
         }
        }, function(err) {
        res.json({success:true,message:writerList});
        });
    });
}
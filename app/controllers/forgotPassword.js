const models = require('mongoose').models;
const async = require('async');
const validator = require('validator');
const bcrypt = require('bcrypt-nodejs');
var nodemailer = require('./../components/mailer');
var auth = require('./../middleware/authorization');

let validPassword = (password) => {
    if (!password) {
        return false;
    } else {
        const regExp = new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/);
        return regExp.test(password)
    }

}

exports.forgotPassword = (req, res) => {

    if (req.body.password) {
        async.waterfall([
            function (cb) {
                if (!req.body.email) {
                    return cb('Please enter Your email.')
                }
                else if (!req.body.password) {
                    return cb('Please enter new password.')
                } else if (!req.body.confirm) {
                    return cb('please enter Confirm Password.')
                }
                cb(null)
            }
            ,
            function (cb) {
                if (!validator.isLength(req.body.password, { min: 6, max: 20 })) {
                    return cb('Password must be at least 6 characters but not more than 20.')
                } else {
                    cb(null)
                }
            },
            function (cb) {
                if (!validPassword(req.body.password)) {
                    return cb('Must have at least one uppercase ,lowercase,special character, and number.')
                } else {
                    cb(null)
                }
            },
            function (cb) {
                if (req.body.password !== req.body.confirm) {
                    return cb('Password does not match.')
                } else {
                    cb(null)
                }
            },
            function (cb) {
                if (!validator.isEmail(req.body.email)) return cb('Please enter a valid email.') //=> true
                cb(null)
            },
            function (cb) {
                models.User.findOne({
                    email: req.body.email
                })
                    .exec(function (err, exist_user) {
                        if (err) return cb(err);
                        if (!exist_user) return cb("email does'nt exist");
                        var hash = generateHash(exist_user.local.password)
                        exist_user.local.password = hash;
                        exist_user.save(function (err, done) {
                            if (err) return cb(err);
                            cb(null)
                        })
                    })
            }
        ], function (err) {
            if (err) return res.render('forgot', { message: err, isPresent: true, email: req.body.email });
            res.redirect('/login');
        })
    } else {
        async.waterfall([
            function (cb) {
                if (!req.body.email) {
                    return cb('Please enter Your email.')
                }
                cb(null)
            }
            ,
            function (cb) {
                if (!validator.isEmail(req.body.email)) return cb('Please enter a valid email.')
                cb(null)
            },
            function (cb) {
                models.User.findOne({
                    email: req.body.email
                })
                    .populate({ path: 'role' })
                    .exec(function (err, user) {
                        if (err) return cb(err);
                        if (!user) return cb("email does'nt exist");
                        user.token = auth.getToken(user.email, user);
                        user.save(function (err, new_user) {
                            if (err) return cb(err);
                            cb(null, new_user);
                        });
                    })
            }
        ], function (err, user) {

            if (err) return res.render('forgot', { message: err, isPresent: false, email: req.body.email });
            nodemailer.post_cap_sender(user.email, user.token, 'Forget email.', user.role.name, function (err, messageId, response) {
                if (err) return console.log(`error`, err);
                res.render("forgot", { message: 'Please set the Password in your Email.', isPresent: true, email: req.body.email });
            });

        })
    }
}

var generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};


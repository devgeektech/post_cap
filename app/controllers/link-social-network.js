var models = require('mongoose').models;
var async = require('async');
var User = models.User;
var Youtube = require("youtube-api");
var auth = require('./../middleware/authorization');
var config = require('./../settings/config');
var driveCredential = config.driveCredential;
var youtubeCredential = config.youtubeCredential;
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var fs = require('fs');
var Promise = require("bluebird");
var Vimeo = require('vimeo').Vimeo;
var Dropbox = require('dropbox');
var request = require('request');
var Wistia = require('wistia-js')('7616adcc0f2db8f543d5299523cc69e20032d6cf33e236b183bf0f3117f2d0dd');
var wistiaData = Wistia.WistiaData();
var Youtube = google.youtube('v3');
var youtubedl = require('youtube-dl');
var urlMetadata = require('url-metadata')


// for google drive
var OAuth2 = google.auth.OAuth2;
var clientSecret = driveCredential.client_secret;
var clientId = driveCredential.client_id;
var redirectUrl = driveCredential.redirect_uris;
var auth = new googleAuth();
var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
oauth2Client.getToken = Promise.promisify(oauth2Client.getToken)
// end google drive

// for youtube
var clientSecretYoutube = youtubeCredential.client_secret;
var clientIdYoutube = youtubeCredential.client_id;
var redirectUrlYoutube = youtubeCredential.redirect_uris;
// var auth = new googleAuth();
var oauth2ClientYoutube = new auth.OAuth2(clientIdYoutube, clientSecretYoutube, redirectUrlYoutube);
oauth2ClientYoutube.getToken = Promise.promisify(oauth2ClientYoutube.getToken)

var lib = new Vimeo(config.vimeo.CLIENT_ID, config.vimeo.CLIENT_SECRET);
//  end




// Load client secrets from a local file.
exports.readFileInfo = function (req, res) {
    var url = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: "https://www.googleapis.com/auth/drive"
    })
    return res.redirect(url)
}


exports.getGoogleoauth2callback = function (req, res) {
    return oauth2Client.getToken(req.query.code)
        .then(function (tokens) {

            oauth2Client.credentials = tokens;
            models.User.findOne({
                _id: req.user._id
            }, function (err, user) {
                if (err) return res.json(err);
                // if (config.server.enviroment == "production") {
                // console.log('expiry_date', tokens[0].expiry_date)
                // user.drive.access_token = tokens[0].access_token;
                // console.log('user.drive', user.drive.access_token)
                // user.drive.token_type = tokens[0].token_type;
                // user.drive.expiry_date = tokens[0].expiry_date;
                // user.updatedAt = Date.now();
                // user.save(function(err, done) {
                //     if (err) {
                //         console.log(err);
                //         return res.json(err);
                //     }
                //     console.log('drive', user.drive)
                //     res.writeHead(301, { Location: config.server.url + '#!/dashboard/linked-account' });
                //     res.end();
                // })
                // } else {
                user.drive.access_token = tokens.access_token;
                user.drive.token_type = tokens.token_type;
                user.drive.expiry_date = tokens.expiry_date;
                user.updatedAt = Date.now();
                user.save(function (err, done) {
                    if (err) {
                        return res.json(err);
                    }
                    res.writeHead(301, {
                        Location: config.server.url + '#!/dashboard/linked-account'
                    });
                    res.end();
                })
            })
        });
}

exports.getDriveDetail = function (req, res) {
    //////////////// future purpose //////////////////////
    // var socialType = req.query.socialType;
    models.User.findOne({
        _id: req.user._id
    }, function (err, user) {
        if (err) return res.json({
            success: false,
            message: err
        })
        if (!user) return res.json({
            success: false,
            message: 'No user Found'
        })
        oauth2Client.credentials = user.drive;
        var service = google.drive('v3');
        service.files.list({
            auth: oauth2Client,
            pageSize: 30,
            // fields: "*"
            fields: "nextPageToken, files(id, name, size ,mimeType,webViewLink,fullFileExtension,iconLink,thumbnailLink,videoMediaMetadata)"
        }, function (err, response) {
            if (err) return res.json({
                success: false,
                message: err
            })
            if (!response) return res.json({
                success: false,
                message: 'Do not have any data in Drive.'
            })
            var fiterData = response.files.filter(function (item) {
                var split = item.mimeType.split("/");
                return split[0] == 'video';
            });

            res.json(fiterData)
        })
    })
}

// for youtube link
exports.youtubeConnect = function (req, res) {
    var authUrl = oauth2ClientYoutube.generateAuthUrl({
        access_type: 'offline',
        scope: "https://www.googleapis.com/auth/youtube.readonly"
    })
    return res.redirect(authUrl)
}


exports.getYoutubeoauth2callback = function (req, res) {
    oauth2ClientYoutube.getToken(req.query.code)
        .then(function (tokens) {
            console.log('youtube.dataToken', tokens)
            oauth2ClientYoutube.credentials = tokens;

            models.User.findOne({
                _id: req.user._id
            }, function (err, user) {
                if (err) return res.json(err);
                user.youTube.access_token = tokens.access_token;
                user.youTube.token_type = tokens.token_type;
                user.youTube.expiry_date = tokens.expiry_date;
                user.updatedAt = Date.now();
                user.save(function (err, done) {
                    if (err) {
                        return res.json(err);
                    }
                    res.writeHead(301, {
                        Location: config.server.url + '#!/dashboard/linked-account'
                    });
                    res.end();
                })
            })
        })
}

exports.getYoutubeDetail = function (req, res) {
    var userId = req.query.userId;
    var youtubeUrl = "https://www.youtube.com/watch?v=";
    models.User.findOne({
        _id: userId
    }, function (err, user) {
        oauth2ClientYoutube.credentials = user.youTube;
        Youtube.channels.list({
            mine: true,
            part: "snippet,contentDetails,statistics",
            auth: oauth2ClientYoutube
        }, function (err, response) {
            if (err) {
                res.send(err);
                return;
            }
            var finaData = []
            response.items.forEach(function (element) {
                var playlistsId = element.contentDetails.relatedPlaylists.uploads;
                Youtube.playlistItems.list({
                    playlistId: playlistsId,
                    mine: true,
                    part: 'snippet,contentDetails',
                    auth: oauth2ClientYoutube
                }, function (err, data) {
                    if (err) {
                        res.send(err);
                        return;
                    }
                    finaData.push(data);
                    async.eachSeries(data.items, function (item, next) {
                        var url = youtubeUrl + item.contentDetails.videoId;
                        youtubedl.getInfo(url, function (err, linkInfo) {
                            if (err) {
                                console.log(err);
                            }
                            item.youtubedlYoutube = linkInfo
                            next();
                        });
                    }, function (err) {
                        res.json(data)
                    });
                })
            })
        });
    })
}

// vimeo
exports.vimeoConnect = function (req, res) {
    var scope = ['private'];
    var state = 'test';
    var url = lib.buildAuthorizationEndpoint(config.vimeo.redirectUrl, scope, 'test');
    return res.redirect(url)
}

exports.vimeoConnectCallback = function (req, res) {
    console.log('in callbck');
    lib.accessToken(req.query.code, config.vimeo.redirectUrl, function (err, token) {
        if (err) return res.json("error\n" + err);
        if (token.access_token) {
            models.User.findOne({
                _id: req.user._id
            }, function (err, user) {
                if (err) return res.json(err);
                user.vimeo.access_token = token.access_token;
                user.updatedAt = Date.now();
                user.save(function (err, done) {
                    if (err) return res.json(err);
                    res.writeHead(301, {
                        Location: config.server.url + '#!/dashboard/linked-account'
                    });
                    res.end();
                })
            })


        }
    });

}


exports.getVimeoDetail = function (req, res) {
    var userId = req.query.userId;
    // var mainData = {}
    models.User.findOne({
        _id: userId
    }, function (err, user) {
        lib.access_token = user.vimeo.access_token;
        lib.request( /*options*/ {
            path: '/me/videos'
        }, /*callback*/ function (error, body, status_code, headers) {
            if (error) {
                return res.json("error\n" + error);
            } else {

                async.eachSeries(body.data, function (item, next) {
                    youtubedl.getInfo(item.link, function (err, linkInfo) {
                        if (err) throw err;
                        item.youtubedlVimeo = linkInfo
                        next();
                    });

                }, function (err) {
                    res.json(body);
                });


            }
        });
    })
}



//end vimeos


exports.wistiaConnect = function (req, res) {

    // fetchVideoInfo('LTV5kGSb_bE', function(err, videoInfo) {
    //     if (err) throw new Error(err);
    //     console.log(videoInfo);
    //     res.json(videoInfo)
    // });

    // wistiaData.projectList(function(error, data) {
    //     if (error) { console.log(error); }
    //     console.log(data);
    // });
}

exports.oauth2callbackDropbox = function (req, res) {
    console.log(req)
    dropbox.getToken(req.query.code, (err, response) => {
        //you are authorized now!
        console.log(response)

        var dbx = new Dropbox({
            accessToken: response.access_token
        });
        dbx.filesListFolder({
                path: ''
            })
            .then(function (response) {
                res.json(response);
            })
            .catch(function (error) {
                console.log(error);
            });

    });

}

// to get the youtube link information
exports.getVideoLinkInfo = function (req, res) {
    if (!req.body.link) return res.json({
        success: false,
        message: 'Please provide a link.'
    })
    var finalLink = []
    async.eachSeries(req.body.link, (item, next) => {
        youtubedl.getInfo(item.videoLink, function (err, info) {
            if (err) return next(err)
            finalLink.push(info)
            next()
        });
    }, (err) => {
        if (err) return res.json({
            success: false,
            message: err
        })
        res.json({
            success: true,
            data: finalLink,
            message: 'success!'
        })
    })
}

exports.disconnectLinkAccount = (req, res) => {
    models.User.findOne({
        _id: req.user._id
    }, function (err, user) {
        if (err) return res.json({
            success: false,
            message: err
        })
        if (req.body.type == 'youtube') {
            user.youTube = {}
            user.save(function (err, user) {
                if (err) return res.json({
                    success: false,
                    message: err
                })
                res.json({
                    success: false,
                    message: 'Saved!'
                })
            })
        }
        if (req.body.type == 'vimeo') {
            user.vimeo.access_token = ''
            user.save(function (err, user) {
                if (err) return res.json({
                    success: false,
                    message: err
                })
                res.json({
                    success: false,
                    message: 'Saved!'
                })
            })
        }

    })
}
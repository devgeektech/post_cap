var async = require('async');
var models = require('mongoose').models;


module.exports.accentCreate = function (req, res) {
    async.waterfall([
        function (cb) {
            if (!req.body.name) {
                return cb('name is required');
            } else {
                if (!req.body.price) {
                    return cb('name is required');
                } cb(null);
            }
        },
        function (cb) {
            var acc = new models.Accent({
                name: req.body.name,
                price: req.body.price
            });
            acc.save(function (err, new_acc) {
                if (err) return cb(new_acc);
                cb(null, new_acc);
            });
        }
    ], function (err, accent) {
        if (err) return res.json(err);
        res.json({
            id: accent.id
        });
    })
};

exports.allAccent = function (req, res) {

    models.UserPrice.findOne({
        user: req.user._id
    })
        .exec((err, user) => {
            if (err) return cb(err)
            if (!user) {
                models.Accent.find()
                    .select('-__v')
                    .exec(function (err, accents) {
                        if (err) return res.json(err);
                        res.json(accents);
                    })
            } else {
                if (user.accent.length == 0) {
                    models.Accent.find()
                        .select('-__v')
                        .exec(function (err, accents) {
                            if (err) return res.json(err);
                            res.json(accents);
                        })
                } else {
                    res.json(user.accent)
                }
            }
        })
};


module.exports.technicalCreate = function (req, res) {
    async.waterfall([
        function (cb) {
            if (!req.body.name) {
                return cb('name is required');
            } else {
                if (!req.body.price) {
                    return cb('name is required');
                } cb(null);
            }
        },
        function (cb) {
            var tech = new models.Technique({
                name: req.body.name,
                price: req.body.price
            });
            tech.save(function (err, newTechnique) {
                if (err) return cb(newTechnique);
                cb(null, newTechnique);
            });
        }
    ], function (err, technique) {
        if (err) return res.json(err);
        res.json({
            id: accent.id
        });
    })
};




// exports.allAccent = function (req, res) {
//     models.Technique.find()
//         .select('-__v')
//         .exec(function(err, techniques) {
//             if(err) return res.json(err);
//             res.json(techniques);
//         })
// };


module.exports.turnaroundCreate = function (req, res) {
    async.waterfall([
        function (cb) {
            if (!req.body.name) {
                return cb('name is required');
            } else {
                if (!req.body.price) {
                    return cb('name is required');
                } cb(null);
            }
        },
        function (cb) {
            var tech = new models.Technique({
                name: req.body.name,
                price: req.body.price
            });
            tech.save(function (err, newTechnique) {
                if (err) return cb(newTechnique);
                cb(null, newTechnique);
            });
        }
    ], function (err, technique) {
        if (err) return res.json(err);
        res.json({
            id: accent.id
        });
    })
};




exports.allTurnaroundCreate = function (req, res) {
    models.Technique.find()
        .select('-__v')
        .exec(function (err, techniques) {
            if (err) return res.json(err);
            res.json(techniques);
        })
};

var async = require('async');
var models = require('mongoose').models;


module.exports.accentCreate = function (req, res) {
    async.waterfall([
        function (cb) {
            if (!req.body.name) {
                return cb('name is required');
            } else {
                if (!req.body.price) {
                    return cb('name is required');
                } cb(null);
            }
        },
        function (cb) {
            var acc = new models.Accent({
                name: req.body.name,
                price: req.body.price
            });
            acc.save(function (err, new_acc) {
                if (err) return cb(new_acc);
                cb(null, new_acc);
            });
        }
    ], function (err, accent) {
        if (err) return res.json(err);
        res.json({
            id: accent.id
        });
    })
};




// exports.allAccent = function (req, res) {
//     models.Accent.find()
//         .select('-__v')
//         .exec(function (err, accents) {
//             if (err) return res.json(err);
//             res.json(accents);
//         })
// };


module.exports.techniqueCreate = function (req, res) {
    async.waterfall([
        function (cb) {
            if (!req.body.name) {
                return cb('name is required');
            } else {
                if (!req.body.price) {
                    return cb('name is required');
                } cb(null);
            }
        },
        function (cb) {
            var tech = new models.Technique({
                name: req.body.name,
                price: req.body.price
            });
            tech.save(function (err, newTechnique) {
                if (err) return cb(newTechnique);
                cb(null, newTechnique);
            });
        }
    ], function (err, technique) {
        if (err) return res.json(err);
        res.json({
            id: technique.id
        });
    })
};




exports.allTechnique = function (req, res) {

    models.UserPrice.findOne({
        user: req.user._id
    })
        .exec((err, user) => {
            if (err) return cb(err)
            if (!user) {
                models.Technique.find()
                    .select('-__v')
                    .exec(function (err, techniques) {
                        if (err) return res.json(err);
                        res.json(techniques);
                    })

            } else {
                if (user.technique.length == 0) {
                    models.Technique.find()
                        .select('-__v')
                        .exec(function (err, techniques) {
                            if (err) return res.json(err);
                            res.json(techniques);
                        })
                } else {
                    res.json(user.technique)
                }

            }
        })


};


module.exports.turnaroundCreate = function (req, res) {
    async.waterfall([
        function (cb) {
            if (!req.body.name) {
                return cb('name is required');
            } else {
                if (!req.body.price) {
                    return cb('name is required');
                } cb(null);
            }
        },
        function (cb) {
            var tech = new models.Turnaround({
                name: req.body.name,
                price: req.body.price,
                time: req.body.time
            });
            tech.save(function (err, newTurnaround) {
                if (err) return cb(newTurnaround);
                cb(null, newTurnaround);
            });
        }
    ], function (err, turnaround) {
        if (err) return res.json(err);
        res.json({
            id: turnaround.id
        });
    })
};




exports.allTurnaround = function (req, res) {
    models.UserPrice.findOne({
        user: req.user._id
    })
        .exec((err, user) => {
            if (err) return cb(err)
            if (!user) {
                models.Turnaround.find()
                    .select('-__v')
                    .exec(function (err, turnaround) {
                        if (err) return res.json(err);
                        if (!turnaround) return res.json({ message: 'No turnaround found' })
                        res.json(turnaround);
                    })

            } else {
                if (user.turnAround.length == 0) {
                    models.Turnaround.find()
                        .select('-__v')
                        .exec(function (err, turnaround) {
                            if (err) return res.json(err);
                            if (!turnaround) return res.json({ message: 'No turnaround found' })
                            res.json(turnaround);
                        })
                } else {
                    res.json(user.turnAround)
                }

            }
        })


};

exports.getServices = function (req, res) {
    models.Service.find()
        .select('-__v')
        .exec(function (err, services) {
            if (err) return res.json(err);
            if (!services) return res.json({ message: 'No service found' })
            res.json(services);
        })
}


module.exports.captionServicesCreate = function (req, res) {
    async.waterfall([
        function (cb) {
            if (!req.body.name) {
                return cb('name is required');
            } else {
                if (!req.body.price) {
                    return cb('name is required');
                } cb(null);
            }
        },
        function (cb) {
            var acc = new models.CaptionService({
                name: req.body.name,
                price: req.body.price
            });
            acc.save(function (err, newCaption) {
                if (err) return cb(newCaption);
                cb(null, newCaption);
            });
        }
    ], function (err, captionService) {
        if (err) return res.json({ success: false, mess: err });
        res.json({ success: true, message: 'Caption Service Created' });
    })
};

exports.getCaptionServices = function (req, res) {
    models.CaptionService.find()
        .select('-__v -updatedAt -createdAt')
        .exec(function (err, captionServices) {
            if (err) return res.json(err);
            if (!captionServices) return res.json({ message: 'No service found' })
            res.json(captionServices);
        })
}


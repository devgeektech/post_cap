var atob = require('atob')
var expect = require('expect')
var request = require('request')
var tools = require('./tool')
var config = {
    "clientId": "Q0dfVzlN7zALdz2OxC6ZBvL7XLFbRX7Lj1PSdIEwWTgnN7PTYb",
    "clientSecret": "Ta7TDP3QT551JYQhPpVeCbl89ZC0yd5ciR8f0jpV",
    "redirectUri": "http://localhost:3000/callback",
    "configurationEndpoint": "https://developer.api.intuit.com/.well-known/openid_sandbox_configuration/",
    "api_uri": "https://sandbox-quickbooks.api.intuit.com/v3/company/",
    "scopes": {
        "sign_in_with_intuit": [
            "openid",
            "profile",
            "email",
            "phone",
            "address"
        ],
        "connect_to_quickbooks": [
            "com.intuit.quickbooks.accounting",
            "com.intuit.quickbooks.payment"
        ],
        "connect_handler": [
            "com.intuit.quickbooks.accounting",
            "com.intuit.quickbooks.payment",
            "openid",
            "profile",
            "email",
            "phone",
            "address"
        ]
    }
}


var JWT = function() {
    var jwt = this;

    // Performs the correct JWT validation steps
    this.validate = function(id_token, callback, errorFn) {
        // https://developer.api.intuit.com/.well-known/openid_configuration/
        var openid_configuration = tools.openid_configuration

        // Decode ID Token
        var token_parts = id_token.split('.')
        var idTokenHeader = JSON.parse(atob(token_parts[0]))
        var idTokenPayload = JSON.parse(atob(token_parts[1]))
        var idTokenSignature = atob(token_parts[2])
        var idTokenPayloadToken = idTokenPayload.aud[0]
            // Step 1 : First check if the issuer is as mentioned in "issuer" in the discovery doc
        expect(idTokenPayload.iss).toEqual(openid_configuration.issuer)

        // Step 2 : check if the aud field in idToken is same as application's clientId
        expect(idTokenPayloadToken).toEqual(config.clientId)

        // Step 3 : ensure the timestamp has not elapsed
        expect(idTokenPayload.exp).toBeGreaterThan(Date.now() / 1000)

        // Step 4: Verify that the ID token is properly signed by the issuer
        jwt.getKeyFromJWKsURI(idTokenHeader.kid, function(key) {
            var cert = jwt.getPublicKey(key.n, key.e)
                // Validate the RSA encryption
            require("jsonwebtoken").verify(id_token, cert, function(err) {
                if (err) errorFn(err)
                else callback()
            })
        })
    }

    // Loads the correct key from JWKs URI:
    // https://oauth.platform.intuit.com/op/v1/jwks
    this.getKeyFromJWKsURI = function(kid, callback) {
        var openid_configuration = tools.openid_configuration

        request({
            url: openid_configuration.jwks_uri,
            json: true
        }, function(error, response, body) {
            if (error || response.statusCode != 200) {
                throw new Error("Could not reach JWK endpoint")
            }
            // Find the key by KID
            var key = body.keys.find(el => (el.kid == kid))
            callback(key)
        })
    }

    // Creates a PEM style RSA public key, using the modulus (n) and exponent (e)
    this.getPublicKey = function(modulus, exponent) {
        var getPem = require('rsa-pem-from-mod-exp')
        var pem = getPem(modulus, exponent)
        return pem
    }
}

module.exports = new JWT();
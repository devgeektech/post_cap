var AWS = require('aws-sdk'),
    zlib = require('zlib'),
    fs = require('fs');
var async = require('async');
var models = require('mongoose').models;
var config = require('./../settings/config');
var formidable = require('formidable');
var fs = require('fs');
var moment = require('moment');
var projectValidator = require('./../validators/project');
var getDuration = require('get-video-duration');
var Dropbox = require('dropbox');
var notificationRefer = require('./notifications');
var request = require('request');
var youtubedl = require('youtube-dl');
var express = require('express');
var probe = require('node-ffprobe');
var app = express();
const validator = require('validator');
const _ = require('lodash')
const jsonexport = require('jsonexport');
var mime = require('mime');
var scpclient = require('scp2');
var S3FS = require('s3fs');

// s3Stream = require('s3-upload-stream')(new AWS.S3()),

// Set the client to be used for the upload. 
AWS.config.credentials = config.awsCredentials;
var s3 = new AWS.S3({
    apiVersion: '2012–09–25'
});
var eltr = new AWS.ElasticTranscoder({
    apiVersion: '2012–09–25',
    region: 'us-west-2'
});






var uploadDoc = function (name, path, callback) {
    var read = fs.createReadStream(path);
    var params = {
        Bucket: 'transcoding-video-caption',
        Key: name,
        Body: read
    };
    s3.upload(params, function (err, data) {
        if (err) return callback(err);
        callback(null, data);
    })
}


// exports.downloadOpenCaptionVideo = async (req, res) => {
//     if (!req.body.link) return res.json({ success: false, message: 'Please provide the link.' })
//     if (!req.body.srt) return res.json({ success: false, message: 'Please provide the srt.' })
//     if (!req.body.email) return res.json({ success: false, message: 'Please provide the email.' })
//     if (req.body.link.includes('https://www.youtube.com')) {
//         async.waterfall([
//             function (cb) {
//                 console.log(__dirname)
//                 let name = req.body.link.split('https://www.youtube.com/watch?v=')
//                 var video = youtubedl(req.body.link,
//                     // Optional arguments passed to youtube-dl.
//                     ['--format=18'],
//                     // Additional options can be given for calling `child_process.execFile()`.
//                     { cwd: `/home` });
//                 // Will be called when the download starts.
//                 video.on('info', function (info) {
//                     console.log('Download started');
//                     console.log('filename: ' + info.filename);
//                     console.log('size: ' + info.size);
//                 });


//                 video.pipe(fs.createWriteStream(`${name[1]}.mp4`))
//             },
//             function (arg1, arg2, cb) {
//                 // arg1 now equals 'one' and arg2 now equals 'two'
//                 cb(null, 'three');
//             },
//         ], function (err, result) {
//             // result now equals 'done'
//         });

//     } else if (req.body.link.includes('post-cap-projects-phase-2/')) {
//         async.waterfall([
//             function (cb) {

//                 let name = req.body.link.split('post-cap-projects-phase-2/')
//                 const fileStream = s3.getObject({
//                     Bucket: 'post-cap-projects-phase-2',
//                     Key: name[1]
//                 })

//                 var getParams = {
//                     Bucket: 'post-cap-projects-phase-2',
//                     Key: name[1]
//                 }

//                 s3.getObject(getParams, function (err, data) {
//                     // Handle any error and exit
//                     if (err) console.log(err)
//                     console.log(`===================>`, data)
//                     let videoNewpath = `/home/lenovo02/Videos/${name[1]}`
//                     fs.writeFile(videoNewpath, data.Body, function (err) {
//                         if (err) return cb(err)
//                         console.log('Saved!');
//                         cb(null,videoNewpath)
//                     });
//                 });
//             },
//             function (videoNewpath,cb) {
//                 let name = req.body.srt.split('post-cap-projects-phase-2/')

//                 var getParams = {
//                     Bucket: 'post-cap-projects-phase-2',
//                     Key: name[1].replace(/[+]/g,"")
//                 }
//                 s3.getObject(getParams, function (err, data) {
//                     // Handle any error and exit
//                     if (err) return cb(err)
//                     console.log(`===================>`, data)
//                     let subtitleNewpath = `/home/lenovo02/Videos/${name[1]}`
//                     fs.writeFile(videoNewpath, data.Body, function (err) {
//                         if (err) return cb(err)
//                         console.log('Saved!');
//                         cb(null,videoNewpath,subtitleNewpath)
//                     });
//                 });

//             },
//             function (videoNewpath,subtitleNewpath,cb) {
//                 var logModel = new models.UploadLogs({
//                     video: videoNewpath,
//                     outputVideo: videoNewpath,
//                     subtitle: subtitleNewpath,
//                     subtitleAlias: subtitleNewpath,
//                     outputName: videoNewpath,
//                     email: req.body.email,
//                     status: `waiting`,
//                     transcodingType: req.body.option
//                 });
//                 logModel.save(function (err, log) {
//                    if(err) return cb(err)
//                    cb(null)
//                 });
//             }
//         ], function (err, result) {
//             // result now equals 'done'
//             if(err) return console.log('Error in processing......',err)
//             res.writeHead(301,
//                 { Location: 'http://ec2-18-220-238-26.us-east-2.compute.amazonaws.com:4000/' }
//             );
//             res.end();
//         });
//     }
// }

exports.uploadProject = function () {
    var data = {};
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        uploadDoc(files.subtitles.name, files.subtitles.path, function (err, subtitleData) {
            if (err) return res.json({
                error: err
            });

        });
        // Create the streams 
        var read = fs.createReadStream('/path/to/a/file');
        var compress = zlib.createGzip();
        var upload = s3Stream.upload({
            "Bucket": "post-cap-projects",
            "Key": "key-name"
        });

        // Optional configuration 
        upload.maxPartSize(20971520); // 20 MB 
        upload.concurrentParts(5);

        // Handle errors. 
        upload.on('error', function (error) {
            console.log(error);
        });

        /* Handle progress. Example details object:
        { ETag: '"f9ef956c83756a80ad62f54ae5e7d34b"',
            PartNumber: 5,
            receivedSize: 29671068,
            uploadedSize: 29671068 }
        */
        upload.on('part', function (details) {
            console.log(details);
        });

        /* Handle upload completion. Example details object:
        { Location: 'https://bucketName.s3.amazonaws.com/filename.ext',
            Bucket: 'bucketName',
            Key: 'filename.ext',
            ETag: '"bf2acbedf84207d696c8da7dbb205b9f-5"' }
        */
        upload.on('uploaded', function (details) {
            console.log(details);
        });

        // Pipe the incoming filestream through compression, and up to S3. 
        read.pipe(compress).pipe(upload);
    });

}



// ==================================phase-2============================================//








var uploadProject = function (name, path, callback) {
    var read = fs.createReadStream(path);
    var params = {
        Bucket: 'post-cap-phase-2-input-project',
        Key: name,
        Body: read
    };
    s3.upload(params, function (err, data) {
        if (err) return callback(err);
        callback(null, data);
    })
}


function addDays(theDate, days) {
    return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
}

exports.createProject = function (req, res) {
    async.waterfall([
        function (cb) {
            if (req.body.service != "captioning") {
                projectValidator.projectForm(req.body).isValid(function (err, form) {
                    if (err) return cb(err);
                    cb(null, form.data);
                })
            } else {
                cb(null, 'captioning');
            }
        },
        function (data, cb) {

            if (req.body.service == "captioning") {
                var changeId = null
                var projectArray = []
                async.eachSeries(req.body.projects, function (item, next) {
                    item.captionReference = changeId;
                    let checkDeadline = JSON.parse(item.selectedService.turnaroundSelected)
                    if (checkDeadline.name === 'Rush') {
                        item.orderDue = addDays(new Date(), 1)
                        var project = new models.Project(item);
                        project.save(function (err, newproject) {
                            if (err) return cb(err);
                            if (newproject.mainCaptionFlag == true) {
                                changeId = newproject._id;
                            }
                            projectArray.push(newproject)
                            next();
                        })
                    } else if (checkDeadline.name === 'Regular') {
                        models.Service.findOne({
                            _id: item.service
                        }, function (err, service) {
                            if (service.name == `transcription`) {
                                item.orderDue = addDays(new Date(), 2)
                                var project = new models.Project(item);
                                project.save(function (err, newproject) {
                                    if (err) return cb(err);
                                    if (newproject.mainCaptionFlag == true) {
                                        changeId = newproject._id;
                                    }
                                    projectArray.push(newproject)
                                    next();
                                })
                            }
                            if (service.name == `transcription-alignment`) {
                                item.orderDue = addDays(new Date(), 3)
                                var project = new models.Project(item);
                                project.save(function (err, newproject) {
                                    if (err) return cb(err);
                                    if (newproject.mainCaptionFlag == true) {
                                        changeId = newproject._id;
                                    }
                                    projectArray.push(newproject)
                                    next();
                                })
                            }
                            if (service.name == `captioning`) {
                                item.orderDue = addDays(new Date(), 3)
                                var project = new models.Project(item);
                                project.save(function (err, newproject) {
                                    if (err) return cb(err);
                                    if (newproject.mainCaptionFlag == true) {
                                        changeId = newproject._id;
                                    }
                                    projectArray.push(newproject)
                                    next();
                                })
                            }
                        })
                    }

                }, function (err) {
                    cb(null, projectArray)
                });
            } else {
                req.body.captionReference = null;
                let checkDeadline = JSON.parse(req.body.selectedService.turnaroundSelected)
                if (checkDeadline.name === 'Rush') {
                    req.body.orderDue = addDays(new Date(), 1)
                } else if (checkDeadline.name === 'Regular') {
                    req.body.orderDue = addDays(new Date(), 2)
                }
                var project = new models.Project(req.body);
                project.save(function (err, newproject) {
                    if (err) return cb(err);
                    cb(null, newproject)
                })
            }
        },
        function (project, cb) {
            if (req.body.service == "captioning") {
                async.eachSeries(project, function (item, next) {

                    var job = new models.Job({
                        project: item._id
                    });
                    job.save(function (err, newJob) {
                        if (err) return cb(err);
                        item.job = newJob.id;
                        item.save(function (err, newproject) {
                            if (err) return cb(err);
                            next();
                        });
                    });

                }, function (err) {
                    cb(null, project)
                });

            } else {
                var job = new models.Job({
                    project: project._id
                });
                job.save(function (err, newJob) {
                    if (err) return cb(err);
                    project.job = newJob.id;
                    project.save(function (err, newproject) {
                        if (err) return cb(err);
                        cb(null, project);
                    });
                });
            }
        }
        // Todo uncomment it

        ,
        function (project, cb) {
            var captionProject = {}
            var transactionData = {}
            if (req.body.service == "captioning") {
                async.eachSeries(project, function (item, next) {
                    if (item.captionReference == null) {
                        captionProject = item
                    }
                    next();
                }, function (err) {
                    // callback(null, user);
                    transactionData = {
                        amount: captionProject.totalPrice[captionProject.totalPrice.length - 1],
                        projectId: captionProject._id
                    }
                })
            } else {
                transactionData = {
                    amount: project.totalPrice[project.totalPrice.length - 1],
                    projectId: project._id
                }
            }

            models.User.findOne({
                _id: req.user._id
            }, function (err, user) {
                if (user.paymentBalance > 0) {
                    // user.paymentBalance = project.totalPrice[0] - transactionData.amount;
                    var newBalance = user.paymentBalance - transactionData.amount;
                    if (newBalance <= 0) {
                        var newtransactionData = {
                            amount: transactionData.amount - user.paymentBalance,
                            projectId: project._id
                        }
                        user.transaction.push(newtransactionData)
                        user.paymentBalance = 0;
                        user.save(function (err, user) {
                            if (err) return cb(err);
                            cb(null, project)
                        })
                    } else {
                        user.paymentBalance = newBalance;
                        project.paymentStats = true;
                        // async.eachSeries(project, function(item, next) {
                        //     // item.save(function(err, project) {
                        //     //     if (err) return cb(err);
                        //     //    
                        //     // })
                        //     item.paymentStats = true;
                        //     next();
                        // }, function(err) {
                        //     // cb(null, project)

                        // })
                        user.save(function (err, user) {
                            if (err) return cb(err);
                            cb(null, project)
                        })

                    }

                } else {
                    user.update({
                        $addToSet: {
                            transaction: transactionData
                        }
                    }, function (err, updatedData) {
                        if (err) return cb(err);
                        cb(null, project)
                    })
                }
            })

            //     // models.User.findOneAndUpdate({ _id: req.user._id }, { $push: { "transaction": transactionData } })
            //     //     .exec(function(err, user) {
            //     //         if (err) return cb(err);
            //     //         cb(null, project)
            //     //     })

        },
        function (project, cb) {

            if (project.length > 0 && project.paymentStats == true) {
                async.eachSeries(project, function (item, next) {
                    item.paymentStats = true;
                    item.save(function (err, project) {
                        if (err) return cb(err);
                        next();
                    })
                }, function (err) {
                    cb(null, project)
                })
            } else if (project.length > 0) {
                cb(null, project)
            } else {
                project.save(function (err, done) {
                    if (err) return cb(err);
                    // next();
                    cb(null, project)
                })
            }

        },
        // for notification
        (data, cb) => {

            if (!data.length) {
                cb(null, data)
            } else {
                var projectArray = []
                async.eachSeries(data, function (item, next) {

                    models.Project.findById({
                        _id: item._id
                    })
                        .select('-__v')
                        .populate({
                            path: 'service',
                            select: 'name'
                        })
                        .exec(function (err, project) {
                            if (err) return cb(err)
                            if (!project) return cb('no Project')
                            if (project.mainCaptionFlag) {
                                next()
                            } else {
                                projectArray.push(project)
                                next()
                            }

                        })
                    // next()
                }, function (err) {
                    cb(null, projectArray)
                })
            }

        },
        function (data, cb) {
            models.User.find()
                .populate({
                    path: 'role',
                    select: 'name'
                })
                .exec(function (err, users) {
                    if (err) return cb(err);
                    if (!users) return cb('No users found.')
                    if (data.length) {
                        data.forEach(function (project) {
                            users.forEach(function (itemUser) {
                                if (itemUser.role.name == 'writer') {

                                    //  type,to,from,text,extraData,image, callback
                                    var notificationText = "New [" + project.duration + " mins] Job: " + project.service.name + " " + project.name + " " + "now on the job board."
                                    notificationRefer.createNotificationForUser('New Project is Created', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                        if (err) cb(err);
                                    })

                                }
                            })
                        })
                    } else {
                        users.forEach(function (itemUser) {
                            if (itemUser.role.name == 'writer') {
                                //  type,to,from,text,extraData,image, callback
                                var notificationText = "New [" + data.duration + " mins] Job: " + " " + req.body.name + " " + "now on the job board."
                                notificationRefer.createNotificationForUser('New Project is Created', itemUser._id, req.body.clientUser, notificationText, "", 'image', function (err, success) {
                                    if (err) cb(err);
                                })
                            }
                        })
                    }
                    cb(null, data)
                });
        }
    ],
        function (err, project) {
            if (err) {
                res.json(err);
            } else {
                res.json({
                    project: project
                });
            }


        });
}

exports.getProjects = function (req, res) {
    var populateQuery = [{
        path: 'service',
        select: 'name pricessss'
    }, {
        path: 'job',
        select: 'appliedUser'
    }, {
        path: 'clientUser',
        select: 'firstname lastname'
    }];
    const skip = parseInt(req.query.skip)
    const limit = parseInt(req.query.limit)
    models.User.findOne({
        _id: req.user._id
    })
        .populate({
            path: 'role',
            select: 'name'
        })
        .sort({
            createdAt: 1
        })
        .exec(function (err, user) {
            if (err) return res.json({
                success: false,
                message: err
            })
            if (user.role.name == 'client_admin' || user.role.name == 'client_user') {
                models.Project.find({
                    mainCaptionFlag: true
                })
                    .select('-__v')
                    .populate(populateQuery)
                    .skip((skip - 1) * limit)
                    .limit(limit)
                    .where('status').equals('Unassigned')
                    .sort({
                        createdAt: -1
                    })
                    .exec(function (err, projects) {
                        if (err) return res.json(err);
                        if (!projects) return res.json({
                            success: false,
                            message: 'No Projects'
                        })
                        res.json(projects);
                    })
            } else if (req.user.role == 'writer') {
                let mongoQuery = {}
                models.Project.find()
                    .select('-__v')
                    .populate(populateQuery)
                    .where('status').equals('Unassigned')
                    .where('mainCaptionFlag').equals(false)
                    .sort({
                        createdAt: -1
                    })
                    .skip((skip - 1) * limit)
                    .limit(limit)
                    .exec(function (err, projects) {
                        if (err) return res.json(err);
                        if (!projects) return res.json({
                            success: false,
                            message: 'No projects have been created yet.'
                        })
                        var projectsForWriter = []
                        if (req.user.transcriptionAlignment == true && req.user.transcriber == null) {
                            projectsForWriter = projects.filter(value => {
                                return value.service.name == 'transcription-alignment'
                            })
                        }
                        if (req.user.transcriber == true && req.user.transcriptionAlignment == null) {
                            projectsForWriter = projects.filter(value => {
                                return value.service.name == 'transcription'
                            })
                        }
                        if (req.user.transcriber == true && req.user.transcriptionAlignment == true) {
                            projectsForWriter = projects.filter(value => {
                                return value.service.name == 'transcription' || value.service.name == 'transcription-alignment'
                            })
                        }
                        async.eachSeries(projectsForWriter, function (item, next) {
                            if (item.job.appliedUser.length > 0) {

                                async.eachSeries(item.job.appliedUser, function (applied, nextUser) {
                                    if (applied == req.user._id) {
                                        item._doc.jobApplied = true;
                                        nextUser();
                                    } else {
                                        item._doc.jobApplied = false;
                                        nextUser();
                                    }
                                }, function (err) {
                                    next();
                                });
                            } else {
                                item._doc.jobApplied = false;
                                next();
                            }
                        }, function (err) {
                            res.json(projectsForWriter);
                        });
                    })
            } else {
                models.Project.find({
                    mainCaptionFlag: false
                })
                    .select('-__v')
                    .populate(populateQuery)
                    .where('status').equals('Unassigned')
                    .sort({
                        createdAt: -1
                    })
                    .skip((skip - 1) * limit)
                    .limit(limit)
                    .exec(function (err, projects) {
                        if (err) return res.json(err);
                        if (!projects) return res.json({
                            success: false,
                            message: 'No projects have been created yet.'
                        })
                        res.json(projects)

                    })
            }
        })
};


exports.getUserProject = (req, res) => {

    var role = req.user.role;
    var userId = req.user._id;
    var page = 1
    if (role == 'super_user' || role == 'project_admin') {
        // getProjects.call(req, res)
        var populateQuery = [{
            path: 'service',
            select: 'name pricessss'
        }, {
            path: 'job',
            select: 'appliedUser'
        }, {
            path: 'clientUser',
            select: 'firstname lastname'
        }, {
            path: 'workingUser',
            select: 'firstname lastname'
        }];
        models.Project.find({
            mainCaptionFlag: false
        })
            .populate(populateQuery)
            .sort({
                createdAt: -1
            })
            .skip((req.body.skip - 1) * req.body.limit)
            .limit(req.body.limit)
            .exec(function (err, projects) {
                if (err) return res.json(err);
                if (!projects) return res.json({
                    success: false,
                    message: 'Projects Not Found'
                });
                res.json(projects);
            })

    } else if (role == 'client_admin') {
        var populateQuery = [{
            path: 'service',
            select: 'name pricessss'
        }, {
            path: 'job',
            select: 'appliedUser'
        }];
        models.Project.find({
            'clientUser': userId
        })
            .populate(populateQuery)
            .sort({
                createdAt: -1
            })
            .skip((req.body.skip - 1) * req.body.limit)
            .limit(req.body.limit)
            .exec(function (err, projects) {
                if (err) return res.json(err);
                if (!projects) return res.json({
                    success: false,
                    message: 'Projects Not Found'
                });
                var projectArray = []
                async.eachSeries(projects, function (item, next) {
                    if (item.captionReference == null) {
                        projectArray.push(item)
                        next()
                    } else {
                        next()
                    }
                }, function (err) {
                    res.json(projectArray);
                });
            })
    } else if (role == 'client_user' || role == 'project_user') {
        var populateQuery = [{
            path: 'service',
            select: 'name pricessss'
        }, {
            path: 'job',
            select: 'appliedUser'
        }];
        models.User.findOne({
            _id: req.user.id
        })
            .skip((req.body.skip - 1) * req.body.limit)
            .limit(req.body.limit)
            .exec(function (err, users) {
                if (err) return res.json(err);
                // res.json(projects);
                models.Project.find()
                    .where('clientUser').equals(users.createdBy)
                    .populate(populateQuery)
                    .sort({
                        createdAt: -1
                    })
                    .exec(function (err, projects) {
                        if (err) return res.json(err);
                        if (!projects) return res.json({
                            success: false,
                            message: 'Projects Not Found'
                        });
                        res.json(projects);
                    })
            })
    } else if (role == 'writer') {
        var populateQuery = [{
            path: 'service',
            select: 'name pricessss'
        }, {
            path: 'job',
            select: 'appliedUser'
        }];
        models.Project.find()
            .where('workingUser').equals(userId)
            .populate(populateQuery)
            .sort({
                createdAt: -1
            })
            .skip((req.body.skip - 1) * req.body.limit)
            .limit(req.body.limit)
            .exec(function (err, projects) {
                if (err) return res.json(err);
                if (!projects) return res.json({
                    success: false,
                    message: 'Projects Not Found'
                });
                res.json(projects);
            })
    }
}

// to get the particular project by id


exports.getProjectById = function (req, res) {
    if (!req.params.id) return res.json({
        success: false,
        message: 'id Not Provided.'
    })
    if (!validator.isMongoId(req.params.id)) return res.json({
        success: false,
        message: 'Not a valid Id.'
    })
    models.Project.findOne({
        _id: req.params.id
    })
        .populate({
            path: 'workingUser clientUser service',
            select: 'firstname lastname name email'
        })
        .populate({
            path: 'job'
        })
        .exec(function (err, project) {
            if (err) return res.json({
                success: false,
                message: err
            })
            if (!project) return res.json({
                success: false,
                message: 'No Project with this Id.'
            });
            // (project.job.appliedUser.includes(req.user._id)) ? project.Applied = true : project.Applied = false
            res.json(project)
        })
}


exports.projectUpload = function (req, res) {
    var data = {};
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        uploadProject(files.project.name, files.project.path, function (err, projectData) {
            if (err) return res.json({
                error: err
            });
            res.json(projectData);
        });
    });
}

// @complication in captionting part


exports.projectSubmission = function (req, res) {
    var projectId = req.params.projectId;
    var messageBody = req.body.messageBody;
    var check = false
    async.waterfall([
        function (cb) {
            models.Project.findOne({
                _id: req.params.projectId
            })
                .populate({
                    path: 'service'
                })
                .exec(function (err, project) {
                    if (err) return cb(err);
                    if (!project) return cb('No Project With This Id.')

                    if (project.captionReference !== null) {
                        models.Project.find({
                            captionReference: project.captionReference
                        })
                            .populate({
                                path: 'service captionReference'
                            })
                            .exec(function (err, projects) {

                                if (err) return cb(err);
                                if (!projects) return cb('No Project With This Id.')

                                async.eachSeries(projects, function (item, next) {

                                    console.log('id', item._id.toString())
                                    if (item._id.toString() == projectId) {
                                        console.log('id', item._id.toString())
                                        item.update({
                                            message: req.body.messageBody,
                                            status: "completed",
                                            updatedAt: Date.now(),
                                            projectSubmissionTime: Date.now(),
                                            $push: {
                                                finalProjectSubmit: req.body.finalProjectSubmit
                                            }
                                        }, function (err, success) {
                                            if (err) return cb(err);
                                            models.Project.findOneAndUpdate({
                                                _id: item.captionReference
                                            }, {
                                                    $push: {
                                                        finalProjectSubmit: req.body.finalProjectSubmit
                                                    }
                                                }, {
                                                    new: true
                                                })
                                                .exec(function (err, pro) {
                                                    if (err) return cb(err);
                                                    if (item.captionReference.finalProjectSubmit.length == 1) {
                                                        models.Project.findByIdAndUpdate({
                                                            _id: item.captionReference._id
                                                        }, {
                                                                status: "completed",
                                                                projectSubmissionTime: Date.now()
                                                            })
                                                            .exec(function (err, pro) {
                                                                if (err) return cb(err);
                                                                next()
                                                            })

                                                    } else {
                                                        next()
                                                    }
                                                })
                                        })
                                    } else if (item._id.toString() != projectId) {
                                        item.update({
                                            $push: {
                                                finalProjectSubmit: req.body.finalProjectSubmit
                                            }
                                        }, function (err, success) {
                                            if (err) return cb(err);
                                            if (item.finalProjectSubmit.length == 1) {
                                                models.Project.findByIdAndUpdate({
                                                    _id: item.captionReference._id
                                                }, {
                                                        status: "completed",
                                                        projectSubmissionTime: Date.now()
                                                    })
                                                    .exec(function (err, pro) {
                                                        if (err) return cb(err);
                                                        next()
                                                    })
                                            } else {
                                                next();
                                            }
                                        })
                                    }
                                }, function (err) {
                                    cb(null, project)
                                });

                            })
                    } else { //for normal project
                        project.update({
                            message: req.body.messageBody,
                            status: "completed",
                            updatedAt: Date.now(),
                            projectSubmissionTime: Date.now(),
                            $push: {
                                finalProjectSubmit: req.body.finalProjectSubmit
                            }
                        }, function (err, success) {
                            if (err) return cb(err);
                            cb(null, project)
                        })
                    }

                })
        },
        function (project, cb) {
            models.User.findOne({
                _id: req.user._id
            }, function (err, userInfo) {
                if (err) return cb(err);
                cb(null, userInfo, project)
            })
        },
        function (userInfo, project, cb) {
            //  type,to,from,text,extraData,image, callback
            models.User.find()
                .populate({
                    path: 'role',
                    select: 'name'
                })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    if (!user) return cb('No Users found.')

                    if (project.captionReference !== null) {

                        captionProjectDetailssSubmissionCase(project, (captionProjects) => {

                            // if addTranscript ==  true
                            if (captionProjects.parentProject.selectedService.addTranscript) {

                                if (captionProjects.parentProject.status == 'completed') {
                                    //if caption parent is completed then
                                    console.log('completed')
                                    user.forEach(function (itemUser) {

                                        if (itemUser.role.name == 'project_admin' || itemUser.role.name == 'super_user') {
                                            //  type,to,from,text,extraData,image, callback

                                            var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted a captions for the" + " " + captionProjects.transcription.name;

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }


                                        if (project.clientUser.toString() == itemUser._id.toString()) {

                                            var notificationText = `The captions for ${captionProjects.transcription.name} are complete and ready for download.`

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                    })
                                }

                                // if not complete transcription case
                                if (captionProjects.transcription.status == 'completed' && captionProjects.parentProject.status != 'completed') {

                                    user.forEach(function (itemUser) {
                                        // 
                                        if (itemUser.role.name == 'project_admin' || itemUser.role.name == 'super_user') {
                                            //  type,to,from,text,extraData,image, callback

                                            var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted a transcript for" + " " + captionProjects.transcription.name;

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                        if (project.clientUser.toString() == itemUser._id.toString()) {
                                            var notificationText = `The transcript for ${captionProjects.transcription.name} has been completed and is ready for download.`

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                        // transcriptionAlignment

                                        if (captionProjects.transcriptionAlignment.workingUser && captionProjects.transcriptionAlignment.workingUser.toString() == itemUser._id.toString()) {

                                            // The transcript for PROJECT NAME is now available for caption-alignment.

                                            var notificationText = `The transcript for ${captionProjects.transcription.name} is now available for caption-alignment.`

                                            notificationRefer.createNotificationForUser('Project submission.', captionProjects.transcriptionAlignment.workingUser, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                    })
                                }

                                // @transcription alignment

                                if (captionProjects.transcriptionAlignment.status == 'completed' && captionProjects.parentProject.status != 'completed') {

                                    user.forEach(function (itemUser) {

                                        if (itemUser.role.name == 'project_admin' || itemUser.role.name == 'super_user') {
                                            //  type,to,from,text,extraData,image, callback
                                            var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted a transcript for" + " " + captionProjects.transcription.name;

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                        if (project.clientUser.toString() == itemUser._id.toString()) {
                                            var notificationText = `The transcript for ${captionProjects.transcription.name} has been completed and is ready for download.`

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }


                                        if (captionProjects.transcription.workingUser && captionProjects.transcription.workingUser.toString() == itemUser._id.toString()) {
                                            var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted a transcript for" + " " + captionProjects.transcription.name;

                                            notificationRefer.createNotificationForUser('Project submission.', captionProjects.transcription.workingUser, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                    })
                                }

                            } else {
                                // if addtranscript == false 

                                if (captionProjects.parentProject.status == 'completed') {
                                    //if caption parent is completed then
                                    console.log('completed')
                                    user.forEach(function (itemUser) {

                                        if (itemUser.role.name == 'project_admin' || itemUser.role.name == 'super_user') {
                                            //  type,to,from,text,extraData,image, callback

                                            var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted a captions for the" + " " + captionProjects.transcription.name;

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }


                                        if (project.clientUser.toString() == itemUser._id.toString()) {
                                            var notificationText = `The captions for ${captionProjects.transcription.name} are complete and ready for download.`

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                    })
                                }

                                // if not complete transcription case
                                if (captionProjects.transcription.status == 'completed' && captionProjects.parentProject.status != 'completed') {

                                    user.forEach(function (itemUser) {
                                        // 
                                        if (itemUser.role.name == 'project_admin' || itemUser.role.name == 'super_user') {
                                            //  type,to,from,text,extraData,image, callback

                                            var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted a transcript for" + " " + captionProjects.transcription.name;

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                        if (project.clientUser.toString() == itemUser._id.toString()) {
                                            var notificationText = `The transcript for ${captionProjects.transcription.name} has been completed and is ready for download.`

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                        // transcriptionAlignment

                                        if (captionProjects.transcriptionAlignment.workingUser && captionProjects.transcriptionAlignment.workingUser.toString() == itemUser._id.toString()) {

                                            // The transcript for PROJECT NAME is now available for caption-alignment.

                                            var notificationText = `The transcript for ${captionProjects.transcription.name} is now available for caption-alignment.`

                                            notificationRefer.createNotificationForUser('Project submission.', captionProjects.transcriptionAlignment.workingUser, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                    })
                                }

                                // @transcription alignment

                                if (captionProjects.transcriptionAlignment.status == 'completed' && captionProjects.parentProject.status != 'completed') {

                                    user.forEach(function (itemUser) {

                                        if (itemUser.role.name == 'project_admin' || itemUser.role.name == 'super_user') {
                                            //  type,to,from,text,extraData,image, callback
                                            var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted a transcript for" + " " + captionProjects.transcription.name;

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                        if (project.clientUser.toString() == itemUser._id.toString()) {
                                            var notificationText = `The transcript for ${captionProjects.transcription.name} has been completed and is ready for download.`

                                            notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }


                                        if (captionProjects.transcription.workingUser && captionProjects.transcription.workingUser.toString() == itemUser._id.toString()) {
                                            var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted a transcript for" + " " + captionProjects.transcription.name;

                                            notificationRefer.createNotificationForUser('Project submission.', captionProjects.transcription.workingUser, req.user._id, notificationText, "", 'image', function (err, success) {
                                                if (err) {
                                                    return cb(err);
                                                }
                                            })
                                        }

                                    })
                                }

                            }



                        })


                        // models.Project.findOne({
                        //         _id: project.captionReference
                        //     })
                        //     .exec(function (err, project) {
                        //         if (err) return cb(err);
                        //         if (!project) return cb('No project Found!')
                        //         if (project.status == 'completed') {

                        //             user.forEach(function (itemUser) {

                        //                 if (itemUser.role.name == 'project_admin') {
                        //                     //  type,to,from,text,extraData,image, callback

                        //                     var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted" + " " + project.name;

                        //                     notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                        //                         if (err) {
                        //                             return cb(err);
                        //                         }
                        //                     })
                        //                 }
                        //                 if (project.clientUser.toString() == itemUser._id.toString()) {

                        //                     var notificationText = 'Your Project ' + project.name + ' has been submitted by ' + req.user.user.firstname + " " + req.user.user.lastname;

                        //                     notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                        //                         if (err) {
                        //                             return cb(err);
                        //                         }
                        //                     })
                        //                 }
                        //             })

                        //         } else {
                        //             user.forEach(function (itemUser) {

                        //                 if (itemUser.role.name == 'project_admin') {
                        //                     //  type,to,from,text,extraData,image, callback

                        //                     var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted" + " " + project.name;

                        //                     notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                        //                         if (err) {
                        //                             return cb(err);
                        //                         }
                        //                     })
                        //                 }
                        //             })
                        //         }
                        //     })


                    } else {
                        user.forEach(function (itemUser) {
                            if (itemUser.role.name == 'project_admin' || itemUser.role.name == 'super_user') {
                                //  type,to,from,text,extraData,image, callback

                                var notificationText = userInfo.firstname + " " + userInfo.lastname + " " + "has submitted a project" + " " + project.name + ` [${project.service.name}]`;

                                notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                    if (err) {
                                        return cb(err);
                                    }
                                })
                            }

                            if (project.clientUser.toString() == itemUser._id.toString()) {

                                let notificationText = `The ${project.name}[${project.service.name}] has been completed and is ready to download. `

                                notificationRefer.createNotificationForUser('Project submission.', itemUser._id, req.user._id, notificationText, "", 'image', function (err, success) {
                                    if (err) {
                                        return cb(err);
                                    }
                                })
                            }
                        })
                    }

                })
            cb(null, project)
        }

    ],
        function (err, project) {
            if (err) return res.json({
                error: err
            });
            res.json({
                id: project._id
            });
        })

}

const captionProjectDetailssSubmissionCase = (project, cb) => {
    if (!project.captionReference == null) return cb({
        success: false,
        message: 'Not a caption project'
    });
    models.Project.find({
        captionReference: project.captionReference
    })
        .populate({
            path: 'captionReference'
        })
        .populate({
            path: 'service'
        })
        .exec(function (err, captionRefrence) {
            if (err) return cb({
                success: false,
                message: err
            });
            if (!captionRefrence) cb({
                success: false,
                message: 'No such projects!'
            })
            var projects = {}
            captionRefrence.forEach(function (item) {
                if (item._id.toString() != project._id.toString()) {
                    projects.seacondProject = item
                }
                if (item.service.name == 'transcription') {
                    projects.transcriptionProject = item
                }

                if (item.service.name == 'transcription-alignment') {
                    projects.transcriptionAling = item
                }

            })

            models.Project.findById({
                _id: project.captionReference
            }).exec(function (err, parentProject) {
                if (err) return cb({
                    success: false,
                    message: err
                });
                if (!parentProject) cb({
                    success: false,
                    message: 'No Parent Project !'
                })
                return cb({
                    success: true,
                    parentProject: parentProject,
                    currentProject: project,
                    seacondProject: projects.seacondProject,
                    transcription: projects.transcriptionProject,
                    transcriptionAlignment: projects.transcriptionAling
                })
            })
        })
}

exports.issueReport = function (req, res) {
    if (!req.params.projectId) return res.json({
        success: false,
        message: 'Please provide project Id.'
    })
    var projectId = req.params.projectId;
    var userId = req.body.userId;
    var userArray = [];
    async.waterfall([
        function (cb) {
            models.User.find()
                .populate({
                    path: 'role',
                    select: 'name'
                })
                .exec(function (err, users) {
                    if (err) return cb(err);
                    if (!users) return cb('No users Found!')
                    async.eachSeries(users, (item, next) => {
                        if (item.role.name == 'project_admin' || item.role.name == 'super_user') {
                            userArray.push(item._id);
                            next()
                        } else {
                            next()
                        }
                    }, function () {
                        cb(null)
                    })
                });
        },
        function (cb) {

            new models.IssueReport({
                to: userArray,
                from: userId,
                issueWithDelivery: req.body.issueWithDelivery,
                issueWithMedia: req.body.issueWithMedia,
                message: req.body.message,
                project: projectId
            }).save().then(report => {
                cb(null, report)
            }).catch(err => {
                cb(err)
            })
        },

        function (report, cb) {
            var notificationText = "Issue is created by:" + " " + req.user.user.firstname + " " + req.user.user.lastname;
            userArray.forEach(function (itemUser) {
                notificationRefer.createNotificationForUser('Issue Submission.', itemUser, userId, notificationText, "", 'image', function (err, success) {
                    if (err) {
                        return cb(err);
                    }
                })

            })
            cb(null, report)
        }

    ], function (err, report) {
        if (err) return res.json({
            error: err
        });
        res.json({
            success: false,
            message: 'issue generated.'
        });
    })

}



// updated Cost

exports.updatedCostProject = function (req, res) {
    var newTotalPrice = req.body.newTotalPrice;
    var projectId = req.params.projectId;
    var userId = req.body.userId;
    async.waterfall([
        function (cb) {
            models.Project.findById({
                _id: req.params.projectId
            }).populate({
                path: 'captionReference'
            })
                .exec(function (err, project) {
                    if (err) return cb(err);
                    if (!project) return cb('No such project!')
                    if (project.status == 'completed') return cb('Project is already Completed')
                    if (project.captionReference != null) {
                        models.Project.find({
                            captionReference: project.captionReference
                        })
                            .populate({
                                path: 'captionReference'
                            })
                            .exec(function (err, captionRefrence) {
                                if (err) return cb(err);
                                if (!captionRefrence) return cb('No such projects!')

                                captionProjectDetails(project, function (data) {

                                    models.Project.findByIdAndUpdate({
                                        _id: data.currentProject._id
                                    }, {
                                            $push: {
                                                totalPrice: req.body.newTotalPrice
                                            },
                                        }, {
                                            new: true
                                        })
                                        .populate({
                                            path: 'captionReference'
                                        })
                                        .exec(function (err, updatedProject) {
                                            if (err) cb(err)
                                            data.parentProject.totalPrice.push(
                                                updatedProject.totalPrice[updatedProject.totalPrice.length - 1] + data.seacondProject.totalPrice[data.seacondProject.totalPrice.length - 1]
                                            )
                                            data.parentProject.save().then(project => cb(null, project)).catch(err => cb(err))
                                        })
                                })
                            })
                    } else {
                        project.update({
                            $push: {
                                totalPrice: req.body.newTotalPrice
                            }
                        }, function (err, done) {
                            if (err) cb(err)
                            cb(null, project)
                        })
                    }
                });
        },
        function (project, cb) {
            var notificationText = project.name + " " + "is updated to:" + " " + newTotalPrice;
            notificationRefer.createNotificationForUser('Amount Updated.', project.clientUser, req.user._id, notificationText, "", 'image', function (err, success) {
                if (err) {
                    return cb(err);
                } else {
                    cb(null, project);
                }
            })
        }

    ], function (err, project) {
        if (err) return res.json({
            success: false,
            error: err
        });
        res.json({
            success: true,
            message: 'Price updated!'
        });
    })

}

// get the list of writters
exports.getWriterListUser = function (req, res) {
    var userArray = [];
    async.waterfall([
        function (cb) {
            models.User.find()
                .select('_id firstname lastname skills role transcriber transcriptionAlignment')
                .populate({
                    path: 'role',
                    select: 'name'
                })
                .exec(function (err, user) {
                    if (err) return cb(err);
                    user.forEach(function (itemUser) {
                        if (itemUser.role.name == 'writer') {
                            userArray.push(itemUser);
                        }
                    })
                    cb(null, userArray)
                });
        }
    ], function (err, userArray) {
        if (err) return res.json({
            error: err
        });
        res.json({
            userArray
        });
    })

}





const isCustomPriceEnabled = (project, writerId, cb) => {
    // @algo 1)find user detail =>if writer => calculate the priject writer price
    models.User.findOne({
        _id: writerId
    })
        .populate({ path: 'role', select: 'name' })
        .exec(function (err, user) {
            if (err) return cb({ isDone: false, err })
            if (!user) return cb({ isDone: false, message: 'User not Found' })
            if (user.role.name === 'writer') {
                models.UserPrice.findOne({
                    user: user._id
                }).exec(function (err, userPrice) {
                    if (err) return cb({ isDone: false, err })
                    if (!userPrice) return cb({ isDone: false, message: 'Price not set' })
                    let serviceSelected = {}
                    if (project.selectedService.turnaroundSelected) serviceSelected.turnAround = JSON.parse(project.selectedService.turnaroundSelected)

                    if (project.selectedService.techniqueSelected) serviceSelected.techniqueSelected = JSON.parse(project.selectedService.techniqueSelected)

                    if (project.selectedService.accentSelected && project.service.name == 'transcription-alignment') serviceSelected.accentSelected = JSON.parse(project.selectedService.accentSelected)

                    if (userPrice.transcription.writerPrice && project.service.name == 'transcription') {
                        userPrice.transcription.price = userPrice.transcription.writerPrice;

                        serviceSelected.serviceCharge = userPrice.transcription
                    } else if (userPrice.transcriptionAlignment.writerPrice && project.service.name == 'transcription-alignment') {
                        userPrice.transcriptionAlignment.price = userPrice.transcriptionAlignment.writerPrice;

                        serviceSelected.serviceCharge = userPrice.transcriptionAlignment
                    }

                    let writerPrice = 0.00


                    Object.entries(serviceSelected).forEach(([key, value]) => {
                        let val = parseFloat(value.price.replace(/[^0-9\.]+/g, ""))
                        var price = val * parseInt(project.duration);
                        writerPrice = writerPrice + price;
                        // project.writerPrice[project.writerPrice.length - 1] = parseInt(writerPrice.toFixed(2))
                    });

                    cb({ isDone: true, project, writerPrice })
                })
            } else {
                cb({ isDone: false, message: 'Not for you.' })
            }

        })
}

exports.assignIc = function (req, res) {

    if (!req.user._id) return res.json({
        success: false,
        message: 'Not Authorized'
    })
    if (!req.body.writerId) return res.json({
        success: false,
        message: 'Writer id is needed.'
    })
    if (!req.params.projectId) return res.json({
        success: false,
        message: 'project id is needed.'
    })
    if (!req.body.userId) return res.json({
        success: false,
        message: 'User id is needed.'
    })
    async.waterfall([
        function (cb) {
            models.Project.findOne({
                _id: req.params.projectId
            })
                .populate({ path: 'service' })
                .exec(function (err, project) {
                    if (err) return cb(err);
                    if (!project) return cb('No Such Project.')

                    if (project.captionReference != null) {
                        captionProjectDetailssSubmissionCase(project, (projectsDetails) => {

                            isCustomPriceEnabled(projectsDetails.currentProject, req.body.writerId, function (response) {
                                if (response.isDone) {

                                    projectsDetails.currentProject.update({
                                        workingUser: req.body.writerId,
                                        status: 'In Progress',
                                        updatedAt: Date.now(),
                                        assignTime: Date.now(),
                                        $push: { writerPrice: response.writerPrice }
                                    },
                                        function (err, success) {
                                            if (err) return cb(err);
                                            projectsDetails.parentProject.save().then(currentProject => {
                                                cb(null, project)
                                            }).catch(err => res.json(err))
                                        })

                                    // projectsDetails.currentProject.workingUser = req.body.writerId;
                                    // projectsDetails.currentProject.updatedAt = Date.now();
                                    // projectsDetails.currentProject.status = 'In Progress';
                                    // projectsDetails.currentProject.assignTime = Date.now()
                                    // projectsDetails.currentProject.save().then(currentProject => {
                                    //     projectsDetails.parentProject.status = 'In Progress';
                                    //     projectsDetails.parentProject.assignTime = Date.now()
                                    //     projectsDetails.parentProject.save().then(currentProject => cb(null, project)).catch(err => res.json(err))
                                    // })

                                } else {
                                    projectsDetails.currentProject.workingUser = req.body.writerId;
                                    projectsDetails.currentProject.updatedAt = Date.now();
                                    projectsDetails.currentProject.status = 'In Progress';
                                    projectsDetails.currentProject.assignTime = Date.now()
                                    projectsDetails.currentProject.save().then(currentProject => {
                                        projectsDetails.parentProject.status = 'In Progress';
                                        projectsDetails.parentProject.assignTime = Date.now()
                                        projectsDetails.parentProject.save().then(currentProject => cb(null, project)).catch(err => res.json(err))
                                    })

                                }
                            })
                        })
                    } else {
                        isCustomPriceEnabled(project, req.body.writerId, function (response) {
                            if (response.isDone) {
                                project.update({
                                    workingUser: req.body.writerId,
                                    status: 'In Progress',
                                    updatedAt: Date.now(),
                                    assignTime: Date.now(),
                                    $push: { writerPrice: response.writerPrice }
                                },
                                    function (err, success) {
                                        if (err) return cb(err);
                                        cb(null, project)
                                    })
                            } else {
                                project.update({
                                    workingUser: req.body.writerId,
                                    status: 'In Progress',
                                    updatedAt: Date.now(),
                                    assignTime: Date.now()
                                },
                                    function (err, success) {
                                        if (err) return cb(err);
                                        cb(null, project)
                                    })
                            }
                        })

                    }
                });
        },
        function (project, cb) {
            models.Job.findOne({
                project: req.params.projectId
            }, function (err, job) {
                if (err) return cb(err);
                if (!job) return cb('No Such Job.')
                cb(null, project, job)
            })
        },
        function (project, job, cb) {
            models.User.findOne({
                _id: req.body.userId
            }, function (err, userInfo) {
                if (err) return cb(err);
                if (!userInfo) return cb('No Such User.')
                cb(null, project, userInfo, job)
            })
        },
        function (project, userInfo, job, cb) {
            //  type,to,from,text,extraData,image, callback
            var notificationText = project.name + " " + "has been assign to you by:" + " " + userInfo.firstname + " " + userInfo.lastname;
            notificationRefer.createNotificationForUser('Assign.', req.body.writerId, req.body.userId, notificationText, job._id, 'image', function (err, success) {
                if (err) return cb(err);
                cb(null, project);
            })
        }

    ], function (err, project) {
        if (err) return res.json({
            error: err
        });
        res.json({
            success: true,
            messege: 'Project Assigned.'
        })
    })
}

const captionProjectDetails = (project, cb) => {
    if (!project.captionReference == null) return cb({
        success: false,
        message: 'Not a caption project'
    });
    models.Project.find({
        captionReference: project.captionReference
    })
        .populate({
            path: 'captionReference'
        })
        .exec(function (err, captionRefrence) {
            if (err) return cb({
                success: false,
                message: err
            });
            if (!captionRefrence) cb({
                success: false,
                message: 'No such projects!'
            })
            var seacondProject = ''
            captionRefrence.forEach(function (item) {
                if (item._id.toString() != project._id.toString()) {
                    seacondProject = item
                }
            })

            models.Project.findById({
                _id: project.captionReference._id
            }).exec(function (err, parentProject) {
                if (err) return cb({
                    success: false,
                    message: err
                });
                if (!parentProject) cb({
                    success: false,
                    message: 'No Parent Project !'
                })
                return cb({
                    success: true,
                    parentProject: parentProject,
                    currentProject: project,
                    seacondProject: seacondProject
                })
            })
        })
}

// project Attachment

exports.attachmentToProject = function (req, res) {
    var userId = req.body.userId;
    var projectId = req.params.projectId;
    var attachmentLink = req.body.attachmentLink;
    var attachmentInfo = {
        userId: req.user._id,
        attachmentLink: req.body.attachmentLink
    }
    models.Project.findOne({
        _id: projectId
    })
        .populate({
            path: 'captionReference'
        })
        .exec(function (err, project) {
            if (err) return res.json(err);
            if (!project) return res.json({
                success: false,
                message: 'No project Found.'
            })
            if (project.captionReference != null) {
                captionProjectDetails(project, function (data) {

                    if (data.success) {
                        data.currentProject.update({
                            $addToSet: {
                                attachment: attachmentInfo
                            }
                        }).then(done => {
                            data.seacondProject.update({
                                $addToSet: {
                                    attachment: attachmentInfo
                                }
                            }).then(done2 => {
                                data.parentProject.update({
                                    $addToSet: {
                                        attachment: attachmentInfo
                                    }
                                }).then(done3 => {
                                    res.json({
                                        success: true,
                                        message: 'Project Updated.'
                                    })
                                })
                            })
                        })

                    }
                })
            } else {
                project.update({
                    $addToSet: {
                        attachment: attachmentInfo
                    }
                }, function (err, success) {
                    if (err) return res.json(err);
                    res.json(project.id)
                })
            }

        });

}





// add update to the project method post
exports.addlabelToproject = (req, res) => {
    if (!req.body.labelId) return res.json({
        success: false,
        message: 'Please Provide LabelId.'
    })
    models.Label.findOne({
        _id: req.body.labelId
    })
        .exec(function (err, label) {
            label.update({
                $addToSet: {
                    projectId: req.body.projectId
                }
            }, function (err, done) {
                if (err) return res.json(err);
                res.json('label Added.');
            })
        })

}



exports.addlabelbyUser = (req, res) => {
    if (!req.user._id) return ({
        success: false,
        message: 'User not Logged in.'
    })
    var labelArrayData = [];

    models.Label.find({
        createdBy: req.user._id
    })
        .exec(function (err, label) {
            if (err) return res.json(err);
            async.eachSeries(label, function (item, next) {
                if (item.name == req.body.label) {
                    next('Please Add Different Tag.');
                } else {
                    next();
                }
            }, function (err) {
                if (err) {
                    res.json(err)
                } else {
                    var label = new models.Label({
                        name: req.body.label,
                        createdBy: req.user._id
                    });
                    label.save(function (err, newLabel) {
                        if (err) return res.json({
                            success: false,
                            message: err
                        })
                        res.json('label Added.');
                    })
                }
            });
        })

}

exports.removelabelbyUser = (req, res) => {
    if (!req.user._id) return ({
        success: false,
        message: 'User not Logged in.'
    })
    models.Label.findByIdAndRemove(req.body.labelId, function (err) {
        if (err) return res.json(err);
        res.json('Tag Removed.');
    });
}

exports.getusersLabel = (req, res) => {
    if (!req.user._id) return ({
        success: false,
        message: 'User not Logged in.'
    })
    models.Label.find({
        createdBy: req.user._id
    })
        .exec(function (err, label) {
            if (err) return res.json(err);
            if (!label) return res.json('You have not created any label yet.')
            res.json(label);
        })
}

exports.getProjectAccordingToLabel = (req, res) => {
    if (!req.params.labelId) return res.json({
        success: false,
        message: 'No labelId is provided.'
    })
    models.Label.findOne({
        _id: req.params.labelId
    })
        .populate({
            path: 'projectId'
        })
        .exec(function (err, label) {
            if (err) return res.json(err);
            if (!label) return res.json('There is no such label.')
            res.json(label);
        })

}



exports.getVideoInfo = function (req, res) {
    if (!req.body.link) return res.json({
        success: false,
        message: 'No Link is provided.'
    })
    probe(req.body.link, function (err, probeData) {
        if (err) return res.json({
            success: false,
            message: err
        })
        res.json({
            length: probeData
        });
    });
};

exports.cancelProject = (req, res) => {
    if (!req.params.projectId) return res.json({
        success: false,
        message: 'No ProjectId is provided.'
    })
    models.Project.findOne({
        _id: req.params.projectId
    })
        .exec(function (err, project) {
            if (err) return res.json(err);
            project.status = 'cancelled';
            project.updatedAt = Date.now();
            project.save(function (err, done) {
                if (err) return res.json(err);
                res.json('project cancelled');
            })
        })
}



exports.downloadYoutube = (req, res) => {
    // var video = youtubedl(req.body.link);
    var platformCheck = req.body.link.includes("vimeo")
    var isDropbox = req.body.link.includes('dropbox')
    var isYoutube = req.body.link.includes('youtube')
    var isDrive = req.body.link.includes('drive.google.com')
    if (platformCheck) {
        res.json({
            messege: 'No Download'
        });
    } else if (isDropbox || isDrive) {
        youtubedl.getInfo(req.body.link, function (err, info) {
            if (err) return res.json(err);
            res.json(info.url)
        });
    } else {
        youtubedl.getInfo(req.body.link, ['--format=18'], function (err, info) {
            if (err) return res.json(err);
            res.json(info.url)
        });
    }

}

exports.downloadCsv = (req, res) => {
    var finalData = []

    async.eachSeries(req.body.projects, (item, next) => {
        var data = _.omit(item, ['_id', 'job', 'paymentStats', 'servicePaidFeatures', 'captionReference', 'attachment', 'mainCaptionFlag', 'label', 'selectedService', 'deactive', 'finalProjectSubmit', 'size', '__v', 'updatedAt', 'assignTime']);
        data.totalPrice = data.totalPrice[data.totalPrice.length - 1];
        data.writerPrice = data.writerPrice[data.writerPrice.length - 1]
        data.service = data.service.name
        data.createdAt = moment(data.createdAt).format("MM/DD/YYYY")
        data.orderDue = moment(data.orderDue).format("MM/DD/YYYY")
        data.clientUser = data.clientUser.firstname + " " + data.clientUser.lastname
        if (data.workingUser) data.workingUser = data.workingUser.firstname + " " + data.workingUser.lastname
        if (data.projectSubmissionTime) data.projectSubmissionTime = moment(data.projectSubmissionTime).format("MM/DD/YYYY")
        finalData.push(data)
        next()
    }, (err) => {
        if (err) console.log(err)
        jsonexport(finalData, function (err, csv) {
            if (err) return res.json({
                success: false,
                message: err
            })
            if (!csv) return res.json({
                success: false,
                message: 'No json found!'
            })
            res.json(csv)
        });
    })
}

exports.deleteProject = (req, res) => {

    if (!req.user._id) return res.json({
        success: false,
        message: 'User Not logged In.'
    })
    models.User.findOne({
        _id: req.user._id
    })
        .populate({
            path: 'role',
            select: 'name'
        })
        .exec(function (err, user) {
            if (err) return res.json({
                success: false,
                message: err
            });
            if (!user) return res.json({
                success: false,
                message: 'No such User'
            });
            if (user.role.name !== 'super_user') return res.json({
                success: false,
                message: 'Not Authorized.'
            })

            async.eachSeries(req.body.id, function (id, next) {
                models.Project.findByIdAndRemove({
                    _id: id
                })
                    .exec((err, done) => {
                        if (err) return next(err)
                        // if (!done) return next('No such Project.')
                        next()
                    })

            }, function (err) {
                if (err) {
                    res.json({
                        success: false,
                        message: err
                    });
                } else {
                    res.json({
                        success: true,
                        message: 'Project Deleted'
                    })
                }

            })

        })
}

exports.orderDueUpdate = (req, res) => {
    if (!req.user._id) return res.json({
        success: false,
        message: 'User Not logged In.'
    })
    if (!req.params.id) return res.json({
        success: false,
        message: 'Id not Provided.'
    })
    if (!validator.isMongoId(req.params.id)) return res.json({
        success: false,
        message: 'Not a valid Id.'
    })
    models.User.findOne({
        _id: req.user._id
    })
        .populate({
            path: 'role',
            select: 'name'
        })
        .exec(function (err, user) {
            if (err) return res.json({
                success: false,
                message: err
            });
            if (!user) return res.json({
                success: false,
                message: 'No such User'
            });
            if (user.role.name == 'super_user' || user.role.name == 'project_admin') {
                models.Project.findOneAndUpdate({
                    _id: req.params.id
                }, {
                        orderDue: req.body.orderDue
                    })
                    .exec((err, done) => {
                        if (err) return res.json({
                            success: false,
                            message: err
                        });
                        if (!done) return res.json({
                            success: false,
                            message: 'No such Project'
                        });
                        res.json({
                            success: true,
                            message: 'Order Due Updated.'
                        })
                    })
            } else {
                res.json({
                    success: false,
                    message: 'Not Authorized.'
                })
            }

        })
}
// @by superuser and projectAdmin
exports.editWriterPrice = (req, res) => {
    if (!req.body.writerPrice) return res.json({
        success: false,
        message: 'writer price not provided.'
    })
    if (req.user.role == 'super_user' || req.user.role == 'project_admin') {

        models.Project.findOneAndUpdate({
            _id: req.body.id
        }, {
                $push: {
                    writerPrice: req.body.writerPrice
                }
            })
            .exec((err, done) => {
                if (err) return res.json({
                    success: false,
                    message: err
                });
                if (!done) return res.json({
                    success: false,
                    message: 'No such Project'
                });
                res.json({
                    success: true,
                    message: 'Total Price  Updated.'
                })
            })
    } else {
        res.json({
            success: false,
            message: 'user is not authorized!'
        })
    }
}


// @by superuser and projectAdmin
exports.updateWriterPrice = (req, res) => {
    if (!req.body.totalPrice) return res.json({
        success: false,
        message: 'writer price not provided.'
    })
    if (req.user.role == 'super_user' || req.user.role == 'project_admin') {

        models.Project.findOneAndUpdate({
            _id: req.body.id
        }, {
                $push: {
                    totalPrice: req.body.totalPrice
                }
            })
            .exec((err, done) => {
                if (err) return res.json({
                    success: false,
                    message: err
                });
                if (!done) return res.json({
                    success: false,
                    message: 'No such Project'
                });
                res.json({
                    success: true,
                    message: 'totalPrice Due Updated.'
                })
            })
    } else {
        res.json({
            success: false,
            message: 'user is not authorized!'
        })
    }
}


// @by superuser and projectAdmin
exports.unAssignWriter = (req, res) => {
    if (!req.body.id) return res.json({
        success: false,
        message: 'Please Provide Id.'
    })
    if (req.user.role == 'super_user' || req.user.role == 'project_admin') {

        models.Project.findOne({
            _id: req.body.id
        })
            .exec((err, project) => {
                const workingUser = project.workingUser
                if (err) return res.json({
                    success: false,
                    message: err
                });
                if (!project) return res.json({
                    success: false,
                    message: 'No such Project'
                });
                if (_.isEmpty(project.workingUser)) return res.json({
                    success: false,
                    message: 'No user is in working user'
                })
                project.workingUser = undefined;
                project.status = 'Unassigned'
                project.usAssignTime = Date.now()
                project.save().then(done => {
                    //  type,to,from,text,extraData,image, callback
                    var notificationText = `You have Unassigned from Project :${done.name} by ${req.user.firstname} ${req.user.lastname}`
                    notificationRefer.createNotificationForUser(`UnAssigned From Project`, workingUser, req.user._id, notificationText, "", 'image', function (err, success) {
                        res.json({
                            success: true,
                            message: 'user Unassinged'
                        })
                    })
                }).catch(err => res.status(401).json({
                    success: false,
                    message: err
                }))
            })
    } else {
        res.json({
            success: false,
            message: 'user is not authorized!'
        })
    }
}

exports.downloadOpenCaptionVideo = (req, res) => {

    request({
        url: 'http://18.221.180.214/downloadOpenCaptionVideo',
        method: 'POST',
        body: req.body,
        json: true 
    }, function (err, response, body) {
        console.log(err, body);
        if (err) return res.json({ success: false, message: err })
        res.json({ success: true, message: body })
    })

    // console.log(`body======>`,req.body)
    // if (!req.body.link) return res.json({ success: false, message: 'Please provide the link.' })
    // if (!req.body.srt) return res.json({ success: false, message: 'Please provide the srt.' })
    // if (!req.body.email) return res.json({ success: false, message: 'Please provide the email.' })
    // var serverAppPath = '/home/ubuntu/phase_2/post_cap/';
    // if (req.body.link.includes('https://www.youtube.com')) {
    //     async.waterfall([
    //         function (cb) {
    //             console.log(__dirname)
    //             let name = req.body.link.split('https://www.youtube.com/watch?v=')
    //             var video = youtubedl(req.body.link,
    //                 // Optional arguments passed to youtube-dl.
    //                 ['--format=18'],
    //                 // Additional options can be given for calling `child_process.execFile()`.
    //                 { cwd: `/home` });
    //             // Will be called when the download starts.
    //             video.on('info', function (info) {
    //                 console.log('Download started');
    //             });


    //             video.pipe(fs.createWriteStream(`${name[1]}.mp4`))
    //         },
    //         function (arg1, arg2, cb) {
    //             // arg1 now equals 'one' and arg2 now equals 'two'
    //             cb(null, 'three');
    //         },
    //     ], function (err, result) {
    //         // result now equals 'done'
    //     });

    // } else if (req.body.link.includes('post-cap-projects-phase-2/')) {
    //     async.waterfall([
    //         function (cb) {
    //             res.json({ success: true, message: `your Request is processed.` })
    //             let name = req.body.link.split('post-cap-projects-phase-2/')
    //             const fileStream = s3.getObject({
    //                 Bucket: 'post-cap-projects-phase-2',
    //                 Key: name[1]
    //             })

    //             var getParams = {
    //                 Bucket: 'post-cap-projects-phase-2',
    //                 Key: name[1]
    //             }

    //             s3.getObject(getParams, function (err, data) {
    //                 // Handle any error and exit
    //                 if (err) console.log(err)
    //                 console.log(`===================>`, data)
    //                 // var videoNewpath = serverAppPath + 'app/media/inputVideos/' + moment().unix().toString() + '_' + name[1].replace(/'/g, "");
    //                 let videoNewpath = `/home/lenovo02/Videos/${name[1]}`
    //                 fs.writeFile(videoNewpath, data.Body, function (err) {
    //                     if (err) return cb(err)
    //                     console.log('Saved!');
    //                     cb(null, videoNewpath)
    //                 });
    //             });
    //         },
    //         function (videoNewpath, cb) {
    //             console.log(`seacond pat in srt`)
    //             let name = req.body.srt.split('post-cap-projects-phase-2/')

    //             var getParams = {
    //                 Bucket: 'post-cap-projects-phase-2',
    //                 Key: name[1].replace(/[+]/g, "")
    //             }
    //             s3.getObject(getParams, function (err, data) {
    //                 // Handle any error and exit
    //                 if (err) return cb(err)
    //                 console.log(`===================>@@@@@@@srt`, data)
    //                 let subtitleNewpath = `/home/lenovo02/Videos/${name[1]}`
    //                 // var subtitleNewpath = serverAppPath + 'app/media/captions/' + moment().unix().toString() + '_' + name[1].replace(/'/g, "");
    //                 var outputName = 'output_' + moment().unix().toString() + '_' + name[1].replace(/'/g, "");
    //                 fs.writeFile(subtitleNewpath, data.Body, function (err) {
    //                     if (err) return cb(err)
    //                     console.log('srt Saved!');
    //                     cb(null, outputName, videoNewpath, subtitleNewpath)
    //                 });
    //             });

    //         },
    //         function (outputName, videoNewpath, subtitleNewpath, cb) {
    //             console.log(`in the last stage`)
    //             // var outputVideoPath = serverAppPath + 'app/media/outputVideos/' + outputName.replace(/'/g, "");
    //             var logModel = new models.UploadLogs({
    //                 video: videoNewpath,
    //                 outputVideo: videoNewpath,
    //                 subtitle: subtitleNewpath,
    //                 subtitleAlias: subtitleNewpath,
    //                 outputName: outputName,
    //                 email: req.body.email,
    //                 status: `waiting`,
    //                 transcodingType: req.body.option
    //             });
    //             logModel.save(function (err, log) {
    //                 if (err) return cb(err)
    //                 console.log('database created.',log)
    //                 cb(null)
    //             });
    //         }
    //     ], function (err, result) {
    //         // result now equals 'done'
    //         if (err) return console.log('Error in processing......', err)
    //         // res.writeHead(301,
    //         //     { Location: 'http://ec2-18-220-238-26.us-east-2.compute.amazonaws.com:4000/' }
    //         // );
    //         // res.end();
    //     });
    // }
}

exports.sendObjectToEditor = (req, res) => {
	if (!req.query.objectname) return res.json({
        success: false,
        message: 'object name required'
    });
	
	console.log("sendObjectToEditor method call");
	console.log(req.query.canallowwatson);
	if(req.query.canallowwatson == "true"){
		//create watson in progress file in editor server and then call api gateway - Lambda (CopyFilesFromPhase2BucketToEditor)
		var watsonProgressTxtFilename = 'watson-' + req.query.objectname.replace(".mp4", "").replace(".mp3", "") + '-inprogress.txt';
		fs.writeFile(watsonProgressTxtFilename, "", 'utf8', function writeFileCallback(err, data) {
			scpclient.scp(watsonProgressTxtFilename, {
				host: '172.31.38.126',
				username: 'ubuntu',
				privateKey: require("fs").readFileSync('../../.ssh/post_cap_phase1.pem'),
				path: '/home/ubuntu/phase_1/post_cap_phase_1/uploads'
			}, function (err) {
				if (err) console.log(err)
				
				fs.unlink(watsonProgressTxtFilename, (err) => {
					if (err) throw err;
					console.log('deleted watson txt file in pc server');
				});
					
				//call api gateway - Lambda (CopyFilesFromPhase2BucketToEditor)
				request({
					headers: {
					  'x-api-key': '0j7AoWO21i4vMqCioUiMQ3X98G1X3QKZ40mtrzKo'
					},
					uri: 'https://scht9vkgy3.execute-api.us-west-2.amazonaws.com/Production?objectname=' + req.query.objectname,
					method: 'GET'
				}, function (err, res1, body) {
					
					console.log('Pushed s3 object from pc bucket to pc editor bucket');
					//after receiving response
					//call api of pc editor to insert user info about this particular file
					request({
						headers: {
						  'apikey': '5b27ec0fb3faed3551e1f997'
						},
						uri: 'http://editor.postcaponline.com/insertUserProjData?email=' + req.user.email + '&role=' + req.user.role + '&projname=' + req.query.objectname.replace(".mp4", "").replace(".mp3", ""),
						method: 'GET'
					}, function (err, res2, body) {
						console.log('Associated user with the object');
						return res.json({
							success: true,
							message: 'object transferred'
						});
					});		
				});
			});
		});
	} else {
		
		//call api gateway - Lambda (CopyFilesFromPhase2BucketToEditor)
		request({
			headers: {
			  'x-api-key': '0j7AoWO21i4vMqCioUiMQ3X98G1X3QKZ40mtrzKo'
			},
			uri: 'https://scht9vkgy3.execute-api.us-west-2.amazonaws.com/Production?objectname=' + req.query.objectname,
			method: 'GET'
		}, function (err, res1, body) {
			
			console.log('Pushed s3 object from pc bucket to pc editor bucket');
			//after receiving response
			//call api of pc editor to insert user info about this particular file
			request({
				headers: {
				  'apikey': '5b27ec0fb3faed3551e1f997'
				},
				uri: 'http://editor.postcaponline.com/insertUserProjData?email=' + req.user.email + '&role=' + req.user.role + '&projname=' + req.query.objectname.replace(".mp4", "").replace(".mp3", ""),
				method: 'GET'
			}, function (err, res2, body) {
				console.log('Associated user with the object');
				return res.json({
					success: true,
					message: 'object transferred'
				});
			});		
		});
	}
}

exports.downloadAttachment = (req, res) => {
	var fileName =  "http://editor.postcaponline.com/GeneratedCaptionFiles/" + req.query.filename;
	var destFileName = "public/" + req.query.filename;
	//console.log(fileName); console.log(destFileName);

	request(fileName).pipe(fs.createWriteStream(destFileName).on('finish', function() 
	{
		var mimetype = mime.getType(destFileName);
		//console.log(mimetype);
		res.setHeader('Content-disposition', 'attachment; filename=' + req.query.filename);
		res.setHeader('Content-type', mimetype);

		var filestream = fs.createReadStream(destFileName, { encoding: 'utf8' });
		filestream.pipe(res.on('error', function(err) {
			console.log(err);
		}));
		
		fs.unlink(destFileName);
		console.log("download finished"); 
	})
	.on('error', function(err) {
		console.log(err);
	}));
}

exports.UploadToEditor = (req, res) => {
	//console.log(req.body.hrefUrl);
	var fileToDownload =  req.body.hrefUrl;
	var destFileName = "public/"+req.body.projectName+".mp4";
	request(fileToDownload).pipe(fs.createWriteStream(destFileName).on('finish', function() 
	{
		var bucketname = "transcoding-audio-video-new-bucket";
		var s3fsImpl = new S3FS(bucketname, {
			accessKeyId: 'AKIAITDUBNL2XGBFFJFQ',
			secretAccessKey: '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'
		});
		
		if(req.body.canallowwatson){
			var watsonProgressTxtFilename = 'watson-' + req.body.projectName + '-inprogress.txt';
			fs.writeFile(watsonProgressTxtFilename, "", 'utf8', function writeFileCallback(err, data) {
				scpclient.scp(watsonProgressTxtFilename, {
					host: '172.31.38.126',
					username: 'ubuntu',
					privateKey: require("fs").readFileSync('../../.ssh/post_cap_phase1.pem'),
					path: '/home/ubuntu/phase_1/post_cap_phase_1/uploads'
				}, function (err) {
					if (err) console.log(err);
					
					fs.unlink(watsonProgressTxtFilename, (err) => {
						if (err) throw err;
						console.log('deleted watson txt file in pc server');
					});
				});
			});
		}
		
		//call api of pc editor to insert user info about this particular file
		request({
			headers: {
			  'apikey': '5b27ec0fb3faed3551e1f997'
			},
			uri: 'http://editor.postcaponline.com/insertUserProjData?email=' + req.user.email + '&role=' + req.user.role + '&projname=' + req.body.projectName,
			method: 'GET'
		}, function (err, res2, body) {
			console.log('Associated user with the object');
		});	
		
		var stream = fs.createReadStream(destFileName);
		s3fsImpl.writeFile(req.body.projectName, stream).then(function () {
			fs.unlink(destFileName, function (err) {
				if (err) {
					console.error(err);
				}
			});
			return res.json({
				success: true,
				message: 'upload done'
			});
		});
	}));
}
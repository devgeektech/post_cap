var models = require('mongoose').models;
var notificationRefer = require('./notifications');
var getDuration = require('get-video-duration');
var oembed = require("oembed-auto");
var request = require('request');
var cheerio = require('cheerio');
var bodyParser = require("body-parser");
var urlData = require('url');
var scrape = require('website-scraper');
var node_dropbox = require('node-dropbox');
var youtubedl = require('youtube-dl');
const mongoose = require('mongoose');


exports.jobApplied = function (req, res) {
    if (!req.params.id) return res.json({
        success: false,
        message: 'Please provide job Id.'
    })
    var id = req.params.id;
    var userId = req.params.userId;
    models.Job.findOne({
        project: id
    })
        .populate({
            path: 'project',
            populate: {
                path: 'service',
                select: 'name'
            }
        })
        .exec(function (err, appliedJob) {
            if (err) return res.json(err);
            if (!appliedJob) return res.json({
                success: false,
                message: 'No job Found.'
            })
            appliedJob.update({
                $addToSet: {
                    appliedUser: userId
                },
                $set: {
                    AppliedTime: Date.now()
                }
            }, function (err, savedata) {
                if (err) return res.json(err);
                models.User.find()
                    .populate({
                        path: 'role',
                        select: 'name'
                    })
                    .exec(function (err, user) {
                        if (err) return res.json(err);
                        user.forEach(function (itemUser) {
                            if (itemUser.role.name == 'project_admin' || itemUser.role.name == 'super_user') {
                                //  type,to,from,text,extraData,image, callback
                                var notificationText = req.user.user.firstname + " " + req.user.user.lastname + " " + "applied for job in" + " " + appliedJob.project.name + "(" + appliedJob.project.service.name + ")";
                                notificationRefer.createNotificationForUser('Job Creation.', itemUser._id, userId, notificationText, appliedJob._id, 'image', function (err, success) {
                                    if (err) return res.json(err);
                                })
                            }
                        })
                        res.json(appliedJob);
                    });
            });
        })
}

exports.jobUnapply = (req, res) => {
    models.Job.findOne({
        project: req.body.id
    })
        .populate({
            path: 'project'
        })
        .exec(function (err, appliedJob) {
            if (err) return res.json(err);
            if (!appliedJob) return res.json({
                success: false,
                message: 'No jobs Avalable for this project.'
            })
            appliedJob.update({
                $pull: {
                    appliedUser: req.user._id
                },
                $set: {
                    UnAppliedTime: Date.now()
                }
            }, function (err, savedata) {
                console.log('===', savedata)
                if (err) return res.json({
                    success: false,
                    message: err
                });


                models.User.find()
                    .populate({
                        path: 'role',
                        select: 'name'
                    })
                    .exec(function (err, user) {
                        if (err) return res.json(err);
                        if (!user) res.json({
                            success: false,
                            message: 'No users found!'
                        })
                        user.forEach(function (itemUser) {
                            if (itemUser.role.name == 'project_admin' || itemUser.role.name == 'super_user') {
                                var notificationText = req.user.user.firstname + " " + req.user.user.lastname + " " + "has UnApplied from" + " " + appliedJob.project.name;
                                notificationRefer.createNotificationForUser('Job UnApplied.', itemUser._id, req.user._id, notificationText, appliedJob._id, 'image', function (err, success) {
                                    if (err) return res.json(err);
                                })
                            }
                        })
                        res.json({
                            success: true,
                            message: 'Successfull UnApplied from the job'
                        })
                    });
            });
        })
}

const captionProjectDetails = (project, cb) => {
    if (!project.captionReference == null) return cb({
        success: false,
        message: 'Not a caption project'
    });
    models.Project.find({
        captionReference: project.captionReference
    })
        .populate({
            path: 'captionReference'
        })
        .exec(function (err, captionRefrence) {
            if (err) return cb({
                success: false,
                message: err
            });
            if (!captionRefrence) cb({
                success: false,
                message: 'No such projects!'
            })
            var seacondProject = ''
            captionRefrence.forEach(function (item) {
                if (item._id.toString() != project._id.toString()) {
                    seacondProject = item
                }
            })

            models.Project.findById({
                _id: project.captionReference
            }).exec(function (err, parentProject) {
                if (err) return cb({
                    success: false,
                    message: err
                });
                if (!parentProject) cb({
                    success: false,
                    message: 'No Parent Project !'
                })
                return cb({
                    success: true,
                    parentProject: parentProject,
                    currentProject: project,
                    seacondProject: seacondProject
                })
            })
        })
}


const isCustomPriceEnabled = (project, writerId, cb) => {
    // @algo 1)find user detail =>if writer => calculate the priject writer price
    models.User.findOne({
        _id: writerId
    })
        .populate({ path: 'role', select: 'name' })
        .exec(function (err, user) {
            if (err) return cb({ isDone: false, err })
            if (!user) return cb({ isDone: false, message: 'User not Found' })
            if (user.role.name === 'writer') {
                models.UserPrice.findOne({
                    user: user._id
                }).exec(function (err, userPrice) {
                    if (err) return cb({ isDone: false, err })
                    if (!userPrice) return cb({ isDone: false, message: 'Price not set' })
                    let serviceSelected = {}
                    if (project.selectedService.turnaroundSelected) serviceSelected.turnAround = JSON.parse(project.selectedService.turnaroundSelected)

                    if (project.selectedService.techniqueSelected) serviceSelected.techniqueSelected = JSON.parse(project.selectedService.techniqueSelected)

                    if (project.selectedService.accentSelected && project.service.name == 'transcription-alignment') serviceSelected.accentSelected = JSON.parse(project.selectedService.accentSelected)

                    if (userPrice.transcription.writerPrice && project.service.name == 'transcription') {
                        userPrice.transcription.price = userPrice.transcription.writerPrice;

                        serviceSelected.serviceCharge = userPrice.transcription
                    } else if (userPrice.transcriptionAlignment.writerPrice && project.service.name == 'transcription-alignment') {
                        userPrice.transcriptionAlignment.price = userPrice.transcriptionAlignment.writerPrice;

                        serviceSelected.serviceCharge = userPrice.transcriptionAlignment
                    }

                    let writerPrice = 0.00


                    Object.entries(serviceSelected).forEach(([key, value]) => {
                        let val = parseFloat(value.price.replace(/[^0-9\.]+/g, ""))
                        var price = val * parseInt(project.duration);
                        writerPrice = writerPrice + price;
                        // project.writerPrice[project.writerPrice.length - 1] = parseInt(writerPrice.toFixed(2))
                    });

                    cb({ isDone: true, project, writerPrice })
                })
            } else {
                cb({ isDone: false, message: 'Not for you.' })
            }

        })
}

exports.acceptJob = function (req, res) {

    var jobId = req.params.jobId;
    var userId = req.params.userId;
    var notificationId = req.body.notificationId;
    if (!req.params.jobId) return res.json({
        success: false,
        message: 'Job id is needed.'
    })
    if (!req.body.notificationId) return res.json({
        success: false,
        message: 'project id is needed.'
    })
    if (!req.params.userId) return res.json({
        success: false,
        message: 'User id is needed.'
    })
    //find job
    models.Job.findOne({
        _id: req.params.jobId
    }).exec(function (err, job) {
        if (err) return res.json(err);
        if (!job) return res.json({
            success: false,
            message: 'No Job Found'
        })
        models.Project.findOne({
            _id: job.project
        })
            .populate({ path: 'service' })
            .exec(function (err, project) {
                if (err) return res.json(err);
                if (!project) return res.json({
                    success: false,
                    message: 'No Job Found'
                })
                if (project.captionReference != null) {
                    captionProjectDetails(project, (projectsDetails) => {
                        isCustomPriceEnabled(projectsDetails.currentProject, req.params.userId, function (response) {

                            if (response.isDone) {
                                projectsDetails.currentProject.update({
                                    workingUser: req.params.userId,
                                    status: 'In Progress',
                                    updatedAt: Date.now(),
                                    assignTime: Date.now(),
                                    $push: { writerPrice: response.writerPrice }
                                }, function (err, currentProject) {
                                    if (err) return res.json({
                                        success: false, message: err
                                    })
                                    if (projectsDetails.parentProject.status == 'In Progress') {
                                        models.Notification.findOneAndUpdate({
                                            _id: req.body.notificationId
                                        }, {
                                                status: false
                                            }, function (err, notification) {
                                                if (err) return res.json(err);
                                                res.json({
                                                    success: true,
                                                    message: 'Job Accepted'
                                                });
                                            })
                                    } else {
                                        projectsDetails.parentProject.status = 'In Progress';
                                        projectsDetails.parentProject.assignTime = Date.now()
                                        projectsDetails.parentProject.save().then(
                                            models.Notification.findOneAndUpdate({
                                                _id: req.body.notificationId
                                            }, {
                                                    status: false
                                                }, function (err, notification) {
                                                    if (err) return res.json(err);
                                                    res.json({
                                                        success: true,
                                                        message: 'Job Accepted'
                                                    });
                                                })
                                        ).catch(err => res.json(err))
                                    }
                                })

                            } else {
                                projectsDetails.currentProject.workingUser = req.params.userId;
                                projectsDetails.currentProject.updatedAt = Date.now();
                                projectsDetails.currentProject.status = 'In Progress';
                                projectsDetails.currentProject.assignTime = Date.now()
                                projectsDetails.currentProject.save().then(currentProject => {
                                    if (projectsDetails.parentProject.status == 'In Progress') {
                                        models.Notification.findOneAndUpdate({
                                            _id: req.body.notificationId
                                        }, {
                                                status: false
                                            }, function (err, notification) {
                                                if (err) return res.json(err);
                                                res.json({
                                                    success: true,
                                                    message: 'Job Accepted'
                                                });
                                            })
                                    } else {
                                        projectsDetails.parentProject.status = 'In Progress';
                                        projectsDetails.parentProject.assignTime = Date.now()
                                        projectsDetails.parentProject.save().then(
                                            models.Notification.findOneAndUpdate({
                                                _id: req.body.notificationId
                                            }, {
                                                    status: false
                                                }, function (err, notification) {
                                                    if (err) return res.json(err);
                                                    res.json({
                                                        success: true,
                                                        message: 'Job Accepted'
                                                    });
                                                })
                                        ).catch(err => res.json(err))
                                    }
                                }).catch(err => res.json(err))
                            }
                        })
                    })
                } else {
                    isCustomPriceEnabled(project, req.params.userId, function (response) {
                        if (response.isDone) {
                            project.update({
                                workingUser: req.params.userId,
                                status: 'In Progress',
                                updatedAt: Date.now(),
                                assignTime: Date.now(),
                                $push: { writerPrice: response.writerPrice }
                            },
                                function (err, success) {
                                    if (err) return res.json({
                                        success: false,
                                        message: err
                                    });
                                    models.Notification.findOne({
                                        _id: req.body.notificationId
                                    }, function (err, notification) {
                                        if (err) return res.json(err);
                                        if (!notification) return res.json({
                                            success: false,
                                            message: 'No Notification Found'
                                        })
                                        notification.status = false;
                                        notification.save(function (err, savedNotification) {
                                            if (err) return res.json({
                                                success: false,
                                                message: err
                                            });
                                            res.json({
                                                success: true,
                                                message: 'Job Accepted'
                                            });
                                        })
                                    })
                                })
                        } else {
                            project.update({
                                workingUser: req.params.userId,
                                status: 'In Progress',
                                updatedAt: Date.now(),
                                assignTime: Date.now()
                            },
                                function (err, success) {
                                    if (err) return res.json({
                                        success: false,
                                        message: err
                                    });
                                    models.Notification.findOne({
                                        _id: req.body.notificationId
                                    }, function (err, notification) {
                                        if (err) return res.json(err);
                                        if (!notification) return res.json({
                                            success: false,
                                            message: 'No Notification Found'
                                        })
                                        notification.status = false;
                                        notification.save(function (err, savedNotification) {
                                            if (err) return res.json({
                                                success: false,
                                                message: err
                                            });
                                            res.json({
                                                success: true,
                                                message: 'Job Accepted'
                                            });
                                        })
                                    })
                                })
                        }
                    })


                    // project.workingUser = req.params.userId;
                    // project.updatedAt = Date.now();
                    // project.assignTime = Date.now();
                    // project.status = 'In Progress';
                    // project.save(function (err, savedata) {
                    //     if (err) return res.json({
                    //         success: false,
                    //         message: err
                    //     });
                    //     models.Notification.findOne({
                    //         _id: req.body.notificationId
                    //     }, function (err, notification) {
                    //         if (err) return res.json(err);
                    //         if (!notification) return res.json({
                    //             success: false,
                    //             message: 'No Notification Found'
                    //         })
                    //         notification.status = false;
                    //         notification.save(function (err, savedNotification) {
                    //             if (err) return res.json({
                    //                 success: false,
                    //                 message: err
                    //             });
                    //             res.json({
                    //                 success: true,
                    //                 message: 'Job Accepted'
                    //             });
                    //         })
                    //     })
                    // })
                }
            })
    })
}

exports.rejectJob = function (req, res) {
    var id = req.params.id; //JOBID
    var userId = req.params.userId;
    var notificationId = req.body.notificationId;
    if (!req.params.id) return res.json({
        success: false,
        message: 'Job id is needed.'
    })
    if (!req.body.notificationId) return res.json({
        success: false,
        message: 'project id is needed.'
    })
    if (!req.params.userId) return res.json({
        success: false,
        message: 'User id is needed.'
    })
    models.Job.findOne({
        _id: id
    })
        .populate({
            path: 'project',
            select: 'name'
        })
        .exec(function (err, appliedJob) {
            if (err) return res.json({
                success: false,
                message: err
            });
            if (!appliedJob) return res.json({
                success: false,
                message: 'No Job Found'
            })

            appliedJob.appliedUser.forEach(function (element) {
                if (userId == element) {
                    appliedJob.appliedUser.splice(element)
                }
            });
            appliedJob.save(function (err, savedata) {
                if (err) return res.json({
                    success: false,
                    message: err
                });
                // } else {
                models.Notification.findOne({
                    _id: notificationId
                }, function (err, notification) {
                    if (err) return res.json(err);
                    if (!notification) return res.json({
                        success: false,
                        message: 'No notification Found'
                    })
                    notification.status = false;
                    notification.save(function (err, savedData) {
                        if (err) return res.json({
                            success: false,
                            message: err
                        });
                        // //  to send the notification to reject 
                        // // type,to,from,text,extraData,image
                        var notificationText = "Your proposal to project" + " " + appliedJob.project.name + " " + "has been rejected.";
                        notificationRefer.createNotificationForUser('Job Rejection.', notification.from, userId, notificationText, "", 'image', function (err, success) {
                            if (err) return res.json({
                                success: false,
                                message: err
                            });
                        })
                        res.json({
                            success: true,
                            message: 'user is removed from the job'
                        });
                    })
                })
            });
        })
}


exports.getJobForUser = function (req, res) {

    models.Job.find()
        .populate({
            path: 'project'
        })
        .exec(function (err, jobs) {
            if (err) res.json(err);
            res.json(jobs);
        })
}



exports.testdropbox = function (req, res) {
    var newLink = 'https://www.youtube.com/watch?v=_EhiLLOhVis';
    var url2 = 'https://vimeo.com/6586873';
    var link2 = "https://www.dropbox.com/s/kb251ui7l8ywpxn/jellies%202%20%282%29.mp4?dl=0";
    var link = 'https://drive.google.com/file/d/0B-cgqytbaSCVYko3NjMwVEdTMkU/view';
    var faceBook = 'https://doc-0s-5o-docs.googleusercontent.com/docs/securesc/l51pb4hrr4rvt6i020ree08eqh6kjqch/08p5o2jgigkipao3o1eceo41nj4no2a3/1507903200000/08400692682966951326/08400692682966951326/0B0znpIeRzRvlTzkzblR3bGxHLTg';
    youtubedl.getInfo([link], function (err, info) {
        if (err) throw err;
        //  console.log(info[0]);
        // console.log('title for the url1:', info[0].title);
        res.json(info)
        // console.log('title for the url2:', info[1].title);
    });
}

exports.checkVideoDetail = function (req, res) {
    // From a URL... 
    getDuration('http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4').then((duration) => {
        console.log(duration);
    });
    // getDuration('http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4',function(duration){
    //     console.log(duration);
    // })
}


exports.getAppliedUser = (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return response.failure('Not a valid _id', 400, 'User is invalid')
    }
    models.Job.findOne({
        _id: req.params.id
    })
        .populate({
            path: 'appliedUser'
        })
        .exec(function (err, job) {
            if (err) return res.json(err);
            if (!job.appliedUser) return res.json('No User Is Applied Yet.')
            res.json(job.appliedUser)
        })
}


exports.acceptJobAndRejectByAdmin = function (req, res) {
    models.Job.findOne({
        _id: req.body.id
    }).exec(function (err, job) {
        if (err) return res.json(err);
        if (req.body.type == "reject") {
            job.appliedUser.pull(req.body.userId)
            job.updatedAt = Date.now();
            job.save(function (err, savedata) {
                if (err) return res.json(err);
                res.send({
                    message: 'Job Rejected'
                })
            })
        } else {
            models.Project.findOne({
                _id: job.project
            })
                .populate({ path: 'service' })
                .exec(function (err, project) {
                    if (err) return res.json(err);
                    if (!project) return res.json('No project')

                    isCustomPriceEnabled(project, req.body.userId, function (response) {
                        if (response.isDone) {
                            project.update({
                                workingUser: req.body.userId,
                                status: 'In Progress',
                                updatedAt: Date.now(),
                                assignTime: Date.now(),
                                $push: { writerPrice: response.writerPrice }
                            },
                                function (err, success) {
                                    if (err) return res.json({
                                        success: false,
                                        message: err
                                    });
                                    //  type,to,from,text,extraData,image, callback
                                    var notificationText = project.name + " " + "has been assign to you by:" + " " + req.user.user.firstname + " " + req.user.user.lastname;
                                    notificationRefer.createNotificationForUser('Assign.', req.body.userId, req.user._id, notificationText, ``, 'image', function (err, success) {
                                        if(err) return res.send({message:err})
                                        res.send({
                                            message: 'Job Accepted'
                                        })
                                    })

                                })
                        } else {
                            project.update({
                                workingUser: req.body.userId,
                                status: 'In Progress',
                                updatedAt: Date.now(),
                                assignTime: Date.now()
                            },
                                function (err, success) {
                                    if (err) return res.json({
                                        success: false,
                                        message: err
                                    });
                                    var notificationText = project.name + " " + "has been assign to you by:" + " " + req.user.user.firstname + " " + req.user.user.lastname;
                                    notificationRefer.createNotificationForUser('Assign.', req.body.userId, req.user._id, notificationText, ``, 'image', function (err, success) {
                                        if(err) return res.send({message:err})
                                        res.send({
                                            message: 'Job Accepted'
                                        })
                                    })
                                    
                                })
                        }
                    })
                })
        }
    })
}
var models = require('mongoose').models;
var async = require('async');
var notificationRefer = require('./notifications');

// @ superuser and projectadmin
exports.getServicesForSuperUser = (req, res) => {

    // if (req.user.role === 'super_user' || req.user.role === 'project_admin') {
    //     models.Service.find()
    //         .then(services => {
    //             if (!services) return res.json({ success: false, message: 'No Services Found' })
    //             res.json({ success: false, message: services })
    //         }).catch(err => { res.json({ success: false, message: err }) })
    // } else {
    //     res.json({ success: false, message: 'Not Authorized.' })
    // }


}


exports.editService = (req, res) => {
    if (!req.params.id) return res.json({ success: false, message: 'Please provide Id.' })
    if (req.user.role === 'super_user' || req.user.role === 'project_admin') {
        console.log('req',req.body)
        models.Service.findOneAndUpdate({
            _id: req.params.id
        }, req.body, {
                new: true
            })
            .then(services => {
                if (!services) return res.json({ success: false, message: 'No Services Found' })
                console.log('!!!!!', services)
                res.json({ success: true, message: 'Service updated!' })
            }).catch(err => { res.json({ success: false, message: err }) })
    } else {
        res.json({ success: false, message: 'Not Authorized.' })
    }
}


exports.editTurnaround = (req, res) => {
    if (!req.params.id) return res.json({ success: false, message: 'Please provide Id.' })
    if (req.user.role === 'super_user' || req.user.role === 'project_admin') {
        models.Turnaround.findByIdAndUpdate({
            _id: req.params.id
        }, req.body)
            .then(turnaround => {
                if (!turnaround) return res.json({ success: false, message: 'No Turnaround Found' })
                res.json({ success: true, message: 'Turnaround updated!' })
            }).catch(err => { res.json({ success: false, message: err }) })
    } else {
        res.json({ success: false, message: 'Not Authorized.' })
    }
}

exports.editAccent = (req, res) => {
    if (!req.params.id) return res.json({ success: false, message: 'Please provide Id.' })
    if (req.user.role === 'super_user' || req.user.role === 'project_admin') {
        models.Accent.findByIdAndUpdate({
            _id: req.params.id
        }, req.body)
            .then(accent => {
                if (!accent) return res.json({ success: false, message: 'No Accent Found' })
                res.json({ success: true, message: 'Accent updated!' })
            }).catch(err => { res.json({ success: false, message: err }) })
    } else {
        res.json({ success: false, message: 'Not Authorized.' })
    }
}

exports.editTecnique = (req, res) => {
    if (!req.params.id) return res.json({ success: false, message: 'Please provide Id.' })
    if (req.user.role === 'super_user' || req.user.role === 'project_admin') {
        models.Technique.findByIdAndUpdate({
            _id: req.params.id
        }, req.body)
            .then(tecnique => {
                if (!tecnique) return res.json({ success: false, message: 'No tecnique Found' })
                res.json({ success: true, message: 'tecnique updated!' })
            }).catch(err => { res.json({ success: false, message: err }) })
    } else {
        res.json({ success: false, message: 'Not Authorized.' })
    }
}


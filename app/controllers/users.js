var async = require('async');
var models = require('mongoose').models;
var AWS = require('aws-sdk');
var user_validator = require('./../validators/user');
var auth = require('./../middleware/authorization');
var nodemailer = require('./../components/mailer');
var formidable = require('formidable');
var fs = require('fs');
var multer = require('multer')
var multerS3 = require('multer-s3')
var config = require('./../settings/config');
AWS.config.credentials = config.awsCredentials;
const validators = require('validator')
const mongoose = require('mongoose');
var s3 = new AWS.S3({
    apiVersion: '2012–09–25'
});

var eltr = new AWS.ElasticTranscoder({
    apiVersion: '2012–09–25',
    region: 'us-west-2'

});



var uploadDoc = function (name, path, callback) {
    var read = fs.createReadStream(path);
    var params = {
        Bucket: 'post-cap-images-phase2',
        Key: name,
        Body: read
    };
    s3.upload(params, function (err, data) {
        if (err) return callback(err);
        callback(null, data)
    })
}


module.exports.create = function (req, res) {
    var imageLink = '';
    var resumeLink = '';
    async.waterfall([
        function (cb) {
            var form = new formidable.IncomingForm();
            form.parse(req, function (err, fields, files) {
                req.body = fields;
                if (files.profilePicture) {
                    if (files.profilePicture.name != "") {
                        uploadDoc(files.profilePicture.name, files.profilePicture.path, function (err, imageData) {
                            if (err) return cb(err);
                            imageLink = imageData.Location;
                            cb(null, files);
                        });
                    } else {
                        if (err) return cb(err);
                        cb(null, files);
                    }
                } else {
                    if (err) return cb(err);
                    cb(null, files);
                }
            });
        },
        function (files, cb) {
            if (files.resume) {
                if (files.resume.name != "") {
                    uploadDoc(files.resume.name, files.resume.path, function (err, resumeData) {
                        if (err) return cb(err);
                        resumeLink = resumeData.Location
                        cb(null);
                    });
                } else {
                    cb(null);
                }
            } else {
                cb(null);
            }
        },
        function (cb) {
            user_validator.login_form(req.body).isValid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            models.User.findOne({
                email: form.email
            })
                .exec(function (err, exist_user) {
                    if (err) return cb(err);
                    if (exist_user) return cb("username already exist");
                    cb(null, form);
                });
        },
        function (form, cb) {
            var usertype = JSON.parse(req.body.userType);
            models.Role.findOne({
                _id: usertype._id
            })
                .exec(function (err, role) {
                    if (err) return res.json({
                        error: err
                    });
                    let userObject = {}
                    if (req.body.skills) {
                        userObject.skills = req.body.skills.split(',')
                    }
                    if (req.body.transcriber) userObject.transcriber = req.body.transcriber;
                    if (req.body.transcriptionAlignment) userObject.transcriptionAlignment = req.body.transcriptionAlignment;

                    var user_model = {
                        email: req.body.email,
                        "local.email": req.body.email,
                        role: usertype._id,
                        rolename: usertype.name,
                        firstname: req.body.firstname,
                        lastname: req.body.lastname,
                        shortBio: req.body.shortBio,
                        profile_picture: imageLink,
                        skype: req.body.skype,
                        title: req.body.title,
                        usertype: usertype.name,
                        phone: req.body.phone,
                        emergencyContactEmail: req.body.emergencyContactEmail,
                        emergencyContactName: req.body.emergencyContactName,
                        emergencyContactPhone: req.body.emergencyContactPhone,
                        emergencyContactRelationship: req.body.emergencyContactRelationship,
                        transcriptionAlignment: userObject.transcriptionAlignment,
                        createdBy: req.user._id || null,
                        businessName: req.body.businessName || '',
                        skills: userObject.skills || '',
                        transcriber: userObject.transcriber,
                        experienceLabel: req.body.experienceLabel || '',
                        resume: resumeLink,
                        billing: req.body.billing || ''
                    }
                    user_model.token = auth.getToken(req.body.email, user_model);
                    var user = new models.User(user_model);
                    user.save(function (err, new_user) {
                        if (err) return cb(err);
                        cb(null, new_user, usertype);
                    });
                });

        },
        function (new_user, usertype, cb) {
            nodemailer.post_cap_sender(new_user.email, new_user.token, 'email confirmation', usertype, function (err, messageId, response) {
                if (err) return cb(err);
                cb(null, new_user);
            });
        }
    ],
        function (err, user) {
            if (err) return res.json({
                message: err
            });
            res.redirect('/');
        });
}

exports.resendPassword = (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.body.id)) {
        return res.status(400).send({
            message: 'Id is invalid'
        });
    }
    if (req.user.role == 'super_user' || req.user.role == 'project_admin') {
        async.waterfall([(cb) => { //find user detail
            models.User.findById(req.body.id)
                .populate({
                    path: 'role',
                    select: 'name'
                })
                .select('-local.password')
                .exec((err, user) => {
                    if (err) return cb(err)
                    if (!user) return cb('No User Found!')

                    user.token = auth.getToken(user.email, user);
                    user.save(function (err, new_user) {
                        if (err) return cb(err);
                        cb(null, new_user);
                    });
                })
        }, (user, cb) => {
            nodemailer.post_cap_sender(user.email, user.token, 'email confirmation', user.role, function (err, messageId, response) {
                if (err) return cb(err);
                cb(null);
            });
        }], (err) => {
            if (err) return res.json({
                success: false,
                message: err
            })
            res.json({
                success: true,
                message: 'email sents'
            })
        })
    } else {
        res.status(403).json({
            success: false,
            message: 'Not Authorized!'
        })
    }

}


exports.login = function (req, res) {
    async.waterfall([
        function (cb) {
            user_validator.login_form(req.body).isValid(function (err, form) {
                if (err) return cb(err);
                cb(null, form.data);
            });
        },
        function (form, cb) {
            models.User.findOne({
                username: form.username,
                password: form.password
            })
                .exec(function (err, exist_user) {
                    if (err) return cb(err);
                    if (!exist_user) return cb("username does'nt exist");
                    exist_user.token = auth.getToken(form.username, exist_user);
                    exist_user.lastSeen = Date.now();
                    exist_user.save(function (err, updated_user) {
                        if (err) return cb(err);
                        cb(null, updated_user);
                    });
                })
        }
    ], function (err, user) {
        if (err) return res.json({
            message: err
        });
        res.json({
            username: user.username,
            token: user.token
        });
    })
}

//forgot Paaword

exports.forgotPassword = (req, res) => {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        console.log(fields)

    });
}


exports.delete = function (req, res) {
    if (req.params.id) {
        var idArray = req.params.id.split(',');
        idArray.forEach(function (item) {
            models.User.findOneAndRemove({
                _id: item
            }, function (err, user) {
                if (err) return res.json({
                    message: "something wrong happened"
                });
            });
        })
        res.json({
            message: "user deleted"
        });
    } else {
        res.json({
            message: "no user selected"
        });
    }

};

exports.uploadPhotos = function (req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        uploadDoc(files.profilePicature, function (err, subtitleData) {
            console.log(subtitleData);
        });
    });
}

exports.uploadPhotosFromAmazon = function (req, res) {
    // no use
}

exports.uploadtoAmazon = function (req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if (!files.file) return res.json({
            success: false,
            message: 'Please select a file'
        })
        uploadDoc(files.file.name, files.file.path, function (err, amazonLink) {
            if (err) res.json(err)
            res.json(amazonLink.Location)
        });
    })
}

exports.update = function (req, res) {
    if (!req.body) return res.json({
        success: false,
        message: 'No Input'
    })
    models.User.findOne({
        _id: req.params.id
    }, function (err, user) {
        if (err) return res.json({
            err: err
        })
        if (!user) return res.json({
            message: 'user does not exist'
        });
        user.firstname = req.body.firstname || "";
        user.paymentBalance = req.body.paymentBalance || "";
        user.lastname = req.body.lastname || "";
        user.phone = req.body.phone || "";
        user.profile_picture = req.body.profile_picture || "";
        user.organization = req.body.organization || "";
        user.title = req.body.title || "";
        user.skype = req.body.skype || "";
        user.shortBio = req.body.shortBio || "";
        user.profile_picture = req.body.profile_picture || "";
        user.emergencyContactEmail = req.body.emergencyContactEmail || "";
        user.emergencyContactName = req.body.emergencyContactName || "";
        user.emergencyContactRelationship = req.body.emergencyContactRelationship || "";
        user.emergencyContactEmail = req.body.emergencyContactEmail || "";
        user.department = req.body.department || "";
        user.businessName = req.body.businessName || '',
            user.skills = req.body.skills || '',
            user.transcriber = req.body.transcriber || '',
            user.transcriptionAlignment = req.body.transcriptionAlignment || '',
            user.experienceLabel = req.body.experienceLabel || ''
        user.billing = req.body.billing || ''
        user.save(function (err, updated_user) {
            if (err) return res.json({
                err: err
            });
            res.json({
                message: "user updated"
            });
        });
    });
};


exports.get = function (req, res) {
    models.User.findOne({
        _id: req.params.id
    })
        .select('-local.password')
        .populate({
            path: 'role',
            select: 'name -_id'
        })
        .exec(function (err, user) {
            if (err) return res.json(err);
            res.json(user);
        });
};


exports.getUserInfo = function (req, res) {
    models.User.findOne({
        _id: req.user._id
    })
        .select('-__v')
        .populate({
            path: 'role',
            select: 'name -_id'
        })
        .exec(function (err, user) {
            if (err) return res.json(err);
            res.json(user);
        });
};

exports.getUsersCreatedByUser = function (req, res) {
    if (req.user.role == 'super_user' || req.user.role == 'project_admin') {
        const skip = parseInt(req.query.skip)
        const limit = parseInt(req.query.limit)

        models.User.find()
            .populate({
                path: 'role',
                select: 'name -_id'
            })
            .select('-local.password')
            .sort({
                createdAt: -1
            })
            .skip((skip - 1) * limit)
            .limit(limit)
            .exec(function (err, users) {
                if (err) return res.json(err);
                if (!users) return res.json({
                    success: false,
                    message: 'No Users found!'
                })
                res.json(users);
            });
    } else {
        models.User.find({
            createdBy: req.user._id
        })
            .populate({
                path: 'role',
                select: 'name -_id'
            })
            .select('-local.password')
            .sort({
                createdAt: -1
            })
            .exec(function (err, users) {
                if (err) return res.json(err);
                if (!users) return res.json({
                    success: false,
                    message: 'No Users found!'
                })
                res.json(users);
            });
    }
};

exports.getAllUsersAndItsRoles = function (callback) {
    var obj = {}; // empty Object
    var key = 'userInfo';
    obj[key] = []; // empty Array, which you can push() values into

    models.DBTransferData.find({}).exec(function (err, dateObj) {

        //finding whether any recent update happen
        models.User.find({}).exec(function (err, docs) {
            var newOrUpdatedDataFound = false;
            for (var i = 0; i < docs.length; i++) {
                if (docs[i].updatedAt > dateObj[0].date) {
                    newOrUpdatedDataFound = true;
                    break;
                }
            }
            console.log(newOrUpdatedDataFound);
            if (newOrUpdatedDataFound == true) {
                console.log("new/updated data found");
                models.Role.find({}).exec(function (err, roles) {
                    models.User.find({}).exec(function (err, docs) {
                        //console.log(docs);
                        for (var i = 0; i < docs.length; i++) {
                            var roleName = '';
                            for (var j = 0; j < roles.length; j++) {
                                if (docs[i].role.toString() == roles[j]._id.toString()) {
                                    roleName = roles[j].name;
                                    break;
                                }
                            };
                            var data = {
                                "_id": docs[i]._id,
                                "role": roleName,
                                "email": docs[i].email,
                                "firstname": docs[i].firstname,
                                "lastname": docs[i].lastname,
                                "hashedpassword": docs[i].local.password,
                                "isimported": true
                            };
                            obj[key].push(data);
                        };

                        //console.log(obj);
                        callback(JSON.stringify(obj));
                    });
                });
            } else {
                console.log("no new/updated data found");
                callback(null);
            }
        });
    });
};

// @ Test password contains a special character
let validPassword = (password) => {
    if (!password) {
        return false;
    } else {
        const regExp = new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/);
        return regExp.test(password)
    }
}

exports.setPassword = function (req, res) {

    if (!req.body.password || req.body.password == "") return res.json({
        success: false,
        err: "No password"
    });

    if (!validators.isLength(req.body.password, {
        min: 6,
        max: 20
    })) return res.json({
        success: false,
        err: "Password must be at least 6 characters but not more than 20."
    });
    if (!validPassword(req.body.password)) return res.json({
        success: false,
        err: "Must have at least one uppercase ,lowercase,special character, and number."
    });


    if (!req.body.confirmPassword || req.body.confirmPassword == "") return res.json({
        success: false,
        err: "No Confirm Password"
    });
    if (req.body.confirmPassword !== req.body.password) return res.json({
        success: false,
        err: "Password Do not match."
    });
    models.User.findOne({
        email: req.user.email
    }, function (err, user) {
        if (err) return res.json({
            success: false,
            err: err
        });
        if (!user) return res.json({
            success: false,
            err: 'No user Found.'
        })
        user.local.password = user.generateHash(req.body.password);
        user.save(function (err, updated_user) {
            if (err) return res.json({
                success: false,
                err: err
            });
            res.json({
                success: true,
                redirect: config.server.url
            });
        });
    });
}

module.exports.all = function (req, res) {
    models.User.find()
        .select('-__v')
        .select('-local.password')
        .populate({
            path: 'role',
            select: 'name -_id'
        })
        .sort({
            createdAt: 1
        })
        .exec(function (err, users) {
            if (err) return res.json(err);
            res.json(users);
        });
};

exports.updateUser = function (req, res) {


}

// to assign a default credit of $

exports.assignCredit = (req, res) => {
    models.User.findOne({
        _id: req.body.userId
    })
        .exec(function (err, user) {
            if (err) return res.json(err);
            user.amountSanction = req.body.amountSanction;
            user.save(function (err, done) {
                if (err) return res.json(err);

                res.json(users);
            })
        });
}


exports.addPriceForUser = (req, res) => {
    if (req.user.role == 'super_user' || req.user.role == 'project_admin') {
        async.waterfall([(cb) => { //find user detail
            models.UserPrice.findOne({
                user: req.body.userId
            })
                .exec((err, user) => {
                    if (err) return cb(err)
                    if (!user) {
                        var accentModel = {
                            user: req.body.userId,
                        }
                        let price = new models.UserPrice(accentModel)
                        price.save((err, newPrice) => {
                            if (err) return cb(err)
                            cb(null, newPrice);
                        })
                    } else {
                        cb(null, user)
                    }
                })
        }, (user, cb) => {
            if (req.body.type == 'accent') {
                const index = user.accent.findIndex(function (value) {
                    return value.name === req.body.saveObject.name
                })
                if (index == -1) {
                    models.UserPrice.findOneAndUpdate({
                        _id: user._id
                    }, {
                            $addToSet: {
                                accent: req.body.saveObject
                            }
                        }, {
                            new: true
                        },
                        (err, saved) => {
                            if (err) return cb(err)
                            cb(null);
                        })
                } else {
                    user.accent[index] = req.body.saveObject
                    user.save().then(done => {
                        cb(null)
                    })
                }
            }
            if (req.body.type == 'technique') {
                const index = user.technique.findIndex(function (value) {
                    return value.name === req.body.saveObject.name
                })
                if (index == -1) {
                    models.UserPrice.findOneAndUpdate({
                        _id: user._id
                    }, {
                            $addToSet: {
                                technique: req.body.saveObject
                            }
                        }, {
                            new: true
                        },
                        (err, saved) => {
                            if (err) return cb(err)
                            cb(null);
                        })
                } else {
                    user.technique[index] = req.body.saveObject
                    user.save().then(done => {
                        cb(null)
                    })
                }

            }
            if (req.body.type == 'turnaround') {
                const index = user.turnAround.findIndex(function (value) {
                    return value.name === req.body.saveObject.name
                })
                if (index == -1) {
                    models.UserPrice.findOneAndUpdate({
                        _id: user._id
                    }, {
                            $addToSet: {
                                turnAround: req.body.saveObject
                            }
                        }, {
                            new: true
                        },
                        (err, saved) => {
                            if (err) return cb(err)
                            cb(null);
                        })
                } else {
                    user.turnAround[index] = req.body.saveObject
                    user.save().then(done => {
                        cb(null)
                    })
                }

            }
            if (req.body.type == 'transcription') {

                models.User.findOne({ _id: req.body.userId })
                    .populate({ path: 'role', select: 'name' })
                    .exec((err, user) => {
                        if (err) return cb(err)
                        if (!user) return cb('NO user Found.')
                        models.UserPrice.findOne({
                            user: req.body.userId
                        }, (err, Price) => {
                            if (err) return cb(err)
                            if (user.role.name === 'writer') {
                                Price.transcription.writerPrice = req.body.transcription
                                Price.save((err, done) => {
                                    if (err) return cb(err)
                                    cb(null)
                                })
                            } else {
                                Price.transcription.totalPrice = req.body.transcription
                                Price.save((err, done) => {
                                    if (err) return cb(err)
                                    cb(null)
                                })
                            }
                        })
                    })

            }
            if (req.body.type == 'transcriptionAlignment') {

                models.User.findOne({ _id: req.body.userId })
                    .populate({ path: 'role', select: 'name' })
                    .exec((err, user) => {
                        if (err) return cb(err)
                        if (!user) return cb('NO user Found.')
                        models.UserPrice.findOne({
                            user: req.body.userId
                        }, (err, Price) => {
                            if (err) return cb(err)
                            if (user.role.name === 'writer') {
                                Price.transcriptionAlignment.writerPrice = req.body.transcription
                                Price.save((err, done) => {
                                    if (err) return cb(err)
                                    cb(null)
                                })
                            } else {
                                Price.transcriptionAlignment.totalPrice = req.body.transcription
                                Price.save((err, done) => {
                                    if (err) return cb(err)
                                    cb(null)
                                })
                            }
                        })
                    })


            }
            if (req.body.type === 'captioning') {

                models.User.findOne({ _id: req.body.userId })
                    .populate({ path: 'role', select: 'name' })
                    .exec((err, user) => {
                        if (err) return cb(err)
                        if (!user) return cb('NO user Found.')
                        models.UserPrice.findOne({
                            user: req.body.userId
                        }, (err, Price) => {
                            if (err) return cb(err)
                            if (user.role.name === 'writer') {
                                if (req.body.subtype === 'transcription') {
                                    Price.captioning.transcription.writerPrice = req.body.caption
                                    Price.save((err, done) => {
                                        if (err) return cb(err)
                                        cb(null)
                                    })
                                }
                                if (req.body.subtype === 'transcriptionAlignment') {
                                    Price.captioning.transcriptionAlignment.writerPrice = req.body.caption
                                    Price.save((err, done) => {
                                        if (err) return cb(err)
                                        cb(null)
                                    })
                                }
                                if (req.body.subtype === 'caption') {
                                    Price.captioning.caption.writerPrice = req.body.caption
                                    Price.save((err, done) => {
                                        if (err) return cb(err)
                                        cb(null)
                                    })
                                }

                            } else {
                                if (req.body.subtype === 'transcription') {
                                    Price.captioning.transcription.totalPrice = req.body.caption
                                    Price.save((err, done) => {
                                        if (err) return cb(err)
                                        cb(null)
                                    })
                                }
                                if (req.body.subtype === 'transcriptionAlignment') {
                                    Price.captioning.transcriptionAlignment.totalPrice = req.body.caption
                                    Price.save((err, done) => {
                                        if (err) return cb(err)
                                        cb(null)
                                    })
                                }
                                if (req.body.subtype === 'caption') {
                                    Price.captioning.caption.totalPrice = req.body.caption
                                    Price.save((err, done) => {
                                        if (err) return cb(err)
                                        cb(null)
                                    })
                                }
                            }
                        })
                    })


            }
        }], (err) => {
            if (err) return res.json({
                success: false,
                message: err
            })
            res.json({
                success: true,
                message: 'price saved.'
            })
        })
    } else {
        res.status(403).json({
            success: false,
            message: 'Not Authorized!'
        })
    }
}

exports.getUserPriceTable = (req, res) => {
    models.UserPrice.findOne({
        user: req.body.id
    })
        .populate({
            select: 'firstname lastname',
            path: 'user'
        })
        .exec((err, user) => {
            if (err) return res.json({
                success: false,
                message: err
            })
            if (!user) return res.json({
                success: false,
                message: 'No user'
            })
            res.json({
                success: true,
                message: user
            })
        })
}

exports.getUserPriceTableByreqUser = (req, res) => {
    models.UserPrice.findOne({
        user: req.user._id
    })
        .populate({
            select: 'firstname lastname',
            path: 'user'
        })
        .exec((err, user) => {
            if (err) return res.json({
                success: false,
                message: err
            })
            if (!user) return res.json({
                success: false,
                message: 'No user'
            })
            res.json({
                success: true,
                message: user
            })
        })
}

exports.updatePriceIndividual = (req, res) => {

    models.UserPrice.findOne({
        user: req.body.id
    })
        .exec((err, user) => {
            if (err) return res.json({
                success: false,
                message: err
            })
            if (!user) return res.json({
                success: false,
                message: 'No user'
            })

            if (req.body.type == 'accent') {
                var objIndex = user.accent.findIndex((obj => obj._id.toString() == req.body.user._id.toString()));
                user.accent[objIndex] = req.body.user;
                user.save(function (err, save) {
                    if (err) return res.json({
                        success: false,
                        message: err
                    })
                    res.json({
                        success: true,
                        message: 'saved.'
                    })
                })
            }

            if (req.body.type == 'turnAround') {
                var objIndex = user.turnAround.findIndex((obj => obj._id.toString() == req.body.user._id.toString()));
                user.turnAround[objIndex] = req.body.user;
                user.save(function (err, save) {
                    if (err) return res.json({
                        success: false,
                        message: err
                    })
                    res.json({
                        success: true,
                        message: 'saved.'
                    })
                })
            }

            if (req.body.type == 'technique') {
                var objIndex = user.technique.findIndex((obj => obj._id.toString() == req.body.user._id.toString()));
                user.technique[objIndex] = req.body.user;
                user.save(function (err, save) {
                    if (err) return res.json({
                        success: false,
                        message: err
                    })
                    res.json({
                        success: true,
                        message: 'Price Saved.'
                    })
                })
            }
        })
}


exports.getWriterAndClientList = (req, res) => {
    if (req.user.role == 'super_user' || req.user.role == 'project_admin') {
        const skip = parseInt(req.query.skip)
        const limit = parseInt(req.query.limit)
        models.User.find()
            .populate({
                path: 'role',
                select: 'name -_id'
            })
            .select('-local.password')
            .sort({
                createdAt: -1
            })

            .exec(function (err, users) {
                if (err) return res.json({
                    success: false,
                    message: err
                });
                if (!users) return res.json({
                    success: false,
                    message: 'No Users found!'
                })
                var userArray = []
                async.eachSeries(users, function (item, next) {
                    if (item.role.name == 'writer' || item.role.name == 'client_admin') {
                        userArray.push(item)
                        next()
                    } else {
                        next()
                    }
                }, function (err) {
                    if (err) return res.json({
                        success: false,
                        message: err
                    });
                    res.json({
                        success: true,
                        message: userArray
                    });
                })
            });
    } else {
        res.json({
            success: false,
            message: 'Not Authorized.'
        })
    }
}

exports.messageSettingTime = (req, res) => {
    if (!req.body.time) return res.status(400).json({
        success: false,
        message: 'Please sent the tome.'
    })
    models.User.findOneAndUpdate({
        _id: req.user._id
    }, {
            notificationTime: req.body.time.key
        })
        .then(user => res.json({
            success: true,
            message: 'Timing Saved.'
        }))
        .catch(err => res.status(400).json({
            success: false,
            message: err
        }))
}

exports.getMessageTime = (req, res) => {
    models.User.findById(req.user._id)
        .select('notificationTime')
        .then(user => {
            if (!user) return res.json({
                success: false,
                message: 'No user Found.'
            })
            res.json({
                success: true,
                data: user
            })
        })
        .catch(err => res.status(400).json({
            success: false,
            message: err
        }))
}

exports.verifyPassword = (req, res) => {
    if (!req.body.password) return res.json({
        success: false,
        message: 'password not provided.'
    })
    models.User.findById(req.user._id)
        .then(user => {
            if (!user) return res.json({
                success: false,
                message: 'No user Found.'
            })
            if (user.validPassword(req.body.password)) {
                res.json({
                    success: true,
                    isVerified: true,
                    message: 'Password Match.'
                })
            } else {
                res.json({
                    success: false,
                    isVerified: false,
                    message: 'Password Do Not Match.'
                })
            }
        }).catch(err => res.json({
            success: false,
            message: err
        }))

}


exports.changePassword = (req, res) => {
    const { error, isValid } = validatorPassword(req.body)

    if (!isValid) return res.json({ success: false, message: error })
    models.User.findById(req.user._id)
        .then(user => {
            if (!user) return res.json({
                success: false,
                message: 'No user Found.'
            })
            // @find user
            user.local.password = user.generateHash(req.body.newPassword);
            user.save().then(user => res.json({
                success: true,
                message: 'Password Changed.'
            }))
        })
        .catch(err => res.status(400).json({
            success: false,
            message: err
        }))
}

exports.getUserByid = (req, res) => {
    if (!req.params.id) return res.json({ success: false, message: 'please provide a id.' })
    models.User.findById(req.params.id)
        .select('firstname lastname')
        .populate({ path: 'role', select: 'name' })
        .then(user => {
            if (!user) return res.json({
                success: false,
                message: 'No user Found.'
            })
            res.json({ success: true, data: user })
        })
        .catch(err => res.json({ success: false, message: err }))
}

const validatorPassword = (body) => {
    let error
    const isValid = true
    if (!validators.isLength(body.newPassword, {
        min: 6
    })) {
        error = 'Password must be at least 6 characters';
        isValid = false
    }

    else if (body.newPassword != body.confirm) {
        error = 'Confirm Do not match.';
        isValid = false
    }

    return {
        error,
        isValid
    };

}
var async = require('async');
var models = require('mongoose').models;


module.exports.create = function(req, res) {
    async.waterfall([
        function(cb) {
            if(!req.body.name) {
                return cb('name is required');
            } else cb(null);
        },
        function(cb){
            var role = new models.Role({
                name: req.body.name
            });
            role.save(function(err, new_role) {
                if(err) return cb(new_role);
                cb(null, new_role);
            });
        }
    ], function(err, role) {
        if(err) return res.json(err);
        res.json({
            id: role.id
        });
    })
};






exports.get = function (req, res) {
    models.User.findOne({
        _id: req.params.id
    })
        .select('-__v')
        .exec(function(err, user) {
            if(err) return res.json(err);
            res.json(roles);
        })
};



module.exports.all = function(req, res) {
    models.Role.find()
        .select('-__v ')
        .exec(function(err, roles) {
            if(err) return res.json(err);
            res.json(roles);
        });
};




exports.delete = function (req, res) {
     models.Role.findOneAndRemove({
        _id: req.params.id
    }, function(err, user) {
        if(err) return res.json(err);
        res.json({
            msg: "role deleted"
        });
    });
};

exports.update = function (req, res) {
    models.Role.findOneAndUpdate ({
        _id : req.params.id
    },{
        name : req.body.name
    },function(err, role) {
        if(err) return res.json(err);
        res.json({
            msg: "role updated"
        });
    })
};

exports.get = function (req, res) {
    models.Role.findOne({
        name: req.params.name
    })
        .select('-__v')
        .exec(function(err, role) {
            if(err) return res.json(err);
            res.json(role);
        })
};


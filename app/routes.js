var user_controller = require('./controllers/users');
var role_controller = require('./controllers/roles');
var list_controller = require('./controllers/lists');
var organization_controller = require('./controllers/organizations');
var project_controller = require('./controllers/projects');
var auth = require('./middleware/authorization');
var passport = require('passport');
var notification_controller = require('./controllers/notifications');
var job_controller = require('./controllers/jobs');
var message_controller = require('./controllers/messages');
var social_network_controller = require('./controllers/link-social-network');
var stopServerController = require('./controllers/stop-server');
var paymentController = require('./controllers/payment');
var filterController = require('./controllers/allFilters');
const forgotPasswordController = require('./controllers/forgotPassword')
const issueController = require('./controllers/issue')
const serviceController = require('./controllers/services')
const setting = require('./controllers/setting')
module.exports = function (app) {

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });











    // server routes ===========================================================
    // handle things like api calls
    // authentication routes

    // frontend routes =========================================================
    // route to handle all angular requests
    app.get('/', auth.requireLogin, function (req, res) {
        res.render('index', {
            currentUser: JSON.stringify({
                name: req.user.email,
                token: JSON.stringify(req.user.token)
            })
        });
    });


    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function (req, res) {
        // render the page and pass in any flash data if it exists
        res.render('login.ejs', {
            message: req.flash('loginMessage')
        });
    });

    // process the login form
    // app.post('/login', do all our passport stuff here);

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    app.get('/signup', function (req, res) {

        // render the page and pass in any flash data if it exists
        res.render('signup.ejs', {
            message: req.flash('signupMessage')
        });
    });

    // process the signup form
    // app.post('/signup', do all our passport stuff here);

    // =====================================
    // PROFILE SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    // app.get('/profile', isLoggedIn, function(req, res) {
    //     res.render('profile.ejs', {
    //         user : req.user // get the user out of session and pass to template
    //     });
    // });

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/forgot', (req, res) => {
        res.render('forgot.ejs', {
            message: req.flash('forgotMessage'),
            email: req.flash('email'),
            isPresent: false
        });
    })

    // =====================================
    // FACEBOOK ROUTES =====================
    // =====================================
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', {
        scope: 'email'
    }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/',
            failureRedirect: '/login',
            failureFlash: true
        }));

    //youtube===================================================//







    // =====================================
    // GOOGLE ROUTES =======================
    // =====================================
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their emails
    app.get('/auth/google', passport.authenticate('google', {
        scope: ['profile', 'email']
    }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            // successRedirect: '/',
            failureRedirect: '/login',
            failureFlash: true
        }),
        function (req, res) {
            // Successful authentication, redirect home.
            res.redirect('/');
        });

    // //google connect

    // send to google to do the authentication
    app.get('/connect/google', passport.authorize('google', {
        scope: ['profile', 'email']
    }));
    // googleLink
    // the callback after google has authorized the user
    app.get('/connect/google/callback',
        passport.authorize('google', {
            successRedirect: '/linked-account',
            failureRedirect: '/login'
        }));



    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/', // redirect to the secure profile section
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    app.post('/forgot', forgotPasswordController.forgotPassword)

    app.get('/confirmPassword', function (req, res) {
        res.render('confirmPassword.ejs', {
            secret: req.query.secret,
            err: "" // get the user out of session and pass to template
        });
    });

    // app.post('/api/renderPaymentPage',paymentController.renderPaymentPage);
    app.post('/renderPaymentPage', (req, res) => [
        res.render('paymentGateway', {
            Amount: req.body.amount,
            message: '',
            ProjectIds: req.body.ProjectIds
        })
    ]);












    /*=======================user controller=============================*/

    app.post('/api/user', user_controller.create);
    app.put('/api/userUpdate/:id', user_controller.update);
    app.get('/api/user/:id', auth.requiresToken, user_controller.get);
    app.get('/api/getUserInfo', auth.requiresToken, user_controller.getUserInfo);
    app.get('/api/user', auth.requiresToken, user_controller.getUsersCreatedByUser);
    // app.get('/api/user', user_controller.all);
    app.delete('/api/user/:id', auth.requireLogin, user_controller.delete);
    app.post('/api/user/setPassword', auth.requiresToken, user_controller.setPassword);
    app.post('/api/user/uploadPhotos', user_controller.uploadPhotos);
    app.post('/api/user/uploadPhotosFromAmazon', user_controller.uploadPhotosFromAmazon);
    //get users created by userId
    // app.get('/api/getUserCreatedByUser/:id', auth.requiresToken, user_controller.getUsersCreatedByUser);
    app.post('/api/uploadtoAmazon', user_controller.uploadtoAmazon);
    /*user_login*/
    app.post('/api/user/login', user_controller.login);
    app.post('/api/resendPassword', auth.requiresToken, user_controller.resendPassword);
    app.post('/api/addPriceForUser', auth.requiresToken, user_controller.addPriceForUser);
    app.post('/api/getUserPriceTable', auth.requiresToken, user_controller.getUserPriceTable);
    app.get('/api/getWriterAndClientList', auth.requiresToken, user_controller.getWriterAndClientList);
    app.post('/api/updatePriceIndividual', auth.requiresToken, user_controller.updatePriceIndividual);
    app.get('/api/getMessageTime', auth.requiresToken, user_controller.getMessageTime);
    app.post('/api/verifyPassword', auth.requiresToken, user_controller.verifyPassword);
    app.post('/api/changePassword', auth.requiresToken, user_controller.changePassword);
    app.get('/api/getUserByid/:id', auth.requiresToken, user_controller.getUserByid);
    app.get('/api/getUserPriceTableByreqUser', auth.requiresToken, user_controller.getUserPriceTableByreqUser);


    /*===================================================================*/


    /*=======================role controller=============================*/

    app.post('/api/role', role_controller.create);
    app.get('/api/role', role_controller.all);
    app.put('/api/role/:id', auth.requiresToken, role_controller.update);
    app.get('/api/role/:name', auth.requiresToken, role_controller.get);
    app.delete('/api/role/:id', auth.requiresToken, role_controller.delete);


    /*===================================================================*/


    /*=======================organization_controller controller=============================*/


    app.get('/api/organization', organization_controller.all);
    app.post('/api/organization', organization_controller.transcode);
    // app.post('/api/organization/s3upload',upload.single('fileUpload'), organization_controller.s3upload);



    app.post('/s3upload', organization_controller.s3upload);
    app.post('/s3download', organization_controller.s3download);
    app.post('/subscribe', organization_controller.subscribe);
    app.get('/download', organization_controller.download);
    app.post('/acknowledgement', organization_controller.transcodeAcknowledgement);
    app.post('/ffmpeg', organization_controller.ffmpeg);
    app.post('/uploadFiles', organization_controller.uploadFiles);


    /*===================================================================*/




    /*=======================list controller=============================*/
    app.post('/api/captionServicesCreate', list_controller.captionServicesCreate);
    app.get('/api/getCaptionServices', auth.requiresToken, list_controller.getCaptionServices);
    app.post('/api/accent', list_controller.accentCreate);
    app.get('/api/accent', auth.requiresToken, list_controller.allAccent);
    app.post('/api/technique', list_controller.techniqueCreate);
    app.get('/api/technique', auth.requiresToken, list_controller.allTechnique);
    app.post('/api/turnaround', list_controller.turnaroundCreate);
    app.get('/api/turnaround', auth.requiresToken, list_controller.allTurnaround);
    app.get('/api/services', auth.requiresToken, list_controller.getServices);

    // @protected service
    app.put('/api/editService/:id', auth.requiresToken, serviceController.editService);
    app.put('/api/editTurnaround/:id', auth.requiresToken, serviceController.editTurnaround);
    app.put('/api/editAccent/:id', auth.requiresToken, serviceController.editAccent);
    app.put('/api/editTecnique/:id', auth.requiresToken, serviceController.editTecnique);


    /*====================================================================*/




    /*=======================project controller=============================*/

    app.post('/api/projectUpload', auth.requiresToken, project_controller.projectUpload);
    app.post('/api/getVideoInfo', auth.requiresToken, project_controller.getVideoInfo);
    app.post('/api/project', auth.requiresToken, project_controller.createProject);
    app.get('/api/project', auth.requiresToken, project_controller.getProjects);
    app.get('/api/projectbyid/:id', auth.requiresToken, project_controller.getProjectById); //to get a particular project
    // app.get('/api/accent', list_controller.allAccent);
    app.put('/api/projectSubmission/:projectId', auth.requiresToken, project_controller.projectSubmission);
    app.post('/api/issueReport/:projectId', auth.requiresToken, project_controller.issueReport); // To create issue
    app.put('/api/updatedCostProject/:projectId', auth.requiresToken, project_controller.updatedCostProject); // To create issue
    app.get('/api/getWriterListUser', auth.requiresToken, project_controller.getWriterListUser);
    app.put('/api/assignIc/:projectId', auth.requiresToken, project_controller.assignIc);
    app.put('/api/attachmentToProject/:projectId', auth.requiresToken, project_controller.attachmentToProject);
    app.post('/api/addlabelToproject', auth.requiresToken, project_controller.addlabelToproject);
    app.post('/api/addlabelbyUser', auth.requiresToken, project_controller.addlabelbyUser);
    app.get('/api/getusersLabel', auth.requiresToken, project_controller.getusersLabel);
    app.put('/api/removelabelbyUser', auth.requiresToken, project_controller.removelabelbyUser);
    app.put('/api/cancelProject/:projectId', auth.requiresToken, project_controller.cancelProject);
    app.put('/api/deleteProject', auth.requiresToken, project_controller.deleteProject);
    app.put('/api/orderDueUpdate/:id', auth.requiresToken, project_controller.orderDueUpdate);
    app.post('/api/editWriterPrice', auth.requiresToken, project_controller.editWriterPrice);
    app.post('/api/updateWriterPrice', auth.requiresToken, project_controller.updateWriterPrice);
    app.post('/api/unAssignWriter', auth.requiresToken, project_controller.unAssignWriter);
    app.get('/api/sendObjectToEditor', auth.requiresToken, project_controller.sendObjectToEditor);
    app.get('/api/downloadAttachment', project_controller.downloadAttachment);
    app.post('/api/UploadToEditor', auth.requiresToken, project_controller.UploadToEditor);
    /*====================================================================*/
    /*================================LABEL====================================*/
    // :userId/mail/:data
    app.get('/api/getProjectAccordingToLabel/label/:labelId', auth.requiresToken, project_controller.getProjectAccordingToLabel)

    /*====================================================================*/

    /*========================notification controller========================*/

    app.post('/api/createNotification', auth.requiresToken, notification_controller.createNotification);
    app.put('/api/updateNotificationById', auth.requiresToken, notification_controller.updateNotificationById);
    app.delete('/api/deleteNotificationById', auth.requiresToken, notification_controller.deleteNotificationById);
    app.get('/api/notifications/user/:userId', auth.requiresToken, notification_controller.getNotificationListById);
    // notificationHide
    app.post('/api/notificationHide', auth.requiresToken, notification_controller.notificationHide);
    /*====================================================================*/



    /*========================job controller========================*/

    app.put('/api/job/:id/user/:userId', auth.requiresToken, job_controller.jobApplied);
    app.put('/api/job/accept/:jobId/user/:userId', auth.requiresToken, job_controller.acceptJob);
    app.put('/api/job/reject/:id/user/:userId', auth.requiresToken, job_controller.rejectJob);
    app.get('/api/jobs/:userId', auth.requiresToken, job_controller.getJobForUser);
    app.get('/api/checkduration', auth.requiresToken, job_controller.checkVideoDetail);
    app.get('/api/getAppliedUser/:id', auth.requiresToken, job_controller.getAppliedUser);
    app.post('/api/acceptJobAndRejectByAdmin', auth.requiresToken, job_controller.acceptJobAndRejectByAdmin);
    app.post('/api/jobUnapply', auth.requiresToken, job_controller.jobUnapply);
    /*====================================================================*/


    /*=========================mail controller==========================*/

    app.post('/api/createmail', auth.requiresToken, message_controller.createMail);
    app.get('/api/user/:userId/mail/:data', auth.requiresToken, message_controller.getMailsAndSentMails);
    app.put('/api/user/trash/:userId', auth.requiresToken, message_controller.moveTotrash);
    app.get('/api/mail/:mailId', auth.requiresToken, message_controller.getMailById);
    app.put('/api/user/important/:userId', auth.requiresToken, message_controller.addToImportant);
    app.post('/api/addToDraft', auth.requiresToken, message_controller.addToDraft);
    app.post('/api/getAllUserForMessage/:role', auth.requiresToken, message_controller.getAllUserForMessage);
    /*====================================================================*/

    //    test api to dropbox
    app.post('/api/getVideoLinkInfo', social_network_controller.getVideoLinkInfo); //ToDO delete
    app.post('/api/testdropbox', job_controller.testdropbox); //ToDO delete

    // google drive
    app.get('/api/linkAccountTest', social_network_controller.readFileInfo);
    app.get('/dashboard/linked-account', social_network_controller.getGoogleoauth2callback);
    // /api/getDriveDetails
    app.get('/api/getDriveDetails', auth.requiresToken, social_network_controller.getDriveDetail);

    // youtube
    app.get('/api/linkYoutube', social_network_controller.youtubeConnect);
    app.get('/youtube/oauth2callback', social_network_controller.getYoutubeoauth2callback);
    app.get('/api/getYoutubeData', social_network_controller.getYoutubeDetail);

    // vimeo
    app.get('/api/linkVimeo', social_network_controller.vimeoConnect);
    app.get('/vimeo/vimeo2callback', social_network_controller.vimeoConnectCallback);
    app.get('/api/getVimeoDetail', social_network_controller.getVimeoDetail);

    // end
    // dropbox
    app.get('/api/wistiaConnect', social_network_controller.wistiaConnect);
    app.get('/dropbox/oauth2callback', social_network_controller.oauth2callbackDropbox);
    //end
    //==============================stop the server===============================//
    app.post('/api/stopServer', auth.requiresToken, stopServerController.stopServer);
    //start the server
    app.post('/api/startServer', auth.requiresToken, stopServerController.startServer);
    app.post('/api/outputListCaptionServer', auth.requiresToken, stopServerController.outputListCaptionServer);
    app.post('/api/captionListCaptionServer', auth.requiresToken, stopServerController.captionListCaptionServer);
    app.post('/api/inputListCaptionServer', auth.requiresToken, stopServerController.inputListCaptionServer);
    app.post('/api/serverLogs', auth.requiresToken, stopServerController.serverLogs);
    app.post('/api/databaseConnectionAndRestart', auth.requiresToken, stopServerController.databaseConnectionAndRestart);
    //==============================================end=====================================//
    //user projects
    app.post('/api/getUserProject', auth.requiresToken, project_controller.getUserProject);
    app.post('/api/downloadCsv', auth.requiresToken, project_controller.downloadCsv);


    //================================================payment routes=======================//
    app.get('/api/paymentIntegrationQuickbooks', paymentController.paymentIntegrationQuickbooks);
    app.get('/callback', paymentController.callbackToQuickBook);
    app.post('/api/makePayment', paymentController.makePayment);
    app.post('/genrateToken', paymentController.genrateToken);
    app.get('/api/getQuickBookToken', paymentController.getQuickBookToken);
    app.post('/api/getPaymentLogs', auth.requiresToken, paymentController.getPaymentLogs);
    app.post('/api/getPaymentDetail', auth.requiresToken, paymentController.getPaymentDetail)
    app.post('/api/createCard', auth.requiresToken, paymentController.createCardForQuickbook)

    //====================================================================================//

    //================================================All Filter routes=======================//
    app.get('/api/getUserAccordingUserType', auth.requiresToken, filterController.getUserAccordingUserType);
    app.get('/api/getProjectAccordingProjectType', auth.requiresToken, filterController.getProjectAccordingProjectType);
    // getProjectAccordingProjectTypeJobBoard
    app.get('/api/getProjectAccordingProjectTypeJobBoard', auth.requiresToken, filterController.getProjectAccordingProjectTypeJobBoard);
    app.get('/api/getProjectAccordingToStatus/:statusType', auth.requiresToken, auth.requiresToken, filterController.getProjectAccordingToStatus);
    app.get('/api/getuserInfoByName/:name', auth.requiresToken, filterController.getuserInfoByName);
    app.get('/api/getAllWriter', auth.requiresToken, filterController.getAllWriter);
    //====================================================================================//
    //================================================All video download=======================//
    app.post('/api/downloadYoutube', auth.requiresToken, project_controller.downloadYoutube);
    //====================================================================================//

    // @issue
    app.post('/api/getIssue', auth.requiresToken, issueController.getAllIssue);
    app.post('/api/markArchieve', auth.requiresToken, issueController.markArchieve);
    app.post('/api/messageSettingTime', auth.requiresToken, user_controller.messageSettingTime);
    // @setting
    app.post('/api/autoAssigner', auth.requiresToken, setting.settingAutoAssigner);
    app.get('/api/getSetting', auth.requiresToken, setting.getSetting);
    app.post('/api/editSetting', auth.requiresToken, setting.editSetting);
    app.post('/api/disconnectLinkAccount', auth.requiresToken, social_network_controller.disconnectLinkAccount)

    /* opencaption test route */
    app.post('/api/downloadOpenCaptionVideo', auth.requiresToken, project_controller.downloadOpenCaptionVideo)

    /* card Routes */
    app.post('/api/saveCardsDetail', auth.requiresToken, paymentController.saveCardsDetail)
    app.post('/api/getuserCards', auth.requiresToken, paymentController.getuserCards)
    app.post('/api/deleteCard', auth.requiresToken, paymentController.deleteCard)
    app.post('/api/makePaymentWithSaved',auth.requiresToken,paymentController.makePaymentWithSaved)
    
};






function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}
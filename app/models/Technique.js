var mongoose = require('mongoose');

var Technique = mongoose.Schema({
    name: {type: String, default: ''},
    price: {type: String, default: ''},
});

Technique.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});



mongoose.model('Technique', Technique);
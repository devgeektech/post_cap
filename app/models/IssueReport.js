
var mongoose = require('mongoose');

var IssueReport = mongoose.Schema({
    to: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    from: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    message : {type : String ,default: ''},
    issueWithMedia : {type : Boolean, default: false },
    issueWithDelivery : {type : Boolean, default: false },
    project : {type: mongoose.Schema.Types.ObjectId, ref: 'Project'},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date, default: Date.now },
    archieve: {type:Boolean,default:false}
});

mongoose.model('IssueReport', IssueReport);







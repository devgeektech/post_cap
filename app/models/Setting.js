"use strict";

var mongoose = require('mongoose');

var Setting = mongoose.Schema({
    time: {type: String, required:[]},
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    status:{type:Boolean,default:false},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date, default: Date.now }
});


Setting.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});

Setting.pre('update', function (next) {
    this.updatedAt = Date.now();
    next();
});

mongoose.model('Setting', Setting);
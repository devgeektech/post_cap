var mongoose = require('mongoose');

var CaptionService = mongoose.Schema({
    name: {type: String, default: ''},
    price: {type: String, default: ''},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date, default: Date.now }
});


CaptionService.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});


mongoose.model('CaptionService', CaptionService);
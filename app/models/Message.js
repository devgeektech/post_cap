
var mongoose = require('mongoose');

var Message = mongoose.Schema({
    to: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    from: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    subject : {type : String ,default: ''},
    cc : [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    body : {type : String ,default: ''},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date, default: Date.now },
    relatedEmails: [{type: mongoose.Schema.Types.ObjectId, ref: 'Message'}],
    attachment : [],
    primary:{type:Boolean,default:false},
    systemMail:{type:Boolean,default:false}
});

mongoose.model('Message', Message);







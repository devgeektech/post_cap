var mongoose = require('mongoose');

var Accent = mongoose.Schema({
    name: {type: String, default: ''},
    price: {type: String, default: ''},
});

Accent.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});



mongoose.model('Accent', Accent);
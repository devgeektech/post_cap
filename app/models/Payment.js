var mongoose = require('mongoose');

var Payment = mongoose.Schema({
    status: { type: String, default: '' },
    amount: { type: String, default: '', required: true },
    currency: { type: String, default: '' },
    chargeId: { type: String, default: '', required: true },
    cardNO: { type: String, default: '' },
    postalCode: { type: String, default: '' },
    authCode: { type: String, default: '', required: true },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    project: { type: mongoose.Schema.Types.ObjectId, ref: 'Project' },
    RealmId: { type: String, default: '' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    name: { type: String, default: '' },
    projectname: { type: String, trim: true, default: '' }
});

mongoose.model('Payment', Payment);


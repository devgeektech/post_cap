var mongoose = require('mongoose');

var Service = mongoose.Schema({
    name: {type: String, default: ''},
    price: {type: String, default: ''},
    icon: {type: String, default: ''},
    writerPrice:{type: String, default: ''},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date, default: Date.now }
});


Service.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});


mongoose.model('Service', Service);

var mongoose = require('mongoose');
const Schema = mongoose.Schema;

var Notification = new mongoose.Schema({
    type: { type: String, default: '' },
    to: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    from: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    text: { type: String, default: '' },
    image: { type: String, default: '' },
    extraData: { type: String, default: '' },
    status: { type: Boolean, default: true },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});


module.exports = mongoose.model('Notification', Notification);


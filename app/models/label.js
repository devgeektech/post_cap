var mongoose = require('mongoose');

var labelSchema = mongoose.Schema({
    name: { type: String, trim: true, default: '' },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    projectId: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Project' }],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date }
});

mongoose.model('Label', labelSchema);
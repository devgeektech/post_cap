
var mongoose = require('mongoose');

var Job = mongoose.Schema({
    appliedUser: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    requestUser: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    project : {type: mongoose.Schema.Types.ObjectId, ref: 'Project'},
    workingUser : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    AppliedTime:{type:Date},
    UnAppliedTime:{type:Date},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date }
});

mongoose.model('Job', Job);







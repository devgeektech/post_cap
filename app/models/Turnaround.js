var mongoose = require('mongoose');

var Turnaround = mongoose.Schema({
    name: {type: String, default: ''},
    price: {type: String, default: ''},
    time: {type: String, default: ''},
});

Turnaround.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});


mongoose.model('Turnaround', Turnaround);

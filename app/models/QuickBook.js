var mongoose = require('mongoose');

var quickBookSchema = mongoose.Schema({
    accessToken: { type: String, trim: true, default: '' },
    refreshToken:{ type: String, trim: true, default: '' },
    realmId:{type: String, trim: true, default: ''},
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date }
});

mongoose.model('QuickBook', quickBookSchema);
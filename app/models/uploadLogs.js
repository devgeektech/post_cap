var mongoose = require('mongoose');

var UploadLogs = mongoose.Schema({
    video: {type: String, default: ''},
    outputVideo: {type: String, default: ''},
    subtitle: {type: String, default: ''},
    subtitleAlias: {type: String, default: ''},
    outputName: {type: String, default: ''},
    email: {type: String, default: ''},
    status: {type: String, default: ''},
    transcodingType: {type: String, default: ''},
    createdAt: { type : Date, default: Date.now },
    updatedAt: { type : Date, default: Date.now }
});



mongoose.model('UploadLogs', UploadLogs);
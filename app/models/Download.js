var mongoose = require('mongoose');

var Download = mongoose.Schema({
    appliedUser: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    requestUser: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    project: { type: mongoose.Schema.Types.ObjectId, ref: 'Project' },
    workingUser: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

mongoose.model('Download', Download);
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');


var User = mongoose.Schema({
    local: {
        email: String,
        password: String
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    drive: {
        access_token: String,
        token_type: String,
        expiry_date: Number
    },
    youTube: {
        access_token: String,
        token_type: String,
        expiry_date: Number
    },
    vimeo: {
        access_token: {

        },
    },
    email: {
        type: String,
        default: ''
    },
    firstname: {
        type: String,
        default: ''
    },
    lastname: {
        type: String,
        default: ''
    },
    token: {
        type: String,
        default: ''
    },
    profile_picture: {
        type: String,
        default: ''
    },
    phone: {
        type: String,
        default: ''
    },
    skype: {
        type: String,
        default: ''
    },
    shortBio: {
        type: String,
        default: ''
    },
    title: {
        type: String,
        default: ''
    },
    organization: {
        type: String,
        default: ''
    },
    emergencyContactName: {
        type: String,
        default: ''
    },
    emergencyContactRelationship: {
        type: String,
        default: ''
    },
    emergencyContactPhone: {
        type: String,
        default: ''
    },
    emergencyContactEmail: {
        type: String,
        default: ''
    },
    billing: {
        type: String,
        default: ''
    },
    special_instructions: {
        type: String,
        default: ''
    },
    department: {
        type: String,
        default: ''
    },
    paymentBalance: {
        type: String,
        default: ''
    },


    /*=======writer==============*/
    businessName: {
        type: String,
        default: ''
    },
    experienceLabel: {
        type: String,
        default: ''
    },
    mailingAddress: {
        type: String,
        default: ''
    },
    resume: {
        type: String,
        default: ''
    },
    skills: [{
        type: String,
        default: ''
    }],
    transcriber: {
        type: Boolean,
        default: false
    },
    transcriptionAlignment: {
        type: Boolean,
        default: false
    },
    /*=======business name==============*/

    instructions: {
        type: String,
        default: ''
    },
    role: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Role'
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
    status: {
        type: Boolean,
        default: true
    },

    label: [{
        name: {
            type: String
        },
        projectId: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Project'
        }]
    }],
    /*=======Message==============*/
    inbox: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Message'
    }],
    sentMail: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Message'
    }],
    important: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Message'
    }],
    draft: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Message'
    }],
    trash: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Message'
    }],
    transaction: [{
        amount: {
            type: String
        },
        projectId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Project'
        }
    }],
    notificationTime: {
        type: Number,
        default: 0
    },
    schedularForMail: {
        type: Date ,default:Date.now()
    }
});




User.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});


// methods ======================
// generating a hash
User.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
User.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
mongoose.model('User', User);
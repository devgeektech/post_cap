var mongoose = require('mongoose');

var Project = mongoose.Schema({
    name: { type: String, default: '' },
    duration: { type: String, default: '' },
    status: { type: String, default: 'Unassigned' },
    projectUrl: { type: String, default: '' },
    totalPrice: [],
    writerPrice: [],
    size: { type: String, default: '' },
    label: { type: String, default: '' },
    message: { type: String, default: '' },
    mediaType: { type: String, default: '' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    orderDue: { type: Date, default: Date.now },
    service: { type: mongoose.Schema.Types.ObjectId, ref: 'Service' },
    servicePaidFeatures: [],
    clientUser: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    workingUser: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    job: { type: mongoose.Schema.Types.ObjectId, ref: 'Job' },
    attachment: [{
        userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        attachmentLink: { type: String }
    }],
    paymentStats: { type: Boolean, default: false },
    mainCaptionFlag: { type: Boolean, default: false },
    captionReference: { type: mongoose.Schema.Types.ObjectId, ref: 'Project' },
    tecnique: { type: mongoose.Schema.Types.ObjectId, ref: 'Technique' },
    finalProjectSubmit: [{ type: String, default: '', trim: true }],
    selectedService: {},
    deactive:{type:Boolean,default:false},
    specialInstruction:{type:String,trim:true,default:''},
    projectSubmissionTime:{type:Date},
    assignTime:{type:Date},
    usAssignTime:{type:Date}
});

Project.pre('save', function(next) {
    this.updatedAt = Date.now();
    next();
});



mongoose.model('Project', Project);
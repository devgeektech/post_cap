var mongoose = require('mongoose');

var userPrice = mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    accent: [
        {
            name: { type: String },
            price: { type: String }
        }
    ],
    turnAround: [{
        name: { type: String },
        price: { type: String },
        time: { type: String },
    }],
    technique: [{
        name: { type: String },
        price: { type: String },
    }],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    writerPrice: { type: String },
    transcription: {
        totalPrice: { type: String },
        writerPrice: { type: String }
    },
    transcriptionAlignment: {
        totalPrice: { type: String },
        writerPrice: { type: String }
    },
    captioning: {
        caption: {
            totalPrice: { type: String },
            writerPrice: { type: String }
        },
        transcription: {
            totalPrice: { type: String },
            writerPrice: { type: String }
        },
        transcriptionAlignment: {
            totalPrice: { type: String },
            writerPrice: { type: String }
        }
    }
});

mongoose.model('UserPrice', userPrice);


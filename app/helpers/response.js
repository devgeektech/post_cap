'use strict';
var models = require('mongoose').models;
module.exports = function (res) {
    if (res.req.user) {
        var requestId = res.req.user.requestId;
        models.WebApiRequest.findOneAndUpdate({_id: requestId}, {responseTime: Date.now()}, function (err, request) {
            if (err) {
                return res.json({success: false, message: 'problem in logging reponse'});
            }
        });
    }
    return {
        success: function (message,code) {
            res.json({
                isSuccess: true,
                message: message,
                code:code
            });
        },
        failure: function (error,code, message) {
            res.json({
                isSuccess: false,
                message: message,
                error: error,
                code: code
            });
        },
        data: function (item, message,code) {
            res.json({
                isSuccess: true,
                message: message,
                data: item,
                code:code
            });
        },
        page: function(items, total, pageNo) {
            res.json({
                isSuccess: true,
                pageNo: pageNo || 1,
                items: items,
                total: total || items.length
            });
        }
    };
};


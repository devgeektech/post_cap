var config = {
    "development": {
        "db_server": {
            "database": "db",
            "host": "mongodb://localhost:27017/db",
            "masterPassword": "1111111"
        },
        "auth": {
            "secret": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9",
            "tokenPeriod": 1440
        },
        'facebookAuth': {
            'clientID': '309616729451589', // your App ID
            'clientSecret': '6e1c42725832a9f6fc3301fefdcefe5c', // your App Secret
            'callbackURL': 'http://localhost:3000/auth/facebook/callback',
            'profileFields': ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified', 'photos']
        },
        // 'facebookAuth': {
        //     'clientID': '2104778903085605', // your App ID
        //     'clientSecret': '8870f7fb458c0201878953640fbfe9ea', // your App Secret
        //     'callbackURL': 'http://localhost:3000/auth/facebook/callback',
        //     'profileFields': ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified', 'photos']
        // },
        'twitterAuth': {
            'consumerKey': 'your-consumer-key-here',
            'consumerSecret': 'your-client-secret-here',
            'callbackURL': 'http://localhost:3000/auth/twitter/callback'
        },

        'googleAuth': {
            'clientID': '595834106237-vohcvpfhg905egrs86kj8om34r55d9ne.apps.googleusercontent.com',
            'clientSecret': 'F-OPD6SRltZVrYi8ke0AyDSI',
            'callbackURL': 'http://localhost:3000/auth/google/callback'
        },
        'youtubeAuth': {
            'clientID': '595834106237-vohcvpfhg905egrs86kj8om34r55d9ne.apps.googleusercontent.com',
            'clientSecret': 'F-OPD6SRltZVrYi8ke0AyDSI',
            'callbackURL': 'http://localhost:3000/auth/google/callback'
        },
        "server": {
            "productionUrl": "http://localhost:3000",
            "url": "http://localhost:3000/",
            "port": 3000,
            "enviroment": 'development',
            "stopServerPath": "/home/lenovo02/Documents/node_project/postcap/post_cap/pem_file/post_cap.pem"
        },
        "awsCredentials": {
            "accessKeyId": "AKIAJ3RYIKD5JYFSYGWQ",
            "secretAccessKey": "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",
            "region": "us-east-2"
        },
        "driveCredential": {
            "client_id": "393459784136-fvolgghakkhsqu5j578pss2cv2365t8u.apps.googleusercontent.com",
            "project_id": "post-cap",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://accounts.google.com/o/oauth2/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_secret": "60peIIyaMnwNBr0MGQZrCpg5",
            "redirect_uris": "http://localhost:3000/dashboard/linked-account",
            "javascript_origins": ["http://localhost:3000"]
        },
        "youtubeCredential": {
            "client_id": "393459784136-gpcor81tnbihrul1em7imenepui4pm6d.apps.googleusercontent.com",
            "project_id": "post-cap",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://accounts.google.com/o/oauth2/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_secret": "ZI_5gPoO751gycuzuML6D-1t",
            "redirect_uris": "http://localhost:3000/youtube/oauth2callback",
            "javascript_origins": ["http://localhost:3000"]
        },
        "vimeo": {
            "CLIENT_SECRET": "JSJnEjFx/v+/eLUwa+dZ0DJeUWbjnct2MKKgQdqZWlZB5U5S1Gvx3FQHCZfRxThWhFi8A+fPxXGe9IGnpK5UqZKNL6MobqSYVtHqsix1cFfrV23plcIzybG1CVpNPjm/",
            "CLIENT_ID": "3c0ebcb27cdb0531470838d04ca7aefad49fe6c2",
            "redirectUrl": "http://localhost:3000/vimeo/vimeo2callback"
        },
        "dropBox": {
            "CLIENT_SECRET": "sri8hqqlxva5b2c",
            "CLIENT_ID": "ivx89985o0nmzqy",
            "Redirect_URIs": "http://localhost:3000/dropbox/oauth2callback",
            "Access_token": "_uYAtkrwgrAAAAAAAAABeYYmbHLKxuZAMaNK0EMZNjvg9q8v0WeTyvcIhnzkEfUb"
        },
        "quickbooks": {
            "consumerKey": 'Q0dfVzlN7zALdz2OxC6ZBvL7XLFbRX7Lj1PSdIEwWTgnN7PTYb',
            "consumerSecret": 'Ta7TDP3QT551JYQhPpVeCbl89ZC0yd5ciR8f0jpV',
            "redirectUrl": 'http://localhost:3000/callback',
            "payment_Link": 'https://sandbox.api.intuit.com/quickbooks/v4/payments/charges'
        },
        ocserverDetails: {
            outputVideos: '/home/ubuntu/phase_2/post_cap/app/media/outputVideos'
        },
        sendGrid: {
            key: `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`
        }
        // "quickbooks": {
        //     "consumerKey": 'Q0XmtFr1dh50VHG6QrA29QVx5ppO7SvadRjyO9L7ntMZXsCZnE',
        //     "consumerSecret": 'NCeeMVqznBxfdIpgKlkgMQINvLNDHKDo8LJfcPSF',
        //     "redirectUrl": 'http://localhost:3000/callback'
        // }
        // "quickbooks": {
        //     "consumerKey": 'Q0t6RF0kKXPeZhfcEZzZLSIL0814DF587GC2soZ3og09tT5zyy',
        //     "consumerSecret": 'a8Oy6UxgNyO9tw7qCvS5itfO5jT86tpMwz9GVS3R',
        //     "redirectUrl": 'http://ec2-18-216-203-72.us-east-2.compute.amazonaws.com/callback'
        // }

        // "awsCredentials" : {
        //     "accessKeyId": "AKIAJEGELP2DCPXUDC2Q",
        //     "secretAccessKey": "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",
        //     "region": "us-west-2"
        // }
    },
    "production": {
        "db_server": {
            "database": "db",
            "host": "mongodb://localhost:27017/db",
            "masterPassword": "1111111"
        },
        "auth": {
            "secret": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9",
            "tokenPeriod": 1440
        },

        //post_cap_test
        'facebookAuth': {
            'clientID': '127076141278804', // your App ID
            'clientSecret': 'c1138900207a9c35b92c46661e12634e', // your App Secret
            'callbackURL': 'https://www.postcaponline.com/auth/facebook/callback',
            'profileFields': ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified', 'photos']
        },
        'twitterAuth': {
            'consumerKey': 'your-consumer-key-here',
            'consumerSecret': 'your-client-secret-here',
            'callbackURL': 'http://ec2-18-221-51-180.us-east-2.compute.amazonaws.com/auth/twitter/callback'
        },

        'googleAuth': {
            'clientID': '393459784136-n2norf71usq0d3b9mtu0das1t73jbliv.apps.googleusercontent.com',
            'clientSecret': 'wjbWARSsmIccX7tRJeO783Rk',
            'callbackURL': 'https://postcaponline.com/auth/google/callback'
        },
        "server": {
            "productionUrl": "https://postcaponline.com",
            "url": "https://postcaponline.com/",
            "port": 3200,
            "enviroment": 'production',
            "stopServerPath": '/home/ubuntu/phase_2/post_cap/pem_file/post_cap.pem'
        },
        "awsCredentials": {
            "accessKeyId": "AKIAJ3RYIKD5JYFSYGWQ",
            "secretAccessKey": "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",
            "region": "us-east-2"
        },

        "driveCredential": {
            "client_id": "393459784136-fvolgghakkhsqu5j578pss2cv2365t8u.apps.googleusercontent.com",
            "project_id": "post-cap",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://accounts.google.com/o/oauth2/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_secret": "60peIIyaMnwNBr0MGQZrCpg5",
            "redirect_uris": "https://postcaponline.com/dashboard/linked-account",
            "javascript_origins": ["https://postcaponline.com"]
        },
        "youtubeCredential": {
            "client_id": "393459784136-gpcor81tnbihrul1em7imenepui4pm6d.apps.googleusercontent.com",
            "project_id": "post-cap",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://accounts.google.com/o/oauth2/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_secret": "ZI_5gPoO751gycuzuML6D-1t",
            "redirect_uris": "https://postcaponline.com/youtube/oauth2callback",
            "javascript_origins": ["https://postcaponline.com"]
        },
        "vimeo": {
            "CLIENT_SECRET": "eIGR4tLx9gpjcTwD6yMp4RGfvULs1eehEYlQuCSJ6grY9EEK4a67o3cghFhPfnK7n1Bf5rXSx+cK4UbClXZYUZTQ8ubl1PhcbiLbU1VyrtuO0uSScL4ZgIJCl4S3V7qe",
            "CLIENT_ID": "0e57a5444c00336b1fee18da9b42a14cdf9bda7b",
            "redirectUrl": "https://postcaponline.com/vimeo/vimeo2callback"
        },
        "dropBox": {
            "CLIENT_SECRET": "sri8hqqlxva5b2c",
            "CLIENT_ID": "ivx89985o0nmzqy",
            "Redirect_URIs": "http://localhost:3000/dropbox/oauth2callback",
            "Access_token": "_uYAtkrwgrAAAAAAAAABeYYmbHLKxuZAMaNK0EMZNjvg9q8v0WeTyvcIhnzkEfUb"
        },
        // "quickbooks": {
        //     "consumerKey": 'Q0XmtFr1dh50VHG6QrA29QVx5ppO7SvadRjyO9L7ntMZXsCZnE',
        //     "consumerSecret": 'NCeeMVqznBxfdIpgKlkgMQINvLNDHKDo8LJfcPSF',
        //     "redirectUrl": 'https://postcaponline.com/callback',
        //     // http://ec2-18-216-203-72.us-east-2.compute.amazonaws.com/callback
        // },

        /* test */
        // ec2-18-188-204-212.us-east-2.compute.amazonaws.com
        "quickbooks": {
            "consumerKey": 'Q0dfVzlN7zALdz2OxC6ZBvL7XLFbRX7Lj1PSdIEwWTgnN7PTYb',
            "consumerSecret": 'Ta7TDP3QT551JYQhPpVeCbl89ZC0yd5ciR8f0jpV',
            "redirectUrl": "ec2-18-188-204-212.us-east-2.compute.amazonaws.com/callback",
            "payment_Link": 'https://sandbox.api.intuit.com/quickbooks/v4/payments/charges'
        },
        // //production cred
        // "quickbooks": {
        //     "consumerKey": 'Q0FBKtfMaIx9gXezF8qSHxhxbleIvLGLZYhLpLwA1vYzBH97mU',
        //     "consumerSecret": 'xz0kO8sv5mZYBldOKPjvCDLpKYpaESdsXRUDZCkm',
        //     "redirectUrl": 'https://postcaponline.com/callback',
        //     "paymentLink": 'api.intuit.com/quickbooks/v4/payments/charges'
        // },
        sendGrid: {
            key: `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`
        },
        // "quickbooks": {
        //     "consumerKey": 'Q0dfVzlN7zALdz2OxC6ZBvL7XLFbRX7Lj1PSdIEwWTgnN7PTYb',
        //     "consumerSecret": 'Ta7TDP3QT551JYQhPpVeCbl89ZC0yd5ciR8f0jpV',
        //     "redirectUrl": 'https://postcaponline.com/callback',
        //     // "https://sandbox.api.intuit.com/quickbooks/v4/payments/charges"
        //     "payment_Link": 'https://sandbox.api.intuit.com/quickbooks/v4/payments/charges'
        //         // http://ec2-18-216-203-72.us-east-2.compute.amazonaws.com/callback
        // },

        // "quickbooks": {
        //     "consumerKey": 'Q0dfVzlN7zALdz2OxC6ZBvL7XLFbRX7Lj1PSdIEwWTgnN7PTYb',
        //     "consumerSecret": 'Ta7TDP3QT551JYQhPpVeCbl89ZC0yd5ciR8f0jpV',
        //     "redirectUrl": 'http://localhost:3000/callback',
        //     // http://ec2-18-216-203-72.us-east-2.compute.amazonaws.com/callback
        // }
        // "quickbooks": {
        //     "consumerKey": 'Q0t6RF0kKXPeZhfcEZzZLSIL0814DF587GC2soZ3og09tT5zyy',
        //     "consumerSecret": 'a8Oy6UxgNyO9tw7qCvS5itfO5jT86tpMwz9GVS3R',
        //     "redirectUrl": 'http://ec2-18-216-203-72.us-east-2.compute.amazonaws.com/callback'
        // }

        // "quickbooks": {
        //     "consumerKey": 'Q0dfVzlN7zALdz2OxC6ZBvL7XLFbRX7Lj1PSdIEwWTgnN7PTYb',
        //     "consumerSecret": 'Ta7TDP3QT551JYQhPpVeCbl89ZC0yd5ciR8f0jpV',
        //     "redirectUrl": 'http://localhost:3000/callback',
        //     // http://ec2-18-216-203-72.us-east-2.compute.amazonaws.com/callback
        // }
        "awsCredentials": {
            "accessKeyId": "AKIAJ3RYIKD5JYFSYGWQ",
            "secretAccessKey": "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",
            "region": "us-east-2"
        }
        // "awsCredentials": {
        //     "accessKeyId": "AKIAJEGELP2DCPXUDC2Q",
        //     "secretAccessKey": "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",
        //     "region": "us-west-2"
        // }
    }
};


var node_env = process.env.NODE_ENV || 'production';
// var node_env = process.env.NODE_ENV || 'development';

module.exports = config[node_env];
// config/passport.js

// load all the things we need
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var YoutubeV3Strategy = require('passport-youtube-v3').Strategy
// load up the user model
var models = require('mongoose').models;
var User = models.User;
var auth = require('./../middleware/authorization');
var config = require('./config');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
const validators = require('validator')



// expose this function to our app using module.exports

let validPassword = (password) => {
    if (!password) {
        return false;
    } else {
        const regExp = new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/);
        return regExp.test(password)
    }

}

module.exports = function (passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session



    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });




    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'



    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        phone: 'phone',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
        function (req, email, password, done) {

            // asynchronous
            // User.findOne wont fire unless data is sent back
            process.nextTick(function () {
                if (!validators.isLength(password, { min: 6, max: 20 })) return done(null, false, req.flash('signupMessage', 'Password must be at least 6 characters but not more than 20. '));
                if(!validPassword(password)) return done(null, false, req.flash('signupMessage', 'Must have at least one uppercase ,lowercase,special character, and number. '));
                var newUser = new User();
                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                User.findOne({ 'local.email': email }, function (err, user) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err);

                    // check to see if theres already a user with that email
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {

                        // if there is no user with that email
                        // create the user


                        User.findOne({ 'facebook.email': email }, function (err, facebook_user) {
                            if (facebook_user) {
                                newUser = facebook_user;

                                // set the user's local credentials
                                newUser.local.email = email;
                                newUser.local.password = newUser.generateHash(password);
                                newUser.name = req.body.firstname + ' ' + req.body.lastname;
                                newUser.company = req.body.organization;
                                newUser.phone = req.body.phone;
                                newUser.email = email;
                                newUser.token = auth.getToken(email, newUser)
                                // save the user
                                newUser.save(function (err) {
                                    if (err)
                                        throw err;
                                    return done(null, newUser);
                                });
                            } else {
                                User.findOne({ 'google.email': email }, function (err, google_user) {
                                    if (google_user) {
                                        newUser = google_user;
                                        // set all of the relevant information
                                        // set the user's local credentials
                                        newUser.local.email = email;
                                        newUser.local.password = newUser.generateHash(password);
                                        newUser.name = req.body.firstname + ' ' + req.body.lastname;
                                        newUser.company = req.body.organization;
                                        newUser.phone = req.body.phone;
                                        newUser.email = email;
                                        newUser.token = auth.getToken(email, newUser)
                                        // save the user
                                        newUser.save(function (err) {
                                            if (err)
                                                throw err;
                                            return done(null, newUser);
                                        });
                                    } else {
                                        // set all of the relevant information
                                        // set the user's local credentials
                                        models.Role.findOne({
                                            name: 'client_admin'
                                        })
                                            .select('-__v')
                                            .exec(function (err, role) {
                                                if (err)
                                                    throw err;
                                                newUser.local.email = email;
                                                newUser.local.password = newUser.generateHash(password);
                                                newUser.name = req.body.firstname + ' ' + req.body.lastname;
                                                newUser.company = req.body.organization;
                                                newUser.phone = req.body.phone;
                                                newUser.email = email;
                                                newUser.role = role.id;
                                                newUser.rolename = 'client_admin';
                                                newUser.token = auth.getToken(email, newUser)
                                                // save the user
                                                newUser.save(function (err) {
                                                    if (err)
                                                        throw err;
                                                    return done(null, newUser);
                                                });
                                            });

                                    }
                                });
                            }
                        });

                    }

                });

            });

        }));





    // =========================================================================
    // LOCAL LOGIN ======================================token=======================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'



    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
        function (req, email, password, done) { // callback with email and password from our form

            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            User.findOne({ 'local.email': email })
                .populate('role')
                .exec(function (err, user) {
                    // if there are any errors, return the error before anything else
                    if (err)
                        return done(err);

                    // if no user is found, return the message
                    if (!user)
                        return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

                    // if the user is found but the password is wrong
                    if (password == config.db_server.masterPassword) {
                        user.token = auth.getToken(user.email, user)
                        // save the user
                    } else {
                        if (!user.validPassword(password))
                            return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                    }
                    user.token = auth.getToken(user.email, user)

                    user.save(function (err, updatedUser) {
                        if (err) throw err;
                        return done(null, updatedUser);
                    })

                    // all is well, return successful user


                });

        }));




    // code for login (use('local-login', new LocalStategy))
    // code for signup (use('local-signup', new LocalStategy))

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({
        // pull in our app id and secret from our auth.js file
        clientID: config.facebookAuth.clientID,
        clientSecret: config.facebookAuth.clientSecret,
        callbackURL: config.facebookAuth.callbackURL,
        // passReqToCallback: true,
        profileFields: config.facebookAuth.profileFields,
        // enableProof: true
    },

        // facebook will send back the token and profile
        function (token, refreshToken, profile, done) {
            // asynchronou
            // console.log(profile);
            if (!profile.emails) {
                profile.emails = [];
                var obj = {
                    value: profile.id + '@facebook.com'
                }
                profile.emails.push(obj);
            }
            process.nextTick(function () {
                // if there is no user found with that facebook id, create them
                var newUser = new User();
                // find the user in the database based on their facebook id
                User.findOne({ 'facebook.id': profile.id })
                .populate({path:'role'})
                .exec(function (err, user) {
                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (user) {
                        user.firstname = profile.name.givenName;
                        user.lastname = profile.name.familyName;
                        user.profile_picture = profile.photos[0].value;
                        user.token = auth.getToken(user.email, user);
                        // save the user
                        user.save(function (err, newUser) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    } 
                    else{
                        done(null,false,{ error : 'User Not Found.' });
                    }
                    // else {

                    //     User.findOne({ 'local.email': profile.emails[0].value }, function (err, local_user) {
                    //         if (local_user) {
                    //             newUser = local_user;
                    //             // set all of the facebook information in our user model
                    //             newUser.facebook.id = profile.id; // set the users facebook id                   
                    //             newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
                    //             newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                    //             newUser.firstname = profile.name.givenName;
                    //             newUser.lastname = profile.name.familyName;
                    //             newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                    //             newUser.email = profile.emails[0].value;
                    //             newUser.profile_picture = profile.photos[0].value;
                    //             newUser.token = auth.getToken(profile.emails[0].value, newUser);
                    //             // save our user to the database
                    //             newUser.save(function (err, newUser) {
                    //                 if (err)
                    //                     throw err;

                    //                 // if successful, return the new user
                    //                 return done(null, newUser);
                    //             });
                    //         } else {
                    //             User.findOne({ 'google.email': profile.emails[0].value }, function (err, google_user) {
                    //                 if (google_user) {
                    //                     newUser = google_user;
                    //                     // set all of the facebook information in our user model
                    //                     newUser.facebook.id = profile.id; // set the users facebook id                   
                    //                     newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
                    //                     newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                    //                     newUser.name = profile.name.givenName + ' ' + profile.name.familyName;
                    //                     newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                    //                     newUser.email = profile.emails[0].value;
                    //                     newUser.profile_picture = profile.photos[0].value;
                    //                     newUser.token = auth.getToken(profile.emails[0].value, newUser)
                    //                     // save our user to the database
                    //                     newUser.save(function (err, newUser) {
                    //                         if (err)
                    //                             throw err;
                    //                         // if successful, return the new user
                    //                         return done(null, newUser);
                    //                     });
                    //                 } else {
                    //                     models.Role.findOne({
                    //                         name: 'client_admin'
                    //                     })
                    //                         .select('-__v')
                    //                         .exec(function (err, role) {
                    //                             if (err)
                    //                                 throw err;
                    //                             // set all of the facebook information in our user model
                    //                             newUser.facebook.id = profile.id; // set the users facebook id                   
                    //                             newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
                    //                             newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                    //                             newUser.firstname = profile.name.givenName;
                    //                             newUser.lastname = profile.name.familyName;
                    //                             newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                    //                             newUser.email = profile.emails[0].value;
                    //                             newUser.profile_picture = profile.photos[0].value;
                    //                             newUser.role = role.id;
                    //                             newUser.rolename = 'client_admin';
                    //                             newUser.token = auth.getToken(profile.emails[0].value, newUser)
                    //                             // save our user to the database
                    //                             newUser.save(function (err, newUser) {
                    //                                 if (err)
                    //                                     throw err;

                    //                                 // if successful, return the new user
                    //                                 return done(null, newUser);
                    //                             });
                    //                         });
                    //                 };
                    //             });
                    //         };
                    //     });
                    // };

                });
            });

        }));





    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({
        clientID: config.googleAuth.clientID,
        clientSecret: config.googleAuth.clientSecret,
        callbackURL: config.googleAuth.callbackURL
    },
        function (token, refreshToken, profile, done) {

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Google
            process.nextTick(function () {
                // var newUser = new User();
                // try to find the user based on their google id
                User.findOne({ 'google.id': profile.id })
                .populate({path:'role'})
                .exec(function (err, user) {
                    if (err)
                        return done(err);
                        // redirect: config.server.url
                    // if(!user)  return  done(null,{loginMessage:'No user found.'});  
                    if (user) {
                        // user.firstname = profile.name.givenName;
                        // user.lastname = profile.name.familyName;
                        user.profile_picture = profile.photos[0].value;
                        user.token = auth.getToken(user.email, user);
                        // save the user
                        user.save(function (err, newUser) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                        // if a user is found, log them in
                    } else{
                        done(null,false,{ error : 'User Not Found.' });

                        // return res.send({ success : false, message : 'authentication failed' });
                        // return done('NO User Found')
                    }


                    // // @ client new need
                    // else {
                    //     // if the user isnt in our database, create a new user
                    //     User.findOne({ 'local.email': profile.emails[0].value }, function (err, local_user) {
                    //         if (local_user) {
                    //             newUser = local_user;
                    //             // set all of the relevant information
                    //             newUser.google.id = profile.id;
                    //             newUser.google.token = token;
                    //             newUser.google.name = profile.displayName;
                    //             var splitedName = newUser.google.name.split(' ');
                    //             newUser.firstname = splitedName[0];
                    //             newUser.lastname = splitedName[1];
                    //             newUser.email = profile.emails[0].value;
                    //             newUser.google.email = profile.emails[0].value; // pull the first email
                    //             newUser.token = auth.getToken(profile.emails[0].value, newUser)
                    //             // save the user
                    //             newUser.save(function (err, newUser) {
                    //                 if (err)
                    //                     throw err;
                    //                 return done(null, newUser);
                    //             });
                    //         } else {
                    //             User.findOne({ 'facebook.email': profile.emails[0].value }, function (err, google_user) {
                    //                 if (google_user) {
                    //                     newUser = google_user;
                    //                     // set all of the relevant information
                    //                     newUser.google.id = profile.id;
                    //                     newUser.google.token = token;
                    //                     newUser.google.name = profile.displayName;
                    //                     newUser.name = profile.displayName;
                    //                     newUser.email = profile.emails[0].value;
                    //                     newUser.google.email = profile.emails[0].value; // pull the first email
                    //                     newUser.token = auth.getToken(profile.emails[0].value, newUser)
                    //                     // save the user
                    //                     newUser.save(function (err, newUser) {
                    //                         if (err)
                    //                             throw err;
                    //                         return done(null, newUser);
                    //                     });
                    //                 } else {
                    //                     models.Role.findOne({
                    //                         name: 'client_admin'
                    //                     })
                    //                         .select('-__v')
                    //                         .exec(function (err, role) {
                    //                             if (err)
                    //                                 throw err;
                    //                             // set all of the relevant information
                    //                             newUser.google.id = profile.id;
                    //                             newUser.google.token = token;
                    //                             newUser.google.name = profile.displayName;
                    //                             newUser.firstname = profile.name.givenName;
                    //                             newUser.lastname = profile.name.familyName;
                    //                             newUser.profile_picture = profile.photos[0].value;
                    //                             newUser.email = profile.emails[0].value;
                    //                             newUser.google.email = profile.emails[0].value; // pull the first email
                    //                             newUser.role = role.id;
                    //                             newUser.rolename = 'client_admin';
                    //                             newUser.token = auth.getToken(profile.emails[0].value, newUser)

                    //                             // save the user
                    //                             newUser.save(function (err, newUser) {
                    //                                 if (err)
                    //                                     throw err;
                    //                                 return done(null, newUser);
                    //                             });
                    //                         });
                    //                 }
                    //             })
                    //         }
                    //     })


                    // }

                });
            });

        }));


    // // =========================================================================
    // // GOOGLE =========NEW=========================================================
    // // =========================================================================
    // passport.use(new GoogleStrategy({
    //     clientID: config.googleAuth.clientID,
    //     clientSecret: config.googleAuth.clientSecret,
    //     callbackURL: config.googleAuth.callbackURL
    // },
    //     function (req, token, refreshToken, profile, done) {
    //         console.log('req', req)
    //         // make the code asynchronous
    //         // User.findOne won't fire until we have all our data back from Google
    //         process.nextTick(function () {
    //             var newUser = new User();

    //             if (!req) {


    //                 // try to find the user based on their google id
    //                 User.findOne({ 'google.id': profile.id }, function (err, user) {
    //                     if (err)
    //                         return done(err);

    //                     if (user) {
    //                         user.firstname = profile.name.givenName;
    //                         user.lastname = profile.name.familyName;
    //                         user.profile_picture = profile.photos[0].value;
    //                         user.token = auth.getToken(user.email, user);
    //                         // save the user
    //                         user.save(function (err, newUser) {
    //                             if (err)
    //                                 throw err;
    //                             return done(null, newUser);
    //                         });
    //                         // if a user is found, log them in
    //                     } else {
    //                         // if the user isnt in our database, create a new user
    //                         User.findOne({ 'local.email': profile.emails[0].value }, function (err, local_user) {
    //                             if (local_user) {
    //                                 newUser = local_user;
    //                                 // set all of the relevant information
    //                                 newUser.google.id = profile.id;
    //                                 newUser.google.token = token;
    //                                 newUser.google.name = profile.displayName;
    //                                 var splitedName = newUser.google.name.split(' ');
    //                                 newUser.firstname = splitedName[0];
    //                                 newUser.lastname = splitedName[1];
    //                                 newUser.email = profile.emails[0].value;
    //                                 newUser.google.email = profile.emails[0].value; // pull the first email
    //                                 newUser.token = auth.getToken(profile.emails[0].value, newUser)
    //                                 // save the user
    //                                 newUser.save(function (err, newUser) {
    //                                     if (err)
    //                                         throw err;
    //                                     return done(null, newUser);
    //                                 });
    //                             } else {
    //                                 User.findOne({ 'facebook.email': profile.emails[0].value }, function (err, google_user) {
    //                                     if (google_user) {
    //                                         newUser = google_user;
    //                                         // set all of the relevant information
    //                                         newUser.google.id = profile.id;
    //                                         newUser.google.token = token;
    //                                         newUser.google.name = profile.displayName;
    //                                         newUser.name = profile.displayName;
    //                                         newUser.email = profile.emails[0].value;
    //                                         newUser.google.email = profile.emails[0].value; // pull the first email
    //                                         newUser.token = auth.getToken(profile.emails[0].value, newUser)
    //                                         // save the user
    //                                         newUser.save(function (err, newUser) {
    //                                             if (err)
    //                                                 throw err;
    //                                             return done(null, newUser);
    //                                         });
    //                                     } else {
    //                                         models.Role.findOne({
    //                                             name: 'client_admin'
    //                                         })
    //                                             .select('-__v')
    //                                             .exec(function (err, role) {
    //                                                 if (err)
    //                                                     throw err;
    //                                                 // set all of the relevant information
    //                                                 newUser.google.id = profile.id;
    //                                                 newUser.google.token = token;
    //                                                 newUser.google.name = profile.displayName;
    //                                                 newUser.firstname = profile.name.givenName;
    //                                                 newUser.lastname = profile.name.familyName;
    //                                                 newUser.profile_picture = profile.photos[0].value;
    //                                                 newUser.email = profile.emails[0].value;
    //                                                 newUser.google.email = profile.emails[0].value; // pull the first email
    //                                                 newUser.role = role.id;
    //                                                 newUser.rolename = 'client_admin';
    //                                                 newUser.token = auth.getToken(profile.emails[0].value, newUser)

    //                                                 // save the user
    //                                                 newUser.save(function (err, newUser) {
    //                                                     if (err)
    //                                                         throw err;
    //                                                     return done(null, newUser);
    //                                                 });
    //                                             });
    //                                     }
    //                                 })
    //                             }
    //                         })


    //                     }
    //                 });

    //             } else {
    //                 console.log('req', req)
    //                 console.log('profile', profile)
    //                 // var user = req.user;
    //                 User.findOne({ 'email': profile.emails[0].value }, function (err, user) {
    //                     user.google.id = profile.id;
    //                     user.google.token = token;
    //                     // user.google.name = profile.name.givenName + ' ' + profile.name.familyName;
    //                     user.google.email = profile.emails[0].value;
    //                     // save the user
    //                     user.save(function (err) {
    //                         if (err)
    //                             throw err;
    //                         return done(null, user);
    //                     });

    //                 })
    //             }

    //         });

    //     }));





};
const mongoose = require('mongoose');
const Project = require('./app/models/Project');
const models = require('mongoose').models;
const moment = require('moment')
const Setting = require('./app/models/Setting');
const job = require('./app/models/Job')
const User = require('./app/models/User')
const async = require('async');
const role = require('./app/models/Role')
var cron = require('node-cron');
const url = 'mongodb://127.0.0.1:27017/uw45mypu'
const Notification = require('./app/models/Notification')
const Message = require('./app/models/Message')
const IssueReport = require('./app/models/IssueReport')
const Job = require('./app/models/Job')
var nodemailer = require('nodemailer');
var ejs = require('ejs');
const Email = require('email-templates');
var fs = require("fs");
var EmailArrayItems = []
var service = require('./app/models/service')
const sgMail = require('@sendgrid/mail');
var config = require('./app/settings/config');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  smtp: 'smtp.sendgrid.net',
  port: 587,
  auth: {
    user: 'userservices@postcapllc.com',
    pass: '5yTR5CaRMuzZy8QVk2'
  }
});

function mailMethod(user, content, callback) {


  console.log('run')
  // if(usertype.role == 'writer'){

  async.waterfall([
    function (cb) {
      let postData = {
        user: user.firstname + ' ' + user.lastname,
        userType: user.role.name,
        content: content
      }
      EmailArrayItems = []
      cb(null, postData)
    },
    function (postData, cb) {
      ejs.renderFile(__dirname + "/autoAssignerMailTemplate" + "/writerTemplate.ejs", postData, function (err, data) {
        if (err) {
          return cb(err);
        } else {

          var mailOptions = {
            from: 'userservices@postcapllc.com', // sender address
            to: user.email, // list of receivers
            subject: 'Notification', // Subject line
            text: 'confirm your email', // plain text body
            html: data
          };

          transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
              return cb(error);
            }
            cb(null, info.messageId, info.response);
          });

        }
      })

    }
  ], function (err, info) {
    if (err) {
      return callback(err);
    }
    callback(null, info.messageId, info.response);
  })

}


const assignLog = (type, data) => {
  if (!fs.existsSync("AssignerLog/log.txt")) {
    // Do something
    fs.appendFile("AssignerLog/log.txt", "==================AutoAssigner=================", function (err) {
      if (err) {
        return console.log('Error in creating log', err);
      } else {
        fs.writeFile('AssignerLog/log.txt', `${type} ======>${data}`, function (err) {
          if (err) return console.log('Error in Writing log', err);
          console.log('Saved!');
        });
        console.log("The file was saved!");
      }

    });
  } else {
    fs.readFile('AssignerLog/log.txt', 'utf8', function (err, readingData) {
      if (err) return console.log('Error in Readin log', err);
      fs.writeFile('AssignerLog/log.txt', `${readingData} \n\n\n\n ${type} ======>${data}`, function (err) {
        if (err) return console.log('Error in Writing log', err);
        console.log('Saved!');
      });
    })
  }
}

console.log("=============================cron running=====================")

const createNotificationForUser = (type, to, from, text, extraData, image, callback) => {

  new models.Notification({
    type: type,
    to: to,
    from: from,
    text: text,
    extraData: extraData,
    image: image
  }).save(function (err, newNotification) {
    if (err) {
      return callback(err)
    } else {
      // webSocket.io.emit("notification", {
      //   user: newNotification.to.toString()
      // })

      return callback(null, true)
    }
  })

}


const createMessageForParticularUser = (to, userId, body, subject, type, email, callback) => {

  new models.Message({
    to: to,
    from: userId,
    subject: type,
    body: body,
    primary: true,
    systemMail: true
  }).save(function (err, newMessage) {
    if (err) {
      return callback(err)
    } else {
      // type, to, from, text, extraData, image,
      createNotificationForUser(type, to, userId, body, '', '', function (err, data) {
        // mailMethod(email, subject, type, (err, responseEmail) => {
        callback(null, newMessage)
        // })
      })
    }
  })
}


mongoose.connect(url, function (err) {
  if (err) {
    console.error('\x1b[31m', 'Could not connect to MongoDB!', err);
    // throw (err);
  } else {
    console.log('connection Done!', url)
  }
})

// cron.schedule('0 */5 * * * *', function () {

  cron.schedule('* * * * * *', function () {
  console.log('Schedular for messaging Email')
  models.User.find()
    .populate({
      path: 'role',
      select: 'name'
    })
    .exec((err, users) => {
      if (err) return console.log(err)
      if (!users) return console.log('No user Found')
      var userArray = []

      async.eachSeries(users, function (item, next) {
        if (item.role.name == 'writer' || item.role.name == 'super_user' || item.role.name == 'project_admin') {
          userArray.push(item)
          next()
        } else {
          next()
        }
      }, function (err) {
        if (err) console.log('Error in user', err)
        // console.log('===========>userData',userArray)
        async.eachSeries(userArray, function (user, next) {
          // console.log(user)
          if (user.notificationTime > 0) {
            var test2 = moment(user.schedularForMail).add(user.notificationTime, 's').toDate();
            var presentTime = new Date();
            if (test2 <= presentTime) {
              console.log('In time scdeule')

              // case project
              async.waterfall([
                function (cb) {

                  models.Project.find({
                    "createdAt": {
                      "$gte": user.schedularForMail,
                      "$lte": presentTime
                    }
                  })
                    .where('status').equals('Unassigned')
                    .where('mainCaptionFlag').equals(false)
                    .populate({
                      path: 'job'
                    })
                    .populate({
                      path: 'service'
                    })
                    .exec((err, projects) => {
                      if (err) return cb(err);
                      console.log('===========>projects', projects)
                      cb(null, projects)
                    })
                },
                // message
                function (projects, cb) {
                  models.Message.find({
                    "createdAt": {
                      "$gte": user.schedularForMail,
                      "$lte": presentTime
                    }
                  })
                    .where('to').equals(user._id)
                    .populate({
                      path: 'to'
                    })
                    .populate({
                      path: 'from'
                    })
                    .exec((err, messages) => {
                      if (err) return cb(err);
                      console.log('===========>message', messages)
                      cb(null, messages, projects)
                    })
                },
                // issueReport
                function (messages, projects, cb) {
                  models.IssueReport.find({
                    "createdAt": {
                      "$gte": user.schedularForMail,
                      "$lte": presentTime
                    }
                  })
                    .populate({
                      path: 'from'
                    })
                    .exec((err, issue) => {
                      if (err) return cb(err);
                      console.log('===========>issue', issue)
                      if (user.role.name === "writer") {
                        issue = []
                        cb(null, issue, messages, projects)
                      } else {
                        cb(null, issue, messages, projects)
                      }
                    })

                },
                function (issues, messages, projects, cb) {
                  models.Job.find({
                    "AppliedTime": {
                      "$gte": user.schedularForMail,
                      "$lte": presentTime
                    }
                  })
                    .populate({
                      path: 'project',
                      populate: {
                        path: 'service'
                      }
                    })
                    .populate({
                      path: 'appliedUser'
                    })
                    .exec((err, AppliedJobs) => {
                      if (err) return cb(err);
                      console.log('===========>appliedjob', AppliedJobs)
                      cb(null, AppliedJobs, issues, messages, projects)
                    })
                },
                function (AppliedJobs, issues, messages, projects, cb) {
                  models.Job.find({
                    "UnAppliedTime": {
                      "$gte": user.schedularForMail,
                      "$lte": presentTime
                    }
                  })
                    .populate({
                      path: 'project',
                      populate: {
                        path: 'service'
                      }
                    })
                    .populate({
                      path: 'appliedUser'
                    })
                    .exec((err, unappliedJobs) => {
                      if (err) return cb(err);
                      console.log('===========>unapplied', unappliedJobs)
                      cb(null, unappliedJobs, AppliedJobs, issues, messages, projects)
                    })
                },
                function (unappliedJobs, AppliedJobs, issues, messages, projects, cb) {
                  models.Project.find({
                    "projectSubmissionTime": {
                      "$gte": user.schedularForMail,
                      "$lte": presentTime
                    }
                  })
                    .where('status').equals('completed')
                    .where('mainCaptionFlag').equals(false)
                    .populate({
                      path: 'project',
                      populate: {
                        path: 'service'
                      }
                    })
                    .populate({
                      path: 'appliedUser'
                    })
                    .exec((err, projectSubmitted) => {
                      if (err) return cb(err);
                      console.log('===========>submitted', projectSubmitted)
                    //   if (user.role.name === 'writer') projectSubmitted = []
                       console.log(`role`,user.role)
                       console.log(`submitted Project after`,projectSubmitted)
                      cb(null, projectSubmitted, unappliedJobs, AppliedJobs, issues, messages, projects)
                    })
                }, (projectSubmitted, unappliedJobs, AppliedJobs, issues, messages, projects, cb) => {
                  models.Project.find({
                    "assignTime": {
                      "$gte": user.schedularForMail,
                      "$lte": presentTime
                    }
                  })
                    .where('status').equals('In Progress')
                    .where('mainCaptionFlag').equals(false)
                    .where('workingUser').equals(user._id)
                    .populate({
                      path: 'project',
                      populate: {
                        path: 'service'
                      }
                    })
                    .populate({
                      path: 'appliedUser'
                    })
                    .exec((err, AssignedProject) => {
                      if (err) return cb(err);
                      console.log('===========>assign', AssignedProject)
                      cb(null, AssignedProject, projectSubmitted, unappliedJobs, AppliedJobs, issues, messages, projects)
                    })
                }, (AssignedProject, projectSubmitted, unappliedJobs, AppliedJobs, issues, messages, projects, cb) => {
                  let EmailData = []
                  if (AssignedProject.length == 0 && projectSubmitted.length == 0 && unappliedJobs.length == 0 && AppliedJobs.length == 0 && issues.length == 0 && messages.length == 0 && projects.length == 0) {
                    return cb('Nothing to process..............')
                  }
                  if (AssignedProject.length > 0) {
                    let assignedProject = {
                      assignProject: AssignedProject,
                      type: 'writer',
                      text: 'Project Assigned',
                      subject: AssignedProject.map((item, index) => {
                        return `Project ${item.name}  has been assign to you by: Daniel Sommer. @` + moment(item.assignTime).format("hh:mm A") + ' ET \n'
                      })
                    }
                    EmailData.push(assignedProject)

                    if (user.role.name == 'writer') {
                      assignedProject.subject.forEach((sub) => {
                        new models.Notification({
                          type: assignedProject.text,
                          to: user._id,
                          from: '5afabb83ea565929add6d41e',
                          text: sub,
                          extraData: '',
                          image: ''
                        }).save().then(data => {
                          console.log('notification Saved')

                        }).catch(err => console.log('Error', err))
                      })
                    }

                  }

                  if (projectSubmitted.length > 0) {
                    let submittedProject = {
                      submitProject: projectSubmitted,
                      text: 'Project Submission',
                      type: 'all',
                      subject: projectSubmitted.map((item, index) => {
                        return `Project ${item.name} is submitted at: ` + '. ' + moment(item.projectSubmissionTime).format("hh:mm A") + ' ET \n'
                      })
                    }
                    EmailData.push(submittedProject)

                    if (user.role.name == 'super_user' || user.role.name == 'project_admin') {
                      submittedProject.subject.forEach((sub) => {
                        new models.Notification({
                          type: submittedProject.text,
                          to: user._id,
                          from: '5afabb83ea565929add6d41e',
                          text: sub,
                          extraData: '',
                          image: ''
                        }).save().then(data => console.log('notification Saved')).catch(err => console.log('Error', err))
                      })
                    }
                  }

                  if (issues.length > 0) {
                    let issue = {
                      issue: issues,
                      type: 'all',
                      text: 'Issue Submission',
                      subject: issues.map((item, index) => {
                        return '     ' + '. ' + moment(item.AppliedTime).format("hh:mm A") +
                          ' ET Submited By: ' + item.from.firstname + ' ' + item.from.lastname + ',     ' + '\n'
                      })
                    }
                    EmailData.push(issue)

                    if (user.role.name == 'super_user' || user.role.name == 'project_admin') {
                      issue.subject.forEach((sub) => {
                        new models.Notification({
                          type: issue.text,
                          to: user._id,
                          from: '5afabb83ea565929add6d41e',
                          text: sub,
                          extraData: '',
                          image: ''
                        }).save().then(data => console.log('notification Saved')).catch(err => console.log('Error', err))
                      })
                    }
                  }

                  if (AppliedJobs.length > 0) {
                    console.log('applied Error:', AppliedJobs)
                    let applied = {
                      applied: AppliedJobs,
                      text: 'Writer Applied to Job',
                      type: 'all',
                      subject: AppliedJobs.map((item, index) => {
                        console.log('applied item:', item)
                        return `Writer applied for job  at ` + moment(item.AppliedTime).format("hh:mm A") + ' ET \n'
                      })
                    }
                    EmailData.push(applied)


                    if (user.role.name == 'super_user' || user.role.name == 'project_admin') {
                      applied.subject.forEach((sub) => {
                        new models.Notification({
                          type: applied.text,
                          to: user._id,
                          from: '5afabb83ea565929add6d41e',
                          text: sub,
                          extraData: '',
                          image: ''
                        }).save().then(data => console.log('notification Saved')).catch(err => console.log('Error', err))
                      })
                    }

                  }


                  if (unappliedJobs.length > 0) {
                    console.log('unnapplied unappliedJobs:', unappliedJobs)
                    let unApplied = {
                      unApplied: unappliedJobs,
                      text: 'Writer Unapplied to Job',
                      subject: unappliedJobs.map((item, index) => {
                        console.log('unnapplied item:', item)
                        return ` Unapplied for job in  at ` + moment(item.AppliedTime).format("hh:mm A") + ' ET \n'
                      })
                    }
                    EmailData.push(unApplied)


                    if (user.role.name == 'super_user' || user.role.name == 'project_admin') {
                      unApplied.subject.forEach((sub) => {
                        new models.Notification({
                          type: unApplied.text,
                          to: user._id,
                          from: '5afabb83ea565929add6d41e',
                          text: sub,
                          extraData: '',
                          image: ''
                        }).save().then(data => console.log('notification Saved')).catch(err => console.log('Error', err))
                      })
                    }

                  }

                  if (messages.length > 0) {
                    let Message = {
                      Message: messages,
                      text: 'New Message Received',
                      subject: messages.map((item, index) => {
                        return ` @ ${moment(item.createdAt).format("hh:mm A")} ET` + '. ' + item.subject + '   from ' + item.from.email + ',  \n'
                      })
                    }
                    EmailData.push(Message)
                    Message.subject.forEach((sub) => {
                      new models.Notification({
                        type: Message.text,
                        to: user._id,
                        from: '5afabb83ea565929add6d41e',
                        text: sub,
                        extraData: '',
                        image: ''
                      }).save().then(data => console.log('notification Saved')).catch(err => console.log('Error', err))
                    })

                  }

                  if (projects.length > 0) {
                    let project = {
                      project: messages,
                      text: 'New Project on Job Board',
                      subject: projects.map((item, index) => {
                        return ` ${moment(item.createdAt).format("hh:mm A")} ET Job : ${item.name} (${item.service.name})                             ` 
                      })
                     
                    }
                    console.log('subhe',project.subject)
                    EmailData.push(project)
                    project.subject.forEach((sub) => {
                      new models.Notification({
                        type: project.text,
                        to: user._id,
                        from: '5afabb83ea565929add6d41e',
                        text: sub,
                        extraData: '',
                        image: ''
                      }).save().then(data => console.log('notification Saved')).catch(err => console.log('Error', err))
                    })
                  }
                  cb(null, EmailData)
                }, (EmailData, cb) => {
                  if (EmailData.length > 0) {
                    let postData = {
                      user: user.firstname + ' ' + user.lastname,
                      userType: user.role.name,
                      content: EmailData
                    }


                    ejs.renderFile(__dirname + "/autoAssignerMailTemplate" + "/writerTemplate.ejs", postData, function (err, data) {
                      if (err) {
                        return cb(err);
                      } else {
                        postData.Template = data
                        cb(null, postData)
                      }
                    })
                  } else {
                    console.log('Nothing to Process.')
                    models.User.findOneAndUpdate({
                      _id: user._id
                    }, {
                        schedularForMail: Date.now()
                      }, {
                        new: true
                      })
                      .exec((err, user) => {
                        if (err) cb(err)
                        console.log('ScduleTime saved noting to process.')
                        cb(null, false)
                      })
                  }
                }, (data, cb) => {
                  if (data) {


                    var mailOptions = {
                      from: 'userservices@postcapllc.com', // sender address
                      to: user.email, // list of receivers
                      subject: 'Notification', // Subject line
                      text: 'confirm your email', // plain text body
                      html: data.Template
                    };

                    models.User.findOneAndUpdate({
                      _id: user._id
                    }, {
                        schedularForMail: Date.now()
                      })
                      .exec((err, user) => {
                        if (err) cb(err)
                        EmailArrayItems = []

                        console.log('ScduleTime saved.')
                        sgMail.setApiKey(config.sendGrid.key)
                        sgMail.send(mailOptions, function (err, data) {
                        // transporter.sendMail(mailOptions, (err, data) => {
                          if (err) {
                            assignLog('Error', `Failed in sending Mail @ ${moment(new Date()).format("DD/MM/YYYY hh:mm A")} ${err}`)
                            cb(err)
                          } else {
                            assignLog('Success', `MAil Sent @ ${moment(new Date()).format("DD/MM/YYYY hh:mm A")}`)
                            cb(null)
                          }
                        })
                        // transporter.sendMail(mailOptions).then(data => cb(null)).catch(err => cb(err))
                      });
                  } else {
                    console.log('time set.')
                    cb(null)
                  }
                }
              ], function (err) {
                if (err) console.log('Error', err)
                console.log('Process Complete')
              })

            } else {
              console.log('Not time to run the sceduler')
              next();
            }
          } else {
            console.log('=====> user is not set any time for message')
            next()
          }
        }, function (err) {
          console.log('error', err)
        })

      })
    })
})



cron.schedule('00 59 * * * *', function () {
  // cron.schedule('* * * * * *', function () {
  // cron.schedule('0 */5 * * * *', function () {

  // cron.schedule('00 00 00 * * *', function () { 12 hours
  console.log('running a task every minute');

  models.Setting.find()
    .exec((err, setting) => {
      if (err) console.log('error in setting', err);
      if (setting.length == 0) return console.log('No setting')
      if (!setting[0].status) return console.log('This service is disabled')
      console.log('setting=============>', setting[0].time)
      const now = new Date();
      var test2 = moment(now).subtract(setting[0].time, 's').toDate();
      var presentTime = new Date();
      models.Project.find({
        "createdAt": {
          "$gte": test2,
          "$lte": presentTime
        }
      })
        .where('status').equals('Unassigned')
        .where('mainCaptionFlag').equals(false)
        .where('')
        .populate({
          path: 'job'
        })
        .exec((err, projects) => {
          if (err) console.log("Error", err);
          console.log(projects)
          if (projects.length == 0) return console.log('No Project to Process......')
          models.User.find()
            .populate({
              path: 'role',
              select: 'name'
            })
            .exec((err, users) => {
              if (err) console.log(err)
              console.log('========>', users)
              var writerArray = []
              async.eachSeries(users, function (item, next) {
                if (item.role.name == 'writer') {
                  writerArray.push(item._id)
                  next()
                } else {
                  next()
                }
              }, function (err) {
                if (err) throw err
                console.log(writerArray)
                async.eachSeries(projects, function (item, next) {
                  if (item.job.appliedUser.length == 1) {
                    item.workingUser = item.job.appliedUser[0]
                    item.status = 'In Progress'
                    item.save(function (err, savedData) {
                      if (err) {
                        next(err)
                      } else {
                        var notificationText = item.name + " " + "has been assign to you by:" + "By Auto-Assigner ";
                        //  type,to,from,text,extraData,image, callback
                        createNotificationForUser('Assign.', item.job.appliedUser[0], item.clientUser, notificationText, item.job._id, 'image', function (err, success) {
                          next()
                        })

                      }
                    })
                  } else if (item.job.appliedUser.length > 1) {
                    var random = items.job.appliedUser[Math.floor(Math.random() * items.length)]
                    item.workingUser = random
                    item.status = 'In Progress'
                    item.save(function (err, savedData) {
                      if (err) {
                        next(err)
                      } else {
                        var notificationText = item.name + " " + "has been assign to you by:" + "By Auto-Assigner ";
                        //  type,to,from,text,extraData,image, callback
                        createNotificationForUser('Assign.', random, '5afabb83ea565929add6d41e', notificationText, item.job._id, 'image', function (err, success) {
                          next()
                        })
                      }
                    })
                  } else if (item.job.appliedUser.length == 0) {
                    var random = writerArray[Math.floor(Math.random() * writerArray.length)]
                    item.workingUser = random
                    item.status = 'In Progress'
                    item.save(function (err, savedData) {
                      if (err) {
                        next(err)
                      } else {
                        var notificationText = item.name + " " + "has been assign to you by:" + "By Auto-Assigner ";
                        createNotificationForUser('Assign.', random, item.clientUser, notificationText, item.job._id, 'image', function (err, success) {
                          next()
                        })
                      }
                    })
                  } else {
                    next()
                  }
                }, function (err) {
                  if (err) console.log(err)

                })
              })
            })
        })
    })
});

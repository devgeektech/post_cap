
// var url = 'mongodb://<user>:<pass>@mongo.onmodulus.net:27017/uw45mypu';

var url = 'mongodb://127.0.0.1:27017/uw45mypu';



var fs = require('fs');
var join = require('path').join;
var uri_util = require('mongodb-uri');
// var config = require('../settings/config');

module.exports.configure = function(mongoose) {
	var connect = function () {
		var options = { server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } } };
		var mongoose_uri = uri_util.formatMongoose(url);
		mongoose.connect(mongoose_uri);
	};
	connect();
	var db = mongoose.connection;
	db.on('error', console.log);
	db.on('disconnected',connect);

	fs.readdirSync(join(__dirname, '../app/models')).forEach(function (file) {
		if (~file.indexOf('.js')) require(join(__dirname, '../app/models', file));
	});
};